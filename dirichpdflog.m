function fl = dirichpdflog(par,eta)
% computes the logarithm of a Dirichlet posterior density 
 
% input
% par ... parameter of the aposteriori density, the number of colums
%         determines the  number of categories
% eta ... arguments of the  aposteriori density

%output: log of the posteriro density
%        M times 1 array with M being the maximum number of row in par and
%        eta

% Author: Sylvia Fruehwirth-Schnatter
% Last change: September 13, 2006

if ~(size(par,2)==size(eta,2))     warn(['Size disagreement in function dirichpdflog']); fl=[]; return; end

if size(par,1)==1  % evaluate a single density at a sequence of draws
    
   par=repmat(par,size(eta,1),1);

elseif size(eta,1)==1  % evaluate a sequence of density at a single draw
    
   eta=repmat(eta,size(par,1),1);
    
elseif ~(size(par,1)==size(eta,1))
    warn(['Size disagreement in function dirichpdflog']); fl=[]; return
else    
%    evaluate  density in row i at the corresponding draw in row i
end

fl=gammaln(sum(par,2))+sum((par-1).*log(eta)-gammaln(par),2);

