%%  choose data
simdata=true;  % 
%simdata=false;

%% simulated data
if simdata

    clear mixtrue;
    mixtrue.dist='Normal';


    mixtrue.K=1;
    mixtrue.K=2;

    if mixtrue.K==1
        mixtrue.par.mu=0;
        mixtrue.par.sigma=1;

    elseif mixtrue.K==2

        mixtrue.par.mu=[0 0];
        mixtrue.par.sigma=[0.5 2];
        mixtrue.weight=[0.4 0.6];
    end

    data=simulate(mixtrue,1000);
    dataplot(data)
    
    Kmin=1;Kmax=mixtrue.K+2;
    Kmin=4;Kmax=4;
else

%%   real data
    
    data=dataget('fishery');  Kmin=1;  Kmax=7;
    data_frontier;            Kmin=1;  Kmax=1;              % azzalini frontier daten
    data=dataget('enzyme');   Kmin=1;  Kmax=7;
    %data=dataget('acidity'); Kmin=1;  Kmax=7;
    %data=dataget('galaxy');  Kmin=1;  Kmax=7;

end

%% Fit mixtures with increasing number of components

for K=Kmin:Kmax

    clear mix;  
    mix.dist='Normal'; 
    mix.K=K;
    mix.parfix.mu=true;
    mix.par.mu=zeros(1,mix.K);

    prior=priordefine(data,mix);
    [data, mix,mcmc]=mcmcstart(data,mix);

    mcmc.M=10000;    mcmc.burnin=3000;
    %mcmc.ranperm=false;

    mcmcout=mixturemcmc(data,mix,prior,mcmc);
    % [est,mcmcout]=mcmcestimate(mcmcout)

    eval(['[marlik' num2str(K) '_r,mcmcout' num2str(K) '_r]=mcmcbf(data,mcmcout)']);
    compare_ml=false;
    if compare_ml
        eval(['[marlik' num2str(K) '_r2,mcmcout' num2str(K) '_r2]=mcmcbf(data,mcmcout)']);
        option=struct('method','collapse');
        eval(['[marlik' num2str(K) '_c,mcmcout' num2str(K) '_c]=mcmcbf(data,mcmcout,option)']);
        eval(['[marlik' num2str(K) '_c2,mcmcout' num2str(K) '_c2]=mcmcbf(data,mcmcout,option)']);
    end

end

%mcmcdiag(data,mcmcout,2)