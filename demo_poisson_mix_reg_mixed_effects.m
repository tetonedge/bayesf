%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% finite mixture of Poisson regression models with fixed effects
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 
clear mixreg mix;


mixreg.dist='Poisson';
mixreg.K=2;


mixreg.weight=[0.3 0.7];

 
mixreg.par.beta=[-1 2 ;-0.5 0; -2 1];


mixreg.par.alpha=[-1;1];
mixreg.indexdf=[4 5]';
mixreg.df=size(mixreg.indexdf,1);
mixreg.d=size(mixreg.par.beta,1)+mixreg.df;

N=1000;

data=simulate(mixreg,N);
if mixreg.d==2 
    scatter(data.X(1,:),data.y);
elseif mixreg.d==3 
    scatter3(data.X(1,:),data.X(2,:),data.y);
end

mcmc=struct('burnin',2000,'M',1000,'storeS',500,'storepost',true);
%mcmc=struct('burnin',2000,'M',1000,'storeS',500,'storepost',true,'ranperm',true,'startpar',true);

% fit mixture regression model

mix.dist=mixreg.dist;
mix.d=mixreg.d;
mix.df=mixreg.df;
mix.indexdf=mixreg.indexdf;  

istartpar=isfield(mcmc,'startpar'); if istartpar istartpar=mcmc.startpar;end 
if istartpar   
    %mix.par.beta=zeros(mixreg.d-mixreg.df,1); 
    %mix.par.alpha=zeros(mixreg.df,1); 
    mix.par=mixreg.par;   %   true value
    mix.weight=mixreg.weight;
end



    namest='mixpoimixreg';

for K=2:2

    mix.K=K;
 
% data.empty=true; % test marginal likelihoods
 
 % starting value for beta
 
 %[S,mu]=mcmcstart(data,mix);
 
% hierarchical independence prior
clear prior;
prior=priordefine(data,mix);

namem=[namest 'K' num2str(K)];
eval(['erg_' namem '=mixturemcmc(data,mix,prior,mcmc);']);
eval(['erg_' namem '.name=namem;']);
eval(['erg_' namem '.data=data;']);
eval(['[estK' num2str(K)  ', erg_' namem ']=mcmcestimate(erg_' namem ');']);
eval(['[marlikK' num2str(K) ', erg_' namem ']=mixturebf(erg_' namem ',data)']);
eval(['mcmcstore(erg_' namem ');']);
 
end