function fd=mixnorprior(mix,prior,varargin)

% Evaluates the prior of a univaraite or multivariate normal mixture model at the parameter values
% stored in mix

% Author: Sylvia Fruehwirth-Schnatter
% Last change: Sept 20, 2006


if nargin==2  % check variables
    if ~isfield(mix,'r') 
        if or(mix.dist(1:6)=='Normal',mix.dist(1:6)=='Studen')
            mix.r=1;
        elseif or(mix.dist(1:6)=='Normul',mix.dist(1:6)=='Stumul')
            mix.r=size(mix.par.mu,1);
        end    
    end
    
    if ~isfield(mix,'K') mix.K=1; end
    
    if and(isfield(mix.par,'sigmainv'),isfield(mix.par,'sigma'))
        if mix.r==1
            if any((mix.par.sigma*mix.par.sigmainv)~=ones(1,K))  warn(['discrepancy in the fields sigma and sigmainv in the definition of the mixture model when evaluating the prior']);fd=[];return;end
        elseif mix.r>1
            for k=1:mix.K
                if any(any(abs((mix.par.sigma(:,:,k)*mix.par.sigmainv(:,:,k)-eye(mix.r))./mix.par.sigma(:,:,k))>10^(-3)))
                    warn(['discrepancy in the fields sigma and sigmainv in the definition of the mixture model when evaluating the prior']);fd=[];
                end
            end
        end
    elseif  mix.r>1 
        if ~isfield(mix.par,'sigmainv') 
            mix.par.sigmainv=zeros(size(mix.par.sigma));
            for k=1:mix.K; 
                mix.par.sigmainv(:,:,k)=inv(mix.par.sigma(:,:,k)); 
            end
        end
        if ~isfield(mix.par,'logdet') 
            for k=1:mix.K; 
                mix.par.logdet(1,k)=log(det(mix.par.sigmainv(:,:,k)));  
            end    
        end
    end
end

r=mix.r; K=mix.K;
   
fd=0;

if r==1
    
    if or(all(mix.dist(1:6)=='SkewNo'),all(mix.dist(1:6)=='SkewSt'))
        
        delta=mix.par.lambda./(1+mix.par.lambda.^2).^.5;
        mix.par.beta=[mix.par.mu;mix.par.sigma.^.5.*delta];
        sigmaeps=mix.par.sigma.*(1-delta.^2);
        priorBinvs=prior.par.beta.Binv;
        for k=1:mix.K priorBinvs(:,:,k)=prior.par.beta.Binv(:,:,k)/sigmaeps(k);end
        if isfield(prior.par,'Q')
            % PX-MCMC: adjust prior of psi
            fd=fd+prodnorpdflog(struct('mu',prior.par.beta.b(1,:),'sigma',1./squeeze(priorBinvs(1,1,:))'),mix.par.beta(1,:));
            df=2*prior.par.Q.g0;
            fd=fd+prodstudmultpdflog(struct('mu',prior.par.beta.b(2,:),'sigmainv',squeeze(priorBinvs(2,2,:))','df',df),mix.par.beta(2,:));
        else
            fd=fd+prodnormultpdflog(struct('mu',prior.par.beta.b,'sigmainv',priorBinvs),mix.par.beta);
        end
        
    elseif isfield(mix,'d')    % compute prior beta in a finite mixture regression model

        if ~isfield(mix,'df')  mix.df=0; end
       
        if size(mix.par.beta,1)==1
            fd=fd+prodnormultpdflog(struct('mu',prior.par.beta.b,'sigmainv',squeeze(prior.par.beta.Binv)'),mix.par.beta);
        else    
            fd=fd+prodnormultpdflog(struct('mu',prior.par.beta.b,'sigmainv',prior.par.beta.Binv),mix.par.beta);
        end
        if mix.df>0
                
            fd=fd+prodnormultpdflog(struct('mu',prior.par.alpha.a,'sigmainv',prior.par.alpha.Ainv),mix.par.alpha);
        end
        
    else    % compute prior mu in a standrad finite mixture model
        
        if isfield(mix,'parfix') 
            if ~isfield(mix.parfix,'mu') mix.parfix.mu=false; end 
        else
            mix.parfix.mu=false; 
        end
        if ~mix.parfix.mu
            if all(prior.type(1:4)=='conc')
                prior.par.mu.B=mix.par.sigma./prior.par.mu.N0;
            else
                prior.par.mu.B=1./prior.par.mu.Binv;
            end
            fd=fd+prodnorpdflog(struct('mu',prior.par.mu.b,'sigma',prior.par.mu.B),mix.par.mu);
        end
    end
    
    %  add hierarchical prior
    
    hier_prior=isfield(prior,'hier');if hier_prior hier_prior=prior.hier;end   
    if hier_prior
        gN=prior.par.sigma.g+sum(prior.par.sigma.c,2);
        if or(all(mix.dist(1:6)=='SkewNo'),all(mix.dist(1:6)=='SkewSt'))
            GN=prior.par.sigma.G+sum(1./sigmaeps,2);
        else
            GN=prior.par.sigma.G+sum(1./mix.par.sigma,2);
        end
        Cstar=gN/GN;
        prior.par.sigma.C=repmat(Cstar,1,K);
        % use candidates formula to compute 
        fd=fd+ prodgampdflog(struct('a',prior.par.sigma.g,'b',prior.par.sigma.G),Cstar)-prodgampdflog(struct('a',gN,'b',GN),Cstar);
    end    
   
    % add prior sigma 
    if or(all(mix.dist(1:6)=='SkewNo'),all(mix.dist(1:6)=='SkewSt'))
        fd=fd+ prodinvgampdflog(struct('a',prior.par.sigma.c,'b',prior.par.sigma.C),sigmaeps);
    else
        if all(prior.type(1:4)=='sigm')
            fd=fd-mix.K*log(prior.par.sigma.max)-sum(log(mix.par.sigma));
            if any(mix.par.sigma.^.5>prior.par.sigma.max)  warn('Sigma greater than upper bound in function prioreval'); end
        else
            fd=fd+ prodinvgampdflog(struct('a',prior.par.sigma.c,'b',prior.par.sigma.C),mix.par.sigma);
        end
    end
    
else
    
    if or(all(mix.dist(1:6)=='SkNomu'),all(mix.dist(1:6)=='SkStmu'))
        
        mix.par.beta=zeros(2*mix.r,mix.K);
        priorBinvs=prior.par.beta.Binv;
        sigmaepsinv=zeros(mix.r,mix.r,mix.K);
        logdetinv=zeros(1,mix.K);
        for k=1:mix.K
            Omega=diag(diag(squeeze(mix.par.sigma(:,:,k))).^.5);
            omegainv=diag(diag(squeeze(mix.par.sigma(:,:,k))).^(-.5));
            Sbar=omegainv*squeeze(mix.par.sigma(:,:,k))*omegainv;
            delom=Omega*Sbar*mix.par.lambda(:,k)/(1+mix.par.lambda(:,k)'*Sbar*mix.par.lambda(:,k))^.5;
            mix.par.beta(:,k)=[mix.par.mu(:,k);delom];
            sigmaeps=squeeze(mix.par.sigma(:,:,k))-delom*delom';
            sigmaepsinv(:,:,k)=inv(sigmaeps);
            logdetinv(k)=log(det(sigmaepsinv(:,:,k)));
            priorBinvs(:,:,k)=prior.par.beta.Binv(1,1,k)*[sigmaepsinv(:,:,k)  zeros(mix.r);  zeros(mix.r) sigmaepsinv(:,:,k)];
        end
        fd=fd+prodnormultpdflog(struct('mu',prior.par.beta.b,'sigmainv',priorBinvs),mix.par.beta);
        if isfield(prior.par,'Q')
            % PX-MCMC: adjust prior of psi
            'PX-MCMC: adjust prior of psi in mixnorprior for multivariate skewN/T'
        end

    else

        if prior.type(1:4)=='conc'
            if isfield(mix.par,'sigmainv')
                prior.par.mu.Binv=mix.par.sigmainv.*permute(repmat(prior.par.mu.N0,[r 1 r]),[1 3 2]);
                fd=fd+prodnormultpdflog(struct('mu',prior.par.mu.b,'sigmainv',prior.par.mu.Binv),mix.par.mu);

            elseif  ~isfield(mix.par,'sigma')
                warn(['field sigma or sigmainv is missing in the definition of the mixture model  when evaluating the prior']);fd=[];return
            else
                prior.par.mu.B=mix.par.sigma./permute(repmat(prior.par.mu.N0,[1 r r ]),[2 3 1]);
                fd=fd+prodnormultpdflog(struct('mu',prior.par.mu.b,'sigma',prior.par.mu.B),mix.par.mu);
            end
        else
            fd=fd+prodnormultpdflog(struct('mu',prior.par.mu.b,'sigmainv',prior.par.mu.Binv),mix.par.mu);
        end
    end
    hier_prior=isfield(prior,'hier');if hier_prior hier_prior=prior.hier;end   %  true: hierarchical prior

    if hier_prior
        gN=prior.par.sigma.g+sum(prior.par.sigma.c,2);
        if or(all(mix.dist(1:6)=='SkNomu'),all(mix.dist(1:6)=='SkStmu'))  
            GN=prior.par.sigma.G+sum(sigmaepsinv,3);
        else
            GN=prior.par.sigma.G+sum(mix.par.sigmainv,3);
        end
        Cstar=gN*inv(GN);
        prior.par.sigma.C=repmat(qincol(Cstar),1,K);
        prior.par.sigma.logdetC=repmat(log(det(Cstar)),1,K);
        % use candidates formula to compute 
        fd=fd+ pwilog(prior.par.sigma.C(:,1),prior.par.sigma.logdetC(:,1),prior.par.sigma.g,qincol(prior.par.sigma.G),log(det(prior.par.sigma.G)));
        fd=fd- pwilog(prior.par.sigma.C(:,1),prior.par.sigma.logdetC(:,1),gN,qincol(GN),log(det(GN)));
    else
        prior.par.sigma.C=qincolmult(prior.par.sigma.C);
    end    
    
    
    % add prior sigma 
    if or(all(mix.dist(1:6)=='SkNomu'),all(mix.dist(1:6)=='SkStmu'))
        arg=struct('yinv',sigmaepsinv,'logdetyinv',logdetinv);
    else
        arg=struct('yinv',mix.par.sigmainv,'logdetyinv',mix.par.logdet);
    end
    fd=fd+ prodinvwipdflog(struct('a',prior.par.sigma.c,'Scol',prior.par.sigma.C,'logdetS',prior.par.sigma.logdetC),arg);
     
    
end    

