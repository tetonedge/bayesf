function fd = prodinvwipdflog(par,arg)
% computes the logarithm of the product of K inverted wishart posterior
% density of dimension r with parameter par at the argument arg


% the function could be applied to the following situations:
% 1. a single density is evaluated at a single argument (M=1)
% 2. a single density may by evaluated at a sequence of M argument
% 3. a sequence of M density may by evaluated at a single argument
% 4. a sequence of M density may by evaluated at a corresponding sequence of M arguments

% Depending on this situation, the fields of par are arrays with following dimensions
% for a single density, for K>1:
%                       par.a is a (1 times K) array, containing the shape parameter 
%                       par.Scol   is a (r*(r+1)/2 times K) array, containing the scale matrix, stored as column 
%                       OR: par.S   is a r times r times K array, containing the scale matrix, stored as matrix 
%                       par.logdetS is a (1 times K) array, containing the  log of the determinante of the scale matrix
%                       
% for a single density, for K=1: 
%                       par.a is a positive real variable, containing the shape parameter   
%                       par.Scol   is a (r*(r+1)/2 times 1) array, containing the scale matrix, stored as column 
%                       OR: par.S   is a r times r times 1 array, containing the scale matrix, stored as matrix 
%                       par.logdetS is a  real variable,

% for a sequence of M densities, the first index is the running index for the sequence:
%                    for K>1:
%                       par.a is a (M times K) array, containing the shape parameter 
%                       par.Scol   is a (M times r*(r+1)/2 times K) array, containing the scale matrix, stored as column 
%                       OR: par.S   is a M times r times r times K array, containing the scale matrix, stored as matrix 
%                       par.logdetS is a (M times K) array, containing the  log of the determinante of the scale matrix

% for a sequence of M density, for K=1: 
%                       par.a is a (M times 1) array, containing the shape parameter 
%                       par.Scol   is a (M times r*(r+1)/2) array,  containing the scale matrices, stored as column 
%                       OR: par.S   is a M times r times r   array, containing the scale matrix, stored as matrix 
%                       par.logdetS is a (M times 1) array, containing the  log of the determinante of the scale matrices

% arg ... argument of the  aposteriori density with two fields:

%      for a single argument:  
%                       arg.yinvcol: array of size (r*(r+1)/2 times K), inverse of the argument matrix, stored as column 
%                       OR: arg.yinv:  array of size r times r times K,  inverse of the argument matrix, stored as matrix
%                       OR: arg.y:  array of size r times r times K,  argument matrix, stored as matrix
%                       arg.logdetyinv: array of size 1 times K, log of the determinante of the inverse of the argument matrix, 
%      for a multiple argument:  the first index is the running index for the sequence:
%                       arg.yinvcol: array of size (M x r*(r+1)/2 times K), inverse of the argument matrix, stored as column 
%                       OR: arg.yinv:  array of size M x r times r times K,  inverse of the argument matrix, stored as matrix
%                       OR: arg.y:  array of size M x r times r times K,  argument matrix, stored as matrix
%                       arg.logdetyinv: array of size M times K, log of the determinante of the inverse of the argument matrix, 

%output: log of the posterior density:  array of size M times 1

% Author: Sylvia Fruehwirth-Schnatter
% Last change: September 13, 2006



if ~isfield(par,'a') warn(['Field a missing in function prodinvwipdflog']);fd=[];return; end  
if  and(~isfield(par,'Scol'),~isfield(par,'S'))  warn(['Either field Scol or S must be specified  in function prodinvwipdflog']);fd=[];return; end  

% determine the dimension

sa=size(par.a);
M=sa(1);K=sa(2);

if isfield(par,'Scol')
    sS=size(par.Scol);
    if M>1 
        if sa(1)~=sS(1)   warn(['Size disagreement in the fields of par in function prodinvwipdflog']); fd=[]; return;end
        r = -.5 + (.25 + 2*sS(2))^.5; %anzahl der Zeilen von Q wird aus der dimension von Qcol rekonstruiert
        if r==1  warn(['Use function prodinvgampdflog for scalar variances']);fd=[];return;end
        if K>1 if  sa(2)~=sS(3)   warn(['Size disagreement in the fields of par in function prodinvwipdflog']); fd=[]; return;end;end

    else 
        if  sa(2)~=sS(2)   warn(['Size disagreement in the fields of par in function prodinvwipdflog']); fd=[]; return;end
        r = -.5 + (.25 + 2*sS(1))^.5; %anzahl der Zeilen von Q wird aus der dimension von Qcol rekonstruiert
        if r==1  warn(['Use function prodinvgampdflog for scalar variances']);fd=[];return;end
        
    end
else
    sS=size(par.S);
    if M>1
        if any([sa(2)~=sS(4) sS(2)~=sS(3) sa(1)~=sS(1)])  warn(['Size disagreement in the fields of par in function prodinvwipdflog']); fd=[]; return;end
        r=sS(2);
        if r==1  warn(['Use function prodinvgampdflog for scalar variances']);fd=[];return;end
        
        
        par.Scol=zeros(M,r*(r+1)/2,K);
        for m=1:M; 
            par.Scol(m,:,:) = qincolmult(squeeze(par.S(m,:,:,:)));  
        end
    else
        if K>1
            if any([sa(2)~=sS(3) sS(2)~=sS(1) ])  warn(['Size disagreement in the fields of par in function prodinvwipdflog']); fd=[]; return;end
        else
            if   sS(2)~=sS(1)   warn(['Size disagreement in the fields of par in function prodinvwipdflog']); fd=[]; return;end
        end
        
        r=sS(1);
        if r==1  warn(['Use function prodinvgampdflog for scalar variances']);fd=[];return;end
         par.Scol = qincolmult(par.S);    
    end
end

if  ~isfield(par,'logdetS')  
    if M>1
        par.logdetS=zeros(M,K);
        for m=1:M; 
            for k=1:K; 
                if isfield(par,'S')
                    par.logdetS(m,k) = log(det(squeeze(par.S(m,:,:,k))));  
                    
                else
                    par.logdetS(m,k) = log(det(qinmatr(par.Scol(m,:,k)')));  
                end
            end 
        end
    else
        par.logdetS=zeros(1,K);
        for k=1:K; 
            if isfield(par,'S')
                par.logdetS(k) = log(det(squeeze(par.S(:,:,k))));  
            else
                par.logdetS(1,k) = log(det(qinmatr(par.Scol(:,k))));   
            end
        end 
    end
end   

if all([~isfield(arg,'yinv') ~isfield(arg,'y') ~isfield(arg,'yinvcol') ])
    warn(['Either field y or yinv or yinvcol must be specified  in function prodinvwipdflog']);fd=[];return; 
elseif  ~isfield(arg,'yinvcol') 
    if isfield(arg,'y')
        sy=size(arg.y);
        if size(sy,2)==4 % sequence of arguments
            
            arg.yinvcol=zeros(sy(1),r*(r+1)/2,sy(4));
            for m=1:sy(1)
                for k=1:sy(4)
                    arg.yinvcol(m,:,k)=qincol(inv(squeeze(arg.y(m,:,:,k))));
                end
            end
            
        elseif size(sy,2)==2   % single argument, single inverted Wishart
            
            arg.yinvcol=qincol(inv(arg.y));
            
            
        elseif all([sy(1)==r sy(2)==r sy(3)==K])   % single argument, product inverted Wishart
            
            arg.yinvcol=zeros(r*(r+1)/2,K);
            for k=1:K
                arg.yinvcol(:,k)=qincol(inv(squeeze(arg.y(:,:,k))));
            end
            
            
        elseif and(sy(2)==r,sy(3)==r)    % multiple argument, single inverted Wishart
            
            arg.yinvcol=zeros(sy(1),r*(r+1)/2);
            for m=1:sy(1)
                arg.yinvcol(m,:)=qincol(inv(squeeze(arg.y(m,:,:))));
            end
            
        else    
            warn(['Size disagreement in the fields of par and arg in function prodinvwipdflog']); fd=[]; return;
        end      
        
        
    elseif isfield(arg,'yinv')
        sy=size(arg.yinv);
        if size(sy,2)==4 % sequence of arguments
            arg.yinvcol=zeros(sy(1),r*(r+1)/2,sy(4));
            for m=1:sy(1)
                for k=1:sy(4)
                    arg.yinvcol(m,:,k)=qincol(squeeze(arg.yinv(m,:,:,k)));
                end
            end
            
        elseif size(sy,2)==2   % single argument, single inverted Wishart
            
            arg.yinvcol=qincol(arg.yinv);
            
        elseif all([sy(1)==r sy(2)==r sy(3)==K])   % single argument, product inverted Wishart
            
            arg.yinvcol=zeros(r*(r+1)/2,K);
            for k=1:K
                arg.yinvcol(:,k)=qincol(squeeze(arg.yinv(:,:,k)));
            end
            
        elseif and(sy(2)==r,sy(3)==r)    % multiple argument, single inverted Wishart
            
            arg.yinvcol=zeros(sy(1),r*(r+1)/2);
            for m=1:sy(1)
                arg.yinvcol(m,:)=qincol(squeeze(arg.yinv(m,:,:)));
            end
            
            
        else    
            warn(['Size disagreement in the fields of par and arg in function prodinvwipdflog']); fd=[]; return;
        end      
        
    end
end

sy=size(arg.yinvcol);
if size(sy,2)==3 % sequence of arguments
    
    if ~isfield(arg,'logdetyinv') 
        arg.logdetyinv=zeros(sy(1),sy(3));
        for m=1:sy(1);
            for k=1:sy(3);
                arg.logdetyinv(m,k)=log(det(qinmatr(arg.yinvcol(m,:,k))));
            end
        end
        
    elseif   size(arg.logdetyinv,2)~=K
        warn(['Size disagreement in the fields logdetyinv of arg in function prodinvwipdflog']); fd=[]; return;
    end
    
    
    if M==1 % single density
        M=sy(1);
        par.logdetS=repmat(par.logdetS,M,1);
        if K==1 
            par.Scol=repmat(par.Scol',M,1);
        else
            par.Scol=permute(repmat(par.Scol,[1 1 M]),[3 1 2]);
        end    
    elseif M~=sy(1)  
        warn(['Size disagreement in the fields of par and arg in function prodinvwipdflog']); fd=[]; return; 
    else  % sequence of densities
        if sy(2)~=size(par.Scol,2) warn(['Size disagreement in the fields of par and arg in function prodinvwipdflog']); fd=[]; return; end
    end
    
elseif sy(end)==1   % single argument, single inverted Wishart
    
    if ~isfield(arg,'logdetyinv')   arg.logdetyinv=log(det(qinmatr(arg.yinvcol)));   end
    if and(M==1,sy(1)~=size(par.Scol,1)) warn(['Size disagreement in the fields of par and arg in function prodinvwipdflog']); fd=[]; return; end
    
    if M>1
        arg.logdetyinv=repmat(arg.logdetyinv,M,1);
        if K==1 
            arg.yinvcol=repmat(arg.yinvcol',M,1);
        else
            arg.yinvcol=permute(repmat(arg.yinvcol,[1 1 M]),[3 1 2]);
        end    
    end    
    
elseif and(sy(1)==(r*(r+1)/2),sy(2)==K)   % single argument, product inverted Wishart
    
    if ~isfield(arg,'logdetyinv') 
        arg.logdetyinv=zeros(1,K);
        for k=1:K;
            arg.logdetyinv(1,k)=log(det(qinmatr(arg.yinvcol(:,k))));
        end
    elseif   size(arg.logdetyinv,2)~=K
        warn(['Size disagreement in the fields logdetyinv of arg in function prodinvwipdflog']); fd=[]; return;
    end
    
    if M>1
        arg.logdetyinv=repmat(arg.logdetyinv,M,1);
        arg.yinvcol=permute(repmat(arg.yinvcol,[1 1 M]),[3 1 2]);
    end        
    
elseif sy(2)==(r*(r+1)/2)    % multiple argument, single inverted Wishart
    
    if ~isfield(arg,'logdetyinv') 
        arg.logdetyinv=zeros(sy(1),1);
        for m=1:sy(1);
            arg.logdetyinv(m)=log(det(qinmatr(arg.yinvcol(m,:))));
        end
    end
    
    if M==1 % single density
        M=sy(1);
        par.logdetS=repmat(par.logdetS,M,1);
        par.Scol=repmat(par.Scol',M,1);
    elseif M~=sy(1)  
        warn(['Size disagreement in the fields of par and arg in function prodinvwipdflog']); fd=[]; return; 
    end
    
else    
    warn(['Size disagreement in the fields of par and arg in function prodinvwipdflog']); fd=[]; return;
end      

index=0.5*[[1:r].*[2:r+1]]';


if and(M==1,K==1)  % single inverted Wishart  distribution
    
    %fd1 = pinvwilog_neu(arg.yinvcol,arg.logdetyinv,par.a,par.Scol,par.logdetS);
    
    trQS=2*sum(arg.yinvcol.*par.Scol,1)-sum(arg.yinvcol(index,:).*par.Scol(index,:),1);
    fd=par.a.*par.logdetS+(par.a+(r+1)/2).*arg.logdetyinv-trQS-r*(r-1)/4*log(pi);
    fd=fd-sum(gammaln(repmat(par.a,r,1)+0.5-0.5*[1:r]'),1);
    
elseif and(M==1,K>1)    %single density with products of inverted Wishart   densities
    
    %fd1=0;for k=1:K;    fd1 = fd1 + pinvwilog_neu(arg.yinvcol(:,k),arg.logdetyinv(1,k),par.a(:,k),par.Scol(:,k),par.logdetS(1,k));end
    
    
    trQS=2*sum(arg.yinvcol.*par.Scol,1)-sum(arg.yinvcol(index,:).*par.Scol(index,:),1);
    fd=sum(par.a.*par.logdetS+(par.a+(r+1)/2).*arg.logdetyinv-trQS-r*(r-1)/4*log(pi),2);
    
    fd=fd-sum(sum(gammaln(repmat(par.a,r,1)+0.5-0.5*repmat([1:r]',1,K)),1),2);
    
    
elseif K==1   % sequence of  single inverted Wishart  distribution
    
   
    %fd1=zeros(M,1);for m=1:M;    fd1(m) = fd1(m) + pinvwilog_neu(arg.yinvcol(m,:)',arg.logdetyinv(m),par.a(m),par.Scol(m,:)',par.logdetS(m));end
    
    
    trQS=squeeze(2*sum(arg.yinvcol.*par.Scol,2)-sum(arg.yinvcol(:,index,:).*par.Scol(:,index,:),2));
    fd=par.a.*par.logdetS+(par.a+(r+1)/2).*arg.logdetyinv-trQS-r*(r-1)/4*log(pi);
    
    fd=fd-sum(sum(gammaln(repmat(par.a,1,r) + 0.5 - 0.5*repmat([1:r],[M 1])) ,2),3);
    

    
else   % sequence of  products of inverted Wishart   densities
    
    %fd1=zeros(M,1);for m=1:M; for k=1:K;    fd1(m) = fd1(m) + pinvwilog_neu(arg.yinvcol(m,:,k)',arg.logdetyinv(m,k),par.a(m,k),par.Scol(m,:,k)',par.logdetS(m,k));end;end
    
    
    trQS=squeeze(2*sum(arg.yinvcol.*par.Scol,2)-sum(arg.yinvcol(:,index,:).*par.Scol(:,index,:),2));
    fd=sum(par.a.*par.logdetS+(par.a+(r+1)/2).*arg.logdetyinv-trQS-r*(r-1)/4*log(pi),2);
    
    fd=fd-sum(sum(gammaln( permute(repmat(par.a,[1 1 r]) , [1 3 2] ) + 0.5 - 0.5*repmat([1:r],[M 1 K])) ,2),3);
    
    
end

