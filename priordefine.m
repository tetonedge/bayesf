function prior=priordefine(data,mix,varargin);

% Define a slightly informative data based prior for a finite mixture model, stored in the structure array mix
% Author: Sylvia Fruehwirth-Schnatter

%% check  arguments

if ~isfield(mix,'dist')
    warn('The field dist is obligatory in the structure array defining the mixture when calling the function priordefine')
    prior=[];return
end

norstud=any([all(mix.dist(1:6)=='Normal') all(mix.dist(1:6)=='Normul') all(mix.dist(1:6)=='Stumul') all(mix.dist(1:6)=='Studen')]);
%      Normal or student mixture;

skew=any([all(mix.dist(1:6)=='SkewNo') all(mix.dist(1:6)=='SkNomu') all(mix.dist(1:6)=='SkewSt') all(mix.dist(1:6)=='SkStmu')]);

studmix=any([all(mix.dist(1:6)=='Stumul') all(mix.dist(1:6)=='Studen') all(mix.dist(1:6)=='SkewSt') all(mix.dist(1:6)=='SkStmu')]);
glm=any([all(mix.dist(1:6)=='Poisso') all(mix.dist(1:6)=='Binomi')]);


if ~isfield(mix,'K') mix.K=1;  end  % single member from the distribution family
if ~isfield(mix,'r') if isfield(data,'r')  mix.r=data.r;  else mix.r=prod(size(data))/data.N; end; end  %

% data are handled as data stored by row
ibycolumn=isfield(data,'bycolumn');
if ibycolumn  ibycolumn=data.bycolumn; end  % ibycolumn true: data stored by column
if ibycolumn      data.y=data.y'; data.bycolumn ='false'; end

if ~isfield(data,'y')
    warn('The field y is obligatory in the structure array defining the data when calling the function priordefine')
    prior=[];return
end

if ~isfield(data,'r') data.r=size(data.y,1); end
if ~isfield(data,'N') N=size(data.y,2); data.N=N; else N=data.N;end
if ~isfield(mix,'indicmod') mix.indicmod.dist='Multinomial';  end

if all([~isfield(mix,'error') or(mix.dist(1:6)=='Normal',mix.dist(1:6)=='Normul')]) % default: switching variane
    mix.error='switch';
end

finmix=all([~isfield(mix,'d') ~isfield(mix,'ar') ~isfield(mix,'arf')]);

%% finite mixtures of Poisson distributions

if and(mix.dist(1:6)=='Poisso',finmix) % standard finite mixture

    if nargin==2
        ihier=true; % default prior hierarchical
    else
        prior=varargin{1};
        ihier=isfield(prior,'hier');if ihier ihier=prior.hier;end   % ihier true: hierarchical prior
    end
    prior=struct('hier',ihier);


    % default prior based on matching moments

    % choose level of overdispersion, depending on the ratio overdispersion/mean^2
    % levover=0;   % no idea, data based choice;
    % levover = 1;  % low  degree of overdispersion, ratio smaller than 1; a0=10;
    % levover = 2;  % meduim  degree of overdispersion; ratio close to 1; a0=1;
    % levover = 3;  % high  degree of overdispersion; ratio much larger than 1; a0=0.1;

    levover=0;
    a00=[10;1;0.1];

    y=data.y;

    v2=mean(y.*max(y-1,0));
    mumean=mean(y);
    over=std(y)^2-mean(y);

    if levover==0
        %if (v2-mumean^2)>0
        %    a0=mumean^2/(v2-mumean^2);
        %end
        if over>0
            a0=mumean^2/over;
        else
            levover=1;a0=a00(1);
        end
    else
        a0=a00(levover);
    end

    if ihier
        g0=0.5;  %        g0=2;
        G0=mumean*g0./a0;    % mtach the mean
        %g0= 0.01; G0= 0.01;
        be= g0/G0;
        prior.par.a=a0*ones(1,mix.K);
        prior.par.b=be*ones(1,mix.K);
        prior.par.g=g0;
        prior.par.G=G0;
    else
        be= a0/mumean;
        prior.par.a=a0*ones(1,mix.K);
        prior.par.b=be*ones(1,mix.K);
    end

    %% binomial mixtures

elseif and(all(mix.dist(1:6)=='Binomi'),finmix) % standard finite mixture

    %  N0=2; a0=mean(data.y./data.Ti)/N0;       b0=N0-a0;  %% data based
    a0=1;b0=1;          %% uniform
    prior.par.a=a0*ones(1,mix.K);
    prior.par.b=b0*ones(1,mix.K);

elseif and(all(mix.dist(1:6)=='Multin'),finmix) % standard finite mixture

    e0=1;       %% uniform
    if ~isfield(mix,'r') mix.r=1; end
    if ~isfield(mix,'cat') mix.cat=2*ones(mix.r,1); end

    prior.par.pi=e0*ones(sum(mix.cat),mix.K);
    
    %% exponential mixtures %HW

elseif and(mix.dist(1:6)=='Expone',finmix) % standard finite mixture

    prior_wag2007=true;

    if  prior_wag2007
        % choose prior as in Wagner (2007), prior 2

        ihier=false; % default prior hierarchical
        a0=0.1;
        prior.par.a=a0*ones(1,mix.K);
        prior.par.b=mean(data.y)*a0*ones(1,mix.K);

    else

        if nargin==2
            ihier=true; % default prior hierarchical
        else
            prior=varargin{1};
            ihier=isfield(prior,'hier');if ihier ihier=prior.hier;end   % ihier true: hierarchical prior
        end
        prior=struct('hier',ihier);


        % default prior based on matching moments

        % choose level of overdispersion, depending on the ratio overdispersion/mean
        % levover=0;   % no idea, data based choice;
        % levover = 1;  % low  degree of overdispersion, ratio smaller than 1; a0=10;
        % levover = 2;  % medium  degree of overdispersion; ratio close to 1; a0=8/3;
        % levover = 3;  % high  degree of overdispersion; ratio much larger than 1; a0=2.1;

        levover=0;
        a00=[10;8/3;2.1];

        y=data.y;

        mumean=mean(y);
        m2mean=mean(y.^2);
        over=std(y)/mean(y)-1;

        if levover==0
            if m2mean-2*mumean^2>0
                a0=2* var(y)/(m2mean-2*mumean^2);
            else
                if over <0.2,
                    levover=1;a0=a00(1);
                elseif over < 2
                    levover=2;a0=a00(2);
                else
                    levover=3;a0=a00(3);
                end
            end
        else
            a0=a00(levover);
        end
        if ihier %CHECK
            g0=0.5;  %
            G0=g0/(mumean*(a0-1));    % match the mean
            be= g0/G0;
            prior.par.a=a0*ones(1,mix.K);
            prior.par.b=be*ones(1,mix.K);
            prior.par.g=g0;
            prior.par.G=G0;
        else
            be= mumean*(a0-1);
            prior.par.a=a0*ones(1,mix.K);
            prior.par.b=be*ones(1,mix.K);
        end
    end

    %% mixtures of skew distributions

elseif and(skew,finmix)

    FLATPRIOR=false; %  FLATPRIOR=true;
    ihier=true; %    ihier=false;
    if FLATPRIOR ihier=false; end

    chtype='concon';
    prior=struct('hier',ihier,'type',chtype);

    B0sc=1/10;  % about 75 percent of heterogeneity due to hidden truncation
    %B0sc=1/3;   % about 50 percent of heterogeneity due to hidden truncation
    %B0sc=1;     % about 25 percent of heterogeneity due to hidden truncation
    %B0sc=3;     % about 10 percent of heterogeneity due to hidden truncation (very informative on the mean)



    if mix.r==1

        b0=[mean(data.y,2);0];  % prior xi and omega*delta
        %b0=zeros(2,1);
        if FLATPRIOR b0=zeros(2,1); B0sc=0;  end%% FLAT PRIOR%%
        B0inv=B0sc*eye(2);
        %B0inv=eye(2);  B0inv(2,2)=B0sc;
        prior.par.beta=struct('b',repmat(b0,1,mix.K),'Binv',repmat(B0inv,[1 1 mix.K]));

    else   % multivariate skew normal

        b0=[mean(data.y,2);zeros(mix.r,1)];  % prior xi and omega*delta
        %b0=zeros(2*mix.r,1);

        if FLATPRIOR b0=zeros(2*mix.r,1); B0sc=0;  end%% FLAT PRIOR%%
        B0inv=B0sc*[eye(mix.r) zeros(mix.r) ; zeros(mix.r) eye(mix.r)];
        
        prior.par.beta=struct('b',repmat(b0,1,mix.K),'Binv',repmat(B0inv,[1 1 mix.K]));

    end

    dfQpr=2.5;% dfQpr=1.5;

    prQnu=(data.r-1)/2+dfQpr;  %

    single_skew=false; % same Rhet for all K in a finite mixture
    %single_skew=true;  % match Rhet and B0sc
    if and(mix.K==1,single_skew)
        Rhet=1/(1+B0sc/(1-2/pi));
        if FLATPRIOR Rhet=0; end
    else
        %% Modify
        Rhet=0.5;
        Rhet=2/3;
    end

    if data.r==1
        sigmaerrhat=var(data.y');
    else
        sigmaerrhat=cov(data.y');
    end
    prQS=sigmaerrhat*(1-Rhet)*(prQnu-(data.r+1)/2);
    if data.r>1 detprQS=log(det(prQS)); end


    if FLATPRIOR prQnu=-1;prQS=zeros(data.r);  end %% FLAT PRIOR%%

    if  ihier
        g0=0.5+(data.r-1)/2;   % in generell, g0 must be a multiple of 0.5 for the inverted Wishart
        %                  and lead to a proper prior
        % test:g0=2+(data.r-1)/2;

        G0= g0*inv(prQS);   %match  hierarchical and non-hierarchical priors
        if data.r>1
            prior.par.sigma=struct('c',repmat(prQnu,1,mix.K),'C',reshape(repmat(prQS,1,mix.K),size(data.y,1),size(data.y,1),mix.K),'logdetC',repmat(detprQS,1,mix.K),'g',g0,'G',G0);
        else
            prior.par.sigma=struct('c',repmat(prQnu,1,mix.K),'C',repmat(prQS,1,mix.K),'g',g0,'G',G0);
        end
    else
        if data.r>1
            prior.par.sigma=struct('c',repmat(prQnu,1,mix.K),'C',reshape(repmat(prQS,1,mix.K),size(data.y,1),size(data.y,1),mix.K),'logdetC',repmat(detprQS,1,mix.K));
        else
            prior.par.sigma=struct('c',repmat(prQnu,1,mix.K),'C',repmat(prQS,1,mix.K));
        end
    end


    %% mixtures of normal or student-t distributions
elseif  norstud     

    if nargin==2   % default prior:  independence hierarchical prior
        ihier=true; chtype='indep';
    else           %
        prior=varargin{1};
        ihier=isfield(prior,'hier');if ihier ihier=prior.hier;else ihier=true; end   % ihier true: hierarchical prior

        if and(finmix,isfield(prior,'type'))   % check the priortype
            priortype={'indep','concon','sigmauniform'};
            typevalid=false;
            for j=1:size(priortype,2)
                if size(priortype{j},2)==size(prior.type,2)
                    if prior.type==priortype{j} typevalid='true';chtype=priortype{j};end
                end
            end
            if ~typevalid
                chtype='indep';
                ['field type is wrongly specified in the prior definition, independence prior assumed']
            end
        else % if field type is not specified or for a regression model, assume independence prior
            chtype='indep';
        end
    end
    prior=struct('hier',ihier,'type',chtype);

    if finmix

        % data are handled as data stored by row
        ibycolumn=isfield(data,'bycolumn');
        if ibycolumn  ibycolumn=data.bycolumn; end  % ibycolumn true: data stored by column
        if ibycolumn      y=data.y'; else     y=data.y; end

        if isfield(data,'r') r=data.r;else r=size(y,1); end
        if isfield(data,'N') N=data.N;else N=size(y,2); end

        conjugate_prior=(prior.type(1:4)=='conc');
        if or(conjugate_prior,~ihier)
            bensmail=(1==1);      %Bensmail et al
            ricgre=(1==0);        %Richardson/Green, r=1, Stephens r=2 only
            raftery=(1==0);       %Raftery (1996, MCMC Practcie), r=1 only
            robert=(1==0);        %Robert(1996, MCMC Practcie), r=2 only
        else
            ricgre=(1==1);        %Richardson/Green, r=1, Stephens r=2 only
            bensmail=(1==0);      %Bensmail et al
            raftery=(1==0);       %Raftery (1996, MCMC Practcie), r=1 only
            robert=(1==0);        %Robert(1996, MCMC Practcie), r=2 only
        end

        %% prior mu

        if ricgre  %Richardson/Green stephens
            empmean=(max(y,[],2)+min(y,[],2))*0.5;
            empcov=diag((max(y,[],2)-min(y,[],2)).^2);
        else
            empmean=mean(y,2);
            empcov=cov(y');
        end


        b0=empmean;

        if conjugate_prior
            % choose B0sc .... info contained in a conjugate  prior (equal to N0)
            B0sc=1;

        elseif ~conjugate_prior
            % choose B0inv .... info matrix in a non-conjugate prior
            B0inv=inv(empcov);
        end


        %%%%%%%%%%%%%%%%%%%%%%%%%%%
        % prior Q  ... IW(prQnu,prQS)
        %  select prQnu ... degrees fo freedom
        %  select Q0, the prior mean

        % prQS  is determined from   prQS=Q0*(prQnu-(r+1)/2)
        %             which machted Q0 to the mean of the distribution of Q (inverted
        %             Wishart) and the to  mode of the distribution of Q {-1} ( Wishart)
        % shrinkage of the variances toward prQS/dfQpr


        % dfQpr bounds the ratio of the variances

        dfQpr=2.5; % for r=1: ratio boundend by  a factor 10.
        prQnu=(r-1)/2+dfQpr;  %

        if mix.K==1
            chet=1;
        else
            %%%%%%%%%%%%%%%%%%%%%%%%
            %%%%%  SELECET THE PRIOR FOR MU

            %  TUNING: choose the expected explained heterogeneity Rhet
            % close to 1: means very different in relation to the variances
            % close to 0: means rather similar in relation to the variances

            % Rhet lies between 0 and one; do not choose 0 or 1;
            %                 SMALL VALUE leads to a very informative
            %                 prior for muk close to b0; should be
            %                 chosen only in combination with a hierarchical prior on b0
            %                LARGE VALUE leads to a very informative
            %                 prior for Sigmak close to prQS/dfQpr; should be
            %                 chosen only in combination with a hierarchical prior on prQS

            Rhet=0.5;  % Rhet=2/3;
            chet=(1-Rhet);
        end

        prQS=empcov*chet*(prQnu-(r+1)/2);

  
        if and(r==1,and (conjugate_prior,raftery))       %RAftery (1996, MCMC Practcie)
            B0sc=1;  prQnu=1.28;prQS=2.6*(max(y)-min(y))^2;
        elseif and(r==2,and (conjugate_prior,robert))    %Robert(1996, MCMC Practcie)
            B0sc=1;  prQnu=3;prQS=0.75*empcov;
        end
        
         
        
        
        compare_skewnormal=false;
        % compare_skewnormal=true;
        if compare_skewnormal
            b0=zeros(size(empmean));   B0sc=0.1;    % change prior mu_k
            % prQS=empcov*(prQnu-(r+1)/2);
            ricgre=false;
            prior.type='conc';
            conjugate_prior=true;
            empcov=cov(y');
            prQS=empcov*chet*(prQnu-(r+1)/2);
        end


        if size(data.y)>1 detprQS=log(det(prQS)); end

        if ~conjugate_prior
            if size(data.y)>1
                prior.par.mu=struct('b',repmat(b0,1,mix.K),'Binv',reshape(repmat(B0inv,1,mix.K),size(data.y,1),size(data.y,1),mix.K));
            else
                prior.par.mu=struct('b',repmat(b0,1,mix.K),'Binv',repmat(B0inv,1,mix.K));
            end
        else
            prior.par.mu=struct('b',repmat(b0,1,mix.K),'N0',repmat(B0sc,1,mix.K));
        end


        if  ihier
            if ricgre
                if  r==1
                    g0=0.2;   %   choice Richardson/Green; sampling from the Gamma allows arbitray g0
                    prQnu=2;
                elseif r==2
                    g0=0.3; % stephens,
                    prQnu=3;
                end
                %    currently sampling  from non-integer Wishart  causes problems, therefore
                g0=0.5+(r-1)/2;   % in generell, g0 must be a multiple of 0.5 for the inverted Wishart
                %                  and lead to a proper prior
                % test:             g0=2+(r-1)/2;
                G0= 100*g0/prQnu*inv(empcov);  % stephens
                prQS=prQnu*empcov/100;  % define starting value for prQS
            else
                g0=0.5+(r-1)/2;   % in generell, g0 must be a multiple of 0.5 for the inverted Wishart
                %                  and lead to a proper prior
                G0= g0*inv(prQS);   %match  hierarchical and non-hierarchical priors
            end
            if r>1
                prior.par.sigma=struct('c',repmat(prQnu,1,mix.K),'C',reshape(repmat(prQS,1,mix.K),size(data.y,1),size(data.y,1),mix.K),'logdetC',repmat(detprQS,1,mix.K),'g',g0,'G',G0);
            else
                prior.par.sigma=struct('c',repmat(prQnu,1,mix.K),'C',repmat(prQS,1,mix.K),'g',g0,'G',G0);
            end
        else
            if r>1
                prior.par.sigma=struct('c',repmat(prQnu,1,mix.K),'C',reshape(repmat(prQS,1,mix.K),size(data.y,1),size(data.y,1),mix.K),'logdetC',repmat(detprQS,1,mix.K));
            else
                if all(prior.type(1:4)=='sigm')
                    % special prior:
                    %   p(\sigma^2) \propto 1/sigma I_c(sigma), meaning that
                    %   \sigma is  uniform on the interval (0,c) (ryd:em)

                    prior.par.sigma.c=-0.5*ones(1,mix.K);
                    prior.par.sigma.C=zeros(1,mix.K);
                    prior.par.sigma.max=max(abs(data.y));
                else
                    prior.par.sigma=struct('c',repmat(prQnu,1,mix.K),'C',repmat(prQS,1,mix.K));
                end
            end
        end
    end
end

if ~finmix  % finite mixture regression model

    if nargin==2
        ihier=true; % default prior hierarchical
    else
        prior=varargin{1};
        ihier=isfield(prior,'hier');if ihier ihier=prior.hier;end   % ihier true: hierarchical prior
    end
    prior=struct('hier',ihier,'type','indep');

    % data are handled as data stored by row
    ibycolumn=isfield(data,'bycolumn');
    if ibycolumn  ibycolumn=data.bycolumn; end  % ibycolumn true: data stored by column
    if ibycolumn      y=data.y'; else     y=data.y; end
    if isfield(data,'r') r=data.r;else r=size(y,1); end
    if isfield(data,'N') N=data.N;else N=size(y,2); end

    if ~isfield(mix,'d')  mix.d=1; end
    if or(isfield(mix,'ar'),isfield(mix,'arf'))
        % convert MSAR model into a large Regression model
        [datareg,mixreg]=designar(data,mix);
    else
        mixreg=mix;
        datareg=data;
    end


    if ~isfield(mix,'indexdf') mix.df=0; else mix.df=size(mix.indexdf,1)*size(mix.indexdf,2); end

    if ~isfield(mixreg,'indexdf') mixreg.df=0; else mixreg.df=size(mixreg.indexdf,1)*size(mixreg.indexdf,2); end
    b0=zeros(mixreg.d-mixreg.df,1);

    interceptdata=true;  %% prior of the intercept from the data
    interceptdata=false;

    if interceptdata
        if isfield(datareg,'X')
            index=[1:size(datareg.X,1)];
            intercept=sum(index(all(diff(datareg.X,[],2)==0,2))); % determine the index of the intercept
        else
            intercept=1;
        end


        if mixreg.df>0
            if all(mixreg.indexdf~=intercept)
                intercept=intercept-sum(mixreg.indexdf<intercept);

                if all(mix.dist(1:6)=='Normal')
                    b0(intercept)=mean(y,2);
                elseif or(all(mix.dist(1:6)=='Poisso'),all(mix.dist(1:6)=='Negbin'))
                    if mean(y,2)>0   b0(intercept)=log(mean(y,2)); end
                elseif all(mix.dist(1:6)=='Binomi')
                    my=mean(y./data.Ti);
                    if and(my>0,my>1)  b0(intercept)=log(my)-log(1-my); end
                end
            end;
        end

    end



    %%%%%%%%%%%  variance of the prior of regression coefficients

    varreg=10; %% varianz of regression coefficients: 10
    varreg=4; %% varianz of regression coefficients: 10

    varar=0.25;  %% varianz of AR coefficients: 0.1


    B0inv=eye(mixreg.d-mixreg.df)/varreg;

    if isfield(mix,'ar')
        if mix.ar>0
            if isfield(mix,'par')
                if ~isfield(mix.par,'indexar')
                    mix.par.indexar=(mix.d-mix.df)+[1:mix.ar]';
                end
            else
                mix.par.indexar=(mix.d-mix.df)+[1:mix.ar]';
            end
            B0inv(mix.par.indexar,mix.par.indexar)=eye(mix.ar)/varar;
        end
    end

    prior.par.beta=struct('b',repmat(b0,1,mix.K),'Binv',repmat(B0inv,[1 1 mix.K]));


    if  mixreg.df>0

        a0=zeros(mixreg.df,1);
        A0inv=eye(mixreg.df)/varreg;

        if interceptdata
            if any(mixreg.indexdf==intercept)
                intercept=sum(mixreg.indexdf<=intercept);
                if mix.dist(1:6)=='Normal'
                    a0(intercept)=mean(y,2);
                elseif or(all(mix.dist(1:6)=='Poisso'),all(mix.dist(1:6)=='Negbin'))
                    if mean(y,2)>0   a0(intercept)=log(mean(y,2)); end
                elseif all(mix.dist(1:6)=='Binomi')
                    my=mean(y./data.Ti);
                    if and(my>0,my>1)  b0(intercept)=log(my)-log(1-my); end
                end
            end;
        end

        if isfield(mix,'arf')
            if isfield(mix,'par')
                if ~isfield(mix.par,'indexar')   defineindex=true;   else    defineindex=false;  end
            else
                defineindex=true;
            end
            if defineindex mix.par.indexar=mix.df+[1:mix.arf]';  end
            A0inv(mix.par.indexar,mix.par.indexar)=eye(mix.arf)/varar;
        end
        prior.par.alpha=struct('a',a0,'Ainv',A0inv);
    end

    %%%%%%%%%%%%    prior variances

    if and(isfield(mix,'error'),mix.dist(1:6)=='Normal')

        prQnu=2.5; %   ratio boundend by  a factor 10.
        Rhet=0.5;%    Rhet=0.9;
        chet=(1-Rhet);
        prQS=chet*var(y')*(prQnu-1);

        if  ihier
            g0=0.5;  % test:             g0=2;
            G0= g0*inv(prQS);   %match  hierarchical and non-hierarchical priors
            prior.par.sigma=struct('c',repmat(prQnu,1,mix.K),'C',repmat(prQS,1,mix.K),'g',g0,'G',G0);
        else
            prior.par.sigma=struct('c',repmat(prQnu,1,mix.K),'C',repmat(prQS,1,mix.K));
        end
    end
end

%%%%%%%%%%  prior degrees of freedom in a Student-t model or a negative binomial model

if or(studmix,all(mix.dist(1:6)=='Negbin'))

    % prior for nu
    if isfield(mix,'parfix')
        if isfield(mix.parfix,'df')
            dffix=mix.parfix.df;
        else
            dffix=false;
        end
    else
        dffix=false;
    end
    if ~dffix
        % prior.par.df.type='Congdon';
        % prior.par.df.type='Steel';
        % prior.par.df.type='hier';  %hierarchical dependent prior for finite mixtures,only with marg-MH
        prior.par.df.type='inhier'; % independent prior for finite
        %mixtures; use this for conditional MH
        %prior.par.df.type='uniform';
        if studmix
            prior.par.df.trans=1; % use a translated prior p*(df)=p(df-prior.par.df.trans);
        else
            prior.par.df.trans=0;
        end
        if all(prior.par.df.type(1:4)=='Cong')
            prior.par.df.lmin=0.01;
            prior.par.df.lmax=0.5;
            % test: exponential prior
            %prior.par.df.lmin=1./100;
            %prior.par.df.lmax=1./100;
        elseif all(prior.par.df.type(1:4)=='Stee')
            % Gamma(2,1/10) prior , see Steel etal, JBES
        elseif or(all(prior.par.df.type(1:4)=='hier'),all(prior.par.df.type(1:4)=='inhi'))
            % Steel etal, JBES
            % prior.par.df.a0=2; prior.par.df.b0=1; dfmedian=10;  prior.par.df.d=(dfmedian-prior.par.df.trans)/(1+sqrt(2));  
            % Scott et al, 2005; JASA
            % prior.par.df.a0=1; prior.par.df.b0=1; dfmedian=10;  prior.par.df.d=dfmedian-prior.par.df.trans;  
            
            % extending the hierarchical Gamma prior of Steel etal, JBES to
            % finite mixtures; posterior moments exist, if b0>1
            %prior.par.df.a0=20; prior.par.df.b0=2; dfmean=10; prior.par.df.d=(dfmean-prior.par.df.trans)*(prior.par.df.b0-1); 
            
            %% default:
            prior.par.df.a0=2; prior.par.df.b0=2; dfmean=10; prior.par.df.d=(dfmean-prior.par.df.trans)*(prior.par.df.b0-1); 
            
        elseif all(prior.par.df.type(1:4)=='unif')
            prior.par.df.d=100;   % upper boud
        end
    end
end

%%%%%%%%%%%%%%    prior indicator model %%%%%%%%%%%%%%%%%%%%%

if mix.K>1
    % prior.K=mix.K;

    if mix.indicmod.dist(1:6)=='Multin'

        e0=4;   % prior of the weight distribtuion;

        prior.weight=repmat(e0,1,mix.K);


    elseif mix.indicmod.dist(1:6)=='Markov'

        ep=4;et=1/(mix.K-1);
        prior.indicmod.xi=(ep-et)*eye(mix.K)+et;

    else
        ['Indicator ' mix.indicmod.dist '  not supported by function priordefine']

    end
end