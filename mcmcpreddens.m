function varargout=mcmcpreddens(data,mcmcout,varargin)
% plots the posterior predictive density  

% Author: Sylvia Fruehwirth-Schnatter
% Last change: July 17, 2006



% data are handled as data stored by row
if ~isfield(data,'y') 
    warn('The field y is obligatory in the structure array defining the data when calling the function mcmcpreddens')
    prior=[];return
end    
ibycolumn=isfield(data,'bycolumn');
if ibycolumn  ibycolumn=data.bycolumn; end  % ibycolumn true: data stored by column 
if ibycolumn      y=data.y'; else     y=data.y; end  


if isfield(data,'r') r=data.r;else r=size(y,1); end
if isfield(data,'N') N=data.N;else N=size(y,2); end

if nargin==2 nfig=1; else nfig=fix(varargin{1}); end



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% loop over features, plot predictive density and compute the grid

if ~isfield(data,'X')
    figure(nfig)

    disc=repmat(false,r,1);
    for i=1:r;  
        
        if i==1 [nr, nc]=plotsub(r); end;        subplot(nr,nc,i);
        
        if isfield(data,'type')  
            disc(i) = all(data.type{i}(1:3)=='dis');
        end
        if disc(i)        % discrete data - barplot of relative frequency over 0:max(y) 
            nbin=max(y(i,:),[],2)+1;
            histneu(y(i,:)',nbin,'k');  
            
            x(i,:)=[0:max(data.y(i,:),[],2)+1];
            
        else           % unspecified data type or continous data -  histogram of absolute frequency
            nbin=40; 
            histneu(y(i,:)',nbin,'k'); 
            M=50;  % number of grid points
            
            if i==1 x=zeros(size(data.y,1),M);   end
            
            x(i,:)=linspace(min(data.y(i,:),[],2),max(data.y(i,:),[],2),M);
            
        end
    end        
    
    %%%%%%%%   compute predictive density 
    
    finmix=all([~isfield(mcmcout.model,'d') ~isfield(mcmcout.model,'ar') ~isfield(mcmcout.model,'arf')]);
    
    if and(finmix,r==1)
        pred=zeros(size(x));
        for m=1:mcmcout.M
            mix=mcmcextract(mcmcout,m);
            pred=pred+mixturepdf(mix,x);
        end 
        pred=pred/mcmcout.M;
        
        % plot predicitve density
        
        hold on;
        if disc(1)     
            bar(x,pred(1,:),0.3);     
            set(gca,'xlim',[x(1,1)-1 x(1,end)+1]);
            %if max(pred(i,:),[],2)>0  axis(  0 max(pred(i,:),[],2)*1.1]);end
        else
            plot(x(1,:),pred(1,:),'k');  cl=0.95 ; cu=1.05; 
            set(gca,'xlim',[min(x(1,1)*cl,x(1,1)/cl) max(x(1,end)*cu,x(end,1)/cu)]);
        end
        xlabel(['y']);
        
        if isfield(data,'name') title(['Predictive posterior density of the ' data.name ' data']); end 
        hold off;  
        
    elseif and(finmix,r>1)    
        
        pred=zeros(size(x));
        for m=1:mcmcout.M
            mix=mcmcextract(mcmcout,m);
            for i=1:r; 
                mixi=mixturemar(mix,i);
                pred(i,:)=pred(i,:) + mixturepdf(mixi,x(i,:));
            end
        end 
        pred=pred/mcmcout.M;
        
        % plot of each marginal predicitve density
        
        for i=1:r;  
            subplot(nr,nc,i); 
            hold on;
            plot(x(i,:),pred(i,:),'k');  cl=0.95 ; cu=1.05; 
            set(gca,'xlim',[min(x(i,1)*cl,x(i,1)/cl) max(x(i,end)*cu,x(end,1)/cu)]);
            xlabel(['y_' num2str(i)]);
            
            if i==1
                if isfield(data,'name')  % adds title  for named data
                    title(['Marginal predictive posterior density of the ' data.name ' data']);
                end 
            end
            if i==r hold off;  end
        end
        
        nfig=nfig+1;figure(nfig);
        
        % contourplot of each pair of bivariate marginal distributions
        
        ij=nchoosek([1:r]',2);
        [nr, nc]=plotsub(size(ij,1));
        
        for ii=1:size(ij,1)
            subplot(nr,nc,ii);
            
            [x1,x2]=meshgrid(x(ij(ii,1),:),x(ij(ii,2),:));
            L=zeros(size(x1)); 
            
            for m=1:mcmcout.M
                mix=mcmcextract(mcmcout,m);
                mixij=mixturemar(mix,ij(ii,:));
                if and(isfield(mixij.par,'sigmainv'),isfield(mixij.par,'logdet')) 
                    Qinv=mixij.par.sigmainv;
                    detQ=mixij.par.logdet;
                else
                    Qinv=zeros(size(mixij.par.sigma)); detQ=zeros(1,mixij.K);    
                    for k=1:mixij.K;
                        Qinv(:,:,k)=inv(mixij.par.sigma(:,:,k));detQ(k)=log(det(Qinv(:,:,k)));
                    end
                end
                for k=1:mix.K;
                    d1=x1-mixij.par.mu(1,k);
                    d2=x2-mixij.par.mu(2,k);
                    L=L+mixij.weight(k)*detQ(k)^.5*exp(-(d1.^2/2*Qinv(1,1,k)+ d1.*d2*Qinv(1,2,k) + d2.^2/2*Qinv(2,2,k) ));
                end
            end
            L=L/mcmcout.M;
            
            [ch,h]=contour(x1,x2,L,10); c=1.05;
            set(gca,'xlim',[min(min(min(x1))/c,min(min(x1))*c)  max(max(max(x1))*c,max(max(x1))/c) ]);
            set(gca,'ylim',[min(min(min(x2))/c,min(min(x1))*c)  max(max(max(x2))*c,max(max(x2))/c) ]);
            xlabel(['y_' num2str(ij(ii,1))]);
            ylabel(['y_' num2str(ij(ii,2))]);
            hold on;scatter(y(ij(ii,1),:),y(ij(ii,2),:),'kx');hold off;
        end
        
    end
end

if nargout==1 varargout{1}=nfig; end


