function mcmcsub=mcmcsubseq(mcmcout,mall);
% stores the m-th MCMC draw as mixture model
% Author: Sylvia Fruehwirth-Schnatter
% Last change: July 15, 2006


if size(mall,1)*size(mall,2)==1
    
    warn(['Use the Subroutine MCMCEXTRACT to extract a single mixture'])
    mcmcsub=[];return
end

if size(mall,2)==1  mall=mall'; end

mcmcsub.model=mcmcout.model;
mcmcsub.prior=mcmcout.prior;
if isfield(mcmcout,'clust') mcmcsub.clust=mcmcout.clust; end

if ~isfield(mcmcout.model,'indicfix')  mcmcout.model.indicfix=false; end
if ~isfield(mcmcout.model,'indicmod') mcmcout.model.indicmod.dist='Multinomial';  end


if ~isfield(mcmcout.model,'K')   mcmcout.model.K=1; end

if all([~mcmcout.model.indicfix mcmcout.model.K>1 isfield(mcmcout,'S')])
    MS=mcmcout.M-size(mcmcout.S,1);
    mallS=mall(mall>MS)-MS;
    mcmcsub.S=mcmcout.S(mallS,:);
end

mcmcsub.M=size(mall,2);
if isfield(mcmcout,'ranperm')  mcmcsub.ranperm= mcmcout.ranperm; end

mcmcsub.log.mixlik=mcmcout.log.mixlik(mall);
mcmcsub.log.mixprior=mcmcout.log.mixprior(mall);

if and(mcmcout.model.K>1,~mcmcout.model.indicfix)
    mcmcsub.entropy=mcmcout.entropy(mall);
    mcmcsub.log.cdpost=mcmcout.log.cdpost(mall);
    if mcmcout.model.indicmod.dist(1:6)=='Multin' 
        mcmcsub.weight=mcmcout.weight(mall,:);
        if isfield(mcmcout,'post')  mcmcsub.post.weight = mcmcout.post.weight(mall,:); end
    elseif mcmcout.model.indicmod.dist(1:6)=='Markov' 
        mcmcsub.indicmod.xi= mcmcout.indicmod.xi(mall,:,:);
        if isfield(mcmcout,'ST') mcmcsub.ST= mcmcout.ST(mall); end
        if isfield(mcmcout,'post')  mcmcsub.post.indicmod.xi = mcmcout.post.indicmod.xi(mall,:,:); end
    end
end

if ~isstruct(mcmcout.par) 
    mcmcsub.par=mcmcout.par(mall,:);
else
    if isfield(mcmcout.par,'beta') 
        mcmcsub.par.beta= mcmcout.par.beta(mall,:,:);
    end
    if isfield(mcmcout.par,'alpha')         mcmcsub.par.alpha= mcmcout.par.alpha(mall,:);    end
    
    if isfield(mcmcout.par,'lambda')            
        if any([all(mcmcout.model.dist(1:6)=='SkNomu') all(mcmcout.model.dist(1:6)=='SkStmu')])
            mcmcsub.par.lambda=mcmcout.par.lambda(mall,:,:);
        else    
            mcmcsub.par.lambda= mcmcout.par.lambda(mall,:);
        end    
    end
    
    if isfield(mcmcout.par,'pi')         mcmcsub.par.pi= mcmcout.par.pi(mall,:,:);    end
    
    
    
    if isfield(mcmcout.par,'mu') 
        if any([all(mcmcout.model.dist(1:6)=='Normul') all(mcmcout.model.dist(1:6)=='Stumul') all(mcmcout.model.dist(1:6)=='SkStmu') all(mcmcout.model.dist(1:6)=='SkNomu') ])
            mcmcsub.par.mu=mcmcout.par.mu(mall,:,:);
        else    
            mcmcsub.par.mu= mcmcout.par.mu(mall,:);
        end    
    end
    
    if isfield(mcmcout.par,'mean') 
        if any([all(mcmcout.model.dist(1:6)=='SkStmu') all(mcmcout.model.dist(1:6)=='SkNomu') ])
            mcmcsub.par.mean=mcmcout.par.mean(mall,:,:);
        else    
            mcmcsub.par.mean= mcmcout.par.mean(mall,:);
        end    
    end
    

    
    if isfield(mcmcout.par,'df')  
        mcmcsub.par.df= mcmcout.par.df(mall,:);
    end
    
    if isfield(mcmcout.par,'sigma')
        if any([all(mcmcout.model.dist(1:6)=='Normul') all(mcmcout.model.dist(1:6)=='Stumul') all(mcmcout.model.dist(1:6)=='SkStmu') all(mcmcout.model.dist(1:6)=='SkNomu') ])
            mcmcsub.par.sigma=mcmcout.par.sigma(mall,:,:);
            if isfield(mcmcout.par,'sigmainv')  mcmcsub.par.sigmainv=mcmcout.par.sigmainv(mall,:,:);    end
            if isfield(mcmcout.par,'logdet')  mcmcsub.par.logdet=mcmcout.par.logdet(mall,:,:);    end
        else
            mcmcsub.par.sigma=mcmcout.par.sigma(mall,:);
        end
        
    end
end

if isfield(mcmcout,'Nk') mcmcsub.Nk=mcmcout.Nk(mall,:); end
if isfield(mcmcout,'hyper') mcmcsub.hyper=mcmcout.hyper(mall,:); end

if isfield(mcmcout,'post')
    
    if ~isstruct(mcmcout.post.par)
        mcmcsub.post.par=mcmcout.post.par(mall,:,:);
    else
        
        if isfield(mcmcout.post.par,'a') 
            mcmcsub.post.par.b=mcmcout.post.par.b(mall,:);
            mcmcsub.post.par.a=mcmcout.post.par.a(mall,:);
        end    
        
        if isfield(mcmcout.post.par,'mu') 
            if any([all(mcmcout.model.dist(1:6)=='Normul') all(mcmcout.model.dist(1:6)=='Stumul') all(mcmcout.model.dist(1:6)=='SkStmu') all(mcmcout.model.dist(1:6)=='SkNomu') ])
                mcmcsub.post.par.mu.b=mcmcout.post.par.mu.b(mall,:,:);
                mcmcsub.post.par.mu.B=mcmcout.post.par.mu.B(mall,:,:,:);
            else
                mcmcsub.post.par.mu.b=mcmcout.post.par.mu.b(mall,:);
                mcmcsub.post.par.mu.B=mcmcout.post.par.mu.B(mall,:);
            end    
        end   
        
        
        if isfield(mcmcout.post.par,'sigma') 
            if any([all(mcmcout.model.dist(1:6)=='Normul') all(mcmcout.model.dist(1:6)=='Stumul') all(mcmcout.model.dist(1:6)=='SkStmu') all(mcmcout.model.dist(1:6)=='SkNomu') ])
                mcmcsub.post.par.sigma.c=mcmcout.post.par.sigma.c(mall,:);
                mcmcsub.post.par.sigma.C=mcmcout.post.par.sigma.C(mall,:,:);
                mcmcsub.post.par.sigma.logdetC=mcmcout.post.par.sigma.logdetC(mall,:);
            else
                mcmcsub.post.par.sigma.c=mcmcout.post.par.sigma.c(mall,:);
                mcmcsub.post.par.sigma.C=mcmcout.post.par.sigma.C(mall,:);
            end    
        end   
        
        if isfield(mcmcout.post.par,'beta') 
            mcmcsub.post.par.beta.b=mcmcout.post.par.beta.b(mall,:,:);
            mcmcsub.post.par.beta.B=mcmcout.post.par.beta.B(mall,:,:,:);
        end  
        
        if isfield(mcmcout.post.par,'alpha') 
            mcmcsub.post.par.alpha.a=mcmcout.post.par.alpha.a(mall,:);
            mcmcsub.post.par.alpha.A=mcmcout.post.par.alpha.A(mall,:,:);
        end  
    end
end