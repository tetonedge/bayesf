function varargout=mcmcbfplot(mcmcout,varargin)

% Monitoring of simulation based approximaions to the marginal likelihood 

% Author: Sylvia Fruehwirth-Schnatter

marlik=mcmcout.marlik;

if nargin==1
    ifig=1;
else
    ifig=fix(varargin{1});
end    
c='k';

figure(ifig);

nr=2;

subplot(nr,3,1);plot(mcmcout.log.mixlik,'k');ylabel('MCMC'); title('Mixture Likelihood')

subplot(nr,3,2);plot(mcmcout.log.mixprior,'k');title('Mixture Prior')
subplot(nr,3,3);plot(marlik.log.qmc,'k');title('Importance Density')

subplot(nr,3,4);plot(marlik.log.loglikq,'k');ylabel('q-draws')


subplot(nr,3,5);plot(marlik.log.priorq,'k'); 
subplot(nr,3,6);plot(marlik.log.qq,'k'); 


ifig=ifig+1;
figure(ifig);

nr=3;nc=1;nbin=40;
subplot(nr,nc,1);hist([mcmcout.log.mixlik marlik.log.loglikq],nbin); title('Mixture Likelihood');legend('MCMC','q-draws');
subplot(nr,nc,2);hist([mcmcout.log.mixprior marlik.log.priorq],nbin); title('Mixture Prior');legend('MCMC','q-draws');
subplot(nr,nc,3);hist([marlik.log.qmc marlik.log.qq],nbin); title('Importance Density');legend('MCMC','q-draws');


if nargout==1
    varargout{1}=ifig;
end    
