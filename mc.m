function simout=mc(model,post,control)

if ~isstruct(control)
    M=max(fix(control),1);
    storelog=false;
else
    M=control.M;
    storelog=control.storelog;
end

finmix=all([~isfield(model,'d') ~isfield(model,'ar') ~isfield(model,'arf')]);

if model.dist(1:6)=='Multin'
    % posterior density of the parameter of  multnomial distribution

    % gam = gamrnd(repmat(post,M,1),1);
    % simout = gam ./ repmat(sum(gam,2),1,model.cat);

    simout = dirichsim(post,M);

elseif model.dist(1:6)=='Poisso'

    if ~isfield(model,'K') K=1; else K=model.K; end

    if K==1
        simout = max(gamrnd(repmat(post.a,M,1),1),1e-10)/post.b;
    else
        simout=prodgamsim(post,M);
    end

elseif or(model.dist(1:6)=='Normal',model.dist(1:6)=='Normul')

    if ~isfield(model,'K')     model.K=1;      end
    if  model.dist(1:6)=='Normal'    r=1;
    elseif ~isfield(model,'r') warn(['Field r has to be added to the model before calling the function mc']);simout=[];return;
    else r=model.r;
    end

    if finmix
        % finite mixture model with conjugate priors

        if ~isfield(post.mu,'N0') warn(['Function mc may be called only under conditionally conjugate prior']);simout=[];return;end

         if r==1
            %simout.sigma=1./gamrnd(repmat(post.sigma.c,M,1),1).*repmat(post.sigma.C,M,1);
            simout.sigma=1./prodgamsim(struct('a',post.sigma.c,'b',post.sigma.C),M);

            simout.mu=repmat(post.mu.b,M,1)+(simout.sigma./repmat(post.mu.N0,M,1)).^.5.*randn(M,model.K);
        else
            if M==1
                simout.mu=zeros(r,model.K);simout.logdet=zeros(1,model.K); simout.sigma=zeros(r,r,model.K); simout.sigmainv=simout.sigma;
                for k=1:model.K
                    [simout.sigma(:,:,k) simout.sigmainv(:,:,k) simout.logdet(1,k)] = invwisim(post.sigma.c(k),squeeze(post.sigma.C(:,:,k)));
                    simout.mu(:,k)=post.mu.b(:,k)+ chol(simout.sigma(:,:,k))'/post.mu.N0(k)*randn(r,1);
                end
            else
                simout.mu=zeros(M,r,model.K);simout.logdet=zeros(M,model.K); simout.sigma=zeros(M,r,r,model.K); simout.sigmainv=simout.sigma;
                for k=1:model.K
                    for m=1:M
                        [simout.sigma(m,:,:,k) simout.sigmainv(m,:,:,k) simout.logdet(m,k)] = invwisim(post.sigma.c(k),squeeze(post.sigma.C(:,:,k)));
                        simout.mu(m,:,k)=post.mu.b(:,k)+ chol(simout.sigma(m,:,:,k))'/post.mu.N0(k)*randn(r,1);
                    end
                end
            end
        end
        
    elseif and(~finmix,r==1) % mixture regression model with conjugate prior

        simout.sigma=1./prodgamsim(struct('a',post.sigma.c,'b',post.sigma.C),M);
        for k=1:model.K
            for m=1:M
                simout.beta(m,:,k)=post.beta.b(:,k)+ chol(simout.sigma(m,k)*post.beta.B(:,:,k))'*randn(model.d,1);
            end
        end
        
    elseif and(~finmix,r>1) % multivariate mixture regression model with conjugate prior  
        'ADD MULTIVARIATE REGRESSION MODEL to function mc',
        
    end


else
    warn(['Distribution '   model.dist ' is not supported by function mc']);
    simout=[]; return;
end
