function pm=mcmcpm(mcmcout)

% compute the approximate posterior mode from the MCMC draws

mixpost=mcmcout.log.mixlik+mcmcout.log.mixprior;    

[postm is]=sort(mixpost);
pmdraw=is(end);
pm=mcmcextract(mcmcout,pmdraw);
