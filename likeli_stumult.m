function [lh,  maxl, llh] = likeli_stumult(y,mu,Qinv,detQinv,df)

% multivaraite mixtures of normals
% sample S from the full conditional 

% S .. (1 times N) vector of group-number

K = size(Qinv,3);
r = size(y,1);
d = size(Qinv,1);
N = size(y,2);

m = zeros(r,K);
llh = zeros(K,N); 

for k=1:K,
  eps = y - mu(:,k*ones(1,N)); 
  err=sum(eps'*squeeze(Qinv(:,:,k)).*eps',2)'; 
llh(k,:) = gammaln((df(k)+r)/2)-gammaln(df(k)/2) +.5*detQinv(k) -.5*r*log(df(k)*pi) -(df(k)+r)/2.*log(1+err./df(k));

end

maxl = max(llh,[],1);
lh = exp(llh - maxl(ones(K,1),:));
  
 