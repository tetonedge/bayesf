function [est,varargout]=mcmcestimate(mcmcout)    

% Derive parameter estimates from MCMC draws for a finite mixture model 

% Author: Sylvia Fruehwirth-Schnatter
% Last change: November 21, 2006

if ~isfield(mcmcout.model,'indicfix')  
    indicfix=false; 
else
    indicfix=mcmcout.model.indicfix;
end
if ~isfield(mcmcout.model,'indicmod') 
    indicmod.dist='Multinomial';  
else
    indicmod.dist=mcmcout.model.indicmod.dist;
end

mcmcout.est.pm=mcmcpm(mcmcout);

[logm is]=sort(mcmcout.log.mixlik);
pmdraw=is(end);
mcmcout.est.ml=mcmcextract(mcmcout,pmdraw);

average=mcmcaverage(mcmcout);

if isfield(mcmcout.model,'K')   K=mcmcout.model.K; else      K=1; end    

if or(K==1,indicfix)  % mixture model with known indicators or no mixture
    mcmcout.est.ident=average;
elseif ~indicfix    % model identification for mixture models
    if     mcmcout.ranperm==false
        mcmcout.est.average=average;
    else
        mcmcout.est.invariant=average;
    end
    if mcmcout.ranperm
        if ~isfield(mcmcout,'perm') mcmcout=mcmcpermute(mcmcout); end
        if mcmcout.Mperm>0
        if indicmod.dist(1:6)=='Multin'
            mcmcout.est.ident=mcmcaverage(struct('model',mcmcout.model,'M',mcmcout.Mperm,'par',mcmcout.parperm,'weight',mcmcout.weightperm));
        elseif indicmod.dist(1:6)=='Markov'
            mcmcout.est.ident=mcmcaverage(struct('model',mcmcout.model,'M',mcmcout.Mperm,'par',mcmcout.parperm,'indicmod',struct('dist','Markovchain','xi',mcmcout.indicmod.xiperm)));
        end
        else
             warn('No identification possible, not a single draw is a permutation in the function mcmcpermute');
        end
    end
end
est=mcmcout.est;
if nargout==2
    varargout{1}=mcmcout;
end    