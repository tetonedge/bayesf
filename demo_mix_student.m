clear mixtrue;
mixtrue.dist='Student';
mixtrue.K=2;

mixtrue.par.mu=[-4 3];
mixtrue.par.sigma=[0.5 0.5];
mixtrue.par.df=[4 10];
mixtrue.weight=[0.2 0.8];

data=simulate(mixtrue,2000);
mixtrue.omega=data.omega;
data.Strue=data.S;
data=rmfield(data,'S');

dataplot(data)


%% define prior

clear mix;
mix.dist='Student';
mix.K=2;

prior=priordefine(data,mix);


%%  define mcmc

[data, mix,mcmc]=mcmcstart(data,mix);

mcmc.M=5000;mcmc.burnin=2000;

mcmc.mh.tune.df=[2 2];  

%%  mcmc

mcmcout=mixturemcmc(data,mix,prior,mcmc);
mcmcplot(mcmcout,2)

[est,mcmcout]=mcmcestimate(mcmcout)

%% marginal likelihoods

[marlik,mcmcout]=mcmcbf(data,mcmcout)

mcmcout.name= 'store_mix_student_K2';
mcmcstore(mcmcout);