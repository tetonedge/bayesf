
rho = randperm(mix.K)';

if and(univ,~isfield(mix,'d'))
    
    mix.par=mix.par(rho);

elseif or(norstud,skew)

    if  isfield(mix,'d') % finite mixture regression model
        mix.par.beta=mix.par.beta(:,rho);
    else  % standard mixture model
        mix.par.mu=mix.par.mu(:,rho);
    end
    if skew    
        mix.par.lambda=mix.par.lambda(:,rho); 
        if isfield(mix.par,'psi') mix.par.psi=mix.par.psi(:,rho); end
        if isfield(mix.par,'Q') mix.par.Q=mix.par.Q(:,rho);  end
    end
    if data.r>1
        mix.par.sigma=mix.par.sigma(:,:,rho);
        if and(skew,isfield(mix.par,'sigmaeps'))   mix.par.sigmaeps=mix.par.sigmaeps(:,:,rho); end
        mix.par.sigmainv=mix.par.sigmainv(:,:,rho);
        mix.par.logdet=mix.par.logdet(1,rho);
    else
        mix.par.sigma=mix.par.sigma(rho);
        if skew  mix.par.sigmaeps=mix.par.sigmaeps(rho);  end
    end
    if and(isfield(mix.par,'df'),~dffix)
        if size(mix.par.df,2)~=1   mix.par.df=mix.par.df(:,rho); end
    end
        
elseif  and(glm,isfield(mix,'d'))

     mix.par.beta=mix.par.beta(:,rho); 
     if  and(all(mix.dist(1:6)=='Negbin'),~dffix)
              mix.par.df=mix.par.df(:,rho);   
     end

  
else

    'ADD DISTRIBUTION FAMILY function ranpermute'

end

if mix.K>1
    if ~mix.indicfix
        if mix.indicmod.dist(1:6)=='Multin'

          mix.weight = mix.weight(rho);

        elseif mix.indicmod.dist(1:6)=='Markov'

             mix.indicmod.xi=mix.indicmod.xi(rho,rho);
        end
    end
end

