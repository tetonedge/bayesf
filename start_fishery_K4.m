%% Fishery data
%  
%  fit a univariate normal mixture with 4 components

clear data; clear mix; clear prior; 

%% load the data

data=dataget('fishery');
    
%% define the structure of the mixture

mix.dist='Normal';
mix.K=4;

%% define the prior

prior=priordefine(data,mix);

%% MCMC

[data,mix,mcmc]=mcmcstart(data,mix);
mcmcout=mixturemcmc(data,mix,prior,mcmc);

%% Plot MCMC

ifig=mcmcplot(mcmcout);

%% parameter estimation

[est,mcmcout]=mcmcestimate(mcmcout);
est.ident.weight
est.ident.par

%% plot the fitted mixture

data.model=est.ident;
dataplot(data,ifig+1)

%% compute the marginal likelihood

[marlik,mcmcout]=mcmcbf(data,mcmcout);
marlik.bs     % bridge sampling estimator of the marginal likelihood

%% store the results
mcmcout.name='store_fishery_K4';
mcmcstore(mcmcout);