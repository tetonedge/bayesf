function margmom=mcmcmargmom(mcmcout)
% Compute standard moments of the marginal distributions of mixtured samples from the posterior predictive

% Author: Sylvia Fruehwirth-Schnatter
% Last change: September 8, 2006

M=mcmcout.M;  

finmix=all([~isfield(mcmcout.model,'d') ~isfield(mcmcout.model,'ar') ~isfield(mcmcout.model,'arf')]);


if    finmix   % ~isfield(mcmcout.model,'d')
    
    for m=1:mcmcout.M
        mix=mcmcextract(mcmcout,m);
        mom=moments(mix);
        if m==1
            names=fieldnames(mom);
            for i=1:size(names,1)
                eval(['margmom.' names{i} '=zeros([M size(mom.' names{i} ')]);']);
            end    
        end    
        
        for i=1:size(names,1)
            eval(['i1=size(mom.' names{i} ',2)==1;']);
            eval(['i2=size(mom.' names{i} ',2)>1;']);
            if i1 
                eval(['margmom.' names{i} '(m,:)=mom.' names{i} ';']);
            elseif i2
                eval(['margmom.' names{i} '(m,:,:)=mom.' names{i} ';']);
            end    
        end    
    end
    
else
     margmom=[];
end



