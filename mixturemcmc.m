function mcmcout=mixturemcmc(data,mix,prior,mcmc)
% Run MCMC for a finite mixture distribution

% Author: Sylvia Fruehwirth-Schnatter/HW
% Last change: July 10, 2007


rand('state',sum(100*clock)) ;
randn('state',sum(100*clock)) ;

%%%%%%%%%%%  CHECK INPUT

if ~isfield(mix,'dist')  warn('The field dist is obligatory in the structure array defining the mixture when calling the function priordefine'); return; end

if ~isfield(mix,'K') mix.K=1;  end  % single member from the distribution family

if and(~isfield(mix,'weight'),mix.K==1) mix.weight=1;  end

if and(~isfield(mix,'indicmod'),mix.K>1)   % default: mulinomial for indicators
    mix.indicmod.dist='Multinomial';
    mix.indicmod.cat=mix.K;
    mix.indicmod.T=1;
elseif and(isfield(mix,'indicmod'),mix.K>1)
    if ~isfield(mix.indicmod,'init') mix.indicmod.cat=mix.K;  end
    if ~isfield(mix.indicmod,'T') mix.indicmod.T=1;    end
end

if and(isfield(mix,'ar'),isfield(mix,'arf'))  warn('The AR coefficients must be either fixed or switching'); return; end
if ~isfield(data,'N') N=size(data.y,2); data.N=N; else N=data.N;end

studmix=any([all(mix.dist(1:6)=='Stumul') all(mix.dist(1:6)=='Studen') all(mix.dist(1:6)=='SkewSt') all(mix.dist(1:6)=='SkStmu')]);
skew=false;
norstud=any([all(mix.dist(1:6)=='Normal') all(mix.dist(1:6)=='Normul') studmix]);  
%      Normal or student mixture;
if and(norstud,~isfield(mix,'omega'))  mix.omega=ones(1,data.N); end
glm=any([all(mix.dist(1:6)=='Poisso') all(mix.dist(1:6)=='Binomi')  all(mix.dist(1:6)=='Negbin') ]);
univ=any([all(mix.dist(1:6)=='Poisso') all(mix.dist(1:6)=='Expone') all(mix.dist(1:6)=='Binomi')]);




if all([~isfield(mix,'error') norstud]) % Normal or student mixture; default: switching variane
    mix.error='switch';
end

if ~isfield(data,'empty') data.empty=false; end
if ~isfield(mcmc,'PX') mcmc.PX=false; end

% data are handled as data stored by row
if isfield(data,'bycolumn') ibycolumn=data.bycolumn; else ibycolumn=false; end  % ibycolumn true: data stored by column
if ibycolumn
    data.y=data.y';
    if isfield(data,'X') data.X=data.X'; end
    data.bycolumn =false;
end
if ~isfield(data,'y')
    warn('The field y is obligatory in the structure array defining the data when calling the function mixturemcmc')
    prior=[];return
end

if ~isfield(data,'r') data.r=size(data.y,1); end
if ~isfield(mix,'r') mix.r=data.r;  end  % single member from the distribution family
mcmcout=struct('model',struct('dist',mix.dist,'r',mix.r),'prior',prior,'M',mcmc.M);


if all(mix.dist(1:6)=='Poisso')
    if ~isfield(data,'exposures') data.exposures=ones(1,data.N); end
end


ihier=isfield(prior,'hier');if ihier ihier=prior.hier;end   % ihier true: hierarchical prior


if norstud
    if isfield(mix,'parfix')
        if isfield(mix.parfix,'df')
            dffix=mix.parfix.df;
            mcmcout.model.parfix.df=true;
            mcmcout.model.par.df=mix.par.df;
        else
            dffix=false;
        end
        if isfield(mix.parfix,'mu')
            mufix=mix.parfix.mu;
            mcmcout.model.parfix.mu=true;
            mcmcout.model.par.mu=mix.par.mu;
        else
            mufix=false;
        end
        if isfield(mix.parfix,'sigma')
            sigmafix=mix.parfix.sigma;
        else
            sigmafix=false;
        end
    else
        dffix=false;
        mufix=false;
        sigmafix=false;
    end
elseif  all(mix.dist(1:6)=='Negbin')
    if isfield(mix,'parfix')
        if isfield(mix.parfix,'df')
            dffix=mix.parfix.df;
            mcmcout.model.parfix.df=true;
             mcmcout.model.par.df=mix.parfix.df;
        else
            dffix=false;
        end
    else
        dffix=false;
    end
else
    dffix=false;
end

 

    
if mix.K>1
    mcmcout.model.K=mix.K;
    if mix.indicmod.dist(1:6)=='Markov'
        if isfield(mix.indicmod,'init')
            mcmcout.model.indicmod=struct('dist','Markovchain','init',mix.indicmod.init);
        else
            mcmcout.model.indicmod.dist='Markovchain';
        end
    end
    if isfield(mix,'indicfix')     mcmcout.model.indicfix=mix.indicfix;   end
else
    if isfield(data,'S') data=rmfield(data,'S'); end
    data.S=ones(1,size(data.y,2));

end
if isfield(mix,'indexdf') mcmcout.model.indexdf=mix.indexdf; mix.df=size(mix.indexdf,1)*size(mix.indexdf,3); else mix.df=0; end
if isfield(mix,'d')  mcmcout.model.d=mix.d; end
if isfield(mix,'ar')
    if ~isfield(mix,'d') mix.d=1; mcmcout.model.d=mix.d;  end
    mcmcout.model.ar=mix.ar;
    if mix.ar>0
        if isfield(mix,'par')
            if ~isfield(mix.par,'indexar') mix.par.indexar=(mix.d-mix.df)+[1:mix.ar]';  end
        else
            mix.par.indexar=(mix.d-mix.df)+[1:mix.ar]';
        end
        mcmcout.model.par.indexar=mix.par.indexar;
    end
end
if isfield(mix,'arf')
    mcmcout.model.arf=mix.arf;
    if isfield(mix,'par')
        if ~isfield(mix.par,'indexar')  mix.par.indexar=mix.df+[1:mix.arf]';  end
    else
        mix.par.indexar=mix.df+[1:mix.arf]';
    end
    mcmcout.model.par.indexar=mix.par.indexar;
end
if ~isfield(mix,'indicfix')  mix.indicfix=false;   end

if and(mix.K>1,~mix.indicfix)
    mcmcout.log=struct('mixlik',zeros(mcmc.M,1),'mixprior',zeros(mcmc.M,1),'cdpost',zeros(mcmc.M,1));
    mcmcout.entropy=zeros(mcmc.M,1);
    mcmcout.ST=zeros(mcmc.M,1);
else
    mcmcout.log=struct('mixlik',zeros(mcmc.M,1),'mixprior',zeros(mcmc.M,1));
end
if isfield(data,'t0')
    if data.t0>1
        mcmcout.log.t0=data.t0;
        mcmcout.clust.t0=data.t0;
    end;
end

if ~isfield(mcmc,'burnin') mcmc.burnin=0; end

if all(mix.dist(1:6)=='Multin')  
    if ~isfield(mix,'label')
        if ~isfield(mix,'r') mix.r=1; end
        if ~isfield(mix,'cat') mix.cat=2*ones(mix.r,1); end
        mix.label=[1:mix.cat(1)]';
        iy=ones(mix.cat(1),1);
        for j=2:mix.r
            mix.label=[mix.label;[1:mix.cat(j)]'];
            iy=[iy; j*ones(mix.cat(j),1)];
        end
    end
    mcmcout.model.cat=mix.cat;
end

istorepost=isfield(mcmc,'storepost');if istorepost istorepost=mcmc.storepost;end
istoreinv=isfield(mcmc,'storeinv');if istoreinv istoreinv=mcmc.storeinv;else istoreinv=true;end
%istoreinv true: store inverse covariances for multivariate mixtures

post.par.empty=true;

if or(mix.indicfix,mix.K==1)  mcmc.ranperm=false; end

iperm=isfield(mcmc,'ranperm');if iperm iperm=mcmc.ranperm;else iperm=true; end  % iperm true: random permuation sampling

if isfield(mix,'d') if and (mix.d==1,~isfield(data,'X')) data.X=ones(1,data.N); end; end

if or(isfield(mix,'ar'),isfield(mix,'arf'))
    % convert MSAR model into a large Regression model
    [data,mix]=designar(data,mix);
end


istoreS=isfield(mcmc,'storeS');if istoreS istoreS=fix(mcmc.storeS);mcmcout.S=zeros(istoreS,data.N);end   % ihier true: hierarchical prior

%%%  check if all staring values have been defined

istartpar=isfield(mcmc,'startpar'); if istartpar istartpar=mcmc.startpar;end



if and(and(istartpar,~mix.indicfix),mix.K>1)  % start with sampling the allocations, check if Starting values for the parameter have been defined,
    if ~isfield(mix,'par')
        warn('Starting values for component parameters have to be assigned to the field par in the structure array defining the mixture')
        mcmcout=[];return;
    elseif and(~isfield(mix,'weight'),mix.indicmod.dist(1:6)=='Multin')
        warn('Starting value for the weight distribution has to be assigned to the field weight in the structure array defining the mixture')
        mcmcout=[];return;
    elseif and(~isfield(mix.indicmod,'xi'),mix.indicmod.dist(1:6)=='Markov')
        warn('Starting value for the transition matric has to be assigned to the field xi in the structure array indicmod defining the Markov mixture')
        mcmcout=[];return;
    else
        if skew mix=skewn_transform(mix); end
        %  check mix.par!!!!
        [class S] =dataclass(data,mix,0);
        if  mix.indicmod.dist(1:6)=='Multin'
            data.S=S;
        elseif mix.indicmod.dist(1:6)=='Markov'
            data.S=S(2:end);  data.S0=S(1);
        end
    end
else
    if and(~isfield(data,'S'),mix.K>1)
        warn('Starting value for the classification has to be assigned to the field S in the structure array defining the data')
        mcmcout=[];return;
    end


    if norstud   %% Normal or student mixture;
        if prior.type(1:4)=='inde'
            % check if a starting value for mu has been defined

            if isfield(mix,'d')  % mixture regression model

                if ~isfield(mix,'par')
                    warn('Starting values for error variances have to be assigned to the field par.sigma in the structure array defining the mixture regression model');mcmcout=[];return;
                elseif ~isfield(mix.par,'sigma')
                    warn('Starting values for error variances have to be assigned to the field par.sigma in the structure array defining the mixture regression model');mcmcout=[];return;
                end

            else   % finite mixture model

                if ~isfield(mix,'par')
                    warn('Starting values for component means have to be assigned to the field par.mu in the structure array defining the mixture'); mcmcout=[];return;
                elseif ~isfield(mix.par,'mu')
                    warn('Starting values for component means have to be assigned to the field par.mu in the structure array defining the mixture'); mcmcout=[];return;
                end
            end
        end

        if   and(or(mix.dist(1:6)=='Normul',mix.dist(1:6)=='Stumul'),~isfield(prior.par.sigma,'logdetC'))
            prior.par.sigma.logdetC=zeros(size(prior.par.sigma.c));
            for k=1:mix.K;
                prior.par.sigma.logdetC(1,k)=log(det(prior.par.sigma.C(:,:,k)));
            end
        end
    elseif skew
        if and(~isfield(mix,'par'),mcmc.startpar)
            warn('Starting values for all component parameters have to be assigned to the field par  in the structure array defining the mixture'); mcmcout=[];return;

        elseif mcmc.startpar
            if ~isfield(mix.par,'mu')  warn('Starting values for mu has to be assigned to the field par.mu in the structure array defining the mixture'); mcmcout=[];return;end
            if ~isfield(mix.par,'sigma')  warn('Starting values for sigma2 have to be assigned to the field par.sigma in the structure array defining the mixture'); mcmcout=[];return;end
            if ~isfield(mix.par,'lambda')  warn('Starting values for sigma2 have to be assigned to the field par.sigma in the structure array defining the mixture'); mcmcout=[];return;end
            mix=skewn_transform(mix);
        end
    end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% START WITH MCMC

t0 = clock;
tlast=t0;
for m=1:(mcmc.burnin+mcmc.M)

    if etime(clock,tlast)>60
        ext=fix(etime(clock,t0)/m*(mcmc.burnin+mcmc.M-m));
        ['estimated completion in about ' num2str(fix(ext/60))   ' minutes  and ' num2str(ext-fix(ext/60)*60) ' seconds']
        tlast = clock;
    end


%% Sample the weight distribution

    if and(mix.K>1,~mix.indicfix)

        if  mix.indicmod.dist(1:6)=='Multin'

            %  complete data posterior for the weights

            dataS.y=data.S;
            if data.empty  dataS.empty=true; else dataS.empty=false; end    % data may be ignored for testing purposes
            post.weight=posterior(dataS,mix.indicmod,prior.weight,0);

 
            eta=dirichsim(post.weight);

            mix.weight = eta;
            

 
        elseif mix.indicmod.dist(1:6)=='Markov'

            %  posterior for the transition matrix

            mix.indicmod.cat=mix.K;
            dataS.y=[data.S0 data.S];
            if data.empty  dataS.empty=true; else dataS.empty=false; end    % data may be ignored for testing purposes

            post.indicmod.xi=posterior(dataS,mix.indicmod,prior.indicmod.xi,0);

            if  all(mix.indicmod.init=='ergodic')
                xinew=dirichsim(post.indicmod.xi);
                pinit=marstat(mix.indicmod.xi)';
                pinitnew=marstat(xinew)';
                acc= pinitnew(data.S0)/pinit(data.S0);
                if (m-mcmc.burnin)==1  accsum=0; end
                if rand<acc
                    xi=xinew;
                    if  m>mcmc.burnin    accsum=accsum+1;  end
                else
                    xi=mix.indicmod.xi;
                end
                if m==(mcmc.burnin+mcmc.M)
                    mcmcout.acc.xi=accsum/mcmc.M;
                end

            elseif all(mix.indicmod.init=='uniform')
                xi=dirichsim(post.indicmod.xi);
            end
            mix.indicmod.xi=xi;
        end
    end

%%   sample the paramters

%% finite mixture of Poisson distributions

    if and(mix.dist(1:6)=='Poisso',~isfield(mix,'d'))  
   
        %  compute parameters of complete data posterior for the parameters

        postpar=posterior(data,mix,prior.par,0);
        mix.par=prodgamsim(postpar); 
        
        if ihier   % hierarchical prior
            prior.par.b=repmat(gamrnd(sum(prior.par.a,2)+prior.par.g,1./(sum(mix.par,2)+prior.par.G)),1,mix.K);
        end
        
%% finite mixtures of exponential distributions (HW)

    elseif and(mix.dist(1:6)=='Expone',~isfield(mix,'d'))  % finite mixture exponential model

        %  compute parameters of complete data posterior for the parameters
        postpar=posterior(data,mix,prior.par,0);
        mix.par=prodgamsim(postpar);
        if ihier
            prior.par.b=repmat(gamrnd(sum(prior.par.a,2)+prior.par.g,1./(sum(mix.par,2)+prior.par.G)),1,mix.K);
        end


%% finite mixture of Binomial distributions

    elseif and(mix.dist(1:6)=='Binomi',~isfield(mix,'d'))  % finite mixture model

        %  compute parameters of complete data posterior for the parameters

        postpar=posterior(data,mix,prior.par,0);
        mix.par=prodbetasim(postpar);
        
%% finite mixture of Multinomial distributions

    elseif and(mix.dist(1:6)=='Multin',~isfield(mix,'d'))  % finite mixture model

        %  compute parameters of complete data posterior for the parameters
        
        if data.empty
            postpar=prior.par.pi;
        else 
            
            postpar=zeros(size(prior.par.pi));
            for k=1:mix.K
                postpar(:,k)=prior.par.pi(:,k)+sum(data.y(iy,data.S==k)==repmat(mix.label,1,sum(data.S==k)),2);
            end     
            
            for j=1:mix.r    
                ij=sum(mix.cat(1:j-1))+[1:mix.cat(j)]';
                mix.par.pi(ij,:)=dirichsim(postpar(ij,:)')'; 
            end
        end
        
        
        %%  finite mixture of GLMs
        
    elseif  and(glm,isfield(mix,'d'))   
        
        %% initialize auxiliary mixture sampling
        
        if m==1
            if  or(all(mix.dist(1:6)=='Poisso'),all(mix.dist(1:6)=='Negbin'))
                if ~isfield(data,'exposures') data.exposures=ones(1,data.N); end
                %% data.exposures=data.Ti;
                [mixaux1,mixaux2]= auxmix_initialize_poisson(data,mix,mcmc);

            elseif  all(mix.dist(1:6)=='Binomi')
                if ~isfield(data,'Ti') data.Ti=ones(1,data.N); end
                mixaux= auxmix_initialize_binomial(data,mix,mcmc);
            end
        end


        % simulate regression coefficients for a mixed-effects mixture of regression  models
        % by constructing a single large multiple regression model with heteroscedastic errors


        if ~isfield(mix,'df') mix.df=0; end


        modreg.dist='Normal';
        modreg.error='hetero';
        modreg.parfix.sigma=true;
        id=(mix.d-mix.df)*mix.K;
        modreg.d=mix.df+id;
        % prior of large parameter vector

        priorreg.Binv=zeros(modreg.d);
        if mix.df~=0
            priorreg.b=[prior.par.alpha.a;reshape(prior.par.beta.b,id,1)];
            priorreg.Binv(1:mix.df,1:mix.df)=prior.par.alpha.Ainv;
        else
            priorreg.b=[reshape(prior.par.beta.b,id,1)];
        end
        for k=1:mix.K;
            ie=mix.df+(k-1)*(mix.d-mix.df)+[1:(mix.d-mix.df)];
            if (mix.d-mix.df)==1
                priorreg.Binv(ie,ie)=prior.par.beta.Binv(:,k);
            else
                priorreg.Binv(ie,ie)=prior.par.beta.Binv(:,:,k);
            end
        end

        if or(m~=1,istartpar)
            % compute log lambda

            if mix.df==0  % no fixed effects
                if mix.K==1
                    loglambda=mix.par.beta'*data.X;
                else
                    ind = repmat(data.S,mix.K,1) == repmat([1:mix.K]',1,data.N);
                    loglambda=sum((mix.par.beta'*data.X).*ind,1);   clear ind;
                end
            else
                intercept=[1:mix.d];
                indexdr=intercept(all(repmat(mix.indexdf,1,mix.d)~=repmat(intercept,mix.df,1),1));
                if mix.K==1
                    loglambda=mix.par.beta'*data.X(indexdr,:) + mix.par.alpha'*data.X(mix.indexdf,:);
                else
                    ind = repmat(data.S,mix.K,1) == repmat([1:mix.K]',1,data.N);
                    loglambda=sum((mix.par.beta'*data.X(indexdr,:) + repmat(mix.par.alpha'*data.X(mix.indexdf,:),mix.K,1)).*ind,1);
                    clear ind;
                end
            end
            if  and(all(mix.dist(1:6)=='Negbin'),m>1)  % simulate random effects only after several interation steps
                if mix.K>1
                    data.bi=gamrnd(mix.par.df(data.S)+data.y,1./(mix.par.df(data.S)+exp(loglambda)));
                    loglambda=loglambda+log(data.bi);
                else
                    %data.bi=gamrnd(mix.par.df+data.y,1./(mix.par.df+exp(loglambda)));
                    data.bi=gamrnd(mix.par.df+data.y,1);
                    loglambda=loglambda+log(data.bi)-log(mix.par.df+exp(loglambda));
                    data.bi=data.bi./(mix.par.df+exp(loglambda));
                end
                
            end
        else
            %%%%%%%%%%%%%%%%%%%%%%%%%%
            % starting values for auxiliary mixture sampling

            if  or(all(mix.dist(1:6)=='Poisso'),all(mix.dist(1:6)=='Negbin'))

                loglambda=log(0.1*ones(1,data.N));
                loglambda(data.y~=0)=log(data.y(data.y~=0));
                loglambda=loglambda-log(data.exposures);
                if all(mix.dist(1:6)=='Negbin')  data.bi=ones(size(data.y));  end

            elseif all(mix.dist(1:6)=='Binomi')
                if all(data.Ti==1)
                    c=min(max(sum(data.y)/sum(data.Ti),0.05),0.95);
                    loglambda=(log(c)-log(1-c))*ones(1,data.N);
                else
                    indexl=and(data.y~=0,data.y~=data.Ti);
                    iTi=data.Ti.*indexl;
                    loglambda(indexl)=log(data.y(indexl))-log(iTi(indexl)-data.y(indexl));
                end
            end
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%

        if  or(all(mix.dist(1:6)=='Poisso'),all(mix.dist(1:6)=='Negbin'))

            [datareg,regvar]=auxmix_poisson(data,loglambda,mixaux1,mixaux2);
            modreg.par.sigma=regvar;

            %%% design matirx of augmented regression model
            iK=reshape(repmat([1:mix.K],mix.d-mix.df,1),id,1);

            if mix.df==0  % no fixed effects
                if mix.K==1
                    datareg.X=[data.X data.X(:,data.y~=0)];
                else
                    ind = repmat(data.S,id,1) == repmat(iK,1,data.N);
                    datareg.X=repmat([data.X data.X(:,data.y~=0)],mix.K,1).*[ind ind(:,data.y~=0)];
                end
            else
                intercept=[1:mix.d];
                indexdr=intercept(all(repmat(mix.indexdf,1,mix.d)~=repmat(intercept,mix.df,1),1));
                if mix.K==1
                    datareg.X=[data.X(mix.indexdf,:) data.X(mix.indexdf,data.y~=0); data.X(indexdr,:) data.X(indexdr,data.y~=0)];
                else
                    ind = repmat(data.S,id,1) == repmat(iK,1,data.N);
                    datareg.X=[data.X(mix.indexdf,:) data.X(mix.indexdf,data.y~=0) ; repmat([data.X(indexdr,:) data.X(indexdr,data.y~=0)],mix.K,1).*[ind ind(:,data.y~=0)]];
                end
            end
            clear ind;


        elseif all(mix.dist(1:6)=='Binomi')
            [datareg,regvar]=auxmix_binomial(data,loglambda,mixaux);
            modreg.par.sigma=regvar;

            %%% design matirx of augmented regression model
            iK=reshape(repmat([1:mix.K],mix.d-mix.df,1),id,1);

            if mix.df==0  % no fixed effects
                if mix.K==1
                    datareg.X=data.X;
                else
                    ind = repmat(data.S,id,1) == repmat(iK,1,data.N);
                    datareg.X=repmat(data.X,mix.K,1).*ind;
                end
            else
                intercept=[1:mix.d];
                indexdr=intercept(all(repmat(mix.indexdf,1,mix.d)~=repmat(intercept,mix.df,1),1));
                if mix.K==1
                    datareg.X=[data.X(mix.indexdf,:) ; data.X(indexdr,:) ];
                else
                    ind = repmat(data.S,id,1) == repmat(iK,1,data.N);
                    datareg.X=[data.X(mix.indexdf,:) ; repmat(data.X(indexdr,:),mix.K,1).*ind];
                end
            end
            clear ind;




        end


        postreg =posterior(datareg,modreg,priorreg);
        clear datareg modreg ;

        alphastar=normultsim(struct('mu',postreg.b,'sigma',postreg.B));
        if mix.df~=0
            mix.par.alpha=alphastar(1:mix.df);
            mix.par.beta=reshape(alphastar(mix.df+1:end,1),(mix.d-mix.df),mix.K);
        else
            mix.par.beta=reshape(alphastar,mix.d,mix.K);
        end
        clear alphastar;

        if  and(all(mix.dist(1:6)=='Negbin'),~dffix)  % update degrees of freedom

            kupdate=[1:mix.K];  %joint update of all parameters
            %kupdate=[1:mix.K]'; %single move update

            prop.mh_da= true(size(kupdate,1),1);    % data augmentation (indicators) and M-H   (single move and multimove)
            prop.ars_da= false(size(kupdate,1),1);  % data augmentation (indicators) and ARS   (single and multi move)
            prop.mh_daomega=false(size(kupdate,1),1); % data augmentation (indicators and scaling factors) and M-H   (only single move) - MODIFY

            if prop.ars_da
                %% mcmc.hull.start=repmat([5 10],mix.K,1);  % starting value for first two points of the hull
                alpha=0.5;
                mcmc.hull.start=repmat(1./[20 10].^alpha,mix.K,1);  % starting value for first two points of the hull

            end
            % [mix,mcmc,sumacc]=mcmc_negbin_df(data,mix,prior,mcmc,kupdate,
            % prop);  % not log-concave in repetitonparameter .df; try
            % transformation as student t
            [mix,mcmc,sumacc]=mcmc_negbin_df(data,mix,prior,mcmc,kupdate,prop);

            if (m-mcmc.burnin)==1 mcmcout.mh.acc.df=zeros(1,mix.K);
                if any(any([prop.mh_da prop.mh_daomega])) mcmcout.mh.tune.df= mcmc.mh.tune.df; end;
            end
            if m>mcmc.burnin  mcmcout.mh.acc.df=mcmcout.mh.acc.df+sumacc/mcmc.M; end
             
        end


 
%% finite mixtures of   normal and    t distribitions
    elseif  norstud   

        if ~skew

%% finite mixtures of normals with conjugate prior

            if and(prior.type(1:4)=='conc',~isfield(mix,'d'))  % conditional conjugate prior

                post.par = posterior(data,mix,prior.par);
                mix.par=mc(mix,post.par,1);  %simulate from the complete-data posterior

                if istorepost % store conditional moment for computing the bayes factor
                    if data.r> 1
                        post.par.mu.B=mix.par.sigma./permute(repmat(post.par.mu.N0,[data.r 1 data.r]),[1 3 2]);
                    else
                        post.par.mu.B=mix.par.sigma./post.par.mu.N0;
                    end
                end

%% finite mixtures of student-t and normal distributions with independce prior
            elseif ~isfield(mix,'d')   % indepenedence prior, standard finite mixture model

                % simulate Q
                if ~sigmafix
                    mix1=mix;
                    mix1.parfix.mu=true;
                    post.par.sigma =posterior(data,mix1,prior.par.sigma);
                    if data.r==1
                        mix.par.sigma=1./prodgamsim(struct('a',post.par.sigma.c,'b',post.par.sigma.C));
                        if all(prior.type(1:4)=='sigm')
                            ind=(mix.par.sigma.^.5>prior.par.sigma.max);
                            icount=0;
                            while and(sum(ind)>0,icount<1000)
                                mix.par.sigma(ind)=1./prodgamsim(struct('a',post.par.sigma.c(ind),'b',post.par.sigma.C(ind)));
                                ind=(mix.par.sigma.^.5>prior.par.sigma.max);
                                icount=icount+1;
                            end
                        end
                    else
                        post.par.sigma.logdetC=zeros(1,mix.K);
                        for k=1:mix.K
                            [mix.par.sigma(:,:,k) mix.par.sigmainv(:,:,k) mix.par.logdet(1,k) post.par.sigma.logdetC(1,k)] = invwisim(post.par.sigma.c(k),squeeze(post.par.sigma.C(:,:,k)));
                        end
                    end
                end


                % simulate mu
                if ~mufix
                    mix1=mix;
                    mix1.parfix.sigma=true;
                    post.par.mu =posterior(data,mix1,prior.par.mu);
                    mix.par.mu=prodnormultsim(struct('mu',post.par.mu.b,'sigma',post.par.mu.B));
                else
                   % mix.par.mu % has to be fixed in advance
                end

                    if or(all(mix.dist(1:6)=='Stumul'),all(mix.dist(1:6)=='Studen'))
                        if mix.K==1  data.S=ones(1,data.N); end
                        if data.r==1
                            scal = mix.par.df(data.S) + (data.y-mix.par.mu(data.S)).^2./mix.par.sigma(data.S);
                        else
                            scal = mix.par.df(data.S);
                            for k=1:mix.K
                                err=(data.y(:,data.S==k)-mix.par.mu(:,k*ones(1,sum(data.S==k))));
                                b=mix.par.sigmainv(:,:,k)*err;
                                scal(data.S==k)= scal(data.S==k) + sum(b.*err,1);
                            end
                        end
                      %  mix.omega = gamrnd(0.5*(mix.par.df(data.S)+data.r),2./scal);
                    end

%% finite mixtures of  multiple regression models
            elseif isfield(mix,'d')   % independence prior,  finite mixture regression model

                if ~isfield(mix,'df') mix.df=0; end

                if ~isfield(mix,'parfix')
                    mix.parfix.sigma=false; mix.parfix.beta=false;
                else
                    if ~isfield(mix.parfix,'beta')  mix.parfix.beta=false; end
                    if ~isfield(mix.parfix,'sigma')  mix.parfix.sigma=false; end
                end

                if ~mix.parfix.beta


                    if mix.df==0
                        % simulate regression coefficients for a mixture of regression  models
                        modreg=mix; modreg.parfix.sigma=true;
                        post.par.beta=posterior(data,modreg,prior.par.beta);
                        if size(post.par.beta.B,1)>1
                            mix.par.beta=prodnormultsim(struct('mu',post.par.beta.b,'sigma',post.par.beta.B));
                        else
                            mix.par.beta=prodnormultsim(struct('mu',post.par.beta.b,'sigma',squeeze(post.par.beta.B)'));
                        end

                    else   %  mixed-effects mixture of regression  models

                        % simulate regression coefficients for a mixed-effects mixture of regression  models
                        % by constructing a single large multiple regression model with heteroscedastic errors
                        clear modreg;
                        modreg.dist='Normal'; modreg.error='hetero'; modreg.parfix.sigma=true;
                        id=(mix.d-mix.df)*mix.K;
                        modreg.d=mix.df+id;

                        if m==1   % prior of large parameter vector
                            priorreg.b=[prior.par.alpha.a;reshape(prior.par.beta.b,id,1)];
                            priorreg.Binv=zeros(modreg.d);
                            priorreg.Binv(1:mix.df,1:mix.df)=prior.par.alpha.Ainv;
                            for k=1:mix.K;
                                ie=mix.df+(k-1)*(mix.d-mix.df)+[1:(mix.d-mix.df)];
                                priorreg.Binv(ie,ie)=prior.par.beta.Binv(:,:,k);
                            end
                        end

                        if data.empty  datareg.empty=data.empty;   end
                        datareg.y=data.y;

                        intercept=[1:mix.d];
                        indexdr=intercept(all(repmat(mix.indexdf,1,mix.d)~=repmat(intercept,mix.df,1),1));

                        iK=reshape(repmat([1:mix.K],mix.d-mix.df,1),id,1);
                        if mix.K==1
                            datareg.X=[data.X(mix.indexdf,:); data.X(indexdr,:)];
                            modreg.par.sigma=repmat(mix.par.sigma,1,data.N);
                        else
                            ind = repmat(data.S,id,1) == repmat(iK,1,data.N);
                            datareg.X=[data.X(mix.indexdf,:); repmat(data.X(indexdr,:),mix.K,1).*ind];
                            modreg.par.sigma=mix.par.sigma(1,data.S);
                        end

                        postreg =posterior(datareg,modreg,priorreg);
                        alphastar=normultsim(struct('mu',postreg.b,'sigma',postreg.B));
                        mix.par.alpha=alphastar(1:mix.df);
                        mix.par.beta=reshape(alphastar(mix.df+1:end,1),(mix.d-mix.df),mix.K);

                    end
                end

                % simulate error variances
                if ~mix.parfix.sigma
                    modreg=mix;
                    modreg.parfix.beta=true;
                    if mix.df>0 modreg.parfix.alpha=true; end
                    post.par.sigma =posterior(data,modreg,prior.par.sigma);
                    mix.par.sigma=1./prodgamsim(struct('a',post.par.sigma.c,'b',post.par.sigma.C));
                end
            end
        end

%% update degrees of freedom for t distributions

        if  and(studmix,~dffix)   

            kupdate=[1:mix.K]'; %single move update

            % define proposals:
            prop.mh_marg= true(size(kupdate,1),1);  %  MH   without data augmentation  (only single move)
            prop.mh_da= false(size(kupdate,1),1);    % data augmentation (indicators) and M-H   (single move and multimove)
            prop.ars_da= false(size(kupdate,1),1);  % data augmentation (indicators) and ARS   (single and multi move)
            prop.mh_daomega=false(size(kupdate,1),1); % data augmentation (indicators and scaling factors) and M-H   (only single move)

            if mix.indicfix  prop.mh_da= true(size(kupdate,1),1); prop.mh_marg= false(size(kupdate,1),1);   end 
            
            if prop.mh_da
                kupdate=[1:mix.K];  %joint update of all parameters
                if all(prior.par.df.type(1:4)=='hier') 
                    'Use independence hierarchcial prior'
                    stop; 
                end

            elseif prop.ars_da
                kupdate=[1:mix.K];  %joint update of all parameters
                 if all(prior.par.df.type(1:4)=='hier') 
                    'Use independence hierarchcial prior'
                    stop; 
                end
                if m==1
                    alpha=0.5;
                    mcmc.hull.start=repmat(1./[20 10].^alpha,mix.K,1);  % starting value for first two points of the hull
                end
            end

            if and(mix.r>1,skew)
                [mix,mcmc,sumacc]=mcmc_student_df(data,mix,prior,mcmc,kupdate,prop); % uses the complete data likelihood to speed up computations

            else
                [mix,mcmc,sumacc]=mcmc_negbin_df(data,mix,prior,mcmc,kupdate,prop);  % uses function dataclass
            end
            if (m-mcmc.burnin)==1 mcmcout.mh.acc.df=zeros(1,mix.K);
                if any(any([prop.mh_marg prop.mh_da prop.mh_daomega])) mcmcout.mh.tune.df= mcmc.mh.tune.df; end;
            end
            if m>mcmc.burnin  mcmcout.mh.acc.df=mcmcout.mh.acc.df+sumacc/mcmc.M; end
        end


        if ihier   % hierarchical prior

            % unrestricted Covariances

            if ~data.empty  gN=sum(prior.par.sigma.c,2)+prior.par.sigma.g; else gN=prior.par.sigma.g;  end

            if size(data.y,1)==1
                if ~data.empty
                    if or(all(mix.dist(1:6)=='SkewNo'),all(mix.dist(1:6)=='SkewSt'))
                        GN=prior.par.sigma.G + sum(1./mix.par.sigmaeps,2);
                    else
                        GN=prior.par.sigma.G + sum(1./mix.par.sigma,2);
                    end
                else
                    GN=prior.par.sigma.G;
                end
                prior.par.sigma.C=gamrnd(gN,1)/GN*ones(1,mix.K);
            else
                if ~data.empty
                    if or(all(mix.dist(1:6)=='SkNomu'),all(mix.dist(1:6)=='SkStmu'))
                        GN=prior.par.sigma.G + squeeze(sum(mix.par.sigmaepsinv,3));
                    else
                        GN=prior.par.sigma.G + squeeze(sum(mix.par.sigmainv,3));
                    end
                else
                    GN=prior.par.sigma.G;
                end
                %[dum1 prQS detprQS] = raninvwi_neu(gN,GN);
                [dum1 prQS detprQS] = invwisim(gN,GN);
                prior.par.sigma.C=reshape(repmat(prQS,1,mix.K),size(data.y,1),size(data.y,1),mix.K);
                prior.par.sigma.logdetC= repmat(detprQS,1,mix.K);
            end

        end

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    else
        ['Mixture type ' mix.dist '  not supported by function mixturemcmc']
    end
    
%% simulate allocations and compute mixture likleihood

    if ~mix.indicfix
        % 
        classify_all=true; %        classify_all=false;

        if any([classify_all m<=mcmc.burnin m>(mcmc.M+mcmc.burnin-istoreS)])
            [class, S] = dataclass(data,mix,0);
            mixlik=class.mixlik;
        else  %classify only part of the data after burn-in till storing S
            %alpha=0.05;
            alpha=betarnd(5,95);
            %alpha=betarnd(1,2);
            index=[1:data.N];
            ipart=(rand(1,data.N)<=alpha);
            indexpart=index(ipart);
            if prod(size(indexpart))>0
                datapart.empty=false;  datapart.N=sum(ipart); datapart.y=data.y(:,indexpart);
                if isfield(data,'X')  datapart.X=data.X(:,indexpart); end
                if isfield(data,'Ti')  datapart.Ti=data.Ti(:,indexpart); end
                [class, Spart] = dataclass(datapart,mix,0);
                S=data.S;S(indexpart)=Spart;
            end
            mixlik=0;
        end

    else
        S=data.S;
        mixlik=likelihoodeval(data,mix);
    end
    mixprior=prioreval(mix,prior);
    
%% permute

    if iperm  % draw a random permuation if iperm true
        ranpermute;
    else
        rho=[1:mix.K]';
    end
    
    if mix.K>1
        [dum,srho]=sort(rho);
        if  mix.indicmod.dist(1:6)=='Multin'
            data.S=srho(S)';
        elseif mix.indicmod.dist(1:6)=='Markov'
            data.S=srho(S(2:end))';  data.S0=srho(S(1));
        end
    end



%% store results
    
    if m>mcmc.burnin
        
        mcmcput;
        
        % mixture prior log p (thmod), marginalized with respect to the hyperparameter

        mcmcout.log.mixprior(m-mcmc.burnin)=mixprior;

        if data.empty  %data may be ignored for testing purposes
            mcmcout.log.mixlik(m-mcmc.burnin)=0;
        else
            mcmcout.log.mixlik(m-mcmc.burnin)=mixlik; % mixture likelihood log p (y|thmod)
        end

        if and(mix.K>1,~mix.indicfix)
            % further results for mixture model with unknwon indicator
            mixpost=mcmcout.log.mixlik(m-mcmc.burnin)+mcmcout.log.mixprior(m-mcmc.burnin);
            mcmcout.log.cdpost(m-mcmc.burnin)=mixpost+class.postS; % complete data posterior prior log p (thmod m,S|y)), prior marginalized with respect
            % to the hyperparameter;  based on p(S,\thmod|\ym)=p(S|\thmod,\ym)* p(\thmod|\ym)\propto p(S|\thmod,\ym)* p(\ym|\thmod)*p(\thmod)
            if (m-mcmc.burnin)==1  maxcdpost=mcmcout.log.cdpost(1)-1;   end
            if mcmcout.log.cdpost(m-mcmc.burnin)> maxcdpost
                mcmcout.clust.Smap=data.S;
            end
            mcmcout.entropy(m-mcmc.burnin)=class.entropy;
            mcmcout.ST(m-mcmc.burnin)=data.S(end);

            if  m>(mcmc.M+mcmc.burnin-istoreS)  mcmcout.S(m-(mcmc.M+mcmc.burnin-istoreS),:)=data.S;     end
        end
    end
end


if mix.K>1 mcmcout.ranperm=iperm;end


