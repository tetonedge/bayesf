function [lh,  maxl, llh] = likeli_normult(y,mu,Qinv,detQinv)

% multivaraite mixtures of normals
% sample S from the full conditional 

% S .. (1 times N) vector of group-number

K = size(Qinv,3);
r = size(y,1);
d = size(Qinv,1);
N = size(y,2);

m = zeros(r,K);
llh = zeros(K,N); 

llh1 = -.5*r*(log(2*pi)); 


for k=1:K,
  eps = y - mu(:,k*ones(1,N)); 
  llh(k,:) = llh1 +.5*detQinv(k)- .5*sum(eps'*squeeze(Qinv(:,:,k)).*eps',2)';   
end

maxl = max(llh,[],1);
lh = exp(llh - maxl(ones(K,1),:));
  
 