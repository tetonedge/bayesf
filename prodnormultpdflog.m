function fd = prodnormultpdflog(par,theta)
% computes the logarithm of the product of K multivariate normal posterior density of dimension r

% input
% par ... parameter of the aposteriori density, is a structure array  with the follwing fields:

% runs most efficiently with the fields mu (mean), sigmainv (inverse of the covariance), logdet (determinante of the inverse of the covariance)
%      instead of sigmainv, sigma may be given; the inverse, however, will be computed for each density, slowing down the evalution
%      logdet may be missing, however, the determinante of the inverse of the covariance will be computed for each density, slowing down the evalution

% the function could be applied to the following situations:
% 1. a single density is evaluated at a single argument (M=1)
% 2. a single density may by evaluated at a sequence of M argument
% 3. a sequence of M density may by evaluated at a single argument
% 4. a sequence of M density may by evaluated at a corresponding sequence of M arguments

% Depending on this situation, the fileds are arrays with following dimensions
% for a single density, for K>1:
%                       par.mu is a (r times K) array
%                       par.sigmainv (or par.sigma) is a (r times r times K) array
%                       par.logdet is a (K times 1) array
% for a single density, for K=1: 
%                       par.mu is a (r times 1) array
%                       par.sigmainv (or par.sigma) is a (r times r) array
%                       par.logdet is a scalar

% for a sequence of M densities, the first index is the running index for the sequence:
%                    for K>1:
%                       par.mu is a (M times r times K) array
%                       par.sigmainv (or par.sigma) is a (M times r times r times K) array
%                       par.logdet is a (M times K) array

% for a sequence of M density, for K=1: 
%                       par.mu is a (M times r) array
%                       par.sigmainv (or par.sigma) is a (M times r times r) array
%                       par.logdet is a (M times 1) array

% theta ... argument of the  aposteriori density;
%      for a single argument:  array of size (r times K) array
%      for a multiple argument:  array of size (M times r times K) array


%output: log of the posterior density:  array of size M times 1

% Author: Sylvia Fruehwirth-Schnatter
 
% uses (x-mu)'*sigma^(-1)*(x-mu)=trace(sigma^(-1)*(x-mu)*(x-mu)')  
% and   trace(A*B)=sum(sum(A.*B),1),2)  



if ~isfield(par,'mu') warn(['Field mu missing in function prodnormultpdflog']);fd=[];return; end  
if and(~isfield(par,'sigmainv'),~isfield(par,'sigma')) warn(['Field sigmainv or sigma must be specified  in function prodnormultpdflog']);fd=[];return; end  

% determine the dimension

if ~isfield(par,'sigmainv')
    sS=size(par.sigma);
else    
    sS=size(par.sigmainv);
end 
sM=size(par.mu);

if size(sS,2)==4 % sequence of products of multivariate normal densities
    if any([sS(2)~=sS(3)  sM(1)~=sS(1) sM(2)~=sS(2)  sM(3)~=sS(4)])  warn(['Size disagreement in the fields of par in function prodnormultpdflog']); fd=[]; return;end
    M=sS(1); r=sS(2);  K=sS(4); 
    if ~isfield(par,'sigmainv') par.sigmainv=par.sigma;  cominv=true; else cominv=false;  end
    if  ~isfield(par,'logdet')  par.logdet=zeros(M,K); comdet=true; else comdet=false; end
    if or(cominv,comdet)     
        for m=1:M; 
            for k=1:K; 
                if cominv  par.sigmainv(m,:,:,k) =inv(squeeze(par.sigma(m,:,:,k)));  end
                if comdet par.logdet(m,k) = log(det(squeeze(par.sigmainv(m,:,:,k))));  end
            end 
        end
    end
elseif size(sS,2)==3
    
    
    
    if sS(1)==sS(2)  % single density with products of multivariate normal densities
        if any([sM(1)~=sS(1)])  warn(['Size disagreement in the fields of par in function prodnormultpdflog']); fd=[]; return;end
        r=sS(1);  K=sS(3);   M=1;
        if ~isfield(par,'sigmainv') par.sigmainv=par.sigma;  cominv=true; else cominv=false;  end
        if  ~isfield(par,'logdet')  par.logdet=zeros(K,1); comdet=true; else comdet=false; end
        if or(cominv,comdet)     
            for k=1:K; 
                if cominv  par.sigmainv(:,:,k) =inv(squeeze(par.sigma(:,:,k)));  end
                if comdet par.logdet(k) = log(det(squeeze(par.sigmainv(:,:,k))));  end
            end 
        end
        
    elseif sS(2)==sS(3) % sequence of multivariate normal densities
        if any([sM(1)~=sS(1) sM(2)~=sS(2)])  warn(['Size disagreement in the fields of par in function prodnormultpdflog']); fd=[]; return;end
        M=sS(1); r=sS(2);  K=1; 
        if ~isfield(par,'sigmainv') par.sigmainv=par.sigma;  cominv=true; else cominv=false;  end
        if  ~isfield(par,'logdet')  par.logdet=zeros(M,1); comdet=true; else comdet=false; end
        if or(cominv,comdet)     
            for m=1:M; 
                if cominv  par.sigmainv(m,:,:) =inv(squeeze(par.sigma(m,:,:)));  end
                if comdet par.logdet(m) = log(det(squeeze(par.sigmainv(m,:,:))));  end
            end
        end
        
        
    else
        warn(['Size disagreement in the fields of par in function prodnormultpdflog']); fd=[]; return;
    end
    
elseif and(size(sS,2)==2,sS(1)==sS(2)) % single  multivariate normal density
    if any([sM(1)~=sS(1)])  warn(['Size disagreement in the fields of par in function prodnormultpdflog']); fd=[]; return;end
    r=sS(1); K=1;  M=1;
    
    if ~isfield(par,'sigmainv')   par.sigmainv=inv(par.sigma);    end
    if  ~isfield(par,'logdet')  par.logdet= log(det(par.sigmainv));  end
    
elseif all(size(sM)==size(sS)) % univariate normal density 
    r=1; K=sS(2); sT=size(theta); 
    if sS(2)~=sT(2) warn(['Size disagreement in the fields of par in function prodnormultpdflog']); fd=[]; return; end
    M=max(sS(1),sT(1));
else    
    warn(['Size disagreement in the fields of par in function prodnormultpdflog']); fd=[]; return;
end    

if r>1
    sT=size(theta);
    if sT(end)==1 sT=sT(1:end-1);end 
    if sM(end)==1 sM=sM(1:end-1);end
    if  size(sT,2)==size(sM,2)
        if  any(sT~=sM)  warn(['Size disagreement in the fields of par and theta in function prodnormultpdflog']); fd=[]; return; end
    elseif  size(sT,2)>size(sM,2)
        if  any(sT(2:end)~=sM)  warn(['Size disagreement in the fields of par and theta in function prodnormultpdflog']); fd=[]; return; end
        M=sT(1);
    elseif  size(sT,2)<size(sM,2)
        if  any(sT~=sM(2:end))  warn(['Size disagreement in the fields of par and theta in function prodnormultpdflog']); fd=[]; return; end
    end
    
    if and(M==1,K==1)  % single multivariate Normal distribution
        err=theta-par.mu;
        fd=-0.5*(r*log(2*pi) - par.logdet + sum(sum(par.sigmainv.*(err*err'))));    
        
    elseif and(M==1,K>1)    %single density with products of multivariate normal densities
        
        err=repmat(theta-par.mu,[1 1 r]);
        errmat=permute(err,[1 3 2]).*permute(err,[3 1 2]);
        fd=-0.5*sum(r*log(2*pi) - par.logdet + squeeze(sum(sum(par.sigmainv.*errmat,1),2)),1);    
        
    elseif K==1 
        
        if  size(sT,2)==size(sM,2)
            err=theta-par.mu;
        elseif  size(sT,2)>size(sM,2)
            err=theta-repmat(par.mu',M,1);
            par.sigmainv=permute(repmat(par.sigmainv,[1 1 M]),[3 1 2]);
        elseif  size(sT,2)<size(sM,2)
            err=repmat(theta',M,1)-par.mu;
        end
        
        err=repmat(err,[1 1 r]);
        errmat=permute(err,[1 2 3]).*permute(err,[1 3 2]);
        
        fd=-0.5*(r*log(2*pi) - par.logdet + squeeze(sum(sum(par.sigmainv.*errmat,2),3)));    
        
    else
        
        if  size(sT,2)==size(sM,2)
            err=theta-par.mu;
        elseif  size(sT,2)>size(sM,2)
            err=theta-permute(repmat(par.mu,[ 1 1 M]),[3 1 2]);
            par.sigmainv=permute(repmat(par.sigmainv,[1 1 1 M]),[4 1 2 3]);
            par.logdet=repmat(par.logdet',M,1);
        elseif  size(sT,2)<size(sM,2)
            err=permute(repmat(theta,[ 1 1 M]),[3 1 2])-par.mu;
        end
        err=repmat(err,[1 1 1 r]);
        errmat=permute(err,[1  2 4 3]).*permute(err,[1 4 2 3]);
        fd=-0.5*sum(r*log(2*pi) - par.logdet + squeeze(sum(sum(par.sigmainv.*errmat,2),3)),2);    
    end
    
else  % univariate densities
    if  ~isfield(par,'logdet')  par.logdet= log(par.sigmainv);  end
    sT=size(theta); 
    if sS(1)>sT(1)
        err=repmat(theta,M,1)-par.mu;
    elseif sS(1)<sT(1)    
         err=theta-repmat(par.mu,M,1);
        par.sigmainv=repmat(par.sigmainv,M,1);
        par.logdet=repmat(par.logdet,M,1);
    else
        err=theta-par.mu;
    end
    fd=-0.5*sum(log(2*pi) - par.logdet +  par.sigmainv.*err.^2,2);    
    
end

