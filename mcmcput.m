
if and(univ,~isfield(mix,'d'))
    if (m-mcmc.burnin)==1
        mcmcout.par=zeros(mcmc.M,mix.K);
        if istorepost
            mcmcout.post.par=struct('a',zeros(mcmc.M,mix.K),'b',zeros(mcmc.M,mix.K));
        end
    end

    mcmcout.par(m-mcmc.burnin,:)=mix.par';
    if istorepost
        mcmcout.post.par.a(m-mcmc.burnin,:)=postpar.a; mcmcout.post.par.b(m-mcmc.burnin,:)=postpar.b;
    end


    if ihier   % hierarchical prior
        if (m-mcmc.burnin)==1  mcmcout.hyper=zeros(mcmc.M,1);end
        mcmcout.hyper(m-mcmc.burnin,1)=prior.par.b(1);
    end
    
elseif and(all(mix.dist(1:6)=='Multin'),~isfield(mix,'d'))
    if (m-mcmc.burnin)==1
        mcmcout.par.pi=zeros(mcmc.M,sum(mix.cat),mix.K);
        if istorepost
            mcmcout.post.par=zeros(mcmc.M,sum(mix.cat),mix.K);
        end
    end

    mcmcout.par.pi(m-mcmc.burnin,:,:)=mix.par.pi;
    if istorepost
        mcmcout.post.par(m-mcmc.burnin,:,:)=postpar;
    end
    

elseif or(norstud,skew)

    if (m-mcmc.burnin)==1
        if size(data.y,1)==1
            if  isfield(mix,'d') % finite mixture regression model
                if mix.df==0
                    mcmcout.par=struct('beta',zeros(mcmc.M,mix.d,mix.K),'sigma',zeros(mcmc.M,mix.K));
                else
                    mcmcout.par=struct('alpha',zeros(mcmc.M,mix.df),'beta',zeros(mcmc.M,mix.d-mix.df,mix.K),'sigma',zeros(mcmc.M,mix.K));
                end
            else  % standard mixture model
                if or(all(mix.dist(1:6)=='SkewNo'),all(mix.dist(1:6)=='SkewSt'))
                    mcmcout.par=struct('mu',zeros(mcmc.M,mix.K),'sigma',zeros(mcmc.M,mix.K),'lambda',zeros(mcmc.M,mix.K));dr=1;
                else
                    mcmcout.par=struct('mu',zeros(mcmc.M,mix.K),'sigma',zeros(mcmc.M,mix.K));dr=1;
                end
                if or(all(mix.dist(1:6)=='SkewNo'),all(mix.dist(1:6)=='SkewSt'))
                    % store cluster means
                    mcmcout.par.mean=zeros(mcmc.M,mix.K);
                end
            end
        else  % multivariate mixture
            dr=size(data.y,1)*(size(data.y,1)+1)/2;
            if  istoreinv
                if or(all(mix.dist(1:6)=='SkNomu'),all(mix.dist(1:6)=='SkStmu'))
                    mcmcout.par=struct('mu',zeros(mcmc.M,size(data.y,1),mix.K),'sigma',zeros(mcmc.M,dr,mix.K),'sigmainv',{zeros(mcmc.M,dr,mix.K)},'logdet',{zeros(mcmc.M,mix.K)},'lambda',zeros(mcmc.M,size(data.y,1),mix.K));
                else
                    mcmcout.par=struct('mu',zeros(mcmc.M,size(data.y,1),mix.K),'sigma',zeros(mcmc.M,dr,mix.K),'sigmainv',{zeros(mcmc.M,dr,mix.K)},'logdet',{zeros(mcmc.M,mix.K)});
                end
            else
                if or(all(mix.dist(1:6)=='SkNomu'),all(mix.dist(1:6)=='SkStmu'))
                    mcmcout.par=struct('mu',zeros(mcmc.M,size(data.y,1),mix.K),'sigma',zeros(mcmc.M,dr,mix.K),'logdet',{zeros(mcmc.M,mix.K)},'lambda',zeros(mcmc.M,size(data.y,1),mix.K));
                else
                    mcmcout.par=struct('mu',zeros(mcmc.M,size(data.y,1),mix.K),'sigma',zeros(mcmc.M,dr,mix.K),'logdet',{zeros(mcmc.M,mix.K)});
                end
            end
            if or(all(mix.dist(1:6)=='SkNomu'),all(mix.dist(1:6)=='SkStmu'))
                % store cluster means
                mcmcout.par.mean=zeros(mcmc.M,size(data.y,1),mix.K);
            end
        end
        %  if or(all(mix.dist(1:6)=='Stumul'),all(mix.dist(1:6)=='Studen')) mcmcout.par.df=zeros(mcmc.M,mix.K); end
        if studmix  mcmcout.par.df=zeros(mcmc.M,mix.K); end

        if istorepost
            mcmcout.Nk=zeros(mcmc.M,mix.K);
            if size(data.y,1)==1
                if  isfield(mix,'d') % finite mixture regression model
                    if mix.df==0
                        mcmcout.post.par=struct('beta',struct('b',zeros(mcmc.M,mix.d,mix.K),'B',zeros(mcmc.M,mix.d,mix.d,mix.K)),'sigma',struct('C',zeros(mcmc.M,mix.K),'c',zeros(mcmc.M,mix.K)));
                    else
                        mcmcout.post.par=struct('alpha',struct('a',zeros(mcmc.M,mix.df),'A',zeros(mcmc.M,mix.df,mix.df)),'beta',struct('b',zeros(mcmc.M,mix.d-mix.df,mix.K),'B',zeros(mcmc.M,mix.d-mix.df,mix.d-mix.df,mix.K)),'sigma',struct('C',zeros(mcmc.M,mix.K),'c',zeros(mcmc.M,mix.K)));
                    end
                else  % standard mixture model
                    if or(all(mix.dist(1:6)=='SkewNo'),all(mix.dist(1:6)=='SkewSt'))
                        mcmcout.post.par=struct('beta',struct('b',zeros(mcmc.M,2,mix.K),'B',zeros(mcmc.M,2,2,mix.K)),'sigma',struct('C',zeros(mcmc.M,mix.K),'c',zeros(mcmc.M,mix.K)));
                    else
                        mcmcout.post.par=struct('mu',struct('b',zeros(mcmc.M,mix.K),'B',zeros(mcmc.M,mix.K)),'sigma',struct('C',zeros(mcmc.M,mix.K),'c',zeros(mcmc.M,mix.K)));
                    end
                end
            else
                if or(all(mix.dist(1:6)=='SkNomu'),all(mix.dist(1:6)=='SkStmu'))
                    mcmcout.post.par=struct('beta',struct('b',zeros(mcmc.M,2*size(data.y,1),mix.K),'B',zeros(mcmc.M,2*size(data.y,1),2*size(data.y,1),mix.K)),'sigma',struct('C',zeros(mcmc.M,dr,mix.K),'c',zeros(mcmc.M,mix.K),'logdetC',zeros(mcmc.M,mix.K)));
                else
                    mcmcout.post.par=struct('mu',struct('b',zeros(mcmc.M,size(data.y,1),mix.K),'B',zeros(mcmc.M,size(data.y,1),size(data.y,1),mix.K)),'sigma',struct('C',zeros(mcmc.M,dr,mix.K),'c',zeros(mcmc.M,mix.K),'logdetC',zeros(mcmc.M,mix.K)));
                end
            end
            % if prior.type(1:4)=='conc' mcmcout.post.par.N0=zeros(mcmc.M,mix.K); end
        end
    end

    if size(data.y,1)==1
        if isfield(mix,'d') % finite mixture regression model
            mcmcout.par.beta(m-mcmc.burnin,:,:)=mix.par.beta;
            if mix.df>0  mcmcout.par.alpha(m-mcmc.burnin,:)=mix.par.alpha';end
        else    % standard mixture model
            mcmcout.par.mu(m-mcmc.burnin,:)=mix.par.mu;
        end
        mcmcout.par.sigma(m-mcmc.burnin,:)=mix.par.sigma;
    else
        mcmcout.par.mu(m-mcmc.burnin,:,:)=mix.par.mu;
        mcmcout.par.sigma(m-mcmc.burnin,:,:)=qincolmult(mix.par.sigma);
        if  istoreinv  mcmcout.par.sigmainv(m-mcmc.burnin,:,:)= qincolmult(mix.par.sigmainv); end
        mcmcout.par.logdet(m-mcmc.burnin,:)=mix.par.logdet;
    end

    if  studmix  mcmcout.par.df(m-mcmc.burnin,:)=mix.par.df;   end

    if or(all(mix.dist(1:6)=='SkewNo'),all(mix.dist(1:6)=='SkewSt')) 
        mcmcout.par.lambda(m-mcmc.burnin,:)=mix.par.lambda; 
        % mix=skewn_transform(mix); % check mix.par.psi
        if all(mix.dist(1:6)=='SkewSt')
            scale=sqrt(2/pi)*sqrt(mix.par.df/2).*exp(gammaln((mix.par.df-1)/2)-gammaln(mix.par.df/2));
            mcmcout.par.mean(m-mcmc.burnin,:)=mix.par.mu+ scale.*mix.par.psi;
        else
            mcmcout.par.mean(m-mcmc.burnin,:)=mix.par.mu+sqrt(2/pi)*mix.par.psi;
        end

    end
    if or(all(mix.dist(1:6)=='SkNomu'),all(mix.dist(1:6)=='SkStmu'))  
        mcmcout.par.lambda(m-mcmc.burnin,:,:)=mix.par.lambda;
        if ~isfield(mix.par,'psi')    mix=skewn_transform(mix);     end
        if all(mix.dist(1:6)=='SkStmu')
            scale=sqrt(2/pi)*sqrt(mix.par.df/2).*exp(gammaln((mix.par.df-1)/2)-gammaln(mix.par.df/2));
            mcmcout.par.mean(m-mcmc.burnin,:,:)=mix.par.mu+repmat(scale,mix.r,1).*mix.par.psi;
        else
            mcmcout.par.mean(m-mcmc.burnin,:,:)=mix.par.mu+sqrt(2/pi)*mix.par.psi;
        end

    end

    if istorepost

        if size(data.y,1)==1
            if isfield(mix,'d') % finite mixture regression model
                if mix.df==0
                    mcmcout.post.par.beta.b(m-mcmc.burnin,:,:)=post.par.beta.b;
                    mcmcout.post.par.beta.B(m-mcmc.burnin,:,:,:)=post.par.beta.B;
                else
                    mcmcout.post.par.beta.b(m-mcmc.burnin,:,:)=reshape(postreg.b(mix.df+1:end,1),(mix.d-mix.df),mix.K);
                    for k=1:mix.K;
                        ie=mix.df+(k-1)*(mix.d-mix.df)+[1:(mix.d-mix.df)];
                        mcmcout.post.par.beta.B(m-mcmc.burnin,:,:,k)=postreg.B(ie,ie);
                    end
                    mcmcout.post.par.alpha.a(m-mcmc.burnin,:)=postreg.b(1:mix.df,1)';
                    mcmcout.post.par.alpha.A(m-mcmc.burnin,:,:)=postreg.B(1:mix.df,1:mix.df);
                end

            else    % standard mixture model

                if skew  % SKEWNORMAL
                    mcmcout.post.par.beta.b(m-mcmc.burnin,:,:)=post.par.beta.b;
                    mcmcout.post.par.beta.B(m-mcmc.burnin,:,:,:)=post.par.beta.B;
                else
                    if isfield(post.par,'mu')
                        mcmcout.post.par.mu.b(m-mcmc.burnin,:)=post.par.mu.b;
                        mcmcout.post.par.mu.B(m-mcmc.burnin,:)=post.par.mu.B;
                    end
                end

            end
            if isfield(mcmcout.post.par,'sigma')
                if isfield(post.par,'sigma')
                    mcmcout.post.par.sigma.c(m-mcmc.burnin,:)=post.par.sigma.c;
                    mcmcout.post.par.sigma.C(m-mcmc.burnin,:)=post.par.sigma.C;
                end
            end
        else     %multivariate mixtures
            if skew % SKEWNORMAL
                mcmcout.post.par.beta.b(m-mcmc.burnin,:,:)=post.par.beta.b;
                mcmcout.post.par.beta.B(m-mcmc.burnin,:,:,:)=post.par.beta.B;
            else

                if isfield(post.par,'mu')
                    mcmcout.post.par.mu.b(m-mcmc.burnin,:,:)=post.par.mu.b;
                    mcmcout.post.par.mu.B(m-mcmc.burnin,:,:,:)=post.par.mu.B;
                end
            end

            if isfield(post.par,'sigma')
                mcmcout.post.par.sigma.C(m-mcmc.burnin,:,:)=qincolmult(post.par.sigma.C);
                mcmcout.post.par.sigma.c(m-mcmc.burnin,:)=post.par.sigma.c;
                mcmcout.post.par.sigma.logdetC(m-mcmc.burnin,:,:)=post.par.sigma.logdetC;
            end
        end
        %if prior.type(1:4)=='conc'  mcmcout.post.par.mu.N0(m-mcmc.burnin,:)=post.par.mu.N0; end
    end
    if ihier   % hierarchical prior
        if (m-mcmc.burnin)==1
            if size(data.y,1)==1
                mcmcout.hyper=zeros(mcmc.M,1);
            else
                %dr=size(data.y,1)*(size(data.y,1)+1)/2;
                mcmcout.hyper=zeros(mcmc.M,dr);
            end
        end
        if size(data.y,1)==1
            mcmcout.hyper(m-mcmc.burnin)=prior.par.sigma.C(1);
        else
            mcmcout.hyper(m-mcmc.burnin,:)=qincol(prior.par.sigma.C(:,:,1))';
        end
    end

elseif  and(glm,isfield(mix,'d'))

    if (m-mcmc.burnin)==1
        if mix.df==0
            mcmcout.par=struct('beta',zeros(mcmc.M,mix.d,mix.K));
        else
            mcmcout.par=struct('alpha',zeros(mcmc.M,mix.df),'beta',zeros(mcmc.M,mix.d-mix.df,mix.K));
        end
        if all(mix.dist(1:6)=='Negbin') mcmcout.par.df=zeros(mcmc.M,mix.K); end

        if istorepost
            mcmcout.Nk=zeros(mcmc.M,mix.K);
            if mix.df==0
                mcmcout.post.par=struct('beta',struct('b',zeros(mcmc.M,mix.d,mix.K),'B',zeros(mcmc.M,mix.d,mix.d,mix.K)));
            else
                mcmcout.post.par=struct('alpha',struct('a',zeros(mcmc.M,mix.df),'A',zeros(mcmc.M,mix.df,mix.df)),'beta',struct('b',zeros(mcmc.M,mix.d-mix.df,mix.K),'B',zeros(mcmc.M,mix.d-mix.df,mix.d-mix.df,mix.K)));
            end
        end
    end
    mcmcout.par.beta(m-mcmc.burnin,:,:)=mix.par.beta;
    if  all(mix.dist(1:6)=='Negbin')  mcmcout.par.df(m-mcmc.burnin,:)=mix.par.df;   end

    if mix.df>0  mcmcout.par.alpha(m-mcmc.burnin,:)=mix.par.alpha';end
    if istorepost
        if mix.K==1
            mcmcout.post.par.beta.b(m-mcmc.burnin,:)=postreg.b(mix.df+1:end,1)';
        else
            mcmcout.post.par.beta.b(m-mcmc.burnin,:,:)=reshape(postreg.b(mix.df+1:end,1),(mix.d-mix.df),mix.K);
        end
        for k=1:mix.K;
            ie=mix.df+(k-1)*(mix.d-mix.df)+[1:(mix.d-mix.df)];
            mcmcout.post.par.beta.B(m-mcmc.burnin,:,:,k)=postreg.B(ie,ie);
        end
        if mix.df>0
            mcmcout.post.par.alpha.a(m-mcmc.burnin,:)=postreg.b(1:mix.df,1)';
            mcmcout.post.par.alpha.A(m-mcmc.burnin,:,:)=postreg.B(1:mix.df,1:mix.df);
        end
    end


else

    'ADD DISTRIBUTION FAMILY function mcmcput'

end



if mix.K>1
    if ~mix.indicfix
        if mix.indicmod.dist(1:6)=='Multin'

            if (m-mcmc.burnin)==1
                if istorepost   mcmcout.post.weight=zeros(mcmc.M,mix.K);   end
                mcmcout.weight=zeros(mcmc.M,mix.K);
            end

            mcmcout.weight(m-mcmc.burnin,:)=mix.weight';
            mcmcout.Nk(m-mcmc.burnin,:)=post.weight(1,rho)-prior.weight(1,rho);
            if istorepost
                mcmcout.post.weight(m-mcmc.burnin,:)=post.weight;
            end

        elseif mix.indicmod.dist(1:6)=='Markov'

            if (m-mcmc.burnin)==1
                if istorepost   mcmcout.post.indicmod.xi=zeros(mcmc.M,mix.K,mix.K);   end
                mcmcout.indicmod.xi=zeros(mcmc.M,mix.K,mix.K);
            end
            mcmcout.indicmod.xi(m-mcmc.burnin,:,:)= mix.indicmod.xi;
            mcmcout.Nk(m-mcmc.burnin,:)=sum(post.indicmod.xi(1,rho)-prior.indicmod.xi(1,rho),1);
            if istorepost
                mcmcout.post.indicmod.xi(m-mcmc.burnin,:,:)=post.indicmod.xi;
            end

        end
    end
end

