%% demo_negbin.m
%
%  Bayesian analysis for data from of a negative binomial distributions
%
%    - estimated using new auxiliary mixture sampling
%    - marginal likelihood using bridge sampling
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%  simulate data

N=1000;

clear mix;
mix.dist='Negbin';
mix.par.df=5;

mu=3;
mix.d=1;    % treated as a regression model  
mix.par.beta=log(mu);  
mix.d=size(mix.par.beta,1);

clear data;
data=simulate(mix,N);   
mixtrue=mix; clear mix


dataplot(data)

%% BAYESIAN ANALYSIS 


mix.dist='Negbin';
mix.d=1;

prior=priordefine(data,mix);

%[data,mix,mcmc]=mcmcstart(data,mix);
mcmc.startpar=true; [data, mix,mcmc]=mcmcstart(data,mix,mcmc); % start from a parameter 

mcmc.M=5000;mcmc.burnin=1000;mcmc.storeS=500;mcmc.storepost='true';

mcmc.mh.tune.df=1;  % 2: 11%

mcmcout=mixturemcmc(data,mix,prior,mcmc);
[est,mcmcout]=mcmcestimate(mcmcout)

%% marginal likelihoods

[marlik,mcmcout]=mcmcbf(data,mcmcout)
