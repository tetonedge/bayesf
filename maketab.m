function tab = maketab (x)
l1 = size(x,1);
l2 = size(x,2);
cha = ' &  ';
chl = ' \\ ';
nn=3;%signifikante stellen
tab = [num2str(squeeze(x(:,1)),nn) cha(ones(1,l1),:)];
for i=2:l2-1;
   tab = [tab num2str(squeeze(x(:,i)),nn) cha(ones(1,l1),:)];
end
tab = [tab num2str(squeeze(x(:,l2)),nn) chl(ones(1,l1),:)];

