%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%  Markov Swichting AR model  -  Simulated data

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



nfig=0;
armax=3;
nameoutall='';

%for K=1:2  %loop over increasing number of states
for K=[2]   %  TEST
    
    %for d=[1 3:3+2*min(K-1,1)];  % loop over various designs (d number of exogenous regressors)
    for d=[1]    % TEST
        %for fixar=0:min(K-1,1)   % loop over fixed and random AR coefficients for K> 1
        for fixar=0:0  % TEST 
            %for p=0:armax     % loop over increasing AR orders
            for p=0:0   % TEST
                %for mixtype=1:1+min(K-1,1)  % compare a mixture model with a MS model for K> 1
                for mixtype=2:2  % TEST
                    
                    %%%%%%%%%  model choice
                    clear mix;
                    mix.K=K;
                    if  mixtype==2   mix.indicmod.dist='Markovchain';    end 
                    if p>0
                        if fixar==0   
                            mix.ar=p; 
                        elseif fixar==1 
                            mix.arf=p; 
                        end
                    end
                    %mix.dist='Poisson';
                    mix.dist='Normal';
                    if ~isfield(mix,'indicmod')   mix.indicmod.dist='Multinomial'; end
                    if d>0 mix.d=d; end
                    if and(d>3,mix.K>1)   mix.indexdf=[3:d-1]';    end    % choose  fix effects
                    
                    %parfix.sigma=true;
                    
                    %    simulate data
                    mixtrue = simulatestart(mix);
                    
                    data=simulate(mixtrue,1000);
                    nfig=dataplot(data,nfig+1);
                    
                    %data.empty=true; % test marginal likelihoods, no data, only prior
                    
                    prior=priordefine(data,mix);
                    %  default independence prior fuehrt zu einer verzerrten Schaetzung
                    %  von sigma^2
                    
                    %if mix.dist(1:6)=='Normal'   prior.par.sigma.C=mean(mixtrue.par.sigma)*(prior.par.sigma.c-1); end
                    
                    
                    %%% MCMC
                    
                    mcmc=struct('burnin',2000,'M',7000,'storeS',500,'storepost',true,'startpar',true);

                    mcmc=struct('burnin',500,'M',1000,'storeS',500,'storepost',true,'startpar',true);
                    
                    %mcmc=struct('burnin',500,'M',1000,'storeS',500,'storepost',true);

                    %mcmc=struct('burnin',10,'M',20,'storeS',10,'storepost',true,'startpar',true);
                    
                    if isfield(mcmc,'startpar') if mcmc.startpar mix=mixtrue; end; end
                    %test: mix.indicfix=true;
                        
                    [data,mix] = mcmcstart(data,mix,mcmc);
                    
                    namem=[mix.dist 'simdat'];
                    namem=[namem 'Design' num2str(d)];   
                    if mixtype==2 [namem 'MS']; end
                    namem=[namem 'K' num2str(K) 'AR' num2str(p)];
                    if fixar==1 namem=[namem 'FIX']; end
                    
                    %test: mix.parfix.beta=true; mix.par.beta= mixtrue.par.beta;
                    
                    eval(['erg_' namem '=mixturemcmc(data,mix,prior,mcmc);']);
                    eval(['erg_' namem '.name=namem;']);
                    nameoutall=[nameoutall 'erg_' namem ',']; 
                    
                    
                    
                    eval(['[estK' num2str(K)  'cp, erg_' namem ']=mcmcestimate(erg_' namem ');']);
                    
                    
                    
                    data.t0= armax +2;
                    eval(['[marlikK' num2str(K) 'random, erg_' namem ']=mixturebf(erg_' namem ',data)']); 
                    
                    if K>1
                    options.M0=100; eval(['options.Smax=erg_' namem '.M;']); options.method='full';
                                        eval(['[marlikK' num2str(K) 'full, erg_' namem ']=mixturebf(erg_' namem ',data,options)']); 
                    eval(['mcmcstore(erg_' namem ');']);
                    end
                    data=rmfield(data,'t0');
                    
                    diagnose=true;
                    if diagnose
                     eval(['nfig=mcmcplot(erg_' namem ',' num2str(nfig+1) ');'])   
                     eval(['nfig=mcmcdiag(data,erg_' namem ',' num2str(nfig+1) ');'])
                     end
                     
                    predict=false;
                    if predict
                        H=3*4;
                        if and(dataname(1:4)=='poli',design~=3)  
                            data.X=[data.X data.X(:,end-H,end)]; % replicate seasonal pattern
                        end
                        eval(['pred=mcmcpredsam(erg_' namem ',12,data);']);
                    end
                    
                end
            end
        end 
    end
end

['erg_' namem ] 

