function [marlik,varargout] = mcmcbf(data,mcmcout,varargin)
% computes the marginal likelihood using simulation-based methods

% Author: Sylvia Fruehwirth-Schnatter

rand('state',sum(100*clock)) ;
randn('state',sum(100*clock)) ;


if ~isfield(data,'empty') data.empty=false; end


if ~isfield(mcmcout,'post')
    warn('No posterior moments available, set .mcmcstorepost=true before running mixturemcmc')
    marlik=[];return
end

if ~isfield(data,'y')
    warn('The field y is obligatory in the structure array defining the data when calling the function mixturebf')
    marlik=[];return
end
ibycolumn=isfield(data,'bycolumn');
if ibycolumn  ibycolumn=data.bycolumn; end  % ibycolumn true: data stored by column
if ibycolumn      data.y=data.y'; data.bycolumn ='false'; end
if ~isfield(data,'N') data.N=size(data.y,2); end


M=mcmcout.M;



model=mcmcout.model;
if ~isfield(model,'K')  model.K =1; end
K=model.K;
if and(K>1,~isfield(model,'indicmod'))  model.indicmod.dist='Multinomial'; model.indicmod.T=1; model.indicmod.cat=K;end
%norstud=any([all(model.dist(1:6)=='Normal') all(model.dist(1:6)=='Normul') all(model.dist(1:6)=='Stumul') all(model.dist(1:6)=='Studen')]);
%      Normal or student mixture;

mix.dist=model.dist;
studmix=any([all(mix.dist(1:6)=='Stumul') all(mix.dist(1:6)=='Studen') all(mix.dist(1:6)=='SkewSt') all(mix.dist(1:6)=='SkStmu')]);
skew=false;
norstudskew=any([all(mix.dist(1:6)=='Normal') all(mix.dist(1:6)=='Normul') skew studmix]);  %% incluse skew?
%      Normal or student mixture;

if or(studmix,all(model.dist(1:6)=='Negbin'))
    if isfield(model,'parfix')  
        if isfield(model.parfix,'df')
        dffix=model.parfix.df;      
        else
            dffix=false;
        end
    else
      dffix=false;  
    end
end
if norstudskew
    if isfield(model,'parfix')  
        if isfield(model.parfix,'mu')
            mufix=model.parfix.mu;
        else
            mufix=false;
        end
    else
        mufix=false;
    end
end
if all(model.dist(1:6)=='Binomi')
    if ~isfield(data,'Ti')  data.Ti=ones(1,data.N); end  % binary data
end

if all(model.dist(1:6)=='Multin')  
    if ~isfield(model,'label')
        if ~isfield(model,'r') mix.r=1; end
        if ~isfield(model,'cat') model.cat=2*ones(mix.r,1); end
        model.label=[1:model.cat(1)]';
        for j=2:model.r
            model.label=[model.label;[1:model.cat(j)]'];
        end
    end
end


if all([~isfield(model,'error') norstudskew]) % default: switching variane
    model.error='switch';
end

if ~isfield(model,'indicfix')  model.indicfix=false;   end
if ~isfield(model,'indicmod')
    model.indicmod.dist='Multinomial';
elseif ~isfield(model.indicmod,'init')
    model.indicmod.init='ergodic';
end

if ~isfield(data,'t0') datat0=1; else  datat0=data.t0; end
if isfield(model,'d') if and (model.d==1,~isfield(data,'X')) data.X=ones(1,data.N); end; end


if datat0>1
    dataar=data; % original data, needed for likelihood computaion, if t0 differs between estimation and BF calculations
end
if any([isfield(model,'ar') isfield(model,'arf') datat0>1])
    % convert MSAR model into a large Regression model
    [data,model]=designar(data,model);
end

if ~isfield(mcmcout.log,'t0') logt0=1; else  logt0=mcmcout.log.t0; end
if logt0~=datat0
    computelogmcmc=true;
    loglikmc=zeros(M,1);
else
    computelogmcmc=false;  clear dataar;
    loglikmc=mcmcout.log.mixlik;
end

prior=mcmcout.prior;
if nargin==3
    if isfield(varargin{1},'Smax')
        Smax=varargin{1}.Smax;
    else
        Smax=2000;  % maximum numbers of components in the importance density
    end
    if isfield(varargin{1},'method')
        method=varargin{1}.method;
    else
        method='random';
    end
    if isfield(varargin{1},'M0')
        M0=varargin{1}.M0;
    else
        if le(K,3)
            M0=100;
        elseif K==4
            M0=25;
        else
            M0=5;
        end
    end
else
    Smax=2000;
    method='random';
    
    %if skew     method='collapse';  end  
    %
    
    if le(K,3) 
        M0=100; 
    elseif K==4
        M0=25;
    else
        M0=5; 
    end
end

%%  construct the importance density q

simstud=false;
%simstud=true;dfq=10;  % use stud-t instead of normal distributions (currently implemented only for regression models)


if method(1:4)=='rand'
    S=round(M0*exp(gammaln(K+1)));
    S=min([S Smax M]);  % truncated, if S is too large

    %indexchoose=[M-S+1:M];  % extract the posterior moments corresponding to the last S components
    index=randperm(M);
    indexchoose=sort(index([M-S+1:M])); % extract M0 randomly chosen posterior moments

    mcmcoutq=mcmcsubseq(mcmcout,indexchoose);
    post=mcmcoutq.post; clear mcmcoutq;

    if or(studmix,all(model.dist(1:6)=='Negbin'))    % ADD degrees pf freedom for t-mixture
        if ~dffix
            kernel_estimator=true;
            post.par.df=log(mcmcout.par.df(indexchoose,:)-prior.par.df.trans);
            post.par.dfvar=var(log(mcmcout.par.df))/10;
        end
    end


elseif method(1:4)=='full'

    S=round(M0*exp(gammaln(K+1)));
    if  S>Smax

        M0min= fix(Smax/exp(gammaln(K+1)));

        if and(~isfield(varargin,'M0'),M0min>=1)
            M0=M0min;
            S=Smax;
        elseif  ~isfield(varargin,'M0')
            warn(['Smax must be at least equal to ' exp(gammaln(K+1)) ' to compute the marginal likleihood using full permutation sampling']);
            marlik=[];return
        else

            warn(['Smax must be at least equal to ' M0*exp(gammaln(K+1)) ' to compute the marginal likleihood using full permutation sampling of ' M0 'components']);
            marlik=[];return
        end
    end

    %indexchoose=[M-M0+1:M];  % extract the posterior moments corresponding to the last S components

    index=randperm(M);
    indexchoose=sort(index([M-M0+1:M])); % extract M0 randomly chosen posterior moments

    mcmcoutq=mcmcsubseq(mcmcout,indexchoose);
    post=mcmcoutq.post; clear mcmcoutq;
    if or(studmix,all(model.dist=='Negbin'))   % ADD degrees pf freedom for t-mixture
        if ~dffix
            kernel_estimator=true;
            post.par.df=log(mcmcout.par.df(indexchoose,:)-prior.par.df.trans);
            post.par.dfvar=var(log(mcmcout.par.df))/10;
        end
    end
    if K>1 post=fullperm(post,K); S=round(M0*exp(gammaln(K+1)));end



elseif all(method(1:4)=='coll')
    S=100;
    S=round(M0*exp(gammaln(K+1)));
    S=min([S Smax fix(M/2)]);  % truncated, if S is too large

    % initialize the importance density by drawing
    % S posterior moments;
    % these momemnts are  redefined below
    index=randperm(M);
    indexchooser=sort(index([M-S+1:M])); % extract M0 randomly chosen posterior moments
    mcmcoutq=mcmcsubseq(mcmcout,indexchooser);
    post=mcmcoutq.post; clear mcmcoutq;
    % end initialize

    collapse=fix(M/S);

    collapse_sigma=   false; collapse_sigma=   true;
    collapse_sigma_df=false; collapse_sigma_df=true;
    collapse_weight=  false; collapse_weight=   true;
    collapse_mu =  false; collapse_mu=   true;


    for s=1:S
        % indexchoose=(s-1)*collapse+[1:collapse];
        indexchoose=index((s-1)*collapse+[1:collapse]);

        if isfield(mcmcout.post.par,'beta')
            if K==1
                post.par.beta.b(s,:)=mean(mcmcout.post.par.beta.b(indexchoose,:));
                post.par.beta.B(s,:,:)=squeeze(mean(mcmcout.post.par.beta.B(indexchoose,:,:)))+cov(mcmcout.post.par.beta.b(indexchoose,:));
            else
                for k=1:K
                    post.par.beta.b(s,:,k)=mean(mcmcout.post.par.beta.b(indexchoose,:,k));
                    post.par.beta.B(s,:,:,k)=squeeze(mean(mcmcout.post.par.beta.B(indexchoose,:,:,k)))+cov(mcmcout.post.par.beta.b(indexchoose,:,k));
                end
            end
        end

        if isfield(mcmcout.post.par,'alpha')
            post.par.alpha.a(s,:)=mean(mcmcout.post.par.alpha.a(indexchoose,:));
            post.par.alpha.A(s,:,:)=squeeze(mean(mcmcout.post.par.alpha.A(indexchoose,:,:)))+cov(mcmcout.post.par.alpha.a(indexchoose,:));
        end

        if collapse_weight
            if isfield(mcmcout.post,'weight')
                Seta=sum(mcmcout.post.weight(indexchoose,:),2);
                Ek=mcmcout.post.weight(indexchoose,:)./repmat(Seta,1,K);
                Est=mean(Ek);
                Vk=Ek.*(1-Ek).*repmat(1./(1+Seta),1,K);
                Vst=mean(Vk)+var(Ek);
                clear Ek Vk;

                % post.weight(s,:)=Est.*(Est.*(1-Est)./Vst - 1);  % fit each marginal -unstable

                % sumstar=mean(Seta);  % tails too thin; sumstar equals N+sum(e0)
                % use second marginal momemt Vst
                sumstar=1./mean(Vst./Est./(1-Est)) - 1;
                % sumstar=mean(Est.*(1-Est)./Vst) - 1;
                % sumstar=median(Est.*(1-Est)./Vst) - 1; % identical for both ways of estimating sumstar
                post.weight(s,:)=Est.*max(sumstar,K);

            end
            
            if isfield(mcmcout.post.par,'indicmod')
                'mcmc bf einbauen'
            end
        end

        if collapse_mu
            if isfield(mcmcout.post.par,'mu')
                if and(K==1,data.r>1)
                    post.par.mu.b(s,:)=mean(mcmcout.post.par.mu.b(indexchoose,:));
                    post.par.mu.B(s,:,:)=squeeze(mean(mcmcout.post.par.mu.B(indexchoose,:,:)))+cov(mcmcout.post.par.mu.b(indexchoose,:));
                elseif and(K>1,data.r>1)
                    for k=1:K
                        post.par.mu.b(s,:,k)=mean(mcmcout.post.par.mu.b(indexchoose,:,k));
                        post.par.mu.B(s,:,:,k)=squeeze(mean(mcmcout.post.par.mu.B(indexchoose,:,:,k)))+cov(mcmcout.post.par.mu.b(indexchoose,:,k));
                    end
                else
                    post.par.mu.b(s,:)=mean(mcmcout.post.par.mu.b(indexchoose,:));
                    post.par.mu.B(s,:)=mean(mcmcout.post.par.mu.B(indexchoose,:))+var(mcmcout.post.par.mu.b(indexchoose,:));

                end

            end
        end


        if collapse_sigma
            if isfield(mcmcout.post.par,'sigma')
                Mi=prod(size(indexchoose));
                if  data.r>1
                    if collapse_sigma_df  % match the determinante of Sigma^(-1)

                        ggf1=([1:data.r]-1)*0.5;
                        ggf1=permute(repmat(ggf1',[1 Mi K]),[2 3 1]);
                        ggf=-mcmcout.post.par.sigma.logdetC(indexchoose,:)+sum(log(repmat(mcmcout.post.par.sigma.c(indexchoose,:),[1 1 data.r])-ggf1),3);
                        mC=max(ggf);
                        % Em=exp(mC).*mean(exp(ggf-repmat(mC,Mi,1)));
                        logEm=mC+log(mean(exp(ggf-repmat(mC,Mi,1))));


                        ggf2=([1:data.r]-3)*0.5;
                        ggf2=permute(repmat(ggf2',[1 Mi K]),[2 3 1]);
                        ggf=-2*mcmcout.post.par.sigma.logdetC(indexchoose,:)+sum(log(repmat(mcmcout.post.par.sigma.c(indexchoose,:),[1 1 data.r])-ggf1)+log(repmat(mcmcout.post.par.sigma.c(indexchoose,:),[1 1 data.r])-ggf2),3);
                        mC=max(ggf);
                        % E2m=exp(mC).*mean(exp(ggf-repmat(mC,Mi,1)));
                        logE2m=mC+log(mean(exp(ggf-repmat(mC,Mi,1))));

                        clear ggf;
                        Rd=exp(logE2m-2*logEm);
                        cr=[Rd-1; -(Rd*(2*data.r-3)*0.5+1.5); Rd*(data.r-2)*(data.r-1)*0.25-0.5];

                        for k=1:K
                            post.par.sigma.c(s,k)=max(roots(cr(:,k)));
                            post.par.sigma.c(s,k)=0.5+fix(2*post.par.sigma.c(s,k)).*0.5;    % round; extend invwisim auf non-integers
                            post.par.sigma.c(s,k)=max(post.par.sigma.c(s,k),(data.r+1)/2+1);
                        end
                    else
                        post.par.sigma.c(s,:)=min(mcmcout.post.par.sigma.c(indexchoose,:));
                    end
                    dr=size(mcmcout.post.par.sigma.C,2);
                    EQc=mean(mcmcout.post.par.sigma.C(indexchoose,:,:)./(permute(repmat(mcmcout.post.par.sigma.c(indexchoose,:),[1 1 dr]),[1 3 2])-(data.r+1)*0.5));

                    if K>1
                        post.par.sigma.C(s,:,:)=squeeze(EQc).*(repmat(post.par.sigma.c(s,:),[dr 1])-(data.r+1)/2);
                        for k=1:K
                            post.par.sigma.logdetC(s,k)= log(det(qinmatr(squeeze( post.par.sigma.C(s,:,k))')));
                        end
                    else

                        post.par.sigma.C(s,:)=EQc.*(repmat(post.par.sigma.c(s),[1 dr])-(data.r+1)/2);
                        post.par.sigma.logdetC(s)=log(det(qinmatr(squeeze(post.par.sigma.C(s,:))')));
                    end

                else  % sigma univariate 
                    Ek=mcmcout.post.par.sigma.c(indexchoose,:)./mcmcout.post.par.sigma.C(indexchoose,:);
                    Vk=Ek./mcmcout.post.par.sigma.C(indexchoose,:);
                    Estar=mean(Ek);
                    Vstar=mean(Vk)+var(Ek);
                    post.par.sigma.c(s,:)=Estar.^2./Vstar;
                    post.par.sigma.C(s,:)=Estar./Vstar;
                end
            end

            if ~isstruct(mcmcout.post.par)
                'INCLDUE FIT mcmcbf'
            end
        end

        if or(studmix,all(model.dist(1:6)=='Negbin'))    % ADD degrees pf freedom for t-mixture

            if ~dffix

                kernel_estimator=true;
                if kernel_estimator
                    if s==1
                        post.par.df=log(mcmcout.par.df(indexchooser,:)-prior.par.df.trans);
                        post.par.dfvar=var(log(mcmcout.par.df))/10;
                    end
                else
                    if s==1 post.par.df=zeros(S,K);   post.par.dfvar=zeros(S,K); end

                    logdf=log(mcmcout.par.df(indexchoose,:)-prior.par.df.trans);
                    post.par.df(s,:)=mean(logdf);
                    post.par.dfvar(s,:)=var(logdf);
                    clear logdf;
                end
            end
        end

    end
end

qout.log.mixprior=zeros(M,1); qout.log.mixlik=zeros(M,1); qout.log.q=zeros(M,1);  mcmcout.log.q=zeros(M,1);




%% compute  bayes factors
%  sample from q and evaluate importance density at the q and the MCMC
%  draws

t0 = clock;tlast=t0;
finmix=all([~isfield(mcmcout.model,'d') ~isfield(mcmcout.model,'ar') ~isfield(mcmcout.model,'arf')]);

% choose the components for sampling from the mixture importance density

qs = simuni(S,M);

if all(model.dist(1:6)=='Multin') post.par=permute(post.par,[1 3 2]); end
    
for m=1:M

    if isfield(model,'weight')  model=rmfield(model,'weight'); end
    if isfield(model,'par')
        % if isstruct(model.par)
        % if isfield(model,'indicmod')  model=rmfield(model,'indicmod'); end
        %  if isfield(model,'alpha')  model=rmfield(model,'alpha'); end
        % if isfield(model,'beta')  model=rmfield(model,'beta'); end
        % if isfield(model,'mu')  model=rmfield(model,'mu'); end
        % else
        model=rmfield(model,'par');
        % end
    end

    if etime(clock,tlast)>60
        ext=fix(etime(clock,t0)/m*(M-m));
        ['estimated completion in about ' num2str(fix(ext/60))  ' minutes  and ' num2str(ext-fix(ext/60)*60) ' seconds']
        tlast = clock;
    end

    qq=zeros(S,1);qmc=zeros(S,1);

    %% weigth distribution

    if and(K>1,~model.indicfix)
        if model.indicmod.dist(1:6)=='Multin'
            model.weight=dirichsim(post.weight(qs(m),:));
            qq= qq + dirichpdflog(post.weight,model.weight);
            qmc=qmc + dirichpdflog(post.weight,mcmcout.weight(m,:));
        elseif   model.indicmod.dist(1:6)=='Markov'
            model.indicmod.xi=dirichsim(squeeze(post.indicmod.xi(qs(m),:,:)));
            qq= qq + proddirichpdflog(post.indicmod.xi,model.indicmod.xi);
            qmc=qmc + proddirichpdflog(post.indicmod.xi,squeeze(mcmcout.indicmod.xi(m,:,:)));
        end
    else
        model.weight=1;
    end

    if finmix

        %% finite mixtures of Poisson distributions

        if all(model.dist(1:6)=='Poisso')

            model.par=prodgamsim(struct('a',post.par.a(qs(m),:),'b',post.par.b(qs(m),:)));
            qq  = qq +prodgampdflog(post.par,model.par);
            qmc = qmc+prodgampdflog(post.par,mcmcout.par(m,:));


            %% finite mixtures of Exponential distributions (HW)

        elseif model.dist(1:6)=='Expone'

            model.par=prodgamsim(struct('a',post.par.a(qs(m),:),'b',post.par.b(qs(m),:)));
            qq  = qq +prodgampdflog(post.par,model.par);
            qmc = qmc+prodgampdflog(post.par,mcmcout.par(m,:));

            %% finite mixtures of Binomial distributions

        elseif all(model.dist(1:6)=='Binomi')


            model.par=prodbetasim(struct('a',post.par.a(qs(m),:),'b',post.par.b(qs(m),:)));
            qq  = qq +prodbetapdflog(post.par,model.par);
            qmc = qmc+prodbetapdflog(post.par,mcmcout.par(m,:));

            
        elseif all(model.dist(1:6)=='Multin')
            
            model.par.pi=zeros(sum(model.cat),model.K);  
            for j=1:model.r    
                ij=sum(model.cat(1:j-1))+[1:model.cat(j)]';
                ppi= squeeze(post.par(qs(m),:,ij));
                model.par.pi(ij,:)=dirichsim(ppi)'; 
                if model.K>1
                    qq= qq + proddirichpdflog(post.par(:,:,ij),model.par.pi(ij,:)');
                    qmc=qmc + proddirichpdflog(post.par(:,:,ij),squeeze(mcmcout.par.pi(m,ij,:))');
                else
                    qq= qq + dirichpdflog(squeeze(post.par(:,:,ij)),model.par.pi(ij,:)');
                    qmc=qmc + dirichpdflog(squeeze(post.par(:,:,ij)),squeeze(mcmcout.par.pi(m,ij,:)));
                end
            end

            %% finite mixtures of Normal and Student-t distributions

        elseif  or(all(mcmcout.model.dist(1:6)=='Normal'),all(mcmcout.model.dist(1:6)=='Studen'))

            if model.error=='switch'
                model.par.sigma=1./prodgamsim(struct('a',post.par.sigma.c(qs(m),:),'b',post.par.sigma.C(qs(m),:)));

                if all(prior.type(1:4)=='sigm')
                    ind=(model.par.sigma.^.5>prior.par.sigma.max);
                    icount=0;
                    while and(sum(ind)>0,icount<1000)
                       model.par.sigma(ind)=1./prodgamsim(struct('a',post.par.sigma.c(qs(m),ind),'b',post.par.sigma.C(qs(m),ind)));
                       ind=(model.par.sigma.^.5>prior.par.sigma.max);
                       icount=icount+1;
                    end
                end
                modseq=struct('a',post.par.sigma.c,'b',post.par.sigma.C);
                qq  = qq +prodinvgampdflog(modseq,model.par.sigma);
                qmc = qmc+prodinvgampdflog(modseq,mcmcout.par.sigma(m,:));
                if all(prior.type(1:4)=='sigm')
                    qq = qq - model.K*log(prior.par.sigma.max);
                    qmc = qmc - model.K*log(prior.par.sigma.max);
                end
            end

            if ~mufix
                model.par.mu=post.par.mu.b(qs(m),:)+post.par.mu.B(qs(m),:).^.5.*randn(1,K);
                modseq=struct('mu',post.par.mu.b,'sigma',post.par.mu.B);
                qq  = qq +prodnorpdflog(modseq,model.par.mu);
                qmc = qmc+prodnorpdflog(modseq,mcmcout.par.mu(m,:));
            else
                model.parfix.mu=true;
                model.par.mu=mcmcout.model.par.mu;
            end
            
%% finite mixtures of multivariate Normal and Student-t distributions

        elseif  or(all(mcmcout.model.dist(1:6)=='Normul'),all(mcmcout.model.dist(1:6)=='Stumul'))

            r=size(post.par.mu.b,2);
            if ~isfield(model,'r') model.r=r; end
            model.par.logdet=zeros(1,model.K); model.par.sigma=zeros(r,r,model.K); model.par.sigmainv=zeros(r,r,model.K);
            for k=1:model.K
                [model.par.sigma(:,:,k) model.par.sigmainv(:,:,k) model.par.logdet(1,k)] = invwisim(post.par.sigma.c(qs(m),k),qinmatr(squeeze(post.par.sigma.C(qs(m),:,k))'));
            end
            modseq=struct('a',post.par.sigma.c,'Scol',post.par.sigma.C,'logdetS',post.par.sigma.logdetC);
            arg=struct('yinv',model.par.sigmainv,'logdetyinv',model.par.logdet);
            qq  = qq +prodinvwipdflog(modseq,arg);
            if K>1
                arg=struct('yinvcol',squeeze(mcmcout.par.sigmainv(m,:,:)),'logdetyinv',mcmcout.par.logdet(m,:));
            else
                arg=struct('yinvcol',mcmcout.par.sigmainv(m,:)','logdetyinv',mcmcout.par.logdet(m));
            end
            qmc = qmc+prodinvwipdflog(modseq,arg);

            % move this to the top - do it only once

            if ~mufix

                if m==1  % compute the information matrix , the cholesky decomposition and the determinante to speed uo computation
                    post.par.mu.Binv=0*post.par.mu.B; post.par.mu.Bchol=0*post.par.mu.B; post.par.mu.logdet=zeros(S,K);
                    for i=1:S;
                        for k=1:K;
                            Binv=inv(squeeze(post.par.mu.B(i,:,:,k)));
                            post.par.mu.Binv(i,:,:,k)=Binv;
                            post.par.mu.logdet(i,k)=log(det(Binv));
                            post.par.mu.Bchol(i,:,:,k)=chol(squeeze(post.par.mu.B(i,:,:,k)))';
                        end;
                    end;
                end

                if K>1
                    model.par.mu=prodnormultsim(struct('mu',squeeze(post.par.mu.b(qs(m),:,:)),'sigmachol',squeeze(post.par.mu.Bchol(qs(m),:,:,:))));
                else
                    model.par.mu=prodnormultsim(struct('mu',post.par.mu.b(qs(m),:)','sigmachol',squeeze(post.par.mu.Bchol(qs(m),:,:))));
                end

                modseq=struct('mu',post.par.mu.b,'sigmainv',post.par.mu.Binv,'logdet',post.par.mu.logdet);
                qq  = qq +prodnormultpdflog(modseq,model.par.mu);
                if K>1
                    qmc  = qmc +prodnormultpdflog(modseq,squeeze(mcmcout.par.mu(m,:,:)));
                else
                    qmc  = qmc +prodnormultpdflog(modseq,mcmcout.par.mu(m,:)');
                end
            else
                model.parfix.mu=true;
                model.par.mu=mcmcout.model.par.mu;
            end

        else
            ['Mixture type ' model.dist ' not supported by   function mcmcbf']
        end


%%  mixture and Markov switching regression

    else

        if isfield(model,'error')
            if model.error=='switch'
                model.par.sigma=1./prodgamsim(struct('a',post.par.sigma.c(qs(m),:),'b',post.par.sigma.C(qs(m),:)));
                % evaluate q
                modseq=struct('a',post.par.sigma.c,'b',post.par.sigma.C);
                qq  = qq +prodinvgampdflog(modseq,model.par.sigma);
                qmc = qmc+prodinvgampdflog(modseq,mcmcout.par.sigma(m,:));
            end
        end

        % move this to the top - do it only once

        if m==1  % compute the information matrix , the cholesky decomposition and the determinante to speed uo computation
            post.par.beta.Binv=0*post.par.beta.B; post.par.beta.Bchol=0*post.par.beta.B; post.par.beta.logdet=zeros(S,K);
            if isfield(model,'indexdf')
                model.df=size(model.indexdf,1)*size(model.indexdf,2);
                post.par.alpha.Ainv=0*post.par.alpha.A; post.par.alpha.Achol=0*post.par.alpha.A; post.par.alpha.logdet=zeros(S,1);
            else
                model.df=0;
            end

            for i=1:S;
                for k=1:K;
                    Binv=inv(squeeze(post.par.beta.B(i,:,:,k)));
                    post.par.beta.Binv(i,:,:,k)=Binv;
                    post.par.beta.logdet(i,k)=log(det(Binv));
                    post.par.beta.Bchol(i,:,:,k)=chol(squeeze(post.par.beta.B(i,:,:,k)))';
                end;
                if model.df>0
                    Ainv=inv(squeeze(post.par.alpha.A(i,:,:)));
                    post.par.alpha.Ainv(i,:,:)=Ainv;
                    post.par.alpha.Achol(i,:,:)=chol(squeeze(post.par.alpha.A(i,:,:)))';
                    post.par.alpha.logdet(i)=log(det(Ainv));
                end
            end;
        end


        if and(K>1,size(post.par.beta.b,2)>1)
            if ~simstud
                model.par.beta=prodnormultsim(struct('mu',squeeze(post.par.beta.b(qs(m),:,:)),'sigmachol',squeeze(post.par.beta.Bchol(qs(m),:,:,:))));
            else
                model.par.beta=prodstudmultsim(struct('mu',squeeze(post.par.beta.b(qs(m),:,:)),'sigmachol',squeeze(post.par.beta.Bchol(qs(m),:,:,:)),'df',dfq*ones(1,K)));
            end
        elseif K>1

            if ~simstud
                model.par.beta=prodnormultsim(struct('mu',squeeze(post.par.beta.b(qs(m),:,:))','sigmachol',squeeze(post.par.beta.Bchol(qs(m),:,:,:))'));
            else
                model.par.beta=prodstudmultsim(struct('mu',squeeze(post.par.beta.b(qs(m),:,:))','sigmachol',squeeze(post.par.beta.Bchol(qs(m),:,:,:))','df',dfq*ones(1,K)));
            end

        else

            if ~simstud
                model.par.beta=prodnormultsim(struct('mu',post.par.beta.b(qs(m),:)','sigmachol',squeeze(post.par.beta.Bchol(qs(m),:,:))));
            else

                model.par.beta=prodstudmultsim(struct('mu',post.par.beta.b(qs(m),:)','sigmachol',squeeze(post.par.beta.Bchol(qs(m),:,:)),'df',dfq));
            end
        end

        if and((model.d-model.df)==1,K==1)
            if simstud
                modseq=struct('mu',post.par.beta.b,'sigmainv',1./post.par.beta.B,'logdet',post.par.beta.logdet,'df',repmat(dfq,size(post.par.beta.logdet)));
                qq  = qq +prodstudmultpdflog(modseq,model.par.beta);
                qmc  = qmc + prodstudmultpdflog(modseq,mcmcout.par.beta(m,:));
            else
                modseq=struct('mu',post.par.beta.b,'sigma',post.par.beta.B,'logdet',post.par.beta.logdet);

                qq  = qq +prodnorpdflog(modseq,model.par.beta);
                qmc  = qmc + prodnorpdflog(modseq,mcmcout.par.beta(m,:));
            end
        elseif and((model.d-model.df)>1,K>1)
            modseq=struct('mu',post.par.beta.b,'sigmainv',post.par.beta.Binv,'logdet',post.par.beta.logdet);
            if simstud
                modseq.df=repmat(dfq,size(post.par.beta.logdet));
                qq  = qq +prodstudmultpdflog(modseq,model.par.beta);
                qmc  = qmc +prodstudmultpdflog(modseq,squeeze(mcmcout.par.beta(m,:,:)));

            else
                qq  = qq +prodnormultpdflog(modseq,model.par.beta);
                qmc  = qmc +prodnormultpdflog(modseq,squeeze(mcmcout.par.beta(m,:,:)));
            end
        elseif and((model.d-model.df)==1,K>1)
            modseq=struct('mu',squeeze(post.par.beta.b),'sigmainv',squeeze(post.par.beta.Binv),'logdet',squeeze(post.par.beta.logdet));
            if simstud
                modseq.df=repmat(dfq,size(post.par.beta.logdet));
                qq  = qq +prodstudmultpdflog(modseq,model.par.beta);
                qmc  = qmc +prodstudmultpdflog(modseq,squeeze(mcmcout.par.beta(m,:,:))');
            else
                qq  = qq +prodnormultpdflog(modseq,model.par.beta);
                qmc  = qmc +prodnormultpdflog(modseq,squeeze(mcmcout.par.beta(m,:,:))');
            end
        else
            modseq=struct('mu',post.par.beta.b,'sigmainv',post.par.beta.Binv,'logdet',post.par.beta.logdet);
            if simstud
                modseq.df=dfq;
                qq  = qq +prodstudmultpdflog(modseq,model.par.beta);
                qmc  = qmc +prodstudmultpdflog(modseq,squeeze(mcmcout.par.beta(m,:))');

            else
                qq  = qq +prodnormultpdflog(modseq,model.par.beta);
                qmc  = qmc +prodnormultpdflog(modseq,squeeze(mcmcout.par.beta(m,:))');
            end
        end

        if model.df>0
            model.par.alpha=prodnormultsim(struct('mu',post.par.alpha.a(qs(m),:)','sigmachol',squeeze(post.par.alpha.Achol(qs(m),:,:))));
            modseq=struct('mu',post.par.alpha.a,'sigmainv',post.par.alpha.Ainv,'logdet',post.par.alpha.logdet);
            qq  = qq +prodnormultpdflog(modseq,model.par.alpha);
            qmc  = qmc +prodnormultpdflog(modseq,squeeze(mcmcout.par.alpha(m,:))');
        end
    end

%% degrees of freedom for models based on the t- and the negative binomial distribution

    if or(studmix,all(model.dist(1:6)=='Negbin'))
        if dffix
            model.par.df=mcmcout.par.df(m,:);
        else
            if or(~all(method(1:4)=='coll'),kernel_estimator)

                model.par.df=post.par.df(qs(m),:)+post.par.dfvar.^.5.*randn(1,K);
                modseq=struct('mu',post.par.df,'sigma',repmat(post.par.dfvar,size(post.par.df,1),1));
            else
                model.par.df=post.par.df(qs(m),:)+post.par.dfvar(qs(m),:).^.5.*randn(1,K);
                modseq=struct('mu',post.par.df,'sigma',post.par.dfvar);

            end
            qq  = qq +prodnorpdflog(modseq,model.par.df)-sum(model.par.df);
            model.par.df=prior.par.df.trans+exp(model.par.df);
            logdfmc=log(mcmcout.par.df(m,:)-prior.par.df.trans);
            qmc = qmc+prodnorpdflog(modseq,logdfmc)-sum(logdfmc);

        end
    end

%% compute the functional value of the importance density

    qqmax=max(qq);
    qout.log.q(m)=qqmax+log(mean(exp(max(qq-qqmax,-1e300))));
    qmcmax=max(qmc);
    mcmcout.log.q(m)=qmcmax+log(mean(exp(max(qmc-qmcmax,-1e300))));


%% compute likelihood and prior for the qsample
    if data.empty
        qout.log.mixlik(m)=0;
    else
        qout.log.mixlik(m)=likelihoodeval(data,model);
        if computelogmcmc
            modelmcmc=mcmcextract(mcmcout,m);
            loglikmc(m)=likelihoodeval(dataar,modelmcmc);
        end
    end
    qout.log.mixprior(m)=prioreval(model,prior);
end

%% compute the marginal likelihoods

priormc=mcmcout.log.mixprior;
qmc= mcmcout.log.q;
loglikq=qout.log.mixlik;
priorq=qout.log.mixprior;
qq=qout.log.q;
Mbs=M;

%% Marginal likelihood based on importance sampling

ratio = loglikq+priorq-qq;
ratiomax=max(ratio);
mllogmi = ratiomax+log(mean(exp(ratio-ratiomax)));

%% Marginal likelihood based on reciprocal importance sampling

ratio = qmc-loglikmc-priormc;
ratiomax=max(ratio);
mllogri = -1*(ratiomax+log(mean(exp(ratio-ratiomax))));

%% Marginal likelihood based on iterative bridge  sampling, starting from ri and mi

maxit=1000;
mllogbs=zeros(maxit,2);
mllogbs(1,1)=real(mllogmi);
mllogbs(1,2)=real(mllogri);

for i=2:maxit
    logpostmc=loglikmc+priormc-mllogbs(i-1,2);
    logpostq=loglikq+priorq-mllogbs(i-1,2);
    maxqmc=max([qq logpostq],[],2);
    rq=logpostq-maxqmc-log(exp(log(Mbs)+logpostq-maxqmc)+exp(log(Mbs)+qq-maxqmc));
    maxqmc=max([qmc logpostmc],[],2);
    rmc=qmc-maxqmc-log(exp(log(Mbs)+logpostmc-maxqmc)+exp(log(Mbs)+qmc-maxqmc));
    mllogbs(i,2)=mllogbs(i-1,2)+max(rq)+log(mean(exp(max(rq-max(rq),-1e15))))-max(rmc)-log(mean(exp(max(rmc-max(rmc),-1e15))));

    logpostmc=loglikmc+priormc-mllogbs(i-1,1);
    logpostq=loglikq+priorq-mllogbs(i-1,1);
    maxqmc=max([qq logpostq],[],2);
    rq=logpostq-maxqmc-log(exp(log(Mbs)+logpostq-maxqmc)+exp(log(Mbs)+qq-maxqmc));
    maxqmc=max([qmc logpostmc],[],2);
    rmc=qmc-maxqmc-log(exp(log(Mbs)+logpostmc-maxqmc)+exp(log(Mbs)+qmc-maxqmc));
    mllogbs(i,1)=mllogbs(i-1,1)+max(rq)+log(mean(exp(max(rq-max(rq),-1e15))))-max(rmc)-log(mean(exp(max(rmc-max(rmc),-1e15))));
end

%% store the results

marlik.ri=mllogbs(1,2);
marlik.is=mllogbs(1,1);
marlik.bs=mllogbs(end,2);

marlik.log.loglikmc=loglikmc;
marlik.log.priormc=priormc;
marlik.log.qmc=qmc;
marlik.log.loglikq=qout.log.mixlik;
marlik.log.priorq= qout.log.mixprior;
marlik.log.qq=qout.log.q;

%% compute standard errors

[sebs,ach,acr] = marginallikelihood_eval(marlik,false,0);
marlik.se.bs=sebs(1:3);
marlik.se.is=sebs(4);
marlik.se.ri=sebs(5);

if nargout==2
    if or(method(1:4)=='rand',method(1:4)=='coll')
        mcmcout.marlik=marlik;

    elseif method(1:4)=='full'
        mcmcout.marlikfull=marlik;
    end
    varargout{1}=mcmcout;
end