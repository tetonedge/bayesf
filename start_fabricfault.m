%%%%%%%%%%%%%%%%%%%%%%%%
%  Fabric fault data

data=dataget('fabricfault');

%%  Estimate a mixture of Poissson Regression Models including model selection

post=[];
Kmin=1;
Kmax=3;
for K=Kmin:Kmax

    clear mix;
    mix.dist='Poisson';
    mix.d=2;
    mix.K=K;
    

    [data, mix,mcmc]=mcmcstart(data,mix);
    mcmc.burnin=2000; mcmc.M=10000;

    prior=priordefine(data,mix);
   
    mcmcout=mixturemcmc(data,mix,prior,mcmc);
    if isfield(data,'S') data=rmfield(data,'S'); end % starting classification for current K has to be deleted 

    [marlik,mcmcout]=mcmcbf(data,mcmcout);
    post=[post;marlik.bs];
    
    mcmcout.name= ['store_' data.name '_Poisson_mixreg_K' num2str(K)];
    mcmcstore(mcmcout);
end

%%  Select model with largest marginal likelihood 

[postsort is]=sort(post,1,'descend');
'Ranking according to marginal likelihood'
format bank;[is postsort],format short
Kselect=(Kmin-1)+is(1);

%% evaluate a fitted model for fixed K

K=Kselect;
eval(['load store_fabricfault_Poisson_mixreg_K' num2str(K)]);
mcmcplot(mcmcout);
[est,mcmcout]=mcmcestimate(mcmcout);
