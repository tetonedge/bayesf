function [clust,varargout]=mcmcclust(data,mcmcout)     
% Bayesian clustering from MCMC draws for a finite mixture model 

% Author: Sylvia Fruehwirth-Schnatter


% classification based on the similarity matrix

limit=1000; %  
%limit=6000;
if ~isfield(data,'N') data.N=size(data.y,2);end

if data.N<=limit
    
    SM=size(mcmcout.S,1);
    
    N=size(mcmcout.S,2);    
    
    
    if data.N<=1000
        jointclass=zeros(N);
        for k=1:mcmcout.model.K
            jointclass=jointclass+double(mcmcout.S==k)'*(mcmcout.S==k);
        end
        jointclass=jointclass/SM; % approximate similarity matrix
        Wloss=1-2*jointclass; clear jointclass;
        
        % compute loss function 
        
        
        lossim=zeros(SM,1); 
        for m=1:SM;
            lossim(m)=sum(sum(Wloss.*(repmat(mcmcout.S(m,:),N,1)==repmat(mcmcout.S(m,:),N,1)')));
            
            
        end;
        
    else
        
        lossim=mcmcclustsim(mcmcout.S,mcmcout.model.K);
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
        %for i=1:data.N
        %    i
        %    jointclass(i,:)=sum(mcmcout.S==repmat(mcmcout.S(:,i),1,data.N),1);
        %end
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%    
        %for m=1:SM;
        %    m
        %jointclass=jointclass+(repmat(mcmcout.S(m,:),N,1)==repmat(mcmcout.S(m,:),N,1)');
        % end
        
    end
    
    clear Wloss;
    
    [lss is]=sort(lossim); 
    clust.Ssim=mcmcout.S(is(1),:);
    
    Nk=sum(repmat(clust.Ssim,mcmcout.model.K,1)==repmat([1:mcmcout.model.K]',1,data.N),2);
    [NN,is]=sort(Nk);
    isa=is(end:-1:1);
    for k=1:mcmcout.model.K; 
        Sperm(clust.Ssim==isa(k))=k; % gruppen werden der Groesse nach sortiert
    end
    clust.Ssim=Sperm;
    
end



ibycolumn=isfield(data,'bycolumn');
if ibycolumn  ibycolumn=data.bycolumn; end  % ibycolumn true: data stored by column 
if ibycolumn      data.y=data.y'; data.bycolumn ='false'; end   


clust.Smap=mcmcout.clust.Smap; % Bayesian MAP has been determined during
if ~isfield(mcmcout.clust,'t0') mcmcout.clust.t0=1; end
if ~isfield(data,'t0') data.t0=1; end

if  mcmcout.clust.t0>=data.t0
    data.y=data.y(:,mcmcout.clust.t0:end);
    data.N=size(data.y,2);
    if isfield(data,'X') data.X=data.X(:,mcmcout.clust.t0:end); end
elseif  mcmcout.clust.t0<data.t0
    warn(['Size disagreement between S and data.y in funtion mcmcclust']);return;
end

if ~isfield(mcmcout,'S') 
    warn('MCMC output does not contain classification, only the MAP estimator is returned; to obatain the other estimators assign an appropriate value to mcmc.storeS and restart MCMC');
    return;
end

if or(nargout==2,~isfield(mcmcout,'perm')) 
    % identify the mixture model before performing clustering
    [est,mcmcout]=mcmcestimate(mcmcout);
    % if the result of clustering is stored in mcmcout, then identification is carried out, even if
    % a permuted MCMC output is contained in the input mcmcout file in order to ensure that the
    % labelling is the same
end    

clust.prob=zeros(mcmcout.model.K,data.N);

if isfield(mcmcout,'perm')

    MS=mcmcout.M-size(mcmcout.S,1);
    indexp=[1:mcmcout.M];    indexperm=indexp(mcmcout.isperm);  % consider only permuted draws
    indexSperm=indexperm(indexperm>MS);
    Smcident=zeros(size(mcmcout.S(indexSperm-MS,:)));

    for k=1:mcmcout.model.K
        Smcident=Smcident+(mcmcout.S(indexSperm-MS,:)==k).*repmat(mcmcout.perm(indexSperm,k),1,data.N);
    end

    for k=1:mcmcout.model.K;
        clust.prob(k,:)=sum(Smcident==k,1)/size(Smcident,1) ;
    end
    [pmax Sident]=max(clust.prob);
    clust.risk=1-pmax;
    clust.Sident=Sident;
    clear pmax Sident Smcident;
    
else
    
    'Warning: Classification without explicit identification: CHECK MCMC DRAWS'

    for k=1:mcmcout.model.K;
        clust.prob(k,:)=sum(mcmcout.S==k,1)/size(mcmcout.S,1) ;
    end
    [pmax Saverage]=max(clust.prob);
    clust.risk=1-pmax;
    clust.Sident=Saverage;
    clear pmax  Saverage;
    
end

if mcmcout.clust.t0>1 clust.t0=mcmcout.clust.t0; end



if nargout==2
    
    mcmcout.clust=clust;
    varargout{1}=mcmcout;
end    