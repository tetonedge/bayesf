%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% demo for Markov mixtures of multiple regression models with  fixed effects
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% simulate data

% define the true regression model

msregtrue.dist='Normal';
msregtrue.K=2;               
msregtrue.indicmod.dist='Markovchain';
msregtrue.indicmod.init='ergodic';
msregtrue.indicmod.xi=[0.9 0.1;0.2 0.8];



msregtrue.par.sigma=[0.1 0.1];

msregtrue.d=5;
msregtrue.indexdf=[4;5];
msregtrue.par.beta=[-2 3 ;1 -2; 0.3 0.1];
msregtrue.par.alpha=[-4;1];

datareg=simulate(msregtrue,1000);
dataplot(datareg);


%%  Estimate Markov mixtures of mixed-effects regression models for increasing K
 
post=[];
Kmin=1;
Kmax=3;
for K=Kmin:Kmax

    clear msreg;
    msreg.dist='Normal';
    msreg.d=size(datareg.X,1);
    
    msreg.K=K;
    
    msreg.indicmod.dist='Markovchain';
    msreg.indicmod.init='ergodic';

    msreg.indexdf=[4;5];

    [datareg,msreg,mcmc]=mcmcstart(datareg,msreg);

    clear prior;
    prior=priordefine(datareg,msreg);

    mcmcout=mixturemcmc(datareg,msreg,prior,mcmc);
    if isfield(datareg,'S') datareg=rmfield(datareg,'S'); end % starting classification for current K has to be deleted 

    [est,mcmcout]=mcmcestimate(mcmcout);
    
    [marlik,mcmcout]=mcmcbf(datareg,mcmcout)
    post=[post;marlik.bs];

    mcmcout.name= ['store_demo_msreg_mixeffects_K' num2str(K)];
    mcmcstore(mcmcout);

end
%%  Select model with largest marginal likelihood 

[postsort is]=sort(post,1,'descend');
'Ranking according to marginal likelihood'
format bank;[is postsort],format short
Kselect=(Kmin-1)+is(1);

%% evaluate a fitted model for fixed K

K=Kselect;
eval(['load store_demo_msreg_mixeffects_K' num2str(K)]);
mcmcplot(mcmcout);
[est,mcmcout]=mcmcestimate(mcmcout);
