clear mixtrue;

mixtrue.dist='Student';
mixtrue.K=2;

mixtrue.par.mu=[-4 3];
mixtrue.par.sigma=[0.2 0.2];
mixtrue.par.df=[4 10];
mixtrue.weight=[0.4 0.6];


data=simulate(mixtrue,1000);
data.Strue=data.S;
data=rmfield(data,'S');

dataplot(data)

%% run mcmc for increasing number of components

Kmin=1;Kmax=mixtrue.K+1;
for K=Kmin:Kmax

    clear mix;
    mix.dist='Student';
    mix.K=K;

    prior=priordefine(data,mix);
    [data, mix,mcmc]=mcmcstart(data,mix);

    mcmc.M=5000;    mcmc.burnin=2000;

    mcmc.mh.tune.df=2*ones(1,mix.K);

    mcmcout=mixturemcmc(data,mix,prior,mcmc);

    if isfield(data,'S') data=rmfield(data,'S'); end % starting classification for current K has to be deleted

    [est,mcmcout]=mcmcestimate(mcmcout);
    [marlik,mcmcout]=mcmcbf(data,mcmcout)

    mcmcout.name= ['store_mix_student_K' num2str(K)];
    mcmcstore(mcmcout);


end