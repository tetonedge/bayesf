function mix=skewn_parameter(mix,varargin);

if nargin==1
    kall=[1:mix.K];
else
    kall=varargin{1};
end

if mix.r==1
    
     mix.par.lambda=mix.par.psi./(mix.par.sigmaeps.^.5);
     mix.par.sigma= mix.par.sigmaeps+mix.par.psi.^2; 
else
    if ~isfield(mix.par,'sigma')  mix.par.sigma=zeros(size(mix.par.sigmaeps)); end
    if ~isfield(mix.par,'lambda')  mix.par.psi=zeros(size(mix.par.psi)); end

    for k=kall
        
        
                        
        mix.par.sigma(:,:,k)= mix.par.sigmaeps(:,:,k)+mix.par.psi(:,k)*mix.par.psi(:,k)';
        mix.par.sigmainv(:,:,k)=inv(mix.par.sigma(:,:,k));
        mix.par.logdet(1,k)=log(det(mix.par.sigmainv(:,:,k)));
                
         % alte version
               
         %       delta=mix.par.psi(:,k)./diag(mix.par.sigma(:,:,k)).^.5;
         %       Omega=diag(diag(mix.par.sigma(:,:,k)).^.5);
         %       dpsidinv=Omega*mix.par.sigmaepsinv(:,:,k)*Omega;%
         %       mix.par.lambda(:,k)=(dpsidinv*delta)./sqrt(1+delta'*dpsidinv*delta);

         
        % neue version
        
        dum1= squeeze(mix.par.sigmainv(:,:,k))*mix.par.psi(:,k);    % omegainv*psi
        mix.par.lambda(:,k)= (diag(squeeze(mix.par.sigma(:,:,k))).^(.5).*dum1)./sqrt(1-mix.par.psi(:,k)'*dum1);  % 

    end
end