function moments=moments(mix,varargin);
% compute theoretical moments of a model mixture distribution

% Author: Sylvia Fruehwirth-Schnatter
% Last change: July 18, 2006

if nargin==1
    J=4;
else
    J=varargin{1};
end
moments.empty=true;

if ~isfield(mix,'K')   % single member from the distribution family
    mix.K=1; mix.weight=1; 
elseif  and(mix.K==1,~isfield(mix,'weight'))
    mix.weight=1; 
end


if ~isfield(mix,'indicmod') mix.indicmod.dist='Multinomial';  end
if   mix.indicmod.dist(1:6)=='Markov'  if mix.K>1 mix.weight=marstat(mix.indicmod.xi); else mix.weight=1; end;end

finmix=all([~isfield(mix,'d') ~isfield(mix,'ar') ~isfield(mix,'arf')]);

if and(mix.dist(1:6)=='Normal',finmix)
    
    groupm1=mix.par.mu;  % group first moment around 0;
    groupm2= mix.par.mu.^2+  mix.par.sigma; % group second moment around 0;
    
    moments.mean=sum(mix.weight.*mix.par.mu,2);
    moments.var=0;
    
    moments.high=mixturemomentsnor(mix,J,moments.mean)'; 
    moments.var=moments.high(2);
    
    if J>=3 moments.skewness=moments.high(3)/moments.high(2)^1.5;end
    if J>=4  moments.kurtosis=moments.high(4)/moments.high(2)^2;end
    
    moments.B=sum(mix.weight.*(mix.par.mu-moments.mean).^2,2);  
    moments.W=sum(mix.weight.*mix.par.sigma,2);  
    
    moments.R=1-moments.W/moments.var;
    moments.empty=false;
    
    
    
elseif and(mix.dist(1:6)=='Normul',finmix)
    r=size(mix.par.mu,1);
    moments.mean=sum(repmat(mix.weight,r,1).*mix.par.mu,2);
    
    moments.var=zeros(r,r);
    
    
    moments.W=zeros(r,r);
    moments.B=zeros(r,r);
    for k=1:mix.K
        moments.var= moments.var+ (mix.par.mu(:,k)*mix.par.mu(:,k)'+squeeze(mix.par.sigma(:,:,k)))*mix.weight(k);
        moments.W= moments.W+ squeeze(mix.par.sigma(:,:,k))*mix.weight(k);
        d=(mix.par.mu(:,k)-moments.mean);
        moments.B= moments.B+ d*d'*mix.weight(k);
    end
    moments.var=moments.var -   moments.mean*moments.mean';
    
    if r>1
        cd=diag(1./diag(moments.var).^.5);
        moments.corr=cd*moments.var*cd;
    end
    
    moments.Rtr=1-trace(moments.W)/trace(moments.var);
    
    moments.Rdet=1-det(moments.W)/det(moments.var);
    
    
    moments.high=zeros(r,J); 
    if J>=3 moments.skewness=zeros(r,1); end
    moments.kurtosis=zeros(r,1); 
    
    for j=1:r
        mixuni=mixturemar(mix,j);
        moments.high(j,:)=mixturemomentsnor(mixuni,J,moments.mean(j))'; 
        if J>=3  moments.skewness(j)=moments.high(j,3)/moments.high(j,2)^1.5; end
        if J>=4  moments.kurtosis(j)=moments.high(j,4)/moments.high(j,2)^2; end
        
    end    
    moments.empty=false;
    
    
elseif and(mix.dist(1:6)=='Poisso',finmix)
    
    groupm1=mix.par;  % group firstmoment around 0;
    groupm2= mix.par.*(mix.par+1); % group second moment around 0;
    moments.mean=sum(mix.weight.*mix.par,2);
    moments.var=sum(mix.weight.*mix.par.*(mix.par+1),2) - moments.mean^2;
    if mix.K > 1 moments.over= moments.var-moments.mean; else moments.over=0;end
    moments.factorial=zeros(1,J); moments.factorial(1)=moments.mean;
    for m=2:J
        moments.factorial(m)=sum(mix.weight.*mix.par.^m,2);
    end
    
    moments.zero=sum(mix.weight.*exp(-mix.par),2);
    moments.empty=false;
    
    
    
    
elseif finmix
    ['Mixture type ' mix.dist '  not supported by function mixturemoments']
end

if  and(mix.indicmod.dist(1:6)=='Markov' ,finmix)
    if mix.K>1
        moments.ergodic=marstat(mix.indicmod.xi); 
        moments.duration=1./(1-diag(mix.indicmod.xi))';
        moments.eigen=sort(eig(mix.indicmod.xi))';
        moments.empty=false;
        
    else
        mix.indicmod.xi=1;
    end
    
    if or(mix.dist(1:6)=='Normal',mix.dist(1:6)=='Poisso')
        % autocorraltion function for univariate time series models 
        
        lag=20;  moments.ac=zeros(1,lag);  moments.acsqu=zeros(1,lag);
        xh=eye(mix.K);
        
        for h=1:lag
            xh=xh*mix.indicmod.xi;
            moments.ac(1,h)=(mix.weight.*groupm1)*xh*groupm1';
            moments.acsqu(1,h)=(mix.weight.*groupm2)*xh*groupm2';
        end    
        moments.ac=(moments.ac-moments.mean^2)./moments.var;
        if mix.dist(1:6)=='Poisso'
            moments.high2=moments.factorial(2)+moments.mean;
            moments.high4=moments.factorial(4)+6*moments.factorial(3)+7*moments.high2-6*moments.mean;
            moments.acsqu=(moments.acsqu-moments.high2^2)./(moments.high4-moments.high2^2);
        elseif mix.dist(1:6)=='Normal'
            moments.high0=mixturemomentsnor(mix,4,0)'; 
            moments.acsqu=(moments.acsqu-moments.high0(2)^2)./(moments.high0(4)-moments.high0(2)^2);
        end
        moments.empty=false;
        
    end
end

if ~moments.empty moments=rmfield(moments,'empty'); end