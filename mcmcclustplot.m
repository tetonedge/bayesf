function varargout=mcmcclustplot(data,clust,varargin)
% Visualize posterior clustering and time series segmentation

% Author: Sylvia Fruehwirth-Schnatter


plot_Smap=false;

if nargin==2
    ifig=1;
elseif nargin==3
    ifig=fix(varargin{1})+1;
end    

% data are handled as data stored by row
ibycolumn=isfield(data,'bycolumn');
if ibycolumn  ibycolumn=data.bycolumn; end  % ibycolumn true: data stored by column 
if ibycolumn      data.y=data.y'; data.bycolumn ='false'; end   

ibycolumn=isfield(data,'bycolumn');
if ibycolumn  ibycolumn=data.bycolumn; end  % ibycolumn true: data stored by column 
if ibycolumn      data.y=data.y'; data.bycolumn ='false'; end   
if ~isfield(data,'t0') data.t0=1; end

if ~isfield(data,'r') data.r=size(data.y,1); end
if isfield(data,'X') if all(all(data.X==1))  data=rmfield(data,'X'); end; end

if ~isfield(data,'istimeseries') data.istimeseries=false; end
K=size(clust.prob,1);
N=size(clust.prob,2);
if ~isfield(clust,'t0') clust.t0=1; end
T=clust.t0-1+N;

if  clust.t0>data.t0
    data.y=data.y(:,clust.t0:end);
    data.N=size(data.y,2);
    if isfield(data,'X') data.X=data.X(:,clust.t0:end); end
elseif  clust.t0<data.t0
    warn(['Size disagreement between S and data.y in funtion clustplot']);return;
end



if data.istimeseries
    
    figure(ifig);
    for k=1:K
        subplot(K,1,k);
        if isfield(data,'dates')
            nj=data.N/(data.dates(end,1)-data.dates(1,1)+1);
            cal = (data.dates(1,1)-1+[clust.t0:T]'/nj);

            
            bar(cal,clust.prob(k,:)');

            axis([cal(1) cal(end) 0 1]);

        else
            bar([clust.t0:T]',clust.prob(k,:)');

            axis([0 T 0 1]);
             xlabel('t');
        end
        if k==1 title('Smoothed State Probabilities'); end

       
        ylabel(['Prob(S_t=',num2str(k),'|y)']);
    end
    
    for ji=1:data.r
        ifig=ifig+1;
        figure(ifig);
        pmax=2+isfield(clust,'Ssim');
        if data.r>1 
            chts=['Time series ' num2str(ji) ': segmentation'];
        else
            chts='Time series segmentation';
        end
        for j=1:pmax
            if j==1
                subplot(pmax,1,1);
                plotclass(data.y(ji,:),clust.Smap,clust.t0);
                title([chts ' based on Bayesian MAP']);
            elseif j==2
                subplot(pmax,1,2);
                plotclass(data.y(ji,:),clust.Sident,clust.t0);
                title([chts ' based on the Misclassification Rate']);

            elseif and(j==3,isfield(clust,'Ssim'))
                subplot(pmax,1,3);
                plotclass(data.y(ji,:),clust.Ssim,clust.t0);
                title([chts ' based on the Similarity Matrix']);

            end
        end
    end
elseif  ~isfield(data,'X') 
    if data.r==1
        % univariate data 
        
        figure(ifig);
        [ys is]=sort(data.y');
        
        for k=1:K
            subplot(K,1,k);
            % bar(ys,clust.prob(k,is)');
            scatter(ys,clust.prob(k,is)');
            if k==1 title('Estimated Classification Probabilities'); end
            axis([min(data.y)-1 max(data.y)+1 0 1]);
            xlabel('observation y_i');
            ylabel(['Prob(S_i=',num2str(k),'|y_i)']);
        end
        
        
        
        ifig=ifig+1;
        figure(ifig);
        for j=1:3 
            if j==1 
                subplot(3,1,1);    
                plotclasscross(data.y,clust.Smap); 
                title('Clustering based on Bayesian MAP');
            elseif j==2 
                subplot(3,1,2);    
                plotclasscross(data.y,clust.Sident); 
                title('Clustering based on the Misclassification Rate');
                
            elseif and(j==3,isfield(clust,'Ssim'))
                subplot(3,1,3);    
                plotclasscross(data.y,clust.Ssim); 
                title('Clustering based on the Similarity Matrix');
            end
        end    
        
        
    else   
        
        % multivariate data   
        
        singleplot=false;  combinationplot=false;  % single color plot for each estimator 
        singleplot=true;  combinationplot=false;
        combinationplot=true; singleplot=false;  % color plot for each combination, for each estimator 
        
        if singleplot
            
            % - single color plot for each estimator 
            
            for j=1:2+isfield(clust,'Ssim') 
                if j==1 
                    S=clust.Smap; 
                elseif j==2 
                    ifig=ifig+1; 
                    S=clust.Sident; 
                elseif j==3
                    ifig=ifig+1; 
                    S=clust.Ssim; 
                end
                
                figure(ifig);
                K=max(S);
                %c=['ro';'bo';'go';'yo';'mo';'bd';'rd';'gd';'yd';'md';];
                c=['ro';'bo';'go';'yo';'mo';'bd';'rd';'gd';'yd';'md';'ko';'kd'];

                if K>12 'K too large in mcmcclustplot' ,end                
                
                ij=nchoosek([1:data.r]',2);
                [nr, nc]=plotsub(size(ij,1));
                
                for ii=1:size(ij,1)
                    subplot(nr,nc,ii);
                    
                    for k=1:K
                        if sum(S==k)>0  
                            scatter(data.y(ij(ii,1),S==k),data.y(ij(ii,2),S==k),1,c(k,:));hold on; 
                            %text(data.y(ij(ii,1),S==k),data.y(ij(ii,2),S==k),num2str(k));hold on; 
                        end
                    end
                    if isfield(data,'model')   
                        finmix=all([~isfield(data.model,'d') ~isfield(data.model,'ar') ~isfield(data.model,'arf')]);
                        if finmix
                            npoint=100;hold on;
                            mixij=mixturemar(data.model,ij(ii,:));
                            if  data.model.dist(1:6)=='Normul'
                                contmix(mixij,npoint);
                            elseif  data.model.dist(1:6)=='Stumul'
                                contmixstud(mixij,npoint);
                            elseif and(data.model.dist(1:6)=='SkNomu',finmix)
                                contmixskewnormal(mixij,npoint);
                            elseif and(data.model.dist(1:6)=='SkStmu',finmix)
                                contmixskewstud(mixij,npoint);
                            end
                            COLORMAP([0 0 0]);
                        end
                        hold off; 
                    end
                    xlabel(['y_' num2str(ij(ii,1))]);
                    ylabel(['y_' num2str(ij(ii,2))]);
                    axis([min(data.y(ij(ii,1),:),[],2)  max(data.y(ij(ii,1),:),[],2)  min(data.y(ij(ii,2),:),[],2)  max(data.y(ij(ii,2),:),[],2)]);
                    if ii==1
                        if j==1 
                            title('Clustering -- Bayesian MAP');
                        elseif j==2 
                            title('Clustering -- Misclassification Rate');
                        elseif j==3 
                            title('Clustering -- Similarity Matrix');
                        end
                    end    
                    
                end
            end
            
        elseif combinationplot
            
            % - color plot for combination, for each estimator 
            ifig=ifig-1; 
            
            if isfield(data,'model')  
                finmix=all([~isfield(data.model,'d') ~isfield(data.model,'ar') ~isfield(data.model,'arf')]); 
            else 
                finmix=false; 
            end
            
            for j=2-plot_Smap:2+isfield(clust,'Ssim') 
                if j==1 
                    S=clust.Smap; 
                elseif j==2 
                    S=clust.Sident; 
                elseif j==3
                    S=clust.Ssim; 
                end
                K=max(S);
                %c=['ro';'bo';'go';'yo';'mo';'bd';'rd';'gd';'yd';'kd';'md';'co';'cd'];
                c=['ro';'bo';'go';'yo';'mo';'bd';'rd';'gd';'yd';'md';'ko';'kd'];

                
                %c=['ro';'bo';'go';'bo';'bo';'bo';'bo';'bo';'bo';'bo';];  % data 3 set1 K=6, collapse [2 4 5 6]
                %c=['bo';'bo';'go';'bo';'mo';'bo';'bo';'bo';'bo';'bo';];  %data 3 set2 K=6, collapse [1 2 4 6]
                %c=['bo';'go';'bo';'bo';'ro';'bo';'bo';'bo';'bo';'bo';];  % data 3 set3 K=6, collapse [1 3 4 6]
                %c=['bo';'bo';'bo';'bo';'go';'bo';'bo';'bo';'bo';'bo';];   % data 3 set4 K=6, collapse [1 2 3 4 6]
                
                if K>12 'K too large in mcmcclustplot' ,end
                
                
                ij=nchoosek([1:data.r]',2);
                [nr, nc]=plotsub(size(ij,1));
                
                for ii=1:size(ij,1)
                    ifig=ifig+1;
                    figure(ifig);
                    for k=1:K
                        if sum(S==k)>0  
                            scatter(data.y(ij(ii,1),S==k),data.y(ij(ii,2),S==k),[c(k,:)]);hold on; 
                            %text(data.y(ij(ii,1),S==k),data.y(ij(ii,2),S==k),num2str(k));hold on; 
                            
                            %plot_comp=true;
                            plot_comp=false; 
                            
                            if and(finmix,~plot_comp)
                                mixij=mixturemar(data.model,ij(ii,:));
                                npoint=100;
                                if  all(data.model.dist(1:6)=='Normul')
                                    contmix(mixij,npoint);
                                elseif  all(data.model.dist(1:6)=='Stumul')
                                    contmixstud(mixij,npoint);
                                elseif all(data.model.dist(1:6)=='SkNomu') 
                                    contmixskewnormal(mixij,npoint);
                                elseif all(data.model.dist(1:6)=='SkStmu') 
                                    contmixskewstud(mixij,npoint);
                                end
                            elseif and(finmix,plot_comp)
                                options.npoint=100;hold on;
                                mixij=mixturemar(data.model,ij(ii,:));
                                compdens.dist=data.model.dist; compdens.K=1; compdens.weight=1;
                                options.cc=c(k,1); if k>5 options.cc=[c(k,1) '--'];end

                                if  data.model.dist(1:6)=='Normul'
                                    compdens.par=struct('mu',mixij.par.mu(:,k),'sigma',mixij.par.sigma(:,:,k));
                                    contmix(compdens,options);
                                elseif  data.model.dist(1:6)=='Stumul'
                                    compdens.par=struct('mu',mixij.par.mu(:,k),'sigma',mixij.par.sigma(:,:,k),'df',mixij.par.df(k));
                                    contmixstud(compdens,options);
                                 elseif all(data.model.dist(1:6)=='SkNomu') 
                                    compdens.par=struct('mu',mixij.par.mu(:,k),'sigma',mixij.par.sigma(:,:,k),'lambda',mixij.par.lambda(:,k)); 
                                    contmixskewnormal(compdens,npoint);    
                                 elseif all(data.model.dist(1:6)=='SkStmu') 
                                    compdens.par=struct('mu',mixij.par.mu(:,k),'sigma',mixij.par.sigma(:,:,k),'lambda',mixij.par.lambda(:,k),'df',mixij.par.df(k)); 
                                    contmixskewstud(compdens,npoint); 
                                end
                            end
                            
                        end
                    end
                    
                    
                    if and(finmix,~plot_comp)
                        options.npoint=100;hold on;
                        options.cc='k';
                        mixij=mixturemar(data.model,ij(ii,:));
                        if  data.model.dist(1:6)=='Normul' 
                            contmix(mixij,options);  
                        elseif  data.model.dist(1:6)=='Stumul' 
                            contmixstud(mixij,options); 
                        elseif  data.model.dist(1:6)=='SkNomu' 
                            contmixskewnormal(mixij,options);  
                        elseif  data.model.dist(1:6)=='SkStmu' 
                            contmixskewstud(mixij,options); 
                        end
                        hold off; 
                    end
                     
                    xlabel(['y_' num2str(ij(ii,1))]);
                    ylabel(['y_' num2str(ij(ii,2))]);
                    axis([min(data.y(ij(ii,1),:),[],2)  max(data.y(ij(ii,1),:),[],2)  min(data.y(ij(ii,2),:),[],2)  max(data.y(ij(ii,2),:),[],2)]);
                    
                    if j==1 
                        title('Clustering -- Bayesian MAP');
                    elseif j==2 
                        title('Clustering -- Misclassification Rate');
                    elseif j==3 
                        title('Clustering -- Similarity Matrix');
                    end
                    
                end
                
            end
            
            
        else
            
            % - multiple plot for each estimator 
            ifig=ifig-1; 
            
            for j=1:2+isfield(clust,'Ssim') 
                if j==1 
                    S=clust.Smap; 
                elseif j==2 
                    S=clust.Sident; 
                elseif j==3
                    S=clust.Ssim; 
                end
                
                ij=nchoosek([1:data.r]',2);
                [nr, nc]=plotsub(size(ij,1));
                
                for k=1:K
                    if sum(S==k)>0  
                        
                        ifig=ifig+1;
                        figure(ifig);
                        for ii=1:size(ij,1)
                            subplot(nr,nc,ii);
                            %scatter(data.y(ij(ii,1),S==k),data.y(ij(ii,2),S==k),[c(k,:)]);hold on; 
                            text(data.y(ij(ii,1),S==k),data.y(ij(ii,2),S==k),num2str(k));hold on; 
                            xlabel(['y_' num2str(ij(ii,1))]);
                            ylabel(['y_' num2str(ij(ii,2))]);
                            axis([min(data.y(ij(ii,1),:),[],2)  max(data.y(ij(ii,1),:),[],2)  min(data.y(ij(ii,2),:),[],2)  max(data.y(ij(ii,2),:),[],2)]);
                            if ii==1
                                if j==1 
                                    title('Clustering -- Bayesian MAP');
                                elseif j==2 
                                    title('Clustering -- Misclassification Rate');
                                elseif j==3 
                                    title(['Clustering -- Similarity Matrix: ' ]);
                                end
                            end
                            if ii==3
                                if j==1 
                                    title([num2str(sum(S==k)) ' observations']);
                                elseif j==2 
                                    title([num2str(sum(S==k)) ' observations']);
                                elseif j==3 
                                    title([num2str(sum(S==k)) ' observations']);
                                end
                            end
                        end
                    end
                end
                
            end
            
        end
        
    end
elseif isfield(data,'X') 
    % regression  
    
    index=[1:size(data.X,1)];
    ic= any(data.X~=1,2);
    X=data.X(ic,:);
    
    
    [nr, nc]=plotsub(size(X,1));
    
    for j=1:2+isfield(clust,'Ssim') 
        if j==1 
            S=clust.Smap; 
        elseif j==2 
            ifig=ifig+1; 
            S=clust.Sident; 
        elseif j==3
            ifig=ifig+1; 
            S=clust.Ssim; 
        end
        K=max(S);
        c=['b';'r';'g';'y';'k';'m'];
        if K>6 'K too large in mcmcclustplot' ,end
        
        figure(ifig);
        for ii=1:size(X,1)
            subplot(nr,nc,ii);
            for k=1:K
                if sum(S==k)>0  scatter(X(ii,S==k),data.y(:,S==k),[c(k,:)]);hold on; end
            end
            xlabel(['x_' num2str(ii)]);
            ylabel(['y']);
            cc=0.5;
            axis([min(X(ii,:)-cc,[],2)  max(X(ii,:)+cc,[],2)  min(data.y-cc,[],2)  max(data.y+cc,[],2)]);
            if ii==1 
                if j==1 
                    title('Clustering -- Bayesian MAP');
                elseif j==2 
                    title('Clustering -- Misclassification Rate');
                elseif j==3 
                    title('Clustering -- Similarity Matrix');
                end
            end    
        end  
        
    end
    
    
    
end    

if nargout==1
    varargout{1}=ifig;
end    
