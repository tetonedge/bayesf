clear mix;

mix.dist='Stumult';
mix.r=2;mix.K=3;
mix.par.mu=[-2 0 3;-1 0 1];
mix.par.sigma=zeros(mix.r,mix.r,mix.K);
mix.par.sigma(:,:,1)=[.1 0;0 .1];
mix.par.sigma(:,:,2)=[.3 -.15;-.15 .5];
mix.par.sigma(:,:,3)=[.1 0;0 .1];
mix.par.df=[4 20 100];
mix.weight=[0.3 0.3 0.4];



mixtrue=mix;
data=simulate(mixtrue,2000);
mixtrue.omega=data.omega;
data.Strue=data.S;
data=rmfield(data,'S');

dataplot(data)

%% run mcmc

clear mix;
mix.dist=mixtrue.dist;
mix.K=mixtrue.K;
mix.r=mixtrue.r;

% mcmc.startpar=true;mix=mixtrue; 


prior=priordefine(data,mix);
[data, mix,mcmc]=mcmcstart(data,mix);

mcmc.M=5000; mcmc.burnin=1000;

mcmc.mh.tune.df=2*ones(1,mix.K);
%mix.parfix.df=true; mix.par.df=mixtrue.par.df;  %   df fix

mcmcout=mixturemcmc(data,mix,prior,mcmc);

[est,mcmcout]=mcmcestimate(mcmcout)
mcmcplot(mcmcout,3)

[marlik,mcmcout]=mcmcbf(data,mcmcout)

mcmcout.name= 'store_mix_multivariate_student_K3';
mcmcstore(mcmcout);