function [datareg,regvar]=auxmix_binomial(data,loglambda,mixaux)

%% auxiliary mixture sampling for poisson counts

% determines the left hand variable and the variance of the augmented model
%   datareg = log lambda + error, error sim Normal(0,regvar),
%  where
%    datareg= aggregated util - m(R) - log(Ti);
%  (aggregated util is the aggregated and R is the indicator)
%   log(Ti) is 0, if the data are not repeated measurement (i.e. binary observations)

%% input variables
%  data ... data.y contains the observed count
%           data.Ti ...  contains repetion parameter
%           data.N ... number of observations
%  loglambda ... value of the loglinear predictor, vector of size 1 x data.N
%  mixaux  .. initialize with auxmix_initialize_binomial

%% output variables
% datareg ... left hand variable of the augmented normal (regression) model
%             note that the mean of the mixture approximation is already substracted from this variable
%             vector of size 1xN
% regvar ... error variance of the augmented normal (regression) model
%             vector of size 1xN


%% check input

if ~isfield(data,'Ti') 
    data.Ti=ones(1,data.N); % binary data
elseif prod(size(data.Ti))==1
    data.Ti=data.Ti*ones(1,data.N);
end
if any(data.Ti==0)     warn(['binomial distribution requires positive number of repetition']);    end


%% auxiliary mixture sampling

if data.empty  datareg.empty=data.empty;  else datareg.empty=false; end

datav=zeros(1,data.N);
indexv=(data.y~=data.Ti);
datav(1,indexv)=gamrnd(data.Ti(indexv)-data.y(indexv),1,1,sum(indexv));
el=exp(loglambda);
datareg.y=gamrnd(data.Ti,1,1,data.N)./(1+el)+datav./el;
datareg.y=-log(datareg.y);

% sample the INDICATORs

dataaux.y=datareg.y-loglambda;
[classaux ri]=dataclass(dataaux,mixaux,0);
ri=ri';
datareg.y=datareg.y-sum(mixaux.par.mu.*(repmat(ri,1,max(mixaux.K))==repmat([1:max(mixaux.K)],data.N,1)  ),2)';
regvar= sum(mixaux.par.sigma.*(repmat(ri,1,max(mixaux.K))==repmat([1:max(mixaux.K)],data.N,1)  ),2)';
 
clear dataaux classaux ri;
