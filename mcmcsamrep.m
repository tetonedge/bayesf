function varargout=mcmcsamrep(mcmcout,varargin)
% Sampling representation of the MCMC draws mcmcout

% Author: Sylvia Fruehwirth-Schnatter/HW

if nargin==1
    ifig=1;
    data.empty=true;
elseif nargin==2
    if ~isstruct(varargin{1})
        ifig=fix(varargin{1});
        data.empty=true;
    else 
        data=varargin{1};ifig=1;
    end
elseif nargin==3
    data=varargin{1};
    ifig=fix(varargin{2});
end    
sizep=2;cp='ko';%
cp='rx';% cp='go';

if ~isfield(mcmcout.model,'K') mcmcout.model.K=1; end
finmix=all([~isfield(mcmcout.model,'d') ~isfield(mcmcout.model,'ar') ~isfield(mcmcout.model,'arf')]);


if ~isfield(mcmcout.model,'indicmod') mcmcout.model.indicmod.dist='Multinomial';  end


indexscatter=fix(linspace(1,mcmcout.M,min(2000,mcmcout.M)));

%  point process presentations

if and(all(mcmcout.model.dist(1:6)=='Binomi'),finmix)
    figure(ifig);
    if or(median(mcmcout.par(:,1))<0.1,median(mcmcout.par(:,1))>0.1)  plots.logitscat=true; else plots.logitscat=false; end
    if ~plots.logitscat
        for k=1:mcmcout.model.K   
            if  mcmcout.model.indicmod.dist(1:6)=='Markov' 
                scatter(mcmcout.par(indexscatter,k),mcmcout.indicmod.xi(indexscatter,k,k),sizep,cp); 
            else
                scatter(mcmcout.par(indexscatter,k),randn(1,size(indexscatter,2)),sizep,cp);     
            end
            hold on;
        end
        xlabel('\pi');
    else
        for k=1:mcmcout.model.K   
            if  mcmcout.model.indicmod.dist(1:6)=='Markov' 
                scatter(log(mcmcout.par(indexscatter,k))-log(1-mcmcout.par(indexscatter,k)),mcmcout.indicmod.xi(indexscatter,k,k),sizep,cp); 
            else
                scatter(log(mcmcout.par(indexscatter,k))-log(1-mcmcout.par(indexscatter,k)),randn(1,size(indexscatter,2)),sizep,cp); 
            end
            hold on;
        end
        xlabel('logit(\pi)');
    end
    if  mcmcout.model.indicmod.dist(1:6)=='Markov'  ylabel('\xi_{kk}'); else  ylabel('standard normal'); end
    
    title(['Point process representation -- K=' num2str(mcmcout.model.K)]); 

elseif and(mcmcout.model.dist(1:6)=='Poisso',finmix)
    
    figure(ifig);
    if median(mcmcout.par(:,1))<1  plots.logscat=true; else plots.logscat=false; end
    if ~plots.logscat
        for k=1:mcmcout.model.K   
            if  mcmcout.model.indicmod.dist(1:6)=='Markov' 
                scatter(mcmcout.par(indexscatter,k),mcmcout.indicmod.xi(indexscatter,k,k),sizep,cp); 
            else
                scatter(mcmcout.par(indexscatter,k),randn(1,size(indexscatter,2)),sizep,cp);     
            end
            hold on;
        end
        xlabel('\mu');
    else
        for k=1:mcmcout.model.K   
            if  mcmcout.model.indicmod.dist(1:6)=='Markov' 
                scatter(log(mcmcout.par(indexscatter,k)),mcmcout.indicmod.xi(indexscatter,k,k),sizep,cp); 
            else
                scatter(log(mcmcout.par(indexscatter,k)),randn(1,size(indexscatter,2)),sizep,cp); 
            end
            hold on;
        end
        xlabel('log(\mu)');
    end
    if  mcmcout.model.indicmod.dist(1:6)=='Markov'  ylabel('\xi_{kk}'); else  ylabel('standard normal'); end
    
    title(['Point process representation -- K=' num2str(mcmcout.model.K)]); 


    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   
 % HW  --Check!
elseif and(mcmcout.model.dist(1:6)=='Expone',finmix) 
    figure(ifig);
    if median(mcmcout.par(:,1))<1  plots.logscat=true; else plots.logscat=false; end
    if ~plots.logscat
        for k=1:mcmcout.model.K   
            if  mcmcout.model.indicmod.dist(1:6)=='Markov' 
                scatter(mcmcout.par(indexscatter,k),mcmcout.indicmod.xi(indexscatter,k,k),sizep,cp); 
            else
                scatter(mcmcout.par(indexscatter,k),randn(1,size(indexscatter,2)),sizep,cp);     
            end
            hold on;
        end
        xlabel('\mu');
    else
        for k=1:mcmcout.model.K   
            if  mcmcout.model.indicmod.dist(1:6)=='Markov' 
                scatter(log(mcmcout.par(indexscatter,k)),mcmcout.indicmod.xi(indexscatter,k,k),sizep,cp); 
            else
                scatter(log(mcmcout.par(indexscatter,k)),randn(1,size(indexscatter,2)),sizep,cp); 
            end
            hold on;
        end
        xlabel('log(\mu)');
    end
    if  mcmcout.model.indicmod.dist(1:6)=='Markov'  ylabel('\xi_{kk}'); else  ylabel('standard normal'); end
    
    title(['Point process representation -- K=' num2str(mcmcout.model.K)]); 
 
 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
elseif and(or(mcmcout.model.dist(1:6)=='Normal',mcmcout.model.dist(1:6)=='Studen'),finmix)
    figure(ifig);
    for k=1:mcmcout.model.K   scatter(mcmcout.par.mu(indexscatter,k),mcmcout.par.sigma(indexscatter,k),sizep,cp); hold on; end
    xlabel('\mu');
    ylabel('\sigma^2')
    title(['Point process representation -- K=' num2str(mcmcout.model.K)]); 
    
elseif ~finmix  % mixture regression models 
    
    %%% point process for each pair of regression parameters
    
    ifig=ifig+1;    figure(ifig); 
    if ~isfield(mcmcout.model,'indexdf')
        
        mcmcout.model.df=0;
    else
        mcmcout.model.df=prod(size(mcmcout.model.indexdf));
    end
    if (mcmcout.model.d-mcmcout.model.df)>1
        ij=nchoosek([1:(mcmcout.model.d-mcmcout.model.df)]',2);
        [nr, nc]=plotsub(size(ij,1));
    else
        ij=1; nr=1; nc=1;
        
    end
    
    for ii=1:size(ij,1)
        if ii==1  title(['Point process representation -- K=' num2str(mcmcout.model.K)]);  end
        subplot(nr,nc,ii);  
        for j=1:mcmcout.model.K
            if (mcmcout.model.d-mcmcout.model.df)>1
                scatter(mcmcout.par.beta(indexscatter,ij(ii,1),j),mcmcout.par.beta(indexscatter,ij(ii,2),j),sizep,cp);hold on;
                ylabel(['\beta_{.,' num2str(ij(ii,2)) '}']);
            else
                scatter(mcmcout.par.beta(indexscatter,ij(ii,1),j),randn(1,size(indexscatter,2)),sizep,cp);hold on;
                ylabel('standard normal');
            end
        end
        xlabel(['\beta_{.,' num2str(ij(ii,1)) '}']);
    end
    
    if  or(mcmcout.model.dist(1:6)=='Normal',mcmcout.model.dist(1:6)=='Studen')
        ifig=ifig+1;    figure(ifig); 
        for k=1:mcmcout.model.K   scatter(mcmcout.par.sigma(indexscatter,k),randn(1,size(indexscatter,2)),sizep,cp); hold on;end
        ylabel('standard normal');
        %for k=1:mcmcout.model.K   scatter(mcmcout.par.sigma(indexscatter,k),mcmcout.Nk(indexscatter,k),sizep,cp); hold on;end
        %ylabel('group size');
        xlabel('\sigma^2');
        title(['Point process representation for the variance -- K=' num2str(mcmcout.model.K)]); 
    end
    
    if ~isfield(data,'empty') 
        
        ifig=ifig+1;    figure(ifig); 
        
        [xmin imin]=min(data.X,[],2);
        [xmax imax]=max(data.X,[],2);
        [nr, nc]=plotsub(mcmcout.model.d-1);
        
        for k=1:mcmcout.model.K 
            isub=0;
            for ij=1:mcmcout.model.d
                %Xdes=[data.X(:,imin(ij)) data.X(:,imax(ij))];
                Xdes=data.X;
                if (xmin(ij)-xmax(ij))~=0
                    isub=isub+1;
                    subplot(nr,nc,isub);
                    if k==1 scatter(data.X(ij,:),data.y);  hold on; end
                    
                    if mcmcout.model.df>0
                        intercept=[1:mcmcout.model.d];
                        indexdr=intercept(all(repmat(mcmcout.model.indexdf,1,mcmcout.model.d)~=repmat(intercept,mcmcout.model.df,1),1));
                        mu=Xdes(indexdr,:)'*mcmcout.par.beta(indexscatter,:,k)';
                        mu=mu+ Xdes(mcmcout.model.indexdf,:)'*mcmcout.par.alpha(indexscatter,:)';
                    else    
                        mu=Xdes'*mcmcout.par.beta(indexscatter,:,k)';
                    end
                    if mcmcout.model.dist(1:6)=='Poisso'  mu=exp(mu); end
                    [Xdes,is]=sort(Xdes(ij,:));
                    scatter(Xdes,mean(mu(is,:)',1),'r');
                    
                    %plot(Xdes(ij,[imin(ij) imax(ij)]),mu([imin(ij) imax(ij)],:)','g');
                    xlabel(['x' num2str(ij)]);
                end
            end
        end
    end


elseif any([strcmp(mcmcout.model.dist(1:6),'Normul') strcmp(mcmcout.model.dist(1:6),'Stumul')])
    r=size(mcmcout.par.mu,2);
    
    indexdiag=[1:fix(r*(r+1)/2)]';        
    indexdiag=diag(qinmatr(indexdiag))';     
    
    %%% point process for each marginal density
    figure(ifig);
    for m=1:r
        
        if m==1 [nr, nc]=plotsub(r); end
        subplot(nr,nc,m);
        
        for j=1:mcmcout.model.K
            scatter(squeeze(mcmcout.par.mu(indexscatter,m,j)),squeeze(mcmcout.par.sigma(indexscatter,indexdiag(m),j)),sizep,cp);
            hold on;
            if ~mcmcout.ranperm  text(mean(squeeze(mcmcout.par.mu(indexscatter,m,j))),mean(squeeze(mcmcout.par.sigma(indexscatter,indexdiag(m),j))),num2str(j));end
        end
        
        title(['Feature ' num2str(m)]);
        xlabel(['\mu_{.,' num2str(m) '}']);
        ylabel(['\Sigma_{.,' num2str(m) num2str(m) '}']);
    end
    
    
    %%% point process for the means of each bivariate density
    
    ifig=ifig+1;
    figure(ifig); 
    ij=nchoosek([1:r]',2);
    [nr, nc]=plotsub(size(ij,1));
    
    for ii=1:size(ij,1)
        subplot(nr,nc,ii);  
        for j=1:mcmcout.model.K
            scatter(squeeze(mcmcout.par.mu(indexscatter,ij(ii,1),j)),squeeze(mcmcout.par.mu(indexscatter,ij(ii,2),j)),sizep,cp);
         hold on;
         if ~mcmcout.ranperm text(mean(squeeze(mcmcout.par.mu(indexscatter,ij(ii,1),j))),mean(squeeze(mcmcout.par.mu(indexscatter,ij(ii,2),j))),num2str(j));end

        end
        
        xlabel(['\mu_{.,' num2str(ij(ii,1)) '}']);
        ylabel(['\mu_{.,' num2str(ij(ii,2)) '}']);
    end
    
    ifig=ifig+1;
    figure(ifig); 
    
    
    subplot(1,2,1);  
    for k=1:mcmcout.model.K;
        scatter(squeeze(mcmcout.par.logdet(indexscatter,k)),sum(mcmcout.par.sigma(indexscatter,indexdiag,k),2),sizep,cp);
        hold on;
    end
    xlabel(['log(det \Sigma^{-1})']);
    ylabel(['trace(\Sigma)']);
    
    subplot(1,2,2);
    for k=1:mcmcout.model.K;
        ew=zeros(size(indexscatter,2),r);
        for m=1:size(indexscatter,2)
            ew(m,:)=eig(qinmatr(squeeze(mcmcout.par.sigma(indexscatter(m),:,k))'))';
        end    
        scatter(ew(:,1),ew(:,end),sizep,cp);
        hold on;
    end
    xlabel(['smallest eigenvalue of \Sigma']);
    ylabel(['largest eigenvalue of \Sigma']);
    
elseif  strcmp(mcmcout.model.dist(1:6),'SkStmu') 
    
    r=size(mcmcout.par.mu,2);
    
    %%% point process for the means of each bivariate density
    
    % compute the means
    
    %indexdf=all(mcmcout.par.df>1,2);
    
    %mcmcoutmean=zeros(size(mcmcout.par.mu));
    %mix.K=mcmcout.model.K;
    %mix.r=mcmcout.model.r;
    %for m=1:mcmcout.M
    %    mix.par.mu=squeeze(mcmcout.par.mu(m,:,:));
    %    mix.par.lambda=squeeze(mcmcout.par.lambda(m,:,:));
    %    mix.par.sigma= qinmatrmult(squeeze(mcmcout.par.sigma(m,:,:)));
    %    mix=skewn_transform(mix);
    %    mcmcoutmean(m,:,:)=  mix.par.psi;
    %end
    %scale=sqrt(2/pi)*sqrt(mcmcout.par.df(indexdf,:)/2).*exp(gammaln((mcmcout.par.df(indexdf,:)-1)/2)-gammaln(mcmcout.par.df(indexdf,:)/2));

    %mcmcoutmean=mcmcout.par.mu(indexdf,:,:)+permute(repmat(scale,[1 1 mix.r]),[1 3 2]).*mcmcoutmean(indexdf,:,:);
    
    
    ['mean/lambda(av,sd)/df/weight']
    if ~mcmcout.ranperm
    tab=maketab([squeeze(mean(mcmcout.par.mean,1));squeeze(mean(mcmcout.par.lambda,1));squeeze(std(mcmcout.par.lambda,1));median(mcmcout.par.df);mean(mcmcout.weight)])
    else
    tab=maketab([squeeze(mean(mcmcout.parperm.mean,1));squeeze(mean(mcmcout.parperm.lambda,1));squeeze(std(mcmcout.parperm.lambda,1));median(mcmcout.parperm.df);mean(mcmcout.weightperm)])
    end
    ifig=ifig+1;
    figure(ifig); 
    ij=nchoosek([1:r]',2);
    [nr, nc]=plotsub(size(ij,1));
    
    plot_scatter=true;
    
    for ii=1:size(ij,1)
        subplot(nr,nc,ii);

        for j=1:mcmcout.model.K
            if plot_scatter
                scatter(squeeze(mcmcout.par.mean(indexscatter,ij(ii,1),j)),squeeze(mcmcout.par.mean(indexscatter,ij(ii,2),j)),sizep,cp);
                hold on;
            end
            if ~isfield(mcmcout,'randperm') text(mean(squeeze(mcmcout.par.mean(indexscatter,ij(ii,1),j))),mean(squeeze(mcmcout.par.mean(indexscatter,ij(ii,2),j))),num2str(j));end
        end

        xlabel(['\mu_{.,' num2str(ij(ii,1)) '}']);
        ylabel(['\mu_{.,' num2str(ij(ii,2)) '}']);
    end
    
else
    ['Point representation of simulated mixture models through mcmcsamrep not supported for mixtures of type ' mcmcout.model.dist]
end  

biv_marg=false;
if biv_marg
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  bivariate marginal distributions for selected models

if  and(mcmcout.model.K>1,isfield(mcmcout,'ranperm'))
    if mcmcout.ranperm
        ifig=ifig+1;
        
        if and(mcmcout.model.dist(1:6)=='Poisso',finmix)
            figure(ifig);
            scatter(mcmcout.par(indexscatter,1),mcmcout.par(indexscatter,2),sizep,cp);
            xlabel('\mu_1');
            ylabel('\mu_2');
            axis([min(mcmcout.par(indexscatter,1)) max(mcmcout.par(indexscatter,1)) min(mcmcout.par(indexscatter,2)) max(mcmcout.par(indexscatter,2))]);
            title(['Sampling representation of p(\mu_1,\mu_2|y) -- K=' num2str(mcmcout.model.K)]);
            
        elseif and(mcmcout.model.dist(1:6)=='Normal',finmix)
            figure(ifig);
            scatter(mcmcout.par.mu(indexscatter,1),mcmcout.par.mu(indexscatter,2),sizep,cp);
            xlabel('\mu_1');
            ylabel('\mu_2');
            axis([min(mcmcout.par.mu(indexscatter,1)) max(mcmcout.par.mu(indexscatter,1)) min(mcmcout.par.mu(indexscatter,2)) max(mcmcout.par.mu(indexscatter,2))]);
            title(['Sampling representation of p(\mu_1,\mu_2|y) -- K=' num2str(mcmcout.model.K)]);
            
            ifig=ifig+1;
            figure(ifig);
            scatter(mcmcout.par.sigma(indexscatter,1),mcmcout.par.sigma(indexscatter,2),sizep,cp);
            xlabel('\sigma^2_1');
            ylabel('\sigma^2_2');
            axis([min(mcmcout.par.sigma(indexscatter,1)) max(mcmcout.par.sigma(indexscatter,1)) min(mcmcout.par.sigma(indexscatter,2)) max(mcmcout.par.sigma(indexscatter,2))]);
            
            title(['Sampling representation of p(\sigma^2_1,\sigma^2_2|y) -- K=' num2str(mcmcout.model.K)]);
        end  
    end
end
end


if nargout==1
    varargout{1}=ifig;
end    
