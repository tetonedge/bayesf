function Q = qinmatrmult(Qcol)

% fuer symm. Matrizen Umkehrung zu Programm qincol

% wandelt eine Spalte der dim dd*(dd+1)/2 eine quadrat Matrix der dim dd in eine quadrat Matrix der dim dd um
% wobei die Elemente der Spalte in die obige Dreiecksmatrix spaltenweise nebeneinander geschrieben werden
% und die Matrix zu einer symmetr. Matrix ergaenzt wird

% input: Qcol .. [dd*(dd+1)/2]x1
% output: Q .. ddxdd

D = size(Qcol,1);

k=size(Qcol,2);
dd = -.5 + (.25 + 2*D)^.5;

if ~(fix(dd)==dd) 
    warn('size inconsistency in function qinmatrmult');
end

Q=zeros(dd,dd,k);

for j=1:k
     
    hilf = 0;
    for i = 1:dd,
        Q(1:i,i,j) = Qcol(hilf+1:hilf+i,j);
        Q(i,1:i,j) = Qcol(hilf+1:hilf+i,j)';
        hilf =hilf + i;
    end   
    
end