function nfig=mixtureplot_biv(model,nfig)

nfig=nfig+1;figure(nfig);

ij=nchoosek([1:model.r]',2);
[nr, nc]=plotsub(size(ij,1));

for ii=1:size(ij,1)
    subplot(nr,nc,ii);

    if ~isfield(model,'K') model.K=1;  model.weight=1; end
    npoint=100;hold on;
    mixij=mixturemar(model,ij(ii,:));
    plot_components=false;

    if ~plot_components
        if  model.dist(1:6)=='Normul'
            contmix(mixij,npoint);
        elseif  model.dist(1:6)=='Stumul'
            contmixstud(mixij,npoint);
        elseif  model.dist(1:6)=='SkNomu'
            contmixskewnormal(mixij,npoint);
        elseif  model.dist(1:6)=='SkStmu'
            contmixskewstud(mixij,npoint);
        end
    else
        compdens.dist=model.dist; compdens.K=1; compdens.weight=1;

        for k=1:model.K;
            if  model.dist(1:6)=='Normul'
                compdens.par=struct('mu',mixij.par.mu(:,k),'sigma',mixij.par.sigma(:,:,k));
                contmix(compdens,npoint);
            elseif  model.dist(1:6)=='Stumul'
                compdens.par=struct('mu',mixij.par.mu(:,k),'sigma',mixij.par.sigma(:,:,k),'df',mixij.par.df(k));
                contmixstud(compdens,npoint);
            elseif  model.dist(1:6)=='SkNomu'
                compdens.par=struct('mu',mixij.par.mu(:,k),'sigma',mixij.par.sigma(:,:,k),'lambda',mixij.par.lambda(:,k));
                contmixskewnormal(compdens,npoint);
            elseif  model.dist(1:6)=='SkStmu'
                compdens.par=struct('mu',mixij.par.mu(:,k),'sigma',mixij.par.sigma(:,:,k),'lambda',mixij.par.lambda(:,k));
                contmixskewstud(compdens,npoint);
            end
        end
        %legend(num2str([1:model.K]'));
    end
    xlabel(['y_' num2str(ij(ii,1))]);
    ylabel(['y_' num2str(ij(ii,2))]);

end
