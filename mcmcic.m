function [ic,varargout]=mcmcic(data,mcmcout)     
% Compute information from MCMC draws for a finite mixture model 

% Author: Sylvia Fruehwirth-Schnatter
% Last change: September 5, 2006

%  maximum of the mixture likelihood 

mix=mcmcout.model;
if ~isfield(mix,'K') mix.K=1; end

if ~isfield(mcmcout,'em')  
    [logm is]=sort(mcmcout.log.mixlik);  % approximate ML estimator
    loglikmax=logm(end);
    mldraw=is(end);
    if mix.K>1     entropy_ml=mcmcout.entropy(mldraw);  end
    
else       % use the result from the EM algorithm
    loglikmax=mcmcout.em.loglikmax;
    if mix.K>1   entropy_ml=mcmcout.em.entropy;   end
end  
ic.loglikmax=loglikmax;

[logpm is]=sort(mcmcout.log.mixlik+mcmcout.log.mixprior);  % approximate posterior mode estimator
pmdraw=is(end);
if mix.K>1     entropy_pm=mcmcout.entropy(pmdraw);  end

% loglikpm=logpm(end);  % log posterior density
loglikpm=mcmcout.log.mixlik(pmdraw);  % log likelihood



%%%%%%%%%%%  number of parameters  
%%%  
dk=0;


%%  Model for the indicator
if mix.K>1
    if  ~isfield(mix,'indicmod')   mix.indicmod.dist='Multinomial';   end

    if ~isfield(mix,'indicfix')
        if mix.indicmod.dist(1:6)=='Multin' 
            dk=mix.K-1;
        elseif mix.indicmod.dist(1:6)=='Markov' 
            dk=(mix.K-1)*mix.K;
        end
    end
end

if mix.dist(1:6)=='Normul'
    if ~isfield(mix,'r') mix.r=data.r;  end  % single member from the distribution family

    dk=dk+mix.K*(mix.r+mix.r*(mix.r+1)/2);

  
    
elseif mix.dist(1:6)=='SkNomu'
    
     if ~isfield(mix,'r') mix.r=data.r;  end   

    dk=dk+mix.K*(2*mix.r+mix.r*(mix.r+1)/2);

   
elseif mix.dist(1:6)=='Stumul'
    if ~isfield(mix,'r') mix.r=data.r;  end   

    dk=dk+mix.K*(1+mix.r+mix.r*(mix.r+1)/2);
    
elseif mix.dist(1:6)=='SkStmu'    
    
if ~isfield(mix,'r') mix.r=data.r;  end   

    dk=dk+mix.K*(1+2*mix.r+mix.r*(mix.r+1)/2);
    
elseif mix.dist(1:6)=='Normal'    

    if ~isfield(mix,'d')
        dk=dk+mix.K*2;
         
    elseif ~isfield(mix,'df')
        dk=dk+mix.K*(mix.d+1);
    else
        dk=dk+mix.K*((mix.d-mix.df)+1)+mix.df;
    end
    
    
elseif mix.dist(1:6)=='Studen' 
     
    dk=dk+mix.K*3;
    
    
elseif mix.dist(1:6)=='SkewNo' 
     
    dk=dk+mix.K*3;
    
elseif mix.dist(1:6)=='SkewSt' 
     
    dk=dk+mix.K*4;
    
elseif all(mix.dist(1:6)=='Poisso')    

    if ~isfield(mix,'d')
        dk=dk+mix.K;
         
    elseif ~isfield(mix,'df')
        dk=dk+mix.K*mix.d;
    else
        dk=dk+mix.K*(mix.d-mix.df)+mix.df;
    end
    
elseif all(mix.dist(1:6)=='Expone')
     dk=dk+mix.K;
else
    'ADD MIXTURE DISTRIDUTION IN mcmcic'
    
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

ic.loglikmax=loglikmax;
ic.aic= -2*loglikmax+2*dk;
ic.bic= -2*loglikmax+log(data.N)*dk;
ic.bicpm= -2*loglikpm+log(data.N)*dk;
ic.bicmc=-2*mean(mcmcout.log.mixlik)+dk*(log(data.N)-1);

if mix.K>1
    ic.iclbic_ml= ic.bic+2*entropy_ml;
    ic.iclbic_pm= ic.bicpm+2*entropy_pm;
else
    ic.iclbic_ml= ic.bic;
    ic.iclbic_pm= ic.bicpm;

end
%ic.awe=ic.iclbic+(3/2+log(data.r*data.N))*dk;
ic.awe=ic.iclbic_ml+(3/2+log(data.N))*dk;
ic.d=dk;

mcmcout.ic=ic;
if nargout==2
    varargout{1}=mcmcout;
end    