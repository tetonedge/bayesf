%% Fisher's iris data
%  
%  fit a multivariate normal mixture with 3 components

clear data; clear mix; clear prior; 

%% load the data

data=dataget('iris');
    
%% define the structure of the mixture

mix.dist='Normul';
mix.K=3;

%% define the prior

prior=priordefine(data,mix);

%% MCMC

[data,mix,mcmc]=mcmcstart(data,mix);
mcmcout=mixturemcmc(data,mix,prior,mcmc);

%% Plot MCMC

ifig=mcmcplot(mcmcout,10);

%% compute the marginal likelihood

[marlik,mcmcout]=mcmcbf(data,mcmcout)
marlik.bs     % bridge sampling estimator of the marginal likelihood

%% clustering

[clust,mcmcout]=mcmcclust(data,mcmcout);
mcmcclustplot(data,clust,ifig)

%% store the results
mcmcout.name='store_iris_K3';
mcmcstore(mcmcout);
     
