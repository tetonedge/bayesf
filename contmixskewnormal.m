function  contmixskewnormal(mix,options);


% contourplot for a bivariate mixture of skew normal distributions

% Author: Sylvia Fruehwirth-Schnatter
% Last change: June 13, 2007

if isfield(options,'npoint') npoint=options.npoint; else npoint=100; end
if isfield(options,'cc') cc=options.cc; else cc='k'; end
if ~isfield(mix,'K') mix.K=1;mix.weight=1;  end

mix1=mixturemar(mix,1);

    mix.par.delta=mix1.par.lambda./(1+mix1.par.lambda.^2).^.5;
    mix1.par.mumarg=mix1.par.mu+0.8*sqrt(mix1.par.sigma).*mix.par.delta;
    mix1.par.smarg=mix1.par.sigma.*(1-0.64*mix.par.delta.^2); 
    [ss is]=sort(mix1.par.mumarg); %sort the means to define the domain of the plot
    c=5; % define the lower and the upper bound from the quantile    

x1min=mix1.par.mumarg(is(1))-c*sqrt(max(mix1.par.smarg));
x1max=mix1.par.mumarg(is(mix.K))+c*sqrt(max(mix1.par.smarg));
x1=linspace(x1min,x1max,npoint);

mix1=mixturemar(mix,2);

    mix.par.delta=mix1.par.lambda./(1+mix1.par.lambda.^2).^.5;
    mix1.par.mumarg=mix1.par.mu+0.8*sqrt(mix1.par.sigma).*mix.par.delta;
    mix1.par.smarg=mix1.par.sigma.*(1-0.64*mix.par.delta.^2); 
    [ss is]=sort(mix1.par.mumarg); %sort the means to define the domain of the plot
    c=5; % define the lower and the upper bound from the quantile    

x2min=mix1.par.mumarg(is(1))-c*sqrt(max(mix1.par.smarg));
x2max=mix1.par.mumarg(is(mix.K))+c*sqrt(max(mix1.par.smarg));
x2=linspace(x2min,x2max,npoint);


[x1,x2]=meshgrid(x1,x2);

L=zeros(size(x1)); 

if and(isfield(mix.par,'sigmainv'),isfield(mix.par,'logdet')) 
    Qinv=mix.par.sigmainv;
    detQ=exp(mix.par.logdet);
else
    Qinv=zeros(size(mix.par.sigma)); detQ=zeros(1,mix.K);    
    for k=1:mix.K;
        Qinv(:,:,k)=inv(mix.par.sigma(:,:,k));detQ(k)=det(Qinv(:,:,k));
    end
end

r=2; % bivariate density
for k=1:mix.K;
    d1=x1-mix.par.mu(1,k);
    d2=x2-mix.par.mu(2,k);
    omegainv=diag(squeeze(mix.par.sigma(:,:,k))).^(-.5);
    FN=2*normcdf(mix.par.lambda(1,k)*omegainv(1)*d1 + mix.par.lambda(2,k)*omegainv(2)*d2);
    L=L+mix.weight(k)*detQ(k)^.5*exp(-(d1.^2/2*Qinv(1,1,k)+ d1.*d2*Qinv(1,2,k) + d2.^2/2*Qinv(2,2,k) )).*FN;
end
ML=max(max(L));

 
%[c,h]=contour(x1,x2,L,50,cc); 
%[c,h]=contour(x1,x2,L,[0.001 0.01 0.05:0.05:1]*ML,cc);

%[c,h]=contour(x1,x2,L,exp([-9:1:-1 -1:0.2:0])*ML,cc);

Ls=sort(reshape(L,npoint^2,1));
frac=0.001;
frac=0.01;
Lslarge=Ls(Ls>frac*ML);
nc=20;
dd=fix(size(Lslarge)/nc);
v=Lslarge(dd:dd:end);
Lssmall=Ls(and(Ls<=frac*ML,Ls>0.01*frac*ML));
Lss=Lssmall([1==1;diff(Lssmall)./Lssmall(2:end)>1e-3]);
nc=20;
dd=fix(size(Lss)/nc);
v=[Lss(7*dd:dd:end-1);v];
[c,h]=contour(x1,x2,L,v,cc);

%  axis equal
set(gca,'xlim',[x1min x1max]);
set(gca,'ylim',[x2min x2max]);
%  title('Posterior of (\mu_1,\mu_2)');
%  hold on;
%  plot(xlim,xlim);

% Posterior of sigma  

 