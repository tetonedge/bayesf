clear mix;
%% simulate data
mix.dist='Stumult';
mix.r=2;
mix.K=3;
mix.par.mu=[-2 0 3;-1 0 1];
mix.par.sigma=zeros(mix.r,mix.r,mix.K);
mix.par.sigma(:,:,1)=[.1 0;0 .1];
mix.par.sigma(:,:,2)=[.3 -.15;-.15 .5];
mix.par.sigma(:,:,3)=[.1 0;0 .1];
mix.par.df=[4 20 100];
mix.weight=[0.3 0.3 0.4];



mixtrue=mix;
data=simulate(mixtrue,2000);
mixtrue.omega=data.omega;
data.Strue=data.S;
data=rmfield(data,'S');

dataplot(data)


%% run mcmc

Kmin=1;Kmax=4;


for K=Kmin:Kmax

    clear mix;
    mix.K=K; 
    mix.dist='Stumult';
    mix.r=mixtrue.r;


    prior=priordefine(data,mix);
    [data, mix,mcmc]=mcmcstart(data,mix);

    mcmc.M=5000; mcmc.burnin=2000;

    if mix.K==1
        mcmc.mh.tune.df=2;
    elseif mix.K==2
        mcmc.mh.tune.df=[2 2];
    elseif mix.K==3
        mcmc.mh.tune.df=[2 2 2];
    elseif mix.K==4
        mcmc.mh.tune.df=[3 3 3 3];    
    end

    mcmcout=mixturemcmc(data,mix,prior,mcmc);

    if isfield(data,'S') data=rmfield(data,'S'); end % starting classification for current K has to be deleted 

    [est,mcmcout]=mcmcestimate(mcmcout);
    [marlik,mcmcout]=mcmcbf(data,mcmcout)

    mcmcout.name= ['store_mix_multivariate_student_K' num2str(K)];
    mcmcstore(mcmcout);
end
