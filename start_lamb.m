%%  Data: lamb data

data=dataget('lamb');

%%  Estimate a Markov mxiture of Poisson distributions for increasing K 
 
post=[];
Kmin=1;
Kmax=4;
for K=Kmin:Kmax

    clear mix;
    mix.dist='Poisson';
    mix.K=K;
    mix.indicmod.dist='Markovchain';    
    mix.indicmod.init='ergodic'; 
    
    [data,mix,mcmc]=mcmcstart(data,mix);  
    clear prior;
    
    % use default prior
    % prior=priordefine(data,mix);
    
    %% use the same prior as Fruehwirth-Schnatter (2006), Section 11.7.3
    prior.hier=false;
    prior.par.a=ones(1,mix.K);
    prior.par.b=0.5*ones(1,mix.K);
    if mix.K>1  prior.indicmod.xi=ones(mix.K)/(mix.K-1);   for k=1:mix.K prior.indicmod.xi(k,k)=2; end; end

    mcmcout=mixturemcmc(data,mix,prior,mcmc);
    if isfield(data,'S') data=rmfield(data,'S'); end % starting classification for current K has to be deleted 

    [marlik,mcmcout]=mcmcbf(data,mcmcout);
    post=[post;marlik.bs];
    
    mcmcout.name= ['store_' data.name '_marmix_K' num2str(K)];
    mcmcstore(mcmcout);
end

%%  Select model with largest marginal likelihood 

[postsort is]=sort(post,1,'descend');
'Ranking according to marginal likelihood'
format bank;[is postsort],format short
Kselect=(Kmin-1)+is(1);

%% evaluate a fitted model for fixed K

K=Kselect;
eval(['load store_' data.name '_marmix_K' num2str(K)]);
mcmcplot(mcmcout);
[est,mcmcout]=mcmcestimate(mcmcout);

'Switching mean'
est.ident.par 

'Transition matrix'

est.ident.indicmod.xi 

