function plotclass(y,S,varargin);

if nargin==2 % define the first time point
   t0=1;
else   
   t0=varargin{1};
end   

K=max(S);
c=['b';'r';'g';'y';'k';'m'];
if K>6 'K too large in mixture point' ,end

indexn=t0-1+[1:size(S,2)];

for k=1:K
    scatter(indexn(1,S==k),y(1,S==k),c(k),'filled');
    hold on;
end    
%axis([0 indexn(end)+1 min(0,min(y)-1) max(y)+1]); 