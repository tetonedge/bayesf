function lh = likelihoodeval(data,mix)

% computes the log likelihood of model mix

% Author: Sylvia Fruehwirth-Schnatter

lh=0;   

if isfield(data,'empty') if data.empty return; end; end
    
if ~isfield(mix,'indicfix') mix.indicfix=false; end
if ~isfield(mix,'K') mix.K=1; end

class=dataclass(data,mix,0);
if or(~mix.indicfix,mix.K==1)     
    %  compute the log likelihood for a model with K=1 or
    %  the log mixture likleihood for a mixture model with unknown allocations
    % 
    
    lh=class.mixlik; 
else  
    % computes the log of the complete data likelihood for a mixture model
    % with known allocations

    lh=sum(class.loglikcd);
end        
        
   
    
