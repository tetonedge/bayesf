function likcd=likeli_skewstudmult_cd(data,mix)

likcd=zeros(mix.K,1);

if or(~isfield(mix.par,'sigmainv'),isfield(mix.par,'logdet'))
    mix.par.sigmainv=zeros(size(mix.par.sigma)); mix.par.logdet=zeros(1,mix.K);
    for k=1:mix.K;
        mix.par.sigmainv(:,:,k)=inv(mix.par.sigma(:,:,k));
        mix.par.logdet(k)=log(det(mix.par.sigmainv(:,:,k)));
    end
end

for k=1:mix.K;
    y=data.y(:,data.S==k);
    [dum1  dum2 llh] = likeli_skewstudmult(y,mix.par.mu(:,k),mix.par.sigma(:,:,k),mix.par.sigmainv(:,:,k),mix.par.logdet(k),mix.par.df(k),mix.par.lambda(:,k));
    likcd(k)=sum(llh);
end

