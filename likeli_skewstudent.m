function [lh,maxl,llh] = likeli_skewstudent(y,beta,sgma2,df,lambda)
%% computes the pointwise likelihood of a skew-t distribution
% defined over K groups with different parameters -- mainly used for classification
%
% input: y ... observations ... array of size 1 times N
%        group specific parameters of the ST(beta,sgma2,lambda,df)
%        distribution: 
%        beta ... location parameter, 
%                 array of size K times 1, if identical for all observations 
%                 array of size K times N, if different for each observation
%        sgma2 .. scale parameter
%                 array of size K times 1, if identical for all observations 
%                 array of size K times N, if different for each observation
%        df  .... tail index defined by degrees of freedom
%                 array of size K times 1, if identical for all observations 
%                 array of size K times N, if different for each observation
%        lambda ... skewness parameter
%                 array of size K times 1, if identical for all observations 
%                 array of size K times N, if different for each observation
% output: llh ... pointwise LOG LIKELIHOOD including all normalising constant
%                 array of size K times N 
%         maxl ... maximum of llh for each row, array of size K times 1 
%         lh  ... pointwise LIKELIHOOD after removing a normalising constant
%                 corresponding to maxl, i.e. ll=exp(llh-repmat(maxl,1,N));
%                 array of size K times N

%%  vectorize the evaluation of the log-likelihood
n=size(y,1);
nst=size(beta,1);

if  size(beta,2)==1    beta=beta(:,ones(1,n)); end
if  size(sgma2,2)==1   sgma2=sgma2(:,ones(1,n));  end
if  size(df,2)==1      df=df(:,ones(1,n));  end
if  size(lambda,2)==1  lambda=lambda(:,ones(1,n)); end
   

%% evaluate the factor in the log-likelihood that depends on the pdf of a
%  univariate t-distribution

sgma=sgma2.^0.5;
z=(y(:,ones(nst,1))' - beta)./sgma;
err=z.^2;
llh = gammaln((df+1)/2)-gammaln(df/2) -.5*(log(df*pi) + log(sgma2)) -(df+1)/2.*log(1+err./df);
sc=((df+1)./(df+err)).^.5;

%% evaluate the factor in the log-likelihood that depends on the cdf of a
%  univariate t-distribution

% built-in MATLAB function tcdf is too slow; we use eval_tcdf_skewt

% tc=tcdf(lambda.*sc.*z,1+df);   
tc = zeros(nst,n); 
for k=1:nst
    tc(k,:)= max(eval_tcdf_skewt(lambda(k,:).*sc(k,:).*z(k,:),1+df(k)),exp(-700)); 
end

%% finalize
llh = llh + log(2)+ log(tc);
maxl = max(llh,[],1);
lh = exp(llh - maxl(ones(nst,1),:));

