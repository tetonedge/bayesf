function priorq=logprior_mixpoi_hpmarg(K,lambda,a0,g0,G0);

%hp prior with hyperparameter integrated out

% use candidates formula

gN=K*a0+g0;
GN=(sum(lambda,1)+G0)';
b0= gN./GN;

M=size(lambda,2);

par=repmat([a0 G0],[K 1 M]);
par(:,2,:)=b0(:,ones(1,K))';


priorq = prodgamlog(lambda,par);
%end
%priorq=priorq+prodgamlog(b0',[g0(ones(1,M))  G0(ones(1,M)) ]);
par=repmat([g0 G0],[1 1 M]);
priorq=priorq+prodgamlog(b0',par);
par(1,1,:)=gN;
par(1,2,:)=GN;
priorq=priorq-prodgamlog(b0',par);
