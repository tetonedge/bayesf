function [lh, maxl,llh] = likeli_poisson(y,lmda)
% LIKELI likelihood (ohne kuerzen der normierungskonstnate)
% function call: lh = likeli(y,lmda)

n=size(y,1);
nst = size(lmda,1);

if  size(lmda,2)==1
    lmda=lmda(:,ones(1,n));
end    

lmda=max(lmda,0.0001);

maxl=zeros(1,nst);
llh= zeros(nst,n);
llh  = repmat(y,1,nst)'.*log(lmda) - lmda - repmat(gammaln(y+1),1,nst)';

 
% extern berechnen

maxl  = max(llh,[],1);
lh  = exp(llh  - repmat(maxl,nst,1));
 