function priorq=mixexpprior(mix,prior)

% ADD: CHECK THE PARAMETERS, NEUE PROGRAMME VERWENDEN, SEE MIXTURE BF

% Evaluates the prior of a Exponential mixture model at the parameter values
% stored in mix

% Author: Sylvia Fruehwirth-Schnatter, Adaption to Exponentials HW
% Last change: Sept 20, 2006 /6,March 2007


if ~isfield(mix,'K') mix.K=1; end
if ~isfield(mix,'df') mix.df=0; end

if isfield(mix,'d')    % compute prior beta in a finite mixture regression model
    warn('no exponential regression models implemented')
else        
    
    lambda=mix.par;
    K=mix.K;
    
   %prl=[prior.par.a' prior.par.b'];
    hier_prior=(isfield(prior,'hier'));if hier_prior hier_prior=prior.hier;end   %  hierarchical prior);
    
    
    if ~hier_prior
        priorq= prodgampdflog(prior.par,lambda);
        
    else warn('no hierarchical prior implemented')
        %g0=prior.par.g;
        %G0=prior.par.G;
        %priorq = logprior_mixexp_hpmarg(K,lambda',prior.par.a(1),g0,G0);  % alte programme
    end 
end