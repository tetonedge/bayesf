function [lh, maxl,llh] = likeli_expon(y,lmda)
% LIKELI likelihood (ohne kuerzen der normierungskonstnate)
% function call: lh = likeli(y,lmda)
% HW March 3, 2007 - Adaption of Sylvia's likeli_poisson

n=size(y,1);
nst = size(lmda,1);

if  size(lmda,2)==1
    lmda=lmda(:,ones(1,n));
end    

lmda=max(lmda,0.0001);

maxl=zeros(1,nst);
llh= zeros(nst,n);
llh  = log(lmda) - repmat(y,1,nst)'.* lmda ;
 
maxl  = max(llh,[],1);
lh  = exp(llh  - repmat(maxl,nst,1));