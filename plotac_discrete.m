function ineff= plotac_discrete(x,r,cl)

% x ... observations
% r ... max lag of ac
% cl ... color of plot

%[ac, hm, sd,  ki, ineff] = autocovneu(x,r);
[ac, hm, sd, ki, varhhat, ineff] = autocov(x,r);

plot([0:r]',[1;ac(:,1)],cl);
axis([0 r 0 1.05])
ylabel('r(h)');xlabel('lag h');
hold on
%sd=2/sqrt(size(x,1));
%plot([0 r],-sd*[1 1],'k');
%plot([0 r],sd*[1 1],'k');
