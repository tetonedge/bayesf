function fd = prodinvgampdflog(par,theta)

% computes the logarithm of a product of Inverted Gamma posterior density 
 
% input
% par ... parameter of the aposteriori density, structrual array with the
%         fields a and b; 
%         the number of colums determine the  number of products
%         for a single colums, this reduces to a single gamma density
% theta ... argument of the  aposteriori density

%output: log of the posterior density
%        M times 1 array with M being the maximum number of row in par and
%        eta

% Author: Sylvia Fruehwirth-Schnatter
% Last change: September 13, 2006


if ~isfield(par,'a') warn(['Field a missing in function prodgampdflog']);fd=[];return; end  
if ~isfield(par,'b') warn(['Field b missing in function prodgampdflog']);fd=[];return; end  

if or(size(par.a,1)~=size(par.b,1),size(par.a,2)~=size(par.b,2)) 
     warn(['Size disagreement in the variable par in function prodgampdflog']); fd=[]; return
end     

if ~(size(par.a,2)==size(theta,2))   warn(['Size disagreement in function prodgampdflog']); fd=[]; return; end

if size(par.a,1)==1  % evaluate a single density at a sequence of draws
    
   par.a=repmat(par.a,size(theta,1),1);
   par.b=repmat(par.b,size(theta,1),1);
   
elseif size(theta,1)==1  % evaluate a sequence of density at a single draw
    
   theta=repmat(theta,size(par.a,1),1);
    
elseif ~(size(par.a,1)==size(theta,1))
    warn(['Size disagreement in function prodgampdflog']); fl=[]; return
else    
%    evaluate  density in row i at the corresponding draw in row i
end

fd=sum((par.a).*log(par.b)-gammaln(par.a)-(par.b)./theta-(par.a+1).*log(theta),2);
