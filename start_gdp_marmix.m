%%  Data: GDP data

data=dataget('gdp');

%%  Estimate a Markov mxiture for increasing K 
 
post=[];
Kmin=1;
Kmax=4;
for K=Kmin:Kmax

    clear msreg;
    msreg.dist='Normal';
    msreg.K=K;
    msreg.indicmod.dist='Markovchain';    
    msreg.indicmod.init='ergodic'; 
    
    [data,msreg,mcmc]=mcmcstart(data,msreg);  
    clear prior;
    prior=priordefine(data,msreg);
 
    mcmcout=mixturemcmc(data,msreg,prior,mcmc);
    if isfield(data,'S') data=rmfield(data,'S'); end % starting classification for current K has to be deleted 

    [marlik,mcmcout]=mcmcbf(data,mcmcout);
    post=[post;marlik.bs];
    
    mcmcout.name= ['store_' data.name '_marmix_K' num2str(K)];
    mcmcstore(mcmcout);
end

%%  Select model with largest marginal likelihood 

[postsort is]=sort(post,1,'descend');
'Ranking according to marginal likelihood'
format bank;[is postsort],format short
Kselect=(Kmin-1)+is(1);

%% evaluate a fitted model for fixed K

K=Kselect;
eval(['load store_' data.name '_marmix_K' num2str(K)]);
mcmcplot(mcmcout);
[est,mcmcout]=mcmcestimate(mcmcout);
