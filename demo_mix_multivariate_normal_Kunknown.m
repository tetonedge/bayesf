%% simulate data

clear mix;
mix.dist='Normul';

mix.r=2;mix.K=2;
mix.r=2;mix.K=3;
%mix.r=3;mix.K=2;

if and(mix.r==2,mix.K==2)

    mix.par.mu=[-2 3;-1 1];
    mix.par.sigma=zeros(mix.r,mix.r,mix.K);
    mix.par.sigma(:,:,1)=[.1 0;0 .1];
    mix.par.sigma(:,:,2)=[.3 -.15;-.15 .5];
    mix.weight=[0.7 0.3];

elseif and(mix.r==2,mix.K==3)

    mix.par.mu=[-2 0 3;-1 0 1];
    mix.par.sigma=zeros(mix.r,mix.r,mix.K);
    mix.par.sigma(:,:,1)=[.1 0;0 .1];
    mix.par.sigma(:,:,2)=[.3 -.15;-.15 .5];
    mix.par.sigma(:,:,3)=[.1 0;0 .1];
    mix.weight=[0.3 0.3 0.4];

elseif and(mix.r==3,mix.K==2)


    mix.par.mu=[-2 3;-1 1;0 0];
    mix.par.sigma=zeros(mix.r,mix.r,mix.K);
    mix.par.sigma(:,:,1)=[.1 0 0;0 .1 0; 0 0 0.1];
    mix.par.sigma(:,:,2)=[.3 -.15 0 ;-.15 .5 0; 0 0 1];
    mix.weight=[0.7 0.3];

end

mixtrue=mix;
data=simulate(mixtrue,1000);
data.Strue=data.S;
data=rmfield(data,'S');

dataplot(data)

%% run MCMC

%Kmin=1;Kmax=3;
Kmin=1;Kmax=mixtrue.K+1;

for K=Kmin:Kmax

    clear mix;
    mix.dist='Normul';
    mix.K=K;
    mix.r=mixtrue.r;

    prior=priordefine(data,mix);
    [data, mix,mcmc]=mcmcstart(data,mix);

    mcmc.M=5000; mcmc.burnin=1000;

    mcmcout=mixturemcmc(data,mix,prior,mcmc);
    if isfield(data,'S') data=rmfield(data,'S'); end % starting classification for current K has to be deleted

    [est,mcmcout]=mcmcestimate(mcmcout);
    [marlik,mcmcout]=mcmcbf(data,mcmcout)

    mcmcout.name= ['store_mix_multivariate_normal_K' num2str(K)];
    mcmcstore(mcmcout);
 
end