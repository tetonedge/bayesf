%%  choose data
simdata=true;  % 
%simdata=false;

%% simulated data
if simdata

    clear mixtrue;
    mixtrue.dist='Normal';
    mixtrue.K=2;

    mixtrue.par.mu=[-4 3];
    mixtrue.par.sigma=[0.5 0.5];
    mixtrue.weight=[0.2 0.8];

    data=simulate(mixtrue,1000);
    data.Strue=data.S;
    data=rmfield(data,'S');

    dataplot(data)

    Kmin=1;Kmax=mixtrue.K+1;

else

%%   real data
    
    data=dataget('fishery');  Kmin=1;  Kmax=7;
    %data_frontier;            Kmin=1;  Kmax=1;              % azzalini frontier daten
    %data=dataget('enzyme');   Kmin=1;  Kmax=7;
    %data=dataget('acidity'); Kmin=1;  Kmax=7;
    %data=dataget('galaxy');  Kmin=1;  Kmax=7;

end

%% Fit mixtures with increasing number of components

for K=Kmin:Kmax

    clear mix;  
    mix.dist='Normal'; 
    mix.K=K;

    prior=priordefine(data,mix);
    [data, mix,mcmc]=mcmcstart(data,mix);

    mcmc.M=5000;    mcmc.burnin=2000;

    mcmcout=mixturemcmc(data,mix,prior,mcmc);
    if isfield(data,'S') data=rmfield(data,'S'); end % starting classification for current K has to be deleted

    [est,mcmcout]=mcmcestimate(mcmcout);
    [marlik,mcmcout]=mcmcbf(data,mcmcout)

    mcmcout.name= ['store_mix_normal_K' num2str(K)];
    mcmcstore(mcmcout);
end