%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% demo for finite mixtures of multiple regression models with fixed effects
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% simulate data

% define the true regression model

mixregtrue.dist='Normal';
mixregtrue.K=2;               
mixregtrue.weight=[0.3 0.7];
mixregtrue.par.sigma=[0.1 0.1];

mixregtrue.d=5;
mixregtrue.indexdf=[4;5];
mixregtrue.par.beta=[-2 3 ;1 -2; 0.3 0.1];
mixregtrue.par.alpha=[-4;1];

datareg=simulate(mixregtrue,1000);
dataplot(datareg);


%%  Estimate finite mixtures of mixed-effects regression models for increasing K
 
post=[];
Kmin=1;
Kmax=3;
for K=Kmin:Kmax

    clear mixreg;
    mixreg.dist='Normal';
    mixreg.d=size(datareg.X,1);
    
    mixreg.K=K;
    mixreg.indexdf=[4;5];

    [datareg,mixreg,mcmc]=mcmcstart(datareg,mixreg);

    clear prior;
    prior=priordefine(datareg,mixreg);

    mcmcout=mixturemcmc(datareg,mixreg,prior,mcmc);
    if isfield(datareg,'S') datareg=rmfield(datareg,'S'); end % starting classification for current K has to be deleted 

    [marlik,mcmcout]=mcmcbf(datareg,mcmcout);
    post=[post;marlik.bs];

    mcmcout.name= ['store_demo_mixreg_mixeffects_K' num2str(K)];
    mcmcstore(mcmcout);

end
%%  Select model with largest marginal likelihood 

[postsort is]=sort(post,1,'descend');
'Ranking according to marginal likelihood'
format bank;[is postsort],format short
Kselect=(Kmin-1)+is(1);

%% evaluate a fitted model for fixed K

K=Kselect;
eval(['load store_demo_mixreg_mixeffects_K' num2str(K)]);
mcmcplot(mcmcout);
[est,mcmcout]=mcmcestimate(mcmcout);
