function [mixaux2]= auxmix_initialize_binomial(data,mix,mcmc)

%%%%%%%%%%%%%%%%%%%%%%%%%%
%% initialize

if ~isfield(data,'Ti') 
    data.Ti=ones(1,data.N); % binary data
elseif prod(size(data.Ti))==1
    data.Ti=data.Ti*ones(1,data.N);
end
if any(data.Ti==0)     warn(['binomial distribution requires positive number of repetition']);    end


% initialise the necessary mixture approximations

data.rep=data.Ti;
[ys is]=sort(data.rep);
index=[2:data.N]';
indexz=[1;index(diff(ys)~=0)];
ny= ys(:,indexz);
clear ys;
ny=ny(ny~=0)';  % ny contains the different degrees of freedom

[dum1,dum2,dum3,Kmax]=compute_mixture(ny(1));

Ny=data.N;
mixaux2=struct('dist','Normal','K',zeros(Ny,1),'par',struct('mu',-1000*ones(Ny,Kmax),'sigma',0.001*ones(Ny,Kmax)),'weight',zeros(Ny,Kmax));
for nj=1:size(ny,1)
    [waux,maux,saux,Kaux]=compute_mixture(ny(nj));
    %           mixaux2.weight(nj,1:Kaux)=waux;mixaux2.par.mu(nj,1:Kaux)=maux;mixaux2.par.sigma(nj,1:Kaux)=saux;mixaux2.K(nj,1:Kaux)=Kaux;
    nm=sum(data.rep==ny(nj),2);
    mixaux2.weight(data.rep==ny(nj),1:Kaux)=repmat(waux,nm,1);
    mixaux2.par.mu(data.rep==ny(nj),1:Kaux)=repmat(maux,nm,1);
    mixaux2.par.sigma(data.rep==ny(nj),1:Kaux)=repmat(saux,nm,1);
    mixaux2.K(data.rep==ny(nj),1)=repmat(Kaux,nm,1);
end
