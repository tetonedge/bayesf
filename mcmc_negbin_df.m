function [mix,mcmc,sumacc]=mcmc_student_df(data,mix,prior,mcmc,kupdate,prop)


sumacc=zeros(1,mix.K);

for block=1:size(kupdate,1);
    k=kupdate(block,:);
    mixnew=mix;

    if prop.mh_da(block)
        %  uniform random walk MH based on  data augmenation (indicator)

        Um= mcmc.mh.tune.df(1,k).*(2*rand(1,size(kupdate,2))-1);  % uniform proposal;
        mixnew.par.df(1,k)=prior.par.df.trans + (mix.par.df(1,k)-prior.par.df.trans).*exp(Um);

        if ~data.empty
            class=dataclass(data,mix);           loglik=class.loglikcd(k);
            class=dataclass(data,mixnew);        logliknew=class.loglikcd(k);
            %loglik=likeli_skewstudmult_cd(data,mix);  
            %logliknew=likeli_skewstudmult_cd(data,mixnew); 
        else
            logliknew=zeros(size(kupdate,2),1);
            loglik=zeros(size(kupdate,2),1);
        end
        acc=zeros(size(kupdate,2),1);

        logprior=mixstudprior(mix,prior);
        logpriornew=mixstudprior(mixnew,prior);

        acc=logliknew(k) +logpriornew(k) -(loglik(k)+logprior(k)) + Um';


        accnew=(log(rand(size(kupdate,2),1))<acc);

        mix.par.df(1,k)=prior.par.df.trans + (mix.par.df(1,k)-prior.par.df.trans).*exp(accnew'.*Um);

        sumacc(1,k)=accnew;

    elseif prop.ars_da(block)

        %%% adaptive rejection sampling for df

        %% transform th=1/df^alpha

        % alpha=1;
        alpha=0.5;

        %% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % define hull

        hull.empty=true;
        hull.xmin=0;
        numax=1000; hull.xmin=1/numax^alpha; % numax upper bound for the degrees of freedom

        if isfield(prior.par.df,'trans')
            hull.xmax=1/prior.par.df.trans^alpha;
        else
            numin=0.001; hull.xmax=1/numin^alpha;  % numin lower bound for the degrees of freedom
        end

        mixh=mix;
        
        %%  choose the first point
        
        mixh.par.df(1,k)=(1./mcmc.hull.start(k,1).^(1/alpha));  % start from old grid

        loglikdiff1=-10*ones(size(k))';
       
        itry=0;trymax=20;
        while and(itry<trymax,any(loglikdiff1<0))  % find   gridpoints with positive tangent
            % mixh.par.df(1,k)=mixh.par.df(1,k) + 0.5*mixh.par.df(1,k).*and(loglikdiff1'<0,itry>0);
                          mixh.par.df(1,k)=mixh.par.df(1,k) + min(10,(numax-mixh.par.df(1,k)).*.5).*and(loglikdiff1'<0,itry>0);

        class=dataclass(data,mixh); loglikcd=class.loglikcd;
        %loglikcd=likeli_skewstudmult_cd(data,mixh);  
        priorh=mixstudprior(mixh,prior);
        logpost1=loglikcd(k)+priorh(k)+(1+alpha)*log(mixh.par.df(1,k)')-log(alpha);

        %% numerical differentiation

        md=mixh.par.df(1,k)';   eps=1e-8;
        mixh.par.df(1,k)=mixh.par.df(1,k)/(1+eps).^(1/alpha);
        class=dataclass(data,mixh); loglikcd=class.loglikcd;
        %loglikcd=likeli_skewstudmult_cd(data,mixh); 
        priorh=mixstudprior(mixh,prior);
        logposth=loglikcd(k)+priorh(k)+(1+alpha)*log(mixh.par.df(1,k)')-log(alpha);
        loglikdiff1=(logposth - logpost1).*(md.^alpha)./eps;
        itry=itry+1;
        end
        hull=hullupdate(hull,1./md.^alpha,logpost1,loglikdiff1,1*ones(size(k))');
        
        
        % define second point

        mixh.par.df(1,k)=1./mcmc.hull.start(k,2).^(1/alpha);
        
        loglikdiff2=10*ones(size(k))';
        itry=0;trymax=20;
        while and(itry<trymax,any(loglikdiff2>0))  % find   gridpoints with negative tangent
            % mixh.par.df(1,k)=mixh.par.df(1,k) + 0.5*(1-mixh.par.df(1,k)).*and(loglikdiff2'>0,itry>0);
            mixh.par.df(1,k)=prior.par.df.trans+(mixh.par.df(1,k)-prior.par.df.trans).* 0.5.^(and(loglikdiff2'>0,itry>0)) ;
            class=dataclass(data,mixh); loglikcd=class.loglikcd;
            %loglikcd=likeli_skewstudmult_cd(data,mixh); 
            priorh=mixstudprior(mixh,prior);
            logpost2=loglikcd(k)+priorh(k)+(1+alpha)*log(mixh.par.df(1,k)')-log(alpha);

            md=mixh.par.df(1,k)';

            mixh.par.df(1,k)=md'/(1+eps).^(1/alpha);
            class=dataclass(data,mixh); loglikcd=class.loglikcd;
            %loglikcd=likeli_skewstudmult_cd(data,mixh); 
            priorh=mixstudprior(mixh,prior);
            logposth=loglikcd(k)+priorh(k)+(1+alpha)*log(mixh.par.df(1,k)')-log(alpha);
            loglikdiff2=(logposth - logpost2).*md.^alpha./eps;
            itry=itry+1;
        end
        
        hull=hullupdate(hull,1./md.^alpha,logpost2,loglikdiff2,2*ones(size(k))');

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        itry=0;trymax=500;
        while and(prod(size(k))>0,itry<trymax)
            itry=itry+1;

            [thsim,U,mindex,low,upp] = simulate_hull(hull);
            mixnew.par.df(1,k)=1./thsim.^(1/alpha);

            accnew=(log(U)< (low-upp));

            class=dataclass(data,mixnew); loglikcd=class.loglikcd;
            %loglikcd=likeli_skewstudmult_cd(data,mixnew); 
            priornew= mixstudprior(mixnew,prior);
            logpostnew=loglikcd(k)+priornew(k) +(alpha+1)*log(mixnew.par.df(1,k)')-log(alpha);

            accnew(~accnew)=(log(U(~accnew))<(logpostnew(~accnew)-upp(~accnew)));

            mix.par.df(1,k(accnew))=mixnew.par.df(1,k(accnew));

            if any (accnew)
                %[lls is]=sort(hull.ll,2);
                %mcmc.hull.start(k(accnew),[1 2])=sort(hull.theta(is(accnew,end-1:end)),2);
                il=cumsum(hull.k>0,2); il= il(:,end);  % index of last gridpoint with increasing tangent
                ir=il+1;    % index of first gridpoint with decreasing tangent 
                %% il=il-(ir>hull.M); ir=ir-(ir>hull.M);
                
                mcmc.hull.start(k(accnew),1)=sum(hull.theta(accnew,:).*(repmat(il(accnew),1,hull.M)==repmat([1:hull.M],sum(accnew),1)),2);
                 mcmc.hull.start(k(accnew),1)= mcmc.hull.start(k(accnew),1)+0.5*hull.theta(accnew,1).*(il(accnew)==0);
                                        % no gridpoint with increasing tangent 
                mcmc.hull.start(k(accnew),2)=sum(hull.theta(accnew,:).*(repmat(ir(accnew),1,hull.M)==repmat([1:hull.M],sum(accnew),1)),2);
                mcmc.hull.start(k(accnew),2)=mcmc.hull.start(k(accnew),2)+0.5*(hull.theta(accnew,end)+1).*(ir(accnew)>hull.M);
                                       % no gridpoint with decreasing tangent 
                sumacc(1,k(accnew))=1/itry;
            end

            if ~all(accnew)

                %%%  UPDATE HULL:
                logpostnew=logpostnew(~accnew);


                % compute derivative of the
                % logposterior for k(~accnew)
                % numercially

                md=mixnew.par.df(1,k(~accnew))';
                mixh=mixnew;
                mixh.par.df(1,k(~accnew))=md'/(1+eps).^(1/alpha);
                class=dataclass(data,mixh); loglikcd=class.loglikcd;
                %loglikcd=likeli_skewstudmult_cd(data,mixh); 
                priorh=mixstudprior(mixh,prior);

                logposth=loglikcd(k(~accnew))+priorh(k(~accnew)) +(alpha+1)*log(mixh.par.df(1,k(~accnew))')-log(alpha);

                loglikdiff=(logposth - logpostnew).*md.^alpha./eps;

                if all(~accnew)
                    %% keep all hulls; add  rejected points
                    %%%
                    hull=hullupdate(hull,1./md.^alpha,logpostnew,loglikdiff,mindex);
                else
                    %  keep only those hulls, which are still not accepted: k(~accnew)
                    %% add  rejected points
                    %%%
                    hull=hullupdate(hull,1./md.^alpha,logpostnew,loglikdiff,mindex(~accnew),~accnew);
                end
            end
            %%%%%  END UPDATE HULL
            k=k(~accnew);
        end
        if prod(size(k))>0 
            warn('increase number of trials for accept-reject in mixturemcmc'); end
        clear hull;


    elseif prop.mh_marg(block)

        if block==1
            % compute loglik for k=1, only;
            loglik=likelihoodeval(data,mix);
            logprior=prioreval(mix,prior);
        end
        %%%  uniform proposal without data augmentation
        Um= mcmc.mh.tune.df(1,k).*(2*rand(1,size(kupdate,2))-1);  % uniform proposal;
        %mixnew.par.df(1,k)=mix.par.df(1,k).*exp(Um);
        mixnew.par.df(1,k)=prior.par.df.trans + (mix.par.df(1,k)-prior.par.df.trans).*exp(Um);

        logliknew=likelihoodeval(data,mixnew);  % use loglikcd=likeli_skewstudmult_cd(data,mixh); 
        logpriornew=prioreval(mixnew,prior);
        acc=logliknew+logpriornew-(loglik+logprior) + sum(Um);

        if log(rand)<acc
            mix=mixnew;
            loglik=logliknew;
            logprior=logpriornew;
            sumacc(1,k)=1;
        end


    elseif prop.mh_daomega(block)
        %% MOVE
        %%% simulate the hyperparameter in congdons prior
        U=rand*(gamcdf(prior.par.df.lmax,mix.K+1,1/sum(mix.par.df))-gamcdf(prior.par.df.lmin,mix.K+1,1/sum(mix.par.df)));
        prior.par.df.lambda=prior.par.df.lmin+ gaminv(U,mix.K+1,1/sum(mix.par.df));
        %% END
        'ADD'

    end
end