function [lh,maxl,llh] = likeli_student(y,beta,sgma2,df)
%
n=size(y,1);
nst=size(beta,1);

if  size(beta,2)==1
    beta=beta(:,ones(1,n));
    sgma2=sgma2(:,ones(1,n));
    df=df(:,ones(1,n));
end    

err=(y(:,ones(nst,1))' - beta).^2 ./ sgma2;
llh = gammaln((df+1)/2)-gammaln(df/2) -.5*(log(df*pi) + log(sgma2)) -(df+1)/2.*log(1+err./df);
 
maxl = max(llh,[],1);
%  maxl = max(llh);
lh = exp(llh - maxl(ones(nst,1),:));

