function [lh,maxl,llh] = likeli_normal(y,beta,sgma2)
%
n=size(y,1);
nst=size(beta,1);

if  size(beta,2)==1
    beta=beta(:,ones(1,n));
    sgma2=sgma2(:,ones(1,n));
end    


llh = -.5*(log(2*pi) + log(sgma2) + (y(:,ones(nst,1))' - beta).^2 ./ sgma2);


maxl = max(llh,[],1);
%  maxl = max(llh);
lh = exp(llh - maxl(ones(nst,1),:));

