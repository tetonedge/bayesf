function predmom=mcmcpredmom(mcmcout,H,data)
% Compute standard moments of  samples from the posterior predictive
% density of size H

% Author: Sylvia Fruehwirth-Schnatter
% Last change: January 10, 2007

M=mcmcout.M; 
if ~isfield(data,'type') for j=1:data.r; data.type{j}='continuous';end; end


if ~isfield(data,'X')

r=data.r;

 
contdat=true;for j=1:r;contdat=and(contdat,data.type{j}(1:3)=='con');end
disdat=true;for j=1:r;disdat=and(disdat,data.type{j}(1:3)=='dis');end
datamom=datamoments(data);

names=fieldnames(datamom);
for i=1:size(names,1)
    eval(['predmom.' names{i} '=zeros([M size(datamom.' names{i} ')]);']);
end    

for m=1:mcmcout.M
    mix=mcmcextract(mcmcout,m);
    if  ~isfield(mix,'indicmod')  mix.indicmod.dist='Multinomial'; end

    if   mix.indicmod.dist(1:6)=='Markov'  mix.indicmod.init=['fix' num2str(mcmcout.ST(m))]; end

    yf=simulate(mix,H,data);
    if  data.istimeseries   yf.istimeseries=true; end
        
    mom=datamoments(yf);
    for i=1:size(names,1)
        eval(['i1=size(datamom.' names{i} ',2)==1;']);
        eval(['i2=size(datamom.' names{i} ',2)>1;']);
        if i1 
            eval(['predmom.' names{i} '(m,:)=mom.' names{i} ';']);
        elseif i2
            eval(['predmom.' names{i} '(m,:,:)=mom.' names{i} ';']);
        end    
    end    
end

else
    predmom=[];
end
