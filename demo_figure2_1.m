% produces the plots shown in figure2.1

par=struct('mu',[-3 1],'sigma',[1 1]);
mix=struct('dist','Normal','K',2,'weight',[0.6 0.4],'par',par);
mixtureplot(mix)

mixpoi=struct('dist','Poisson','K',3,'weight',[0.3 0.4 0.3],'par',[0.1 2 5])  
mixtureplot(mixpoi)