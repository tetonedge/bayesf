function varargout=mcmcdiag(data,varargin)
% model diagnostics

% Author: Sylvia Fruehwirth-Schnatter
% Last change: November 10, 2006


if nargin==2
    ifig=0;
    nmodel=1;
else
    if isnumeric(varargin{nargin-1})
        ifig=varargin{nargin-1}-1;
        nmodel=nargin-2;
    else
        ifig=0;
        nmodel=nargin-1;
    end    
end   

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% data are handled as data stored by row
ibycolumn=isfield(data,'bycolumn');
if ibycolumn  ibycolumn=data.bycolumn; end  % ibycolumn true: data stored by column 
if ibycolumn      data.y=data.y'; data.bycolumn ='false'; end   
if isfield(data,'r') r=data.r; else data.r=size(data.y,1); end
if ~isfield(data,'istimeseries') data.istimeseries=false; end
if isfield(data,'S') data = rmfield(data,'S'); end  % compute no completet data moments for predictive check
datamom=datamoments(data);
disc=false;      if isfield(data,'type')      disc = data.type{1}(1:3)=='dis';    end


%%%%%%%%%%%%% method of moments %%%%%%%%%%%%%%


methmod=true;
for imodel=1:nmodel;
    mcmcout=varargin{imodel};
    sammom=min(500,mcmcout.M);
    im=randperm(mcmcout.M);
    
    predmom{imodel}=mcmcmargmom(mcmcsubseq(mcmcout,im(1:sammom)));
    methmod=and(methmod,isstruct(predmom{imodel}));
    methmod=and(methmod,~isfield(predmom{imodel},'empty'));
end    

if methmod
    
    sizep=2;cp='ko';
    
    
    
    if disc  %  discrete-valued data
        
        chmall={'mean','var','over','zero'};
        
        ifig=ifig+1; figure(ifig);
        
        for im=1:4
            subplot(2,2,im);  
            if im==1  title('Posterior distribution of Mixture Distribution Characteristics'); end
            
            chp=['predmom{1}.' chmall{im} ]; for j=2:nmodel;  chp=[chp ',predmom{' num2str(j) '}.' chmall{im} ];  end
            eval(['boxplotvar(-9999,' chp ');']);
            % eval(['boxplotvar(datamom.' chmall{im} ',' chp ');']);
            ylabel(chmall{im});xlabel('Model');
        end
        
        ifig=ifig+1; figure(ifig);
        for im=1:4
            subplot(2,2,im);  
            if im==1  title('Posterior distribution of Mixture Distribution Characteristics'); end
            chp=['predmom{1}.factorial(:,im)']; for j=2:nmodel;  chp=[chp ',predmom{' num2str(j) '}.factorial(:,im)'];  end
            eval(['boxplotvar(-9999,' chp ');']);
            %eval(['boxplotvar(datamom.factorial(im),'  chp ');']);
            
            ylabel([num2str(im) 'th factorial']);xlabel('Model');
        end
        
        
    else    % continous data
        chmall={'mean','var','skewness','kurtosis'};
        for i=1:r
            ifig=ifig+1;
            figure(ifig);
            for im=1:4
                subplot(2,2,im);  
                if im==1  title('Posterior distribution of Mixture Distribution Characteristics'); end
                
                chp=['predmom{1}.' chmall{im} '(:,i)']; for j=2:nmodel;  chp=[chp ',predmom{' num2str(j) '}.' chmall{im} '(:,i)'];  end
                eval(['boxplotvar(-9999,' chp ');']);
                %eval(['boxplotvar(datamom.' chmall{im} ',' chp ');']);
                ylabel(chmall{im});xlabel('Model');
                if and(r>1,im==2) title(['Feature ' num2str(i)]); end
            end
            
        end
        
        if r>1  % multivariate data
            
            ifig=ifig+1;
            figure(ifig); 
            ij=nchoosek([1:r]',2);
            [nr, nc]=plotsub(size(ij,1)+2);
            
            clear chmall;chmall={'Rdet','Rtr'};
            for im=1:2
                subplot(nr,nc,im);  
                if im==1  title('Posterior distribution of Mixture Distribution Characteristics'); end
                
                chp=['predmom{1}.' chmall{im}]; for j=2:nmodel;  chp=[chp ',predmom{' num2str(j) '}.' chmall{im} ];  end
                eval(['boxplotvar(-9999,' chp ');']);
                %eval(['boxplotvar(datamom.' chmall{im} ',' chp ');']);
                
                ylabel(chmall{im});xlabel('Model');
            end
            for ii=1:size(ij,1)
                subplot(nr,nc,ii+2); 
                
                chi=[num2str(ij(ii,1)) ',' num2str(ij(ii,2))];
                im=1;chmall{im}='corr';
                
                chp=['predmom{1}.' chmall{im} '(:,' chi ')']; 
                for j=2:nmodel;  chp=[chp ',predmom{' num2str(j) '}.' chmall{im}  '(:,'  chi ')'];  end
                %eval(['boxplotvar(-9999,' chp ');']);
                eval(['boxplotvar(datamom.' chmall{im} '(' chi '),' chp ');']);
                
                ylabel([chmall{im} '(' chi ')']);xlabel('Model');
                
            end
        end    
    end
    
    if  data.istimeseries
        lag=size(datamom.ac,1);
        for j=1:2
            ifig=ifig+1; figure(ifig);
            [nr, nc]=plotsub(nmodel);
            
            for imodel=1:nmodel;
                subplot(nr,nc,imodel); 
                
                if j==1 
                    if ~isfield(predmom{imodel},'ac')
                        rho=[zeros(100,lag);-1*ones(1,lag);1*ones(1,lag)];   
                    else
                        rho=squeeze(predmom{imodel}.ac); 
                    end
                else 
                    if ~isfield(predmom{imodel},'acsqu')
                        rho=[zeros(100,lag);-1*ones(1,lag);1*ones(1,lag)];   
                    else
                        rho=squeeze(predmom{imodel}.acsqu); 
                    end
                end
                %plotac_discrete(data.y',lag,'k');
                plot([0:lag],[1 mean(rho)],'b--');hold on;
                ki=zeros(lag,2);
                %for l=1:lag;[acac, hm, sd, med, ki(l,:), ineff] = autocovneu(rho(:,l),1);end
                for l=1:lag;[acac, hm, sd, ki(l,:), varhhat, ineff] = autocov(rho(:,l),1);end

                plot([0:lag],[1 ki(:,1)'],'r');
                plot([0:lag],[1 ki(:,2)'],'r');
                axis([0 lag min(0,min(ki(:,1))) 1])
                xlabel('lag');
                if j==1 
                    title(['Posterior of autocorrelation \rho(h) (Model' num2str(nmodel) ')' ]); 
                else 
                    title(['Posterior of autocorrelation of squared data (Model' num2str(nmodel) ')' ]); 
                end
            end        
        end
    end    
end



%%%%%%%%%%%%%  predictive check %%%%%%%%%%%%%%%
clear predmom;
sampred=min(200,mcmcout.M); % size of the predictive sample

methmod=true;

for imodel=1:nmodel;
    mcmcout=varargin{imodel};
    im=randperm(mcmcout.M);
    
    
    if isfield(mcmcout.model,'ar') 
        arp=mcmcout.model.ar; 
    elseif isfield(mcmcout.model,'arf') 
        arp=mcmcout.model.arf; 
    else
        arp=0;
    end
    
    if arp>0 
        data.ystart=data.y(1,1:arp);
        data.y=data.y(1,arp+1:end);
        data.N=data.N-arp;
        if isfield(data,'X')   data.X=data.X(:,arp+1:end); end
    end
    
    predmom{imodel}=mcmcpredmom(mcmcsubseq(mcmcout,im(1:sampred)),data.N,data);
    methmod=and(methmod,isstruct(predmom{imodel}));
end    

if methmod
    sizep=2;cp='ko';
    
    if disc  %  discrete-valued data
        
        chmall={'mean','var','over','zero'};
        ifig=ifig+1; figure(ifig);
        
        for im=1:4
            
            subplot(2,2,im);  
            if im==1  title('Predictive model checking'); end
            if im==2  title('(Horizontal line: observed value)'); end
            
            chp=['predmom{1}.' chmall{im} ]; for j=2:nmodel;  chp=[chp ',predmom{' num2str(j) '}.' chmall{im} ];  end
            eval(['boxplotvar(datamom.' chmall{im} ',' chp ');']);
            ylabel(chmall{im});xlabel('Model');
        end
        
        ifig=ifig+1; figure(ifig);
        for im=1:4
            subplot(2,2,im);  
            if im==1  title('Predictive model checking'); end
            if im==2  title('(Horizontal line: observed value)'); end
            
            chp=['predmom{1}.factorial(:,im)']; for j=2:nmodel;  chp=[chp ',predmom{' num2str(j) '}.factorial(:,im)'];  end
            eval(['boxplotvar(datamom.factorial(im),' chp ');']);
            ylabel([num2str(im) 'th factorial']);xlabel('Model');
        end
        
    else   % continuous data
        clear chmall; chmall={'mean','var','skewness','kurtosis'};
        for i=1:r
            ifig=ifig+1;
            figure(ifig);
            for im=1:4
                
                subplot(2,2,im);  
                if im==1  title('Predictive model checking'); end
                if im==2  title('(Horizontal line: observed value)'); end
                
                if all(chmall{im}(1:3)=='var') inn=(i-1)*r+i; else inn=i; end
                
                chp=['predmom{1}.' chmall{im} '(:,inn)']; for j=2:nmodel;  chp=[chp ',predmom{' num2str(j) '}.' chmall{im} '(:,inn)'];  end
                eval(['boxplotvar(datamom.' chmall{im} '(inn),' chp ');']);
                ylabel(chmall{im});xlabel('Model');
                if and(r>1,im==2) title(['Feature ' num2str(i)]); end
            end
            
        end
        
        if r>1  % multivariate data
            
            ifig=ifig+1;
            figure(ifig); 
            ij=nchoosek([1:r]',2);
            [nr, nc]=plotsub(size(ij,1));
            
            for ii=1:size(ij,1)
                subplot(nr,nc,ii); 
                if im==1  title('Predictive model checking'); end
                if im==2  title('(Horizontal line: observed value)'); end
                
                chi=[num2str(ij(ii,1)) ',' num2str(ij(ii,2))];
                im=1;chmall{im}='corr';
                
                chp=['predmom{1}.corr(:,' chi ')']; for j=2:nmodel;  chp=[chp ',predmom{' num2str(j) '}.corr(:,'  chi ')'];  end
                eval(['boxplotvar(datamom.corr(' chi ')' ',' chp ');']);
                ylabel(['corr(' chi ')']);xlabel('Model');
                
            end
        end    
    end
    
    if  data.istimeseries
        lag=size(datamom.ac,1);
        for j=1:2
            ifig=ifig+1; figure(ifig);
            [nr, nc]=plotsub(nmodel);
            
            for imodel=1:nmodel;
                subplot(nr,nc,imodel); 
                kk=1;% number of time series
                if j==1 
                    rho=predmom{imodel}.ac; 
                    plotac_discrete(data.y',lag,'k');
                else 
                    rho=predmom{imodel}.acsqu; 
                    plotac_discrete(data.y.^2',lag,'k');
                end
                plot([0:lag],[1 mean(rho(:,:,kk),1)],'b--');hold on;
                ki=zeros(lag,2);
              %  for l=1:lag;[acac, hm, sd, med, ki(l,:), ineff] = autocovneu(rho(:,l),1);end
                for l=1:lag;[acac, hm, sd, ki(l,:), varhhat,  ineff] = autocov(squeeze(rho(:,l,kk)),1);end

                plot([0:lag],[1 ki(:,1)'],'r');
                plot([0:lag],[1 ki(:,2)'],'r');
                axis([0 lag min(0,min(ki(:,1))) 1])
                xlabel('lag');
                if j==1 
                    title(['Posterior of autocorrelation \rho(h) (Model' num2str(nmodel) ')' ]); 
                else 
                    title(['Posterior of autocorrelation of squared data (Model' num2str(nmodel) ')' ]); 
                end
            end
        end
    end
end

if nargout==1
    varargout{1}=ifig;
end    
