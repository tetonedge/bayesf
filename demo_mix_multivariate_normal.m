clear mix;

mix.dist='Normul';

%mix.r=2;mix.K=2;
mix.r=2;mix.K=3;
% mix.r=3;mix.K=2;

%% simulate data 
% 
if and(mix.r==2,mix.K==2)

    mix.par.mu=[-2 3;-1 1];
    mix.par.sigma=zeros(mix.r,mix.r,mix.K);
    mix.par.sigma(:,:,1)=[.1 0;0 .1];
    mix.par.sigma(:,:,2)=[.3 -.15;-.15 .5];
    mix.weight=[0.7 0.3];

elseif and(mix.r==2,mix.K==3)

    mix.par.mu=[-2 0 3;-1 0 1];
    mix.par.sigma=zeros(mix.r,mix.r,mix.K);
    mix.par.sigma(:,:,1)=[.1 0;0 .1];
    mix.par.sigma(:,:,2)=[.3 -.15;-.15 .5];
    mix.par.sigma(:,:,3)=[.1 0;0 .1];
    mix.weight=[0.3 0.3 0.4];

elseif and(mix.r==3,mix.K==2)

    mix.par.mu=[-2 3;-1 1;0 0];
    mix.par.sigma=zeros(mix.r,mix.r,mix.K);
    mix.par.sigma(:,:,1)=[.1 0 0;0 .1 0; 0 0 0.1];
    mix.par.sigma(:,:,2)=[.3 -.15 0 ;-.15 .5 0; 0 0 1];
    mix.weight=[0.3 0.7];

end

mixtrue=mix;
data=simulate(mixtrue,2000);

dataplot(data)


%% run MCMC 

clear mix;
mix.dist=mixtrue.dist;
mix.K=mixtrue.K;
mix.r=mixtrue.r;

prior=priordefine(data,mix);

%mcmc.startpar=true;   
%[data, mix,mcmc]=mcmcstart(data,mix,mcmc);


[data, mix,mcmc]=mcmcstart(data,mix);

mcmc.M=7000; mcmc.burnin=2000;

mcmcout=mixturemcmc(data,mix,prior,mcmc);
mcmcplot(mcmcout,3); % plot mcmcoutput, starting with figure 3

[est,mcmcout]=mcmcestimate(mcmcout);

[marlik,mcmcout]=mcmcbf(data,mcmcout);

mcmcout.name= 'store_mix_multivariate_normal_K3';
mcmcstore(mcmcout);
