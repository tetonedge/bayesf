function [lh,maxl,llh] = likeli_skewnormal(y,beta,sgma2,lambda)
%
n=size(y,1);
nst=size(beta,1);

if  size(beta,2)==1
    beta=beta(:,ones(1,n));
    sgma2=sgma2(:,ones(1,n));
    lambda=lambda(:,ones(1,n));
end    


llh = -.5*(log(2*pi) + log(sgma2) + (y(:,ones(nst,1))' - beta).^2 ./ sgma2);
sc=max(-37,(y(:,ones(nst,1))' - beta)./sgma2.^.5.*lambda);  % avoid log(0), if normcdf(x) is 0 for x < -37
llh = log(2)+llh + log(normcdf(sc));


maxl = max(llh,[],1);
%  maxl = max(llh);
lh = exp(llh - maxl(ones(nst,1),:));

