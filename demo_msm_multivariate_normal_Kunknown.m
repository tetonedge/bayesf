clear mix;

mix.dist='Normul';

mix.r=2;mix.K=2;
mix.r=2;mix.K=3;
mix.r=3;mix.K=2;

mix.r=2;mix.K=4; % hahn et al , JFEC paper

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if and(mix.r==2,mix.K==4)  % hahn et al , JFEC paper
    
 mix.par.mu=[1.5 1.5  -1.5 -1.5; 1.5 -1.5 1.5 -1.5];
mix.par.sigma=zeros(mix.r,mix.r,mix.K);
mix.par.sigma(:,:,1)=[0.0225 0.0075;0.0075 0.025];
for k=2:mix.K; mix.par.sigma(:,:,k)=mix.par.sigma(:,:,1); end

Q=[-94.1 49.5 30.3 14.3; 33.6 -78.2 30.3 14.3; 14.3 30.3 -78.2 33.6; 14.3 30.3 49.5 -94.1];
dt=1/250;
[V,D]=eig(Q);
mix.indicmod.xi=V*diag(exp(dt*diag(D)))*inv(V);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%

elseif and(mix.r==2,mix.K==2)
    
mix.par.mu=[-2 3;-1 1];
mix.par.sigma=zeros(mix.r,mix.r,mix.K);
mix.par.sigma(:,:,1)=[.1 0;0 .1];
mix.par.sigma(:,:,2)=[.3 -.15;-.15 .5];
mix.indicmod.xi=[0.7 0.3;0.1 0.9];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

elseif and(mix.r==2,mix.K==3)


mix.par.mu=[-2 0 3;-1 0 1];
mix.par.sigma=zeros(mix.r,mix.r,mix.K);
mix.par.sigma(:,:,1)=[.1 0;0 .1];
mix.par.sigma(:,:,2)=[.3 -.15;-.15 .5];
mix.par.sigma(:,:,3)=[.1 0;0 .1];
mix.indicmod.xi=[0.7 0.2 0.1;0.1 0.8 0.1;0.1 0.3 0.6];

%test mix.par.mu=[-2 -1 0;-1 0 1];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%

elseif and(mix.r==3,mix.K==2)


mix.par.mu=[-2 3;-1 1;0 0];
mix.par.sigma=zeros(mix.r,mix.r,mix.K);
mix.par.sigma(:,:,1)=[.1 0 0;0 .1 0; 0 0 0.1];
mix.par.sigma(:,:,2)=[.3 -.15 0 ;-.15 .5 0; 0 0 1];
mix.indicmod.xi=[0.7 0.3;0.1 0.9];

end

%%%%%%%%%%%%%%%%%%%%
mix.indicmod.dist='Markovchain';    
mix.indicmod.init='uniform';   

mixtrue=mix;
data=simulate(mixtrue,1500);


% dataplot(data)


Kmin=1;Kmax=3;
Kmin=1;Kmax=mixtrue.K+2;
%Kmin=mixtrue.K;Kmax=mixtrue.K;


for K=Kmin:Kmax
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear mix; 
mix.K=K; mix.dist='Normul';mix.r=mixtrue.r;
mix.indicmod.dist='Markovchain';    
mix.indicmod.init='uniform';   



prior=priordefine(data,mix);

[data, mix,mcmc]=mcmcstart(data,mix);

mcmc.M=6000; mcmc.burnin=1000;
%mcmc.ranperm=false;

mcmcout=mixturemcmc(data,mix,prior,mcmc);
if isfield(data,'S') data=rmfield(data,'S'); end % starting classification for current K has to be deleted 


[est,mcmcout]=mcmcestimate(mcmcout);

[marlik,mcmcout]=mcmcbf(data,mcmcout);
[ic,mcmcout]=mcmcic(data,mcmcout);

eval(['save erg_jfe_simdat_' num2str(K) ' mcmcout']);  

end

%% model selection
select=true;
%select=false;

% mcmcdiag(data,mcmcout,10)

% marginal likelihood: K=3;

if select % model selection
    priorK=2;  % 2 groups expected
    %priorK=1;  % 

    marlikK=zeros(3,Kmax);
    postK=zeros(3,Kmax);
    icK=zeros(5,Kmax);

    for K=Kmin:Kmax
       eval(['load erg_jfe_simdat_' num2str(K) ' mcmcout']);  

        marlikK(1,K)=mcmcout.marlik.bs;
        marlikK(2,K)=mcmcout.marlik.is;
        marlikK(3,K)=mcmcout.marlik.ri;
 
        prior=log(poisspdf(K,priorK));
        %prior=log(nbinpdf(K,1,1/priorK));

        for j=1:3; postK(j,K)=marlikK(j,K)+prior;  end

        ic=mcmcout.ic;
        icK(1,K)=ic.aic;
        icK(2,K)=ic.bic;
        icK(3,K)=ic.iclbic;
        icK(4,K)=ic.awe;
        icK(5,K)=ic.bicmc;

    end

    postK=postK(:,Kmin:Kmax);
    Kmax1=size(Kmin:Kmax,2);

    format bank

    'marginal likelihood (bs/is/ri)'

    [Kmin:Kmax;marlikK(:,Kmin:Kmax)]


    'posterior of K (bs/is/ri)'

    [Kmin:Kmax;postK]

    postK=exp(postK-repmat(max(postK,[],2),1,Kmax1));

    postK= postK./repmat(sum(postK,2),1,Kmax1)

    'information criteria (aic/bic/icl-bic/awe/BIC-MC)'

    [Kmin:Kmax;icK(:,Kmin:Kmax)]

end


% mcmcdiag(data,mcmcout,10)
