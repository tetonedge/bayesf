%% demo_mix_binomial.m

% mixtures of binomial distributions
%
%% simulate data

clear mixtrue;
mixtrue.dist='Binomial';
mixtrue.K=2;
mixtrue.par=[0.1 0.8];
mixtrue.weight=[0.3 0.7];

reprate=10;  % expected number of repetitions
%data.Ti=max(1,poissrnd(reprate,1,N));
data.Ti=reprate;

data=simulate(mixtrue,100,data);
data.Strue=data.S; data=rmfield(data,'S');

dataplot(data)

%%  BAYESIAN ANALYSIS 

clear mix;
mix.dist='Binomial';
mix.K=2;

prior=priordefine(data,mix);

[data,mix,mcmc]=mcmcstart(data,mix);
mcmcout=mixturemcmc(data,mix,prior,mcmc);
[est,mcmcout]=mcmcestimate(mcmcout)

%% marginal likelihoods

[marlik,mcmcout]=mcmcbf(data,mcmcout)

mcmcout.name= 'store_mix_binomial_K2';
mcmcstore(mcmcout);