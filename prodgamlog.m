function fd = prodgamlog(theta,par)

%input: theta ... argument (spaltenvektor)
%       par ... parameter
%       par(k,1), par(k,2) ... entspricht j.ter komponente von theta

if size(par,1)>1
alpha = squeeze(par(:,1,:));
beta = squeeze(par(:,2,:));
else
alpha = squeeze(par(:,1,:))';
beta = squeeze(par(:,2,:))';
end
fd=sum(alpha.*log(beta)-gammaln(alpha)-beta.*theta+(alpha-1).*log(theta),1);
