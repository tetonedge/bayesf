function Qcol = qincol(Q)

% wandelt eine quadrat Matrix der dim dd in eine Spalte der dim dd*(dd+1)/2 um
% wobei nur die obige Dreiecksmatrix spaltenweise hintereinander geschrieben wird

% input: Q .. ddxdd
% output: Qcol .. dd*(dd+1)/2x1

dd = size(Q,2);

k=size(Q,3);
Qcol=zeros(dd,k);

for j=1:k

hilf = 0;
for i = 1:dd,
   Qcol(hilf+1:hilf+i,j) = Q(1:i,i,j);
   hilf =hilf + i;
end   

end