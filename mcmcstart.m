function [data,mix,mcmc]=mcmcstart(data,mix,varargin);
% determines   starting value for MCMC estimation

% Author: Sylvia Fruehwirth-Schnatter

if nargin==2  % default choice for mcmc
    mcmc.startpar=false;
else
    mcmc=varargin{1};
end


%mcmc.M=50;        
%mcmc.burnin=10;

if ~isfield(mcmc,'M')         mcmc.M=5000;         end
if ~isfield(mcmc,'burnin')    mcmc.burnin=1000;    end
if ~isfield(mcmc,'storeS')    mcmc.storeS=500;     end
if ~isfield(mcmc,'storepost') mcmc.storepost=true; end
if ~isfield(mcmc,'startpar')  mcmc.startpar=false; end
if ~isfield(mcmc,'ranperm')   mcmc.ranperm=true; end   

if ~isfield(mix,'K') K=1; mix.K=1; else K=mix.K; end

norstud=any([all(mix.dist(1:6)=='Normal') all(mix.dist(1:6)=='Normul') all(mix.dist(1:6)=='Stumul') all(mix.dist(1:6)=='Studen')]);
skew=any([all(mix.dist(1:6)=='SkewNo') all(mix.dist(1:6)=='SkNomu') all(mix.dist(1:6)=='SkewSt') all(mix.dist(1:6)=='SkStmu')]);
studmix=any([all(mix.dist(1:6)=='Stumul') all(mix.dist(1:6)=='Studen') all(mix.dist(1:6)=='SkewSt') all(mix.dist(1:6)=='SkStmu')]);

if studmix
    if isfield(mix,'par')
        if ~isfield(mix.par,'df') mix.par.df=10*ones(1,K); end
    else
        mix.par.df=10*ones(1,K);
    end
end





if ~isfield(mix,'r') if isfield(data,'r')  mix.r=data.r;  else mix.r=prod(size(data.y))/data.N; end; end  %



if K>1
    if ~isfield(mix,'indicmod')  mix.indicmod.dist='Multinomial'; end
    if mix.indicmod.dist(1:6)=='Markov'
        if ~isfield(mix.indicmod,'init')  mix.indicmod.init='ergodic';  end
        if all([or(mix.indicmod.init=='ergodic',mcmc.startpar) ~isfield(mix.indicmod,'xi')])
            dk=0.3/(K-1);
            mix.indicmod.xi= (0.7-dk)*eye(mix.K)+dk;
        end
    else
        if and(mcmc.startpar,~isfield(mix,'weight')) mix.weight=ones(1,mix.K)/mix.K; end
    end

end

if mcmc.startpar

%% Starting values for regression models
    
    if any([isfield(mix,'d') isfield(mix,'ar') isfield(mix,'arf')])

        if or(isfield(mix,'ar'),isfield(mix,'arf'))

            % convert MSAR model into a large Regression model
            [datareg,mixreg]=designar(data,mix);
            if ~isfield(mix,'d') mix.d=1; end
        else
            mixreg=mix;
            datareg=data;
        end
        if ~isfield(mixreg,'indexdf')
            mixreg.df=0;
        else
            mixreg.df=size(mixreg.indexdf,1)*size(mixreg.indexdf,2);
        end
        if  mixreg.df>0
            startalpha=false;
            if ~isfield(mix,'par')  startalpha=true;   elseif ~isfield(mix.par,'alpha')   startalpha=true;  end
            if startalpha   mix.par.alpha=zeros(mixreg.df,1);  end
        end

        startbeta=false;
        if ~isfield(mix,'par')  startbeta=true;  elseif ~isfield(mix.par,'beta') startbeta=true;  end

        if startbeta
            mix.par.beta=zeros(mixreg.d-mixreg.df,K);
            if isfield(datareg,'X')
                index=[1:size(datareg.X,1)];
                intercept=sum(index(all(diff(datareg.X,[],2)==0,2))); % determine the index of the intercept
                if mixreg.df>0 intercept=intercept-sum(mixreg.indexdf<intercept); end;
            else
                intercept=1;
            end
            if or(all(mix.dist(1:6)=='Poisso'),all(mix.dist(1:6)=='Negbin'))
                if K==1
                    pm=max(mean(data.y),0.1);
                else
                    pm=max(mean(data.y)*exp(0.5*randn(1,K)),0.1);
                end
                mix.par.beta(intercept,:)=log(pm);
                if all(mix.dist(1:6)=='Negbin') mix.par.df=5.*ones(1,K); end
            elseif all(mix.dist(1:6)=='Binomi')

                if K==1
                    pm=min(max(mean(data.y./data.Ti),0.1),0.9);
                else
                    pm=min(max(mean(data.y./data.Ti)*exp(0.2*randn(1,K)),0.1),0.9);
                end
                mix.par.beta(intercept,:)=log(pm)-log(1-pm);
                
                

            elseif all(mix.dist(1:6)=='Normal')
                if K==1
                    mix.par.beta(intercept,:)=mean(data.y);
                else
                    mix.par.beta(intercept,:)=mean(data.y)+std(data.y)*randn(1,mix.K);
                end

            end
        end

        if and(mix.dist(1:6)=='Normal',~isfield(mix.par,'sigma'))  mix.par.sigma=repmat(std(data.y),1,K);  end

    else
%% starting values for finite mixtures

        if and(all(mix.dist(1:6)=='Poisso'),~isfield(mix,'par'))
            
                if isfield(data,'exposures')
                    if K==1
                        pm=max(mean(data.y./data.exposures),0.1);
                    else
                        pm=max(mean(data.y./data.exposures)*exp(0.5*randn(1,K)),0.1);
                    end
                else
                    if K==1
                        pm=max(mean(data.y),0.1);
                    else
                        pm=max(mean(data.y)*exp(0.5*randn(1,K)),0.1);
                    end
                end
                mix.par=pm;
             
          elseif and(all(mix.dist(1:6)=='Expone'),~isfield(mix,'par'))

            if K==1
                pm=1/mean(data.y);
            else
                pm=exp(0.5*randn(1,K))/mean(data.y);
            end
            mix.par=pm;

        elseif and(all(mix.dist(1:6)=='Binomi'),~isfield(mix,'par'))
            
            if K==1
                pm=min(max(mean(data.y./data.Ti),0.1),0.9);
            else
                pm=min(max(mean(data.y./data.Ti)*exp(0.2*randn(1,K)),0.1),0.9);
            end
            mix.par=pm;
            
        elseif and(all(mix.dist(1:6)=='Multin'),~isfield(mix,'par'))
            
            
            'ADD MULTINOMIAL'
            
            
        elseif or(all(mix.dist(1:6)=='Normal'),all(mix.dist(1:6)=='Studen'))
            startmu=false;startsigma=false;
            if ~isfield(mix,'par')
                startmu=true; startsigma=true;
            else
                if ~isfield(mix.par,'mu') startmu=true; end
                if ~isfield(mix.par,'sigma') startsigma=true; end
            end
            if startmu
                if K==1
                    mix.par.mu=mean(data.y);
                else
                    mix.par.mu=mean(data.y)+std(data.y)*randn(1,mix.K);
                end
            end
            if startsigma    mix.par.sigma=repmat(std(data.y),1,K);   end

        elseif all(mix.dist(1:6)=='SkewNo')
            if K==1
                mixkest=momentest(struct('dist','SkewNo'),data);
                mix.par.mu=mixkest.par.mu; mix.par.lambda=mixkest.par.lambda; mix.par.sigma=mixkest.par.sigma;
            else
                [Skm,mukm]=kmeans(data.y',K, 'EmptyAction','singleton');
                dataS=Skm';
                for k=1:K;
                    mixkest=momentest(struct('dist','SkewNo'),struct('y',data.y(:,dataS==k)));
                    mix.par.mu(:,k)=mixkest.par.mu;
                    mix.par.lambda(:,k)=mixkest.par.lambda;
                    if mix.r==1 mix.par.sigma(:,k)=mixkest.par.sigma; else mix.par.sigma(:,:,k)=mixkest.par.sigma;end
                end
            end

        elseif mix.dist(1:6)=='Normul'
            startmu=false;startsigma=false;
            if ~isfield(mix,'par')
                startmu=true; startsigma=true;
            else
                if ~isfield(mix.par,'mu') startmu=true; end
                if ~isfield(mix.par,'sigma') startsigma=true; end
            end
            empcov=cov(data.y');
            if startmu
                if K==1
                    mix.par.mu=mean(data.y,2);
                else
                    mix.par.mu=zeros(mix.r,mix.K);
                    for k=1:K; mix.par.mu(:,k)=mean(data.y,2)+chol(empcov)'*randn(mix.r,1);end
                end
            end
            if startsigma    mix.par.sigma=repmat(empcov,[1 1 K]);   end

        end
    end

else

%% start with a classification

    if and(or(isfield(mix,'ar'),isfield(mix,'arf')),~isfield(mix,'d')) mix.d=1; end
 
    if ~isfield(mix,'d')

        if any([all(mix.dist(1:6)=='Poisso') all(mix.dist(1:6)=='Negbin') all(mix.dist(1:6)=='Expone')])
            if and(~isfield(data,'S'),K>1)
                [Skm,mukm]=kmeans(data.y.^.5',K, 'EmptyAction','singleton');
                data.S=Skm';
            end
            if all(mix.dist(1:6)=='Negbin') mix.par.df=5.*ones(1,K); end


        elseif all(mix.dist(1:6)=='Binomi')
            if and(~isfield(data,'S'),K>1)
                if (max(data.y)-min(data.y)+1)>2*K
                    % use kmeans clustering to determine a starting classification
                    [Skm,mukm]=kmeans(data.y.^.5',K, 'EmptyAction','singleton');
                    data.S=Skm';
                else  % random classification
                    data.S= simuni(1,data.N);
                end
            end
    
         elseif all(mix.dist(1:6)=='Multin')
            if and(~isfield(data,'S'),K>1)
                % random classification
                    data.S= simuni(1,data.N);
            end
    
            
        elseif norstud
            startmu=true; if isfield(mix,'par') if isfield(mix.par,'mu') startmu=false; end; end
            if and(K==1,startmu)
                mix.par.mu=mean(data.y')';
            elseif all([isfield(data,'S') K>1 startmu])
                mix.par.mu=zeros(size(data.y,1),K);for k=1:K; mix.par.mu(:,k)=mean(data.y(:,data.S==k)')';end
            elseif and(~isfield(data,'S'),K>1)
                [Skm,mukm]=kmeans(data.y',K, 'EmptyAction','singleton');
                data.S=Skm';
                if startmu  mix.par.mu=mukm';  end
            end
        elseif skew     
            momdist=mix.dist;
            if studmix if mix.r==1 momdist='SkewNo'; else momdist='SkNomu'; end;  end

            if K==1
                data.S=ones(1,size(data.y,2));
            elseif  ~isfield(data,'S')
                [Skm,mukm]=kmeans(data.y',K, 'EmptyAction','singleton');
                data.S=Skm';
            end
            for k=1:K;
                mixkest=momentest(struct('dist',momdist),struct('y',data.y(:,data.S==k)));
                mix.par.mu(:,k)=mixkest.par.mu;
                mix.par.lambda(:,k)=mixkest.par.lambda;
                if mix.r==1 mix.par.sigma(:,k)=mixkest.par.sigma; else mix.par.sigma(:,:,k)=mixkest.par.sigma;end
            end
        end

    elseif isfield(mix,'d')
        
        if  and(~isfield(data,'S'),K>1)
            if mix.d>1
                [Skm,mukm]=kmeans([data.y;data.X]',K, 'EmptyAction','singleton');
            else
                [Skm,mukm]=kmeans(data.y',K, 'EmptyAction','singleton');
            end
            data.S=Skm';
        end
        if or(norstud,skew)
            startsigma=true; if isfield(mix,'par') if isfield(mix.par,'sigma') startsigma=false; end; end
            if startsigma  mix.par.sigma=repmat(std(data.y),1,K);  end
        end
        if all(mix.dist(1:6)=='Negbin') 
            startsigma=true; if isfield(mix,'par') if isfield(mix.par,'nu') startsigma=false; end; end
            if startsigma mix.par.df=5.*ones(1,K);  end
        end


    end
    if K>1 if and(~isfield(data,'S0'),mix.indicmod.dist(1:6)=='Markov') data.S0=data.S(1); end; end
end