par1=struct('mu',[ -1 0  2],'sigma',[2 1 3]);
mix1=struct('dist','Normal','K',3,'weight',[0.3 0.5 0.2],'par',par1);
mixturepoint(mix1,1)

par2=struct('mu',[ -1 0  0],'sigma',[1 1 3]);
mix2=struct('dist','Normal','K',3,'weight',[0.3 0.5 0.2],'par',par2);
mixturepoint(mix2,2)
 