function mix=mcmcaverage(mcmcout)
% Compute the average of the MCMC draws,result stored as mixture 

% Author: Sylvia Fruehwirth-Schnatter
% Last change: November 27, 2006

mix.dist=mcmcout.model.dist;
if isfield(mcmcout.model,'K') 
    mix.K=mcmcout.model.K; 
else
    mcmcout.model.K=1;
end
if isfield(mcmcout.model,'indicmod')
    if mcmcout.model.indicmod.dist(1:6)=='Markov' 
        if isfield(mcmcout.model.indicmod,'init')
            mix.indicmod=struct('dist','Markovchain','init',mcmcout.model.indicmod.init); 
        else    
            mix.indicmod.dist='Markovchain'; 
        end
    end
else
    mcmcout.model.indicmod.dist='Multinomial';
end    
if isfield(mcmcout.model,'indicfix') 
    mix.indicfix=mcmcout.model.indicfix;   
else
    mcmcout.model.indicfix=false;
end

if isfield(mcmcout.model,'d')  mix.d=mcmcout.model.d; end
if isfield(mcmcout.model,'indexdf') mix.indexdf=mcmcout.model.indexdf; end
if isfield(mcmcout.model,'ar')  mix.ar=mcmcout.model.ar; end
if isfield(mcmcout.model,'arf')  mix.arf=mcmcout.model.arf; end
if isfield(mcmcout.model,'par') if isfield(mcmcout.model.par,'indexar')  mix.par.indexar=mcmcout.model.par.indexar; end; end


if ~isstruct(mcmcout.par) 
    mix.par=mean(mcmcout.par,1);
    
else
    if isfield(mcmcout.par,'beta') 
        if size(mcmcout.par.beta,2)>1
            mix.par.beta=squeeze(mean(mcmcout.par.beta,1));
        else
            mix.par.beta=squeeze(mean(mcmcout.par.beta,1))';
        end
    end
    
    if isfield(mcmcout.par,'alpha')         mix.par.alpha=mean(mcmcout.par.alpha,1);     end
    
    if isfield(mcmcout.par,'lambda')  
        
        if or(all(mcmcout.model.dist(1:6)=='SkNomu'),all(mcmcout.model.dist(1:6)=='SkStmu'))
            if size(mcmcout.par.lambda,3) > 1
                mix.par.lambda=squeeze(mean(mcmcout.par.lambda,1));
                mix.par.mean=squeeze(mean(mcmcout.par.mean,1));
            else
                mix.par.lambda=squeeze(mean(mcmcout.par.lambda,1))';
                mix.par.mean=squeeze(mean(mcmcout.par.mean,1))';
            end
        else
            mix.par.lambda=mean(mcmcout.par.lambda,1); 
            mix.par.mean=mean(mcmcout.par.mean,1); 
        end
                   
    end
    
    if isfield(mcmcout.par,'pi') 
        mix.par.pi=squeeze(mean(mcmcout.par.pi,1));
    end
    
    if isfield(mcmcout.par,'df')        mix.par.df=mean(mcmcout.par.df,1);     end
    
    if isfield(mcmcout.par,'mu')
        if any([all(mcmcout.model.dist(1:6)=='Normul') all(mcmcout.model.dist(1:6)=='Stumul')  all(mcmcout.model.dist(1:6)=='SkStmu')  all(mcmcout.model.dist(1:6)=='SkNomu')])
            if size(mcmcout.par.sigma,3) > 1
                mix.par.mu=squeeze(mean(mcmcout.par.mu,1));
            else
                mix.par.mu=squeeze(mean(mcmcout.par.mu,1))';
            end
        else
            mix.par.mu=mean(mcmcout.par.mu,1);
        end
    end
    
    if isfield(mcmcout.par,'sigma')
        if any([all(mcmcout.model.dist(1:6)=='Normul') all(mcmcout.model.dist(1:6)=='Stumul')  all(mcmcout.model.dist(1:6)=='SkStmu')  all(mcmcout.model.dist(1:6)=='SkNomu')])
            if size(mcmcout.par.sigma,3) > 1
                mix.par.sigma=qinmatrmult(squeeze(mean(mcmcout.par.sigma,1)));
            else
                mix.par.sigma=qinmatrmult(squeeze(mean(mcmcout.par.sigma,1))');
            end
        else
            mix.par.sigma=mean(mcmcout.par.sigma,1);
        end
    end
end

if mcmcout.model.K>1
    if ~mcmcout.model.indicfix
        if mcmcout.model.indicmod.dist(1:6)=='Multin' 
            mix.weight=mean(mcmcout.weight,1);
        elseif mcmcout.model.indicmod.dist(1:6)=='Markov' 
            mix.indicmod.xi=squeeze(mean(mcmcout.indicmod.xi,1));
        end
    end
end



