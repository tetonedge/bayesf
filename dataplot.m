function varargout=dataplot(data,varargin);
% Plot data

% Author: Sylvia Fruehwirth-Schnatter

if nargin==1 nfig=0; else nfig=fix(varargin{1})-1; end

% data are handled as data stored by row
ibycolumn=isfield(data,'bycolumn');
if ~ibycolumn  
    if size(data.y,1)==1
    data.bycolumn=false; 
    elseif size(data.y,2)==1
        data.bycolumn=true;
    else
       data.bycolumn=false;
    end
end  % ibycolumn true: data stored by column
if data.bycolumn  y=data.y'; else     y=data.y; end

if ~isfield(data,'r') data.r=size(y,1); end
r=data.r;
if ~isfield(data,'istimeseries') data.istimeseries=false; end
if ~isfield(data,'N') data.N=size(y,2); end

%%  time series plot

if  data.istimeseries

    cal=[1:data.N]';
    if isfield(data,'dates')
        nj=data.N/(data.dates(end,1)-data.dates(1,1)+1);
        cal = (data.dates(1,1)-1+cal/nj);
    end


    nfig=nfig+1;
    figure(nfig);

    [nr, nc]=plotsub(r);

    for j=1:r
        subplot(nr,nc,j);

        disc=false;      if isfield(data,'type')     disc = all(data.type{1}(1:3)=='dis');    end
        if and(disc,max(data.y(j,:),[],2)<15)        % discrete data
            bar(cal,data.y(j,:)',0.2);
            if ~isfield(data,'dates') xlabel('t'); end
            axis([cal(1) cal(end) 0 max(data.y(j,:),[],2)+1]);
        else           % unspecified data type or continous data
            plot(cal,data.y(j,:)','k');
            if ~isfield(data,'dates') xlabel('t'); end
            samescale=false;
            samescale=true;
            if samescale
                axis([cal(1) cal(end) min(min(data.y))-0.05*abs(min(min(data.y))) max(max(data.y))+0.05*abs(max(max(data.y)))]);
            else
                axis([cal(1) cal(end) min(data.y(j,:),[],2)-0.05*abs(min(data.y(j,:),[],2)) max(data.y(j,:),[],2)+0.05*abs(max(data.y(j,:),[],2))]);
            end
        end

        if isfield(data,'name')  % adds title  for named data
            if r==1
                title(['Time series plot of the ' data.name ' data']);
            else
                title(['Time series plot of the ' data.name ' data; time series ' num2str(j)]);
            end

        elseif r>1
            title(['Time series plot of  time series ' num2str(j)]);
        end
    end


    nfig=nfig+1;    figure(nfig);

    lag=20;

    if isfield(data,'model')
        mom=moments(data.model);
    else
        mom.empty=true;
    end
    for j=1:r

        subplot(r,2,(j-1)*2+1);
        plotac(data.y(j,:)',lag,'k'); xlabel('lag');title('Autocorrelation of y_t');
        if ~isfield(mom,'empty')
            hold on;
            bar(mom.ac,0.3);
        end
        subplot(r,2,j*2);
        plotac(data.y(j,:)'.^2,lag,'k'); xlabel('lag');title('Autocorrelation of y_t^2');
        if ~isfield(mom,'empty')
            hold on;
            bar(mom.acsqu,0.3);
        end
    end
end

if ~isfield(data,'X')
    plotmarg=true;
else  % plot marginal density only, if regression model contains only intercept
    if all(all(data.X==1)) plotmarg=true; else plotmarg=false; end;
end

if plotmarg
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % loop over features and plot marginal distribution
    nfig=nfig+1; figure(nfig);
    for i=1:r;

        if r>1
            if i==1 [nr, nc]=plotsub(r); end;        subplot(nr,nc,i);
        end

        disc=false;
        if isfield(data,'type')
            disc = data.type{i}(1:3)=='dis';
        end
        if disc        % discrete data
            ymin=min(y(i,:),[],2);
            ymax=max(y(i,:),[],2);
            nbin=ymax-ymin+1;
            HN=hist(y(i,:)',nbin);
            bar([ymin:ymax]',HN/data.N,1,'w');
        else           % unspecified data type or continous data
            nbin=40;
            if data.N>1000   nbin=100; end
            histneu(y(i,:)',nbin,'k');
        end

        if isfield(data,'model')    % adds (marginal) density plot for the model data.model
            hold on;
            plotmod=true;
            if r==1
                if all(data.model.dist(1:6)=='Binomi')
                    if isfield(data,'Ti') data.model.Ti=data.Ti; else data.model.Ti=1;end
                    if  prod(size(data.model.Ti))~=1 plotmod=false; end
                end
                if all(data.model.dist(1:6)=='Negbin') plotmod=false; end
                if plotmod
                    nfigneu= mixtureplot(data.model,nfig);
                    %legend('observed','fitted')
                    if nfigneu>nfig close([nfig+1:nfigneu]); end
                end
            else
                mixmar=mixturemar(data.model,i);
                nfigneu= mixtureplot(mixmar,nfig);
                legend('observed','fitted')
                if nfigneu>nfig close([nfig+1:nfigneu]); end
            end
            hold off;
        end


        if i==r
            if isfield(data,'name')  % adds title  for named data
                title(['Histogram of the ' data.name ' data']);
            end
        end

    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % plot the joint distrbution


    if r==2    % scatter plot of bivariate data

        nfig=nfig+1;figure(nfig);

        if ~isfield(data,'model')
            scatter(y(1,:),y(2,:),'d');

        else
            %scatter(y(1,:),y(2,:),3);
            plot(y(1,:),y(2,:),'.');

            finmix=all([~isfield(data.model,'d') ~isfield(data.model,'ar') ~isfield(data.model,'arf')]);


            if and(data.model.dist(1:6)=='Normul',finmix)
                plot_components=false;
                hold on;
                if  plot_components
                    % adds 95 percent allipsoid of of each bivariate component density
                    c=['r';'b';'g';'y';'k';'m'];

                    for k=1:data.model.K;
                        err=plot_biv_normal(data.model.par.mu(:,k),squeeze(data.model.par.sigma(:, :,k)),c(k));
                    end
                    legend(num2str([1:data.model.K]'));

                else
                    npoint=100;
                    contmix(data.model,npoint);
                end
                xlabel(['y_1']);
                ylabel(['y_2']);
                hold off;

            elseif and(data.model.dist(1:6)=='Stumul',finmix)
                hold on;
                npoint=100;
                contmixstud(data.model,npoint);
                xlabel(['y_1']);
                ylabel(['y_2']);
                hold off;

            elseif and(data.model.dist(1:6)=='SkNomu',finmix)
                hold on;
                npoint=100;
                contmixskewnormal(data.model,npoint);
                xlabel(['y_1']);
                ylabel(['y_2']);
                hold off;


            elseif and(data.model.dist(1:6)=='SkStmu',finmix)
                hold on;
                npoint=100;
                contmixskewstud(data.model,npoint);
                xlabel(['y_1']);
                ylabel(['y_2']);
                hold off;


            end



        end

    end

    if r>2    % scatter plot of each pair for multivariate data

        nfig=nfig+1;figure(nfig);


        ij=nchoosek([1:data.r]',2);
        [nr, nc]=plotsub(size(ij,1));

        for ii=1:size(ij,1)
            subplot(nr,nc,ii);
            scatter(y(ij(ii,1),:),y(ij(ii,2),:),3);hold on;

            if isfield(data,'model')
                finmix=all([~isfield(data.model,'d') ~isfield(data.model,'ar') ~isfield(data.model,'arf')]);
                if ~isfield(data.model,'K') data.model.K=1;  data.model.weight=1; end
                if finmix
                    npoint=100;hold on;
                    mixij=mixturemar(data.model,ij(ii,:));
                    plot_components=false;
                    if ~plot_components
                        if  data.model.dist(1:6)=='Normul'
                            contmix(mixij,npoint);
                        elseif  data.model.dist(1:6)=='Stumul'
                            contmixstud(mixij,npoint);
                        elseif  data.model.dist(1:6)=='SkNomu'
                            contmixskewnormal(mixij,npoint);
                        elseif  data.model.dist(1:6)=='SkStmu'
                            contmixskewstud(mixij,npoint);

                        end
                    else
                        compdens.dist=data.model.dist; compdens.K=1; compdens.weight=1;

                        for k=1:data.model.K;
                            if  data.model.dist(1:6)=='Normul'
                                compdens.par=struct('mu',mixij.par.mu(:,k),'sigma',mixij.par.sigma(:,:,k));
                                contmix(compdens,npoint);
                            elseif  data.model.dist(1:6)=='Stumul'
                                compdens.par=struct('mu',mixij.par.mu(:,k),'sigma',mixij.par.sigma(:,:,k),'df',mixij.par.df(k));
                                contmixstud(compdens,npoint);
                            elseif  data.model.dist(1:6)=='SkNomu'
                                compdens.par=struct('mu',mixij.par.mu(:,k),'sigma',mixij.par.sigma(:,:,k),'lambda',mixij.par.lambda(:,k));
                                contmixskewnormal(compdens,npoint);
                            elseif  data.model.dist(1:6)=='SkStmu'
                                compdens.par=struct('mu',mixij.par.mu(:,k),'sigma',mixij.par.sigma(:,:,k),'lambda',mixij.par.lambda(:,k));
                                contmixskewstud(compdens,npoint);

                            end


                        end
                        %legend(num2str([1:data.model.K]'));

                    end
                end
                hold off;
            end

            xlabel(['y_' num2str(ij(ii,1))]);
            ylabel(['y_' num2str(ij(ii,2))]);
            axis([min(y(ij(ii,1),:),[],2)  max(y(ij(ii,1),:),[],2)  min(y(ij(ii,2),:),[],2)  max(y(ij(ii,2),:),[],2)]);
            if ii==1
                if isfield(data,'name')  % adds title  for named data
                    title(['Scatterplot of the ' data.name ' data']);
                end
            end
        end
        hold off;

        nfig=nfig+1;figure(nfig);
        plotmatrix(y');  % matrix scatter of the whole data set

    end
end

if isfield(data,'X')   % regression analysis
    index=[1:size(data.X,1)];
    ic= any(data.X~=1,2);
    X=data.X(ic,:);

    nfig=nfig+1;figure(nfig);

    [nr, nc]=plotsub(size(X,1));

    for ii=1:size(X,1)
        subplot(nr,nc,ii);
        scatter(X(ii,:),y,'+');hold on;
        xlabel(['x_' num2str(ii)]);
        ylabel(['y']);
        c=0.5;
        axis([min(X(ii,:)-c,[],2)  max(X(ii,:)+c,[],2)  min(y-c,[],2)  max(y+c,[],2)]);
        if ii==1
            if isfield(data,'name')  % adds title  for named data
                title(['Scatterplot of the ' data.name ' data']);
            end
        end

    end
    hold off;

    if size(X,1)>1
        nfig=nfig+1;figure(nfig);
        plotmatrix(X');  % matrix scatter of the whole data set
        title(['Scatterplot of the Designmatrix']);
    end


end

if nargout==1 varargout{1}=nfig; end
