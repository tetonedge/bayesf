function post=posterior(data,model,prior,varargin)

% computes the moments of the posterior distribution  under a conditionally
% conjugate  prior disribution

% If the procedure is used many times, for instance as part of an MCMC algorithm,  automatic check
% should be suppressed for the sake of efficiency, by calling the function with an additional argument equals
% 0, e.g.
%  post=posterior(data,model,prior,0);}

% Author: Sylvia Fruehwirth-Schnatter/HW

if ~isfield(model,'K')  K=1; model.K=1; end
norstud=any([model.dist(1:6)=='Normal' model.dist(1:6)=='Normul' model.dist(1:6)=='Stumul' model.dist(1:6)=='Studen']);
%      Normal or student mixture;
if all([~isfield(model,'error') norstud]) % Normal or student mixture; default: switching variane
   model.error='switch';
end    

studmix=any([all(model.dist(1:6)=='Stumul') all(model.dist(1:6)=='Studen') all(model.dist(1:6)=='SkewSt') all(model.dist(1:6)=='SkStmu')]);

%if and(norstud,~studmix)  model.omega=ones(1,data.N); end

if nargin==3
    
    
    % check the dimension of the data
    % data are handled as data stored by row
    if isfield(data,'bycolumn') ibycolumn=data.bycolumn; else ibycolumn=false; end  % ibycolumn true: data stored by column 
    if ibycolumn      
        data.y=data.y'; 
        if isfield(data,'X') data.X=data.X'; end
        data.bycolumn =false; 
    end  
    if ~isfield(data,'empty') data.empty=false; end
    if ~isfield(data,'N') N=size(data.y,2); data.N=N; end
    
    if and(model.K>1,~isfield(data,'S'))   
        warn(['The data have to be completely specified, including the allocations in field S']);
        post=[];return;
    end
end

if model.dist(1:6)=='Multin'
    % posterior density of the parameter of  multnomial distribution
     if data.empty     post=prior;
    else 
        post = countex(data.y,model.cat)' + prior;
    end

elseif model.dist(1:6)=='Markov'
    % posterior density of the parameter of  a Markov chain
     if data.empty   post=prior;
    else 
        post = statecount(data.y,model.cat) + prior;
    end
    
elseif model.dist(1:6)=='Poisso'
    
    if data.empty
        post=prior;
    else 
        if model.K==1  % single member from the distribution family
            post=struct('a',prior.a+sum(data.y),'b',prior.b+data.N);    
        else    % mixture distribution
            ind = repmat(data.S,model.K,1) == repmat([1:model.K]',1,data.N);
            sprod = sum(ind .* repmat(data.y,model.K,1),2);
            sind = sum(ind,2);
            post=struct('a',prior.a+sprod','b',prior.b+sind');
        end
    end
    
elseif all(model.dist(1:6)=='Binomi')
    
    if data.empty
        post=prior;
    else 
        if prod(size(data.Ti))==1 data.Ti=data.Ti*ones(1,data.N); end
        if model.K==1  % single member from the distribution family
            SN=sum(data.y);
            post=struct('a',prior.a+SN,'b',prior.b+sum(data.Ti)-SN);    
        else    % mixture distribution
            ind = repmat(data.S,model.K,1) == repmat([1:model.K]',1,data.N);
            sprod = sum(ind .* repmat(data.y,model.K,1),2);
            sind = sum(ind .* repmat(data.Ti,model.K,1),2);
            post=struct('a',prior.a+sprod','b',prior.b+sind'-sprod');
        end
    end    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%HW
elseif model.dist(1:6)=='Expone'
    
    if data.empty
        post=prior;
    else 
        if model.K==1  % single member from the distribution family
            post=struct('a',prior.a+data.N,'b',prior.b+sum(data.y));    
        else    % mixture distribution
            ind = repmat(data.S,model.K,1) == repmat([1:model.K]',1,data.N);
            sprod = sum(ind .* repmat(data.y,model.K,1),2);
            sind = sum(ind,2);
            post=struct('a',prior.a+sind','b',prior.b+sprod');
        end
    end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
 
elseif norstud
    
    mufix=false;sigmafix=false;betafix=false;
    if isfield(model,'parfix')
        if  isfield(model.parfix,'mu')    mufix=model.parfix.mu;     end  
        if  isfield(model.parfix,'beta')    betafix=model.parfix.beta;     end  
        if  isfield(model.parfix,'sigma')  sigmafix=model.parfix.sigma;     end  
    end  
    
    if isfield(data,'r') r=data.r; else r=size(data.y,1); end
    
    
    
    if data.empty
        if or(mufix,betafix)
            post.c=prior.c;post.C=prior.C;
            if r>1    post.logdetC=prior.logdetC;    end
        elseif and(~mufix,~sigmafix)           
            if isfield(prior,'mu')
                post.mu.N0=prior.mu.N0;     post.mu.b=prior.mu.b;
            else
                post.beta.b=prior.beta.b;
                post.beta.Binv= prior.beta.Binv;
                post.beta.B= 0*post.beta.Binv;
                if  size(prior.beta.b,1)>1
                    for k=1:model.K;post.beta.B(:,:,k)=inv(squeeze(post.beta.Binv(:,:,k)));end
                else
                    post.beta.B=1./post.beta.Binv;
                end
            end
            post.sigma.c=prior.sigma.c;post.sigma.C=prior.sigma.C;
            if r>1       post.sigma.logdetC=prior.sigma.logdetC;    end
        elseif sigmafix
            post.b=prior.b; post.Binv= prior.Binv;
            if or(r>1,isfield(model,'d'))
                for k=1:model.K;post.B(:,:,k)=inv(squeeze(post.Binv(:,:,k)));end
            else
                post.B=1./post.Binv; 
            end    
        end
        return;
        
    end 
    
    
    if model.K==1   % single member from the distribution family
        S=ones(1,data.N); 
    else    % mixture distribution
        S=data.S;
    end   
    ind = repmat(S,model.K,1) == repmat([1:model.K]',1,data.N);
    Nk = sum(ind,2)';
    
    if and(mufix,sigmafix)   
        warn(['It is not possible to fix both the mean and the variance in function posterior']);post=[];return;   
        
    elseif all([betafix sigmafix isfield(model,'d')])  
        warn(['It is not possible to fix both the regression parameters and the variance in function posterior']);post=[];return;   
        
    %elseif all([~betafix ~sigmafix isfield(model,'d')])  
    %    warn(['It is not possible to estimate both the regression parameters and the variance in function posterior']);post=[];return;   
        
    elseif all([~mufix ~sigmafix ~isfield(model,'d')])   % complete data posterior of a finite mixture model under a conjugate prior
        
        if ~isfield(prior.mu,'N0')   warn(['It is not possible to fix both the regression parameters and the variance in function posterior']);post=[];return;end
        
        post.mu.N0=prior.mu.N0 + Nk;
        if r==1 
            sk = sum(ind .* repmat(data.y,model.K,1),2)';   
            post.mu.b=(prior.mu.b.*prior.mu.N0+sk)./post.mu.N0;
        else
            sk = zeros(r,model.K); for k=1:model.K;sk(:,k)=sum(data.y(:,S==k),2);end
            post.mu.b=(prior.mu.b.*repmat(prior.mu.N0,r,1)+sk)./repmat(post.mu.N0,r,1);
        end   
        post.sigma.c=prior.sigma.c + 0.5*Nk;
        
        
        ck=prior.mu.N0.*Nk./post.mu.N0;
        post.sigma.C=0*prior.sigma.C;
        if r>1 post.sigma.logdetC=0*prior.sigma.c; end
        for k=1:model.K
            if Nk(k)>0
                yk(:,k)=sk(:,k)/Nk(k);
                dk=data.y(:,S==k)-yk(:,k*ones(1,Nk(k)));
            else
                yk(:,k)=sk(:,k);  dk=0; % equals 0
            end
            if r>1
                post.sigma.C(:,:,k)=prior.sigma.C(:,:,k)+ 0.5*(dk*dk'+(yk(:,k)-prior.mu.b(:,k))*(yk(:,k)-prior.mu.b(:,k))'*ck(k));
                post.sigma.logdetC(k)=log(det(post.sigma.C(:,:,k)));
            else    
                post.sigma.C(k)=prior.sigma.C(k)+ 0.5*(dk*dk'+(yk(:,k)-prior.mu.b(:,k))*(yk(:,k)-prior.mu.b(:,k))'*ck(k));
            end
        end      
        
     elseif all([isfield(model,'d') model.K==1 sigmafix model.error=='hetero'])  
         % a single multiple regression models with fixed heteroscedastic errors
        
         if ~isfield(model.par,'sigma')  warn(['A value has to be assigned to the variance before calling the function posterior for a fixed mean']);post=[];return;  end
         
         sigmainv = 1./model.par.sigma.^.5;  %squareroot
         y=(data.y.*sigmainv)';
         X=(data.X.*repmat(sigmainv,size(data.X,1),1))';
         post.B=inv(prior.Binv + X'*X);
         post.b  = post.B*(prior.Binv*prior.b + X'*y);
         
         
     elseif and(sigmafix,isfield(model,'d'))  % finite mixture of multiple regression models with fixed variance
        
        if ~isfield(model.par,'sigma')  warn(['A value has to be assigned to the variance before calling the function posterior for a fixed mean']);post=[];return;  end
        
        post.Binv = 0*prior.Binv;   post.B  = 0*prior.Binv;  post.b  = 0* prior.b;  
        sigmainv = 1./model.par.sigma;  
        for k=1:model.K
            Xk=data.X(:,S==k)'; 
            post.Binv(:,:,k)= prior.Binv(:,:,k)+ sigmainv(k)*Xk'*Xk;
            post.B(:,:,k)=inv(post.Binv(:,:,k));
            post.b(:,k)  = post.B(:,:,k)*(squeeze(prior.Binv(:,:,k))*prior.b(:,k) + sigmainv(k)*Xk'*data.y(:,S==k)');
        end
        
    elseif  sigmafix   % standard finite mixture  with fixed variances
        
        if and(~isfield(model.par,'sigma'),~isfield(model.par,'sigmainv'))  warn(['A value has to be assigned to the variance before calling the function posterior for a fixed mean']);post=[];return;  end
        
        if studmix
            Nkomega=sum(ind.*repmat(model.omega,model.K,1),2)'; 
        else
            Nkomega=Nk;
        end
        
        if r==1
            if ~isfield(model.par,'sigmainv')  model.par.sigmainv = 1./model.par.sigma;    end
            
            post.Binv = prior.Binv+Nkomega.*model.par.sigmainv;
            post.B  = 1./post.Binv;
            sk = sum(ind .*  repmat(model.omega.*data.y,model.K,1),2)';  % sk=NK*mean(y)
            post.b  = post.B.*(prior.Binv.*prior.b + sk.*model.par.sigmainv);
            
        else    
            
            post.Binv = 0*prior.Binv;   post.B  = 0*prior.Binv;  post.b  = 0* prior.b;  
            sk = zeros(r,model.K);
            
            for k=1:model.K
                if ~isfield(model.par,'sigmainv')  sigmainv=inv(model.par.sigma(:,:,k)); else sigmainv=squeeze(model.par.sigmainv(:,:,k)); end
                
                post.Binv(:,:,k)= prior.Binv(:,:,k)+Nkomega(k)*sigmainv;
                post.B(:,:,k)=inv(post.Binv(:,:,k));
                post.b(:,k)  = post.B(:,:,k)*(squeeze(prior.Binv(:,:,k))*prior.b(:,k) + sigmainv*sum(repmat(model.omega(:,S==k),r,1).*data.y(:,S==k),2));
            end
            
        end   
        
    elseif mufix   % standard finite mixture  with fixed means
        
        if ~isfield(model.par,'mu') warn(['A value has to be assigned to the mean before calling the function posterior for a fixed mean']);post=[];return; end
        
        
        post.c=prior.c + 0.5*Nk;
        post.C=0*prior.C; 
        if r>1 post.logdetC=0*prior.c; end
        
        for k=1:model.K
            b=(data.y(:,S==k)-model.par.mu(:,k*ones(1,Nk(k)))).*repmat(model.omega(:,S==k).^.5,r,1);
            if r>1
                post.C(:,:,k)=prior.C(:,:,k) + 0.5*b*b'; 
                post.logdetC(k)=log(det(post.C(:,:,k)));
            else    
                post.C(k)=prior.C(k) + 0.5*b*b';
            end
        end
        
    elseif betafix  % finite mixture of multiple regression models with fixed regression parameters
        
        if ~isfield(model.par,'beta') warn(['A value has to be assigned to the regression parameters before calling the function posterior for a fixed mean']);post=[];return; end
        
        post.c=prior.c + 0.5*Nk;
        if ~isfield(model,'df') model.df=0; end
        if model.df>0
            intercept=[1:model.d];
            indexdr=intercept(all(repmat(model.indexdf,1,model.d)~=repmat(intercept,model.df,1),1));
            mu=model.par.beta'*data.X(indexdr,:);
            mu=mu+repmat(model.par.alpha'*data.X(model.indexdf,:),model.K,1);
        else    
            mu=model.par.beta'*data.X;
        end

        
        error=(repmat(data.y,model.K,1)-mu).*ind;
        post.C=prior.C  + 0.5*  sum(   error.^2  ,2)';
        
     elseif all([~betafix ~sigmafix isfield(model,'d')])    % finite mixture of multiple regression models under a conjugate prior
        
        % new October  2007
%         
        post.beta.Binv = 0*prior.beta.Binv;   post.beta.B  = 0*prior.beta.Binv;  post.beta.b  = 0* prior.beta.b;  
        post.sigma.C=0*prior.sigma.C;
        for k=1:model.K
            Xk=data.X(:,S==k)'; 
            post.beta.Binv(:,:,k) = prior.beta.Binv(:,:,k) + Xk'*Xk;
            post.beta.B(:,:,k)=inv(post.beta.Binv(:,:,k));
            post.beta.b(:,k)  = post.beta.B(:,:,k)*(squeeze(prior.beta.Binv(:,:,k))*prior.beta.b(:,k) + Xk'*data.y(:,S==k)');
            yhat = (Xk*post.beta.b(:,k))';
            post.sigma.C(1,k)=prior.sigma.C(1,k)  + 0.5*sum((data.y(1,S==k)- yhat).^2 ,2)' + 0.5*(prior.beta.b(:,k)-post.beta.b(:,k))'*prior.beta.Binv(:,:,k)*(prior.beta.b(:,k)-post.beta.b(:,k));
        end
        post.sigma.c=prior.sigma.c + 0.5*Nk;
    end     
else
    post.error=true; return;
end
