function pd=mixturepdf(mix,x);

% computes the marginal density of a univariate finite mixture model at grid points x
% x is stored by row

% Author: Sylvia Fruehwirth-Schnatter
 
if ~isfield(mix,'K') mix.K=1; mix.weight=1; end  % single member from the distribution family

M=size(x,2);
r=size(x,1);
pd=zeros(r,M);


%%   univariate mixture of normal distributions  
if mix.dist(1:6)=='Normal'
    
     
    for j=1:mix.K
        pd=pd+mix.weight(j)*normpdf(x,mix.par.mu(j),sqrt(mix.par.sigma(j)));
    end


%%   univariate mixture of skew normal distributions
     
elseif mix.dist(1:6)=='SkewNo'
    
    for j=1:mix.K
        pd=pd+2*mix.weight(j)*normpdf(x,mix.par.mu(j),sqrt(mix.par.sigma(j))).*normcdf(mix.par.lambda(j)*(x-mix.par.mu(j))/sqrt(mix.par.sigma(j)));
    end
    
%%   univariate mixture of skew-t distributions
    
elseif mix.dist(1:6)=='SkewSt'
     
    for j=1:mix.K
        z=(x-mix.par.mu(j))./sqrt(mix.par.sigma(j));
        sc=((mix.par.df(j)+1)./(mix.par.df(j)+z.^2)).^.5;
        pd=pd+2*mix.weight(j)*tpdf(z,mix.par.df(j)).*tcdf(mix.par.lambda(j)*sc.*z,1+mix.par.df(j))/sqrt(mix.par.sigma(j));
    end
    
    
%%   univariate mixture of t distributions  

elseif mix.dist(1:6)=='Studen'
     
    for j=1:mix.K
        pd=pd+mix.weight(j)*tpdf((x-mix.par.mu(j))./sqrt(mix.par.sigma(j)),mix.par.df(j))/sqrt(mix.par.sigma(j));
    end
    
%%   mixtures of Poisson distributions  
   
elseif all(mix.dist(1:6)=='Poisso')
    
    for j=1:mix.K
        pd=pd+mix.weight(j)*poisspdf(x,mix.par(j));
    end
    
%%   mixtures of Binomial distributions  
    
elseif all(mix.dist(1:6)=='Binomi')
    
    for j=1:mix.K
        pd=pd+mix.weight(j)*binopdf(x,mix.Ti,mix.par(j));
    end
    
%% mixtures of multinomial distributions

elseif all(mix.dist(1:6)=='Multin')
    
    if all(mix.Ti==1)
        for j=1:mix.K
            pd=pd+mix.weight(j)*mix.par.pi(:,j);
        end
    else
        'ADD MULTINOMIAL with T>1 IN mixturepdf'
        
    end

    
%%   mixtures of Exponential distributions  
     
elseif mix.dist(1:6)=='Expone'
    
     for j=1:mix.K
        pd=pd+mix.weight(j)*exppdf(x,1/mix.par(j));
    end
    
    
%%    multivariate mixture of normal distributions  
    
elseif all(mix.dist(1:6)=='Normul')
    %% WHen is this used? 
    
    for i=1:r
         for j=1:mix.K
            pd(i,:)=pd(i,:)+mix.weight(j)*normpdf(x(i,:),mix.par.mu(i,j),sqrt(squeeze(mix.par.sigma(i,i,j))));
        end
    end
    
    
else
    ['Mixture type ' mix.dist ' unknown']
    
end

 