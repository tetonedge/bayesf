function [x,phia1] =simulate_truncated_normal(mu,sigma,a,b,N)

if prod(size(sigma))<N sigma=repmat(sigma,1,N);end
if prod(size(mu))<N mu=repmat(mu,1,N);end

phib=normcdf((b-mu)./sqrt(sigma));
phia=normcdf((a-mu)./sqrt(sigma));


U=rand(1,N);

x=a*ones(1,N);

p=phia+U.*(phib-phia);

phia1= (p < 0.9999999999999999);

x(phia1)=mu(phia1)+sqrt(sigma(phia1)).*norminv(p(phia1));

if ~all(phia1) 
    'add extrem value in truncated normal'  
end

%ADD: How to simulate if phia==1??