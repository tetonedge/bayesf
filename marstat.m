function eta=marstat(xi)

K=size(xi,1);
A=[eye(K)-xi';ones(1,K)];
b=inv(A'*A)*A';
eta=b(:,K+1)';