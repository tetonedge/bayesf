function data=simulate(mix,N,varargin)

% Simulate from of a model  stored in the structure array mix
% Store the result in the structure array data

% Author: Sylvia Fruehwirth-Schnatter/Hw


rand('state',sum(100*clock)) ;
randn('state',sum(100*clock)) ;

mixdef=mix;
if ~isfield(mix,'K')   % single member from the distribution family
    mix.K=1;
end

%% simulate the allocations
if  mix.K==1
    
    mix.weight=1;
    S=ones(1,N);
    
else
    if ~isfield(mix,'indicmod') mix.indicmod.dist='Multinomial';  end

    if   mix.indicmod.dist(1:6)=='Multin'

        %Simulate the allocations S for a finite mixture model

        rnd = rand(N,1);
        fv=mix.weight(ones(N,1),:);
        S = (sum(cumsum(fv,2) < rnd(:,ones(mix.K,1)),2) + 1)';

    elseif   mix.indicmod.dist(1:6)=='Markov'

        %Simulate the states S for a finite Markov mixture model
        data.istimeseries=true;

        if ~isfield(mix.indicmod,'init') mix.indicmod.init='ergodic';  end

        % simulate S_0:
        if mix.indicmod.init(1:3)=='fix'
            data.S0 = str2num(mix.indicmod.init(4:size(mix.indicmod.init,2)));
            if any([data.S0<1 data.S0>mix.K abs(data.S0-fix(data.S0))>1e-6])
                warn('wrong starting value for S0');
                data=[]; return;
            end
        else
            if mix.indicmod.init(1:3)=='erg'
                p0=marstat(mix.indicmod.xi)';
            elseif mix.indicmod.init(1:3)=='uni'
                p0=ones(mix.K,1)/mix.K;
            end
            data.S0 = sum(cumsum(p0) < rand) + 1;
        end

        S=zeros(1,N);
        % simulate S_t:

        for t=1:N
            if t>1
                p_pred = mix.indicmod.xi(S(1,t-1),:)';   % p_pred is the predicitve density
            else
                p_pred = mix.indicmod.xi(data.S0,:)';
            end
            S(1,t) = sum(cumsum(p_pred) < rand) + 1;
        end
    end

end

%% Simulate the observations y


%% Check repetion parameter for the Binomial and multinomial distribution
if or(all(mix.dist(1:6)=='Binomi'),all(mix.dist(1:6)=='Multin'))
    % if no information about the repetition parameter is availbale, it is
    % assumed that the data are binary
    if nargin==3
        if ~isfield(varargin{1},'Ti')
            data.Ti=1;
        else
            data.Ti=varargin{1}.Ti;
        end
    else
        data.Ti=1;
    end
    if prod(size(data.Ti))>1
        if prod(size(data.Ti))~=N warn('Size mismatch when simulating from a binomial or multinomail distribution with heterogeneous repetition parameter'); end
    end
end

%% Simulate from a standard finite mixture model

finmix=all([~isfield(mix,'d') ~isfield(mix,'ar') ~isfield(mix,'arf')]);
if finmix 

    if all(mix.dist(1:6)=='Normal')

        y=mix.par.mu(S)+sqrt(mix.par.sigma(S)).*randn(1,N);
        data.type{1}='continuous';r=1;

    elseif mix.dist(1:6)=='SkewNo'
        delta=mix.par.lambda(S)./(1+mix.par.lambda(S).^2).^.5;
        data.bi=abs(randn(1,N));
        y=mix.par.mu(S)+sqrt(mix.par.sigma(S)).*delta.*data.bi+sqrt(mix.par.sigma(S)).*(1-delta.^2).^.5.*randn(1,N);
        data.type{1}='continuous';r=1;

    elseif mix.dist(1:6)=='Studen'

        omega=gamrnd(mix.par.df(S)/2,2./mix.par.df(S));
        y=mix.par.mu(S)+sqrt(mix.par.sigma(S)./omega).*randn(1,N);
        data.type{1}='continuous';r=1;
        data.omega=omega;

    elseif mix.dist(1:6)=='SkewSt'
        delta=mix.par.lambda(S)./(1+mix.par.lambda(S).^2).^.5;
        data.bi=abs(randn(1,N));
        omega=gamrnd(mix.par.df(S)/2,2./mix.par.df(S));
        sigmada=mix.par.sigma(S)./omega;
        y=mix.par.mu(S)+sqrt(sigmada).*delta.*data.bi+sqrt(sigmada).*(1-delta.^2).^.5.*randn(1,N);
        data.type{1}='continuous';r=1;
        data.omega=omega;

    elseif any([all(mix.dist(1:6)=='Normul') all(mix.dist(1:6)=='Stumul')])

        r=size(mix.par.mu,1);
        data.type=cell(r,1);for j=1:r;data.type{j,1}='continuous';end
        y=zeros(r,N);
        Qchol=zeros(r,r,mix.K);
        for k=1:mix.K;
            [Qchol(:,:,k) p]=chol(squeeze(mix.par.sigma(:,:,k)));
            if p==0
                Qchol(:,:,k)=Qchol(:,:,k)';
            elseif p~=0 %  mix.par.sigma(:,:,k) not positive definite
                Qchol(:,:,k)=mychol(squeeze(mix.par.sigma(:,:,k)),1e-8); % use cholesky decomposition by Rudi Fruehwirth
            end
        end

        if all(mix.dist(1:6)=='Stumul')
            omega=gamrnd(mix.par.df(S)/2,2./mix.par.df(S));
            data.omega=omega;
        else
            omega=ones(1,N);
        end

        for i=1:N;
            y(:,i)=mix.par.mu(:,S(i))+Qchol(:,:,S(i))*(randn(r,1)./repmat(omega(i)^.5,r,1));
        end

    elseif or(all(mix.dist(1:6)=='SkNomu'),all(mix.dist(1:6)=='SkStmu'))

        r=size(mix.par.mu,1);
        data.type=cell(r,1);for j=1:r;data.type{j,1}='continuous';end
        y=zeros(r,N);
        Qchol=zeros(r,r,mix.K);
        delom=zeros(r,mix.K);
        for k=1:mix.K;
            Omega=diag(diag(squeeze(mix.par.sigma(:,:,k))).^.5);
            omegainv=diag(diag(squeeze(mix.par.sigma(:,:,k))).^(-.5));
            Sbar=omegainv*squeeze(mix.par.sigma(:,:,k))*omegainv;
            delom(:,k)=Omega*Sbar*mix.par.lambda(:,k)/(1+mix.par.lambda(:,k)'*Sbar*mix.par.lambda(:,k))^.5;
            psi=squeeze(mix.par.sigma(:,:,k))-delom(:,k)*delom(:,k)';
            [Qchol(:,:,k) p]=chol(psi);
            if p==0
                Qchol(:,:,k)=Qchol(:,:,k)';
            elseif p~=0 %  mix.par.sigma(:,:,k) not positive definite
                Qchol(:,:,k)=mychol(psi,1e-8); % use cholesky decomposition by Rudi Fruehwirth
            end
        end


        data.bi=abs(randn(1,N));
        if all(mix.dist(1:6)=='SkStmu')
            omega=gamrnd(mix.par.df(S)/2,2./mix.par.df(S));
            data.omega=omega;
        else
            omega=ones(1,N);
        end

        for i=1:N;
            y(:,i)=mix.par.mu(:,S(i))+(delom(:,S(i))*data.bi(i)+Qchol(:,:,S(i))*randn(r,1))./repmat(omega(i)^.5,r,1);
        end

    elseif mix.dist(1:6)=='Poisso'

        data.type{1,1}='discrete';
        muall=mix.par(S)';
        y=poissrnd(muall,N,1)';
        r=1;

    elseif all(mix.dist(1:6)=='Binomi')

        data.type{1,1}='discrete';
        muall=mix.par(S);
        if prod(size(data.Ti))==1
            y=sum(rand(data.Ti,N)<repmat(muall,data.Ti,1),1);
        else
            y=binornd(data.Ti,muall,1,N,1);
        end
        r=1;
        
   elseif all(mix.dist(1:6)=='Multin')
       
       if ~isfield(mix,'r') mix.r=1; end
       r=mix.r;
       if ~isfield(mix,'cat') mix.cat=2*ones(r,1); end
       
       data.type=cell(mix.r,1);
       for j=1:mix.r;data.type{j,1}='discrete';end
       y=zeros(mix.r,N);
       for j=1:mix.r
           ij=sum(mix.cat(1:j-1))+[1:mix.cat(j)]';
           if all(data.Ti==1)
               rnd = rand(N,1);
               y(j,:) = (sum(cumsum(mix.par.pi(ij,S)',2) < rnd(:,ones(mix.cat(j),1)),2) + 1)';
           else
              'ADD MULTINOMIAL with T>1 IN simulate'
               %y(j,:)=binornd(data.Ti,muall,1,N,1);
           end
       end

    elseif mix.dist(1:6)=='Expone'
        mu=1./(mix.par(S));
        y=exprnd(mu,1,N);
        data.type{1}='continuous';r=1;
         
    else
        ['Mixture type ' mix.dist '  not supported by function mixuresimulate']
    end

else
%% simulate from finite mixtures of multiple regression models and MSAR models

    if isfield(mix,'ar')
        arp=mix.ar;
        data.istimeseries=true;
    elseif isfield(mix,'arf')
        arp=mix.arf;
        data.istimeseries=true;
    else
        arp=0;
    end

    if isfield(mix,'d')
        simulatedesign=false;
        if nargin==3
            if ~isfield(varargin{1},'X')
                if mix.d==1
                    data.X=ones(1,N);
                else
                   % warn('No design matrix is available to simulate from a mixture regression model; design is simulated');
                    simulatedesign=true;
                end
            else
                data.X=varargin{1}.X;
            end
        else
            if mix.d>1   simulatedesign=true;  else  data.X=ones(1,N);  end
        end

        if  simulatedesign
            % no design
            if all(mix.dist(1:6)=='Normal')
                a=0;b=1;
            elseif all(mix.dist(1:6)=='Poisso')
                a=0.5; b=1;
            elseif all(mix.dist(1:6)=='Negbin')
                a=0.5; b=1;
            elseif all(mix.dist(1:6)=='Binomi')
                a=-1; b=2;
            end
            data.X=[a+b*rand(mix.d-1,N); ones(1,N)];
        end
    else
        data.X=ones(1,N); mix.d=1;
    end

    if arp==0  % no AR TERMS

        if isfield(mix,'indexdf')
            mix.df=size(mix.indexdf,1)*size(mix.indexdf,2);
            % intercept=[1:mix.d];
            % indexdr=intercept(all(repmat(mix.indexdf,1,mix.d)~=repmat([1:mix.d],mix.df,1),1));
            indexdr= all(repmat(mix.indexdf,1,mix.d)~=repmat([1:mix.d],mix.df,1),1);
            mu=mix.par.beta'*data.X(indexdr,:);
            mu=mu+repmat(mix.par.alpha'*data.X(mix.indexdf,:),mix.K,1);
        else
            mu=mix.par.beta'*data.X;
        end

        if mix.dist(1:6)=='Normal'
            y=sum(mu.*(repmat(S,mix.K,1)==(repmat([1:mix.K]',1,N))),1)+sqrt(mix.par.sigma(S)).*randn(1,N);
        elseif mix.dist(1:6)=='Poisso'
            muall=sum(mu.*(repmat(S,mix.K,1)==(repmat([1:mix.K]',1,N))),1);
            y=zeros(size(muall));
            if any(muall<7)
                y(1,muall<7)=poissrnd(exp(muall(muall<7)),1,sum(muall<7,2));
            end
            if any(muall>=7)
                if any(muall>709,1)   warn('simulations from the Poisson distribution are exploding'); end
                y(1,muall>=7)=fix(exp(muall(muall>=7))+exp(0.5*muall(muall>=7))*randn(1,sum(muall>=7,2))+.5);
            end

        elseif all(mix.dist(1:6)=='Binomi')
            muall=sum(mu.*(repmat(S,mix.K,1)==(repmat([1:mix.K]',1,N))),1);
            y=binornd(data.Ti,exp(muall)./(1+exp(muall)));

        elseif all(mix.dist(1:6)=='Negbin')

            muall=sum(mu.*(repmat(S,mix.K,1)==(repmat([1:mix.K]',1,N))),1);
            nuall=mix.par.df(S);
            lamall=gamrnd(nuall,exp(muall)./nuall);
            y=poissrnd(lamall);


        end

    else    % AR TERMS
        if isfield(mix,'indexdf')
            mix.df=size(mix.indexdf,1)*size(mix.indexdf,2);
        else
            mix.df=0;
        end


        if nargin==3
            if ~isfield(data,'ystart')  defstart=true; else  defstart=false; end
        else
            defstart=true;
        end

        if defstart   % no starting value for the first observations
            data.ystart=zeros(1,arp);
            if  all(all(data.X==1))
                if ~isfield(data,'S0') S0=S(1); else S0=data.S0; end
                if isfield(mix,'ar')
                    if ~isfield(mix.par,'indexar')  mix.par.indexar=(mix.d-mix.df)+[1:mix.ar]';   end
                    ic= all(repmat([1:mix.ar+1]',1,mix.ar)~= repmat(mix.par.indexar',mix.ar+1,1),2);
                    data.ystart=repmat(mix.par.beta(ic,S0)/(1-sum(mix.par.beta(mix.par.indexar,S0))),1,arp);
                elseif isfield(mix,'arf')
                    data.ystart=repmat(mix.par.beta(S0)/(1-sum(mix.par.alpha)),1,arp);
                end

            end
        end

        datareg.y=[data.ystart zeros(1,N)];


        datareg.N=N+arp;
        datareg.X=[zeros(size(data.X,1),arp) data.X];
        % convert MSAR model into a large Regression model

        [datareg,mixreg]=designar(datareg,mix);



        if isfield(mixreg,'indexdf')
            mixreg.df=size(mixreg.indexdf,1)*size(mixreg.indexdf,2);
        else
            mixreg.df==0;
        end

        for t=1:N
            if t>1
                j= [1: min(t-1,arp)];
                datareg.X(mixreg.indexar(j),t)=datareg.y(1,t-j);
            end

            if mixreg.df>0
                % intercept=[1:mix.d];
                % indexdr=intercept(all(repmat(mix.indexdf,1,mix.d)~=repmat([1:mix.d],mix.df,1),1));
                indexdr= all(repmat(mixreg.indexdf,1,mixreg.d)~=repmat([1:mixreg.d],mixreg.df,1),1);
                mu=mixreg.par.beta'*datareg.X(indexdr,t);
                mu=mu+repmat(mixreg.par.alpha'*datareg.X(mixreg.indexdf,t),mix.K,1);
            else
                mu=mixreg.par.beta'*datareg.X(:,t);
            end

            if all(mix.dist(1:6)=='Normal')
                datareg.y(t)=sum(mu.*(repmat(S(t),mix.K,1)==[1:mix.K]'),1)+sqrt(mix.par.sigma(S(t))).*randn;
            elseif all(mix.dist(1:6)=='Poisso')
                muall=sum(mu.*(repmat(S(t),mix.K,1)==[1:mix.K]'),1)';

                if muall<=7
                    datareg.y(t)=poissrnd(exp(muall));
                elseif muall>709
                    warn('simulations from the Poisson distribution are exploding');
                else
                    datareg.y(t)=fix(exp(muall)+exp(0.5*muall)*randn+.5);
                end
            elseif all(mix.dist(1:6)=='Binomi')
                muall=sum(mu.*(repmat(S(t),mix.K,1)==[1:mix.K]'),1)';
                datareg.y(t)=binornd(data.Ti(t),exp(muall)/(1+exp(muall)));
            end

        end
        y=datareg.y; clear datareg;

    end

    if mix.dist(1:6)=='Normal'
        data.type{1}='continuous';
        r=1;
    elseif any([all(mix.dist(1:6)=='Poisso') all(mix.dist(1:6)=='Negbin')  all(mix.dist(1:6)=='Binomi')])
        data.type{1}='discrete';
        r=1;
    end
end

%% define the output structural array

data.y=y;
data.S=S;
data.N=N;
data.r=r;
data.sim=true;
data.model=mixdef;

if isfield(data,'ystart') data=rmfield(data,'ystart'); end