function fd = prodnorpdflog(par,theta)

% computes the logarithm of the prodcut of univariate normal posterior density 
 
% input
% par ... parameter of the aposteriori density, structrual array with the
%         fields mu and sigma; 
%         the number of colums determine the  number of products
%         for a single colums, this reduces to a single normal density
% theta ... argument of the  aposteriori density

%output: log of the posterior density
%        M times 1 array with M being the maximum number of row in par and
%        theta

% Author: Sylvia Fruehwirth-Schnatter
% Last change: September 13, 2006


if ~isfield(par,'mu') warn(['Field mu missing in function prodnorpdflog']);fd=[];return; end  
if ~isfield(par,'sigma') warn(['Field sigma missing in function prodnorpdflog']);fd=[];return; end  

if or(size(par.mu,1)~=size(par.sigma,1),size(par.mu,2)~=size(par.sigma,2)) 
     warn(['Size disagreement in the variable par in function prodnorpdflog']); fl=[]; return
end     

if ~(size(par.mu,2)==size(theta,2))   warn(['Size disagreement in function prodnorpdflog']); fl=[]; return; end

if size(par.mu,1)==1  % evaluate a single density at a sequence of draws
    
   par.mu=repmat(par.mu,size(theta,1),1);
   par.sigma=repmat(par.sigma,size(theta,1),1);
   
elseif size(theta,1)==1  % evaluate a sequence of density at a single draw
    
   theta=repmat(theta,size(par.mu,1),1);
    
elseif ~(size(par.mu,1)==size(theta,1))
    warn(['Size disagreement in function prodnorpdflog']); fl=[]; return
else    
%    evaluate  density in row i at the corresponding draw in row i
end

fd=-0.5*sum(log(2*pi*par.sigma)+(theta-par.mu).^2./par.sigma,2);
