function [seblock, sechib]=chib_se(r,varargin)

B=nargin-1;
M=size(varargin{1},1);
h=zeros(M,B);
for b=1:B
    h(:,b)=varargin{b};
    h(:,b)=exp(h(:,b)-max(h(:,b)));
end

[ac, hm, sd,ki, varhhat, ineff] = autocov(h,r);
seblock=(diag(varhhat).^.5./hm)';
sechib=(((1./hm)'*varhhat*(1./hm))^.5);

