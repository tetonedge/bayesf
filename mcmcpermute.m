function mcmcout=mcmcpermute(mcmcout)

if ~isfield(mcmcout.model,'indicmod')
    indicmod.dist='Multinomial';
else
    indicmod.dist=mcmcout.model.indicmod.dist;
end

estpm=mcmcpm(mcmcout);
indexp=[1:mcmcout.M];
finmix=all([~isfield(mcmcout.model,'d') ~isfield(mcmcout.model,'ar') ~isfield(mcmcout.model,'arf')]);

if and(mcmcout.model.dist(1:6)=='Poisso',finmix)

    parclust=mcmcout.par';
    clustart=estpm.par';

    %  log
    %  parclust=log(mcmcout.par)';
    %  clustart=log(estpm.par)';

    % square root
    parclust=(mcmcout.par.^.5)';
    clustart=(estpm.par.^.5)';


    parclust=reshape(parclust,size(parclust,1)*size(parclust,2),1);

elseif and(all(mcmcout.model.dist(1:6)=='Binomi'),finmix)

    parclust=mcmcout.par';
    clustart=estpm.par';

    %  logit
    parclust=log(mcmcout.par)'-log(1-mcmcout.par)';
    clustart=log(estpm.par)'-log(1-estpm.par)';
    parclust=reshape(parclust,size(parclust,1)*size(parclust,2),1);


    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % HW
elseif and(mcmcout.model.dist(1:6)=='Expone',finmix)

    parclust=mcmcout.par';
    clustart=estpm.par';

    %  log
    %  parclust=log(mcmcout.par)';
    %  clustart=log(estpm.par)';

    % square root
    parclust=(mcmcout.par.^.5)';
    clustart=(estpm.par.^.5)';

    parclust=reshape(parclust,size(parclust,1)*size(parclust,2),1);
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

elseif  and(or(all(mcmcout.model.dist(1:6)=='SkewNo'),all(mcmcout.model.dist(1:6)=='SkewSt')),finmix)

    %mucll=false; % clustering based   on the component mean
    mucll=true; % clustering based   on the component parameters

    if mucll
        parclust=zeros(mcmcout.M,3,mcmcout.model.K);
        parclust(:,1,:)=mcmcout.par.mu;
        parclust(:,2,:)=mcmcout.par.lambda;
        parclust(:,3,:)=mcmcout.par.sigma.^.5;   % sqrt(var) of the component
        clustart=[estpm.par.mu; estpm.par.lambda;estpm.par.sigma.^.5]';
        parclust=permute(parclust,[3 1 2]); %permute such that first index is equal to group, second index to MCMC iteratio, third index to the components of group specific parameter
        parclust=reshape(parclust,size(parclust,1)*size(parclust,2),3);

    else
        parclust=reshape(mcmcout.par.mean',size(mcmcout.par.mean,1)*size(mcmcout.par.mean,2),1);

        estpm=skewn_transform(estpm);
        if all(mcmcout.model.dist(1:6)=='SkewSt')
            scale=sqrt(2/pi)*sqrt(estpm.par.df/2).*exp(gammaln((estpm.par.df-1)/2)-gammaln(estpm.par.df/2));
            clustart=(estpm.par.mu+ scale.*estpm.par.psi)';
        else
            clustart=(estpm.par.mu+sqrt(2/pi)*estpm.par.psi)';
        end
    end



elseif and(or(all(mcmcout.model.dist(1:6)=='SkNomu'),all(mcmcout.model.dist(1:6)=='SkStmu')),finmix)

    r=size(mcmcout.par.mu,2);

    mucll=false; % clustering based   on the component mean
    %mucll=true; % clustering based   on the location vector

    if mucll
        parclust=mcmcout.par.mu;
        clustart=[estpm.par.mu'];
    else
        parclust=mcmcout.par.mean;

        estpm=skewn_transform(estpm);
        if all(mcmcout.model.dist(1:6)=='SkStmu')
            scale=sqrt(2/pi)*sqrt(estpm.par.df/2).*exp(gammaln((estpm.par.df-1)/2)-gammaln(estpm.par.df/2));
            clustart=(estpm.par.mu+repmat(scale,r,1).*estpm.par.psi)';
        else
            clustart=(estpm.par.mu+sqrt(2/pi)*estpm.par.psi)';
        end
    end
    parclust=permute(parclust,[3 1 2]); %permute such that first index is equal to group, second index to MCMC iteratio, third index to the components of group specific parameter
    parclust=reshape(parclust,size(parclust,1)*size(parclust,2),size(parclust,3));


elseif and(or(mcmcout.model.dist(1:6)=='Normal',mcmcout.model.dist(1:6)=='Studen'),finmix)

    parclust=zeros(mcmcout.M,2,mcmcout.model.K);
    parclust(:,1,:)=mcmcout.par.mu;
    parclust(:,2,:)=mcmcout.par.sigma.^.5;

    parclust=permute(parclust,[3 1 2]); %permute such that first index is equal to group, second index to MCMC iteratio, third index to the components of group specific parameter

    parclust=reshape(parclust,size(parclust,1)*size(parclust,2),2);

    clustart=[estpm.par.mu' estpm.par.sigma.^.5'];


elseif and(or(mcmcout.model.dist(1:6)=='Normul',mcmcout.model.dist(1:6)=='Stumul'),finmix)

    r=size(mcmcout.par.mu,2);

    mucll=(1==1); % clustering based   on the mean vector
    mucll=(1==0); % clustering based   on the mean vector and the diagonals of Q

    if mucll
        parclust=mcmcout.par.mu;
        parclust=permute(parclust,[3 1 2]); %permute such that first index is equal to group, second index to MCMC iteratio, third index to the components of group specific parameter
        parclust=reshape(parclust,size(parclust,1)*size(parclust,2),size(parclust,3));
        clustart=[estpm.par.mu'];

    else

        indexdiag=[1:fix(r*(r+1)/2)]';
        indexdiag=diag(qinmatr(indexdiag))';
        indexdiag2=reshape([1:r^2]',r,r);
        indexdiag2=diag( indexdiag2);

        parclust=cat(2,mcmcout.par.mu,mcmcout.par.sigma(:,indexdiag,:));
        parclust=permute(parclust,[3 1 2]); %permute such that first index is equal to group, second index to MCMC iteratio, third index to the components of group specific parameter
        parclust=reshape(parclust,size(parclust,1)*size(parclust,2),size(parclust,3));

        Qst=qincolmult(estpm.par.sigma);
        clustart=[estpm.par.mu'  Qst(indexdiag,:)'];

    end

else

    parclust=mcmcout.par.beta;
    parclust=permute(parclust,[3 1 2]); %permute such that first index is equal to group, second index to MCMC iteratio, third index to the components of group specific parameter
    parclust=reshape(parclust,size(parclust,1)*size(parclust,2),size(parclust,3));
    clustart=[estpm.par.beta'];

end

[S,clu]=kmeans(parclust,mcmcout.model.K,'start',clustart,'EmptyAction','singleton');
clear parclust;
mcmcout.perm=reshape(S,mcmcout.model.K,size(S,1)/mcmcout.model.K)'; % reshape M times K

% not sufficient
% mcmcout.isperm=(sum(mcmcout.perm,2)==(mcmcout.model.K*(mcmcout.model.K+1)/2)); % check if permutation, M times 1
mcmcout.isperm=all(sort(mcmcout.perm,2)==repmat([1:mcmcout.model.K],mcmcout.M,1),2);

mcmcout.nonperm=sum(~mcmcout.isperm); % number of draws that may not be permuted uniquely

indexperm=indexp(mcmcout.isperm);  % consider only permuted draws
if size(indexperm,2)>1
    mcmcsub=mcmcsubseq(mcmcout,indexperm);


    mcmcout.parperm=mcmcsub.par;
    if indicmod.dist(1:6)=='Multin'
        mcmcout.weightperm=mcmcsub.weight;
    elseif indicmod.dist(1:6)=='Markov'
        mcmcout.indicmod.xiperm=mcmcsub.indicmod.xi;
    end

    if isfield(mcmcout.model,'d') d=mcmcout.model.d; else  d=1; end
    if isfield(mcmcout.model,'indexdf')
        df=size(mcmcout.model.indexdf,1)*size(mcmcout.model.indexdf,2);
    else
        df=0;
    end
    if isfield(mcmcout.model,'arf') d=d+mcmcout.model.arf; df=df+mcmcout.model.arf; end
    if isfield(mcmcout.model,'ar') d=d+mcmcout.model.ar;end


    Mperm=mcmcout.M-mcmcout.nonperm;
    mcmcout.Mperm=Mperm;

    for k=1:mcmcout.model.K

        ik=(mcmcout.perm(indexperm,:)==k*ones(Mperm,mcmcout.model.K,1));

        if indicmod.dist(1:6)=='Multin'


            mcmcout.weightperm(:,k)=sum(ik.*mcmcsub.weight,2);


        elseif indicmod.dist(1:6)=='Markov'
            % permute columns
            mcmcout.indicmod.xiperm(:,:,k)=sum(permute(repmat(ik,[1 1 mcmcout.model.K ]),[1 3 2]).*mcmcsub.indicmod.xi,3);

        end

        if and(or(all(mcmcout.model.dist(1:6)=='Normal'),all(mcmcout.model.dist(1:6)=='Studen')),finmix)

            mcmcout.parperm.mu(:,k)=sum(ik.*mcmcsub.par.mu,2);
            if mcmcout.model.dist(1:6)=='Studen'
                mcmcout.parperm.df(:,k)=sum(ik.*mcmcsub.par.df,2);
            end
            mcmcout.parperm.sigma(:,k)=sum(ik.*mcmcsub.par.sigma,2);

        elseif and(or(all(mcmcout.model.dist(1:6)=='SkewNo'),all(mcmcout.model.dist(1:6)=='SkewSt')),finmix)

            mcmcout.parperm.mu(:,k)=sum(ik.*mcmcsub.par.mu,2);
            mcmcout.parperm.mean(:,k)=sum(ik.*mcmcsub.par.mean,2);
            mcmcout.parperm.lambda(:,k)=sum(ik.*mcmcsub.par.lambda,2);
            mcmcout.parperm.sigma(:,k)=sum(ik.*mcmcsub.par.sigma,2);

            if all(mcmcout.model.dist(1:6)=='SkewSt')
                mcmcout.parperm.df(:,k)=sum(ik.*mcmcsub.par.df,2);
            end

        elseif and(any([all(mcmcout.model.dist(1:6)=='Normul') all(mcmcout.model.dist(1:6)=='Stumul') all(mcmcout.model.dist(1:6)=='SkStmu') all(mcmcout.model.dist(1:6)=='SkNomu') ]),finmix)

            mcmcout.parperm.logdet(:,k)=sum(ik.*mcmcsub.par.logdet,2);
            if or(all(mcmcout.model.dist(1:6)=='Stumul'),all(mcmcout.model.dist(1:6)=='SkStmu'))   mcmcout.parperm.df(:,k)=sum(ik.*mcmcsub.par.df,2);    end
            ikr(:,1,:)=ik; clear ik;
            mcmcout.parperm.mu(:,:,k)=sum(repmat(ikr,[1 r 1]).*mcmcsub.par.mu,3);
            mcmcout.parperm.sigma(:,:,k)=sum(repmat(ikr,[1 r*(r+1)/2 1]).*mcmcsub.par.sigma,3);
            mcmcout.parperm.sigmainv(:,:,k)=sum(repmat(ikr,[1 r*(r+1)/2 1]).*mcmcsub.par.sigmainv,3);
            if or(all(mcmcout.model.dist(1:6)=='SkStmu'),all(mcmcout.model.dist(1:6)=='SkNomu'))
                mcmcout.parperm.mean(:,:,k)=sum(repmat(ikr,[1 r 1]).*mcmcsub.par.mean,3);
                mcmcout.parperm.lambda(:,:,k)=sum(repmat(ikr,[1 r 1]).*mcmcsub.par.lambda,3);
            end

        elseif and(mcmcout.model.dist(1:6)=='Poisso',finmix)
            mcmcout.parperm(:,k)=sum(ik.*mcmcsub.par,2);

        elseif and(mcmcout.model.dist(1:6)=='Expone',finmix)
            mcmcout.parperm(:,k)=sum(ik.*mcmcsub.par,2);

        elseif and(mcmcout.model.dist(1:6)=='Binomi',finmix)
            mcmcout.parperm(:,k)=sum(ik.*mcmcsub.par,2);
        else
            if  mcmcout.model.dist(1:6)=='Normal' mcmcout.parperm.sigma(:,k)=sum(ik.*mcmcsub.par.sigma,2); end
            ikr(:,1,:)=ik; clear ik;
            mcmcout.parperm.beta(:,:,k)=sum(repmat(ikr,[1 (d-df) 1]).*mcmcsub.par.beta,3);
        end
    end

    if indicmod.dist(1:6)=='Markov'
        mcmcout.indicmod.xipermcol=mcmcout.indicmod.xiperm;
        for k=1:mcmcout.model.K
            ik=(mcmcout.perm(indexperm,:)==k*ones(Mperm,mcmcout.model.K,1));
            % permute rows
            mcmcout.indicmod.xiperm(:,k,:)=sum(repmat(ik,[1 1 mcmcout.model.K]).*mcmcout.indicmod.xipermcol,2);
        end
        clear mcmcout.indicmod.xipermcol;
    end

else
    mcmcout.Mperm=0;
    warn('Not a single draw is a permutation in the function mcmcpermute');
end

