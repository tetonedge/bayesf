function moments=mixturemomentsnor(mix,J,a);  

zm([1:2:J])=0;
zm([2:2:J])=exp(cumsum(log([1:2:(J-1)])));

moments=zeros(J,1);
for m=2:J
    
    moments(m)=sum(mix.weight.*(mix.par.mu-a).^m,2);    
    for n=1:m
        cm=(mix.par.mu-a).^(m-n).*(mix.par.sigma).^(n/2)*zm(n); 
        moments(m)=moments(m)+nchoosek(m,n)*sum(mix.weight.*cm,2);
    end
    
end    
  