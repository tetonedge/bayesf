function varargout=mixtureplot(mix,varargin);

% Plot the density of a finite mixture model, stored in the structure array mix 

% Author: Sylvia Fruehwirth-Schnatter

if nargin==1 nfig=1; else nfig=fix(varargin{1}); end

if ~isfield(mix,'K') mix.K=1; mix.weight=1; end  % single member from the distribution family
if and(mix.K==1,~isfield(mix,'weight')) mix.weight=1; end  % single member from the distribution family

if ~isfield(mix,'indicmod') mix.indicmod.dist='Multinomial';  end
if   mix.indicmod.dist(1:6)=='Markov'  if mix.K>1 mix.weight=marstat(mix.indicmod.xi); else mix.weight=1; end;end

finmix=all([~isfield(mix,'d') ~isfield(mix,'ar') ~isfield(mix,'arf')]);


%% Check repetion parameter for the Binomial and multinomial distribution
if or(all(mix.dist(1:6)=='Binomi'),all(mix.dist(1:6)=='Multin'))
    if ~isfield(mix,'Ti')         mix.Ti=1;    end
end



%%   univariate mixture of normal distributions - density plot

if and(mix.dist(1:6)=='Normal',finmix)
    
    % define the grid on the x-axis    
    M=1000;  % number of grid points
    [ss is]=sort(mix.par.mu); %sort the means to define the domain of the plot
    c=5; % define the lower and the upper bound from the quantile    
    
    x=linspace(mix.par.mu(is(1))-c*sqrt(mix.par.sigma(is(1))),mix.par.mu(is(mix.K))+c*sqrt(mix.par.sigma(is(mix.K))),M);
    
    pd=mixturepdf(mix,x);
    figure(nfig);
    plot(x,pd,'k');
    
    
    %%   univariate mixture of skew normal distributions - density plot
    
elseif and(mix.dist(1:6)=='SkewNo',finmix)
    
    % define the grid on the x-axis    
    M=1000;  % number of grid points
    mix.par.delta=mix.par.lambda./(1+mix.par.lambda.^2).^.5;
    mix.par.mumarg=mix.par.mu+0.8*sqrt(mix.par.sigma).*mix.par.delta;
    mix.par.smarg=mix.par.sigma.*(1-0.64*mix.par.delta.^2); 
    [ss is]=sort(mix.par.mumarg); %sort the means to define the domain of the plot
    c=5; % define the lower and the upper bound from the quantile    
    
    x=linspace(mix.par.mumarg(is(1))-c*sqrt(mix.par.sigma(is(1))),mix.par.mumarg(is(mix.K))+c*sqrt(mix.par.sigma(is(mix.K))),M);
    
    pd=mixturepdf(mix,x);
    figure(nfig);
    plot(x,pd,'k');
    
    %%   univariate mixture of skew-t distributions - density plot
    
elseif and(mix.dist(1:6)=='SkewSt',finmix)
    
    % define the grid on the x-axis    
    M=1000;  % number of grid points
    mix.par.delta=mix.par.lambda./(1+mix.par.lambda.^2).^.5;
    sc=5*ones(1,mix.K); 
    gl=[gammaln(mix.par.df(mix.par.df>1)/2-0.5); gammaln(mix.par.df(mix.par.df>1)/2)];
    
    %sc(mix.par.df>1)=(mix.par.df(mix.par.df>1)/pi).^.5.*gamma(mix.par.df(mix.par.df>1)/2-0.5)./gamma(mix.par.df(mix.par.df>1)/2);
    sc(mix.par.df>1)=(mix.par.df(mix.par.df>1)/pi).^.5.*exp(diff(-gl));
    
    mix.par.mumarg=mix.par.mu+sqrt(mix.par.sigma).*mix.par.delta.*sc;
    
    scv=10*ones(1,mix.K); 
    scv(mix.par.df>2)=mix.par.df(mix.par.df>2)./(mix.par.df(mix.par.df>2)-2) ;
    
    
    mix.par.smarg=mix.par.sigma.*(scv-(mix.par.delta.*sc).^2); 
    
    [ss is]=sort(mix.par.mumarg); %sort the means to define the domain of the plot
    c=5; % define the lower and the upper bound from the quantile    
    
    x=linspace(mix.par.mumarg(is(1))-c*sqrt(mix.par.sigma(is(1))),mix.par.mumarg(is(mix.K))+c*sqrt(mix.par.sigma(is(mix.K))),M);
    
    pd=mixturepdf(mix,x);
    figure(nfig);
    plot(x,pd,'k');
    
    
    %%   univariate mixture of t distributions - density plot
    
elseif and(mix.dist(1:6)=='Studen',finmix)
    
    % define the grid on the x-axis    
    M=1000;  % number of grid points
    [ss is]=sort(mix.par.mu); %sort the means to define the domain of the plot
    
    cl=min(tinv(0.005,mix.par.df(is(1))),-5); % define the lower and the upper bound from the quantile    
    cu=max(tinv(0.995,mix.par.df(is(mix.K))),5); % define the lower and the upper bound from the quantile    
    
    x=linspace(mix.par.mu(is(1))+cl*sqrt(mix.par.sigma(is(1))),mix.par.mu(is(mix.K))+cu*sqrt(mix.par.sigma(is(mix.K))),M);
    
    pd=mixturepdf(mix,x);
    figure(nfig);
    plot(x,pd,'k');
    
    
    %%    multivariate mixture of normal distributions - contourplot 
    
elseif and(mix.dist(1:6)=='Normul',finmix)
    
    % define the grid on the x-axis    
    r=mix.r;    
    
    if r==2    % surface and two contour plots  for bivariate distributions
        
        option.npoint=100;
        %option.contonly=true;
        nfig=surfcontmix(mix,option,nfig);
        
    elseif r <=5  % contourplot of each pair of bivariate marginal distributions
        
        %option.npoint=100;
        ij=nchoosek([1:r]',2);
        [nr, nc]=plotsub(size(ij,1));
        
        for ii=1:size(ij,1)
            subplot(nr,nc,ii);
            mixij=mixturemar(mix,ij(ii,:));
            contmix(mixij,npoint);
            xlabel(['y_' num2str(ij(ii,1))]);
            ylabel(['y_' num2str(ij(ii,2))]);
        end
        
    else
        'Dimension of the multivaraite density too high in function mixture plot' 
    end    
    
    %%    multivariate mixture of t distributions  
    
elseif and(mix.dist(1:6)=='Stumul',finmix)
    
    % define the grid on the x-axis    
    r=mix.r;    
    
    %if r==2    % surface and two contour plots  for bivariate distributions
    %           % curretnly no surface plot
    %    option.npoint=100; 
    %    nfig=surfcontmix(mix,option,nfig);  % has to be adapted
    %elseif r <=5  % contourplot of each pair of bivariate marginal distributions
    
    if r <=5  % contourplot of each pair of bivariate marginal distributions        
        
        ij=nchoosek([1:r]',2);
        [nr, nc]=plotsub(size(ij,1));
        
        for ii=1:size(ij,1)
            subplot(nr,nc,ii);
            mixij=mixturemar(mix,ij(ii,:));
            contmixstud(mixij,npoint);
            xlabel(['y_' num2str(ij(ii,1))]);
            ylabel(['y_' num2str(ij(ii,2))]);
        end
        
    else
        'Dimension of the multivaraite density too high in function mixture plot' 
    end    
    
    %% mixture of Poisson distributions    
    
elseif and(mix.dist(1:6)=='Poisso',finmix)
    
    figure(nfig);
    
    [ss is]=sort(mix.par); %sort the means to define the domain of the plot
    cl=0.01; % define the lower bound from the lower quantile    
    cu=0.995; % define the upper bound from the upper quantile    
    
    x=[poissinv(cl,mix.par(is(1))):poissinv(cu,mix.par(is(mix.K)))];
    
    pd=mixturepdf(mix,x);
    
    bar(x,pd,0.5); %SHADING FLAT; 
    %axis([x(1)-1 x(end)+1  0 max(pd)*1.1])
    
    %% mixtures of Binomial distributions
    
elseif and(all(mix.dist(1:6)=='Binomi'),finmix)
    
    if  prod(size(mix.Ti))==1
        figure(nfig);
        
        [ss is]=sort(mix.par); %sort the means to define the domain of the plot
        cl=0.01; % define the lower bound from the lower quantile    
        cu=0.995; % define the upper bound from the upper quantile    
        
        x=[binoinv(cl,mix.Ti,mix.par(is(1))):binoinv(cu,mix.Ti,mix.par(is(mix.K)))];
        
        pd=mixturepdf(mix,x);
        
        bar(x,pd,0.5); %SHADING FLAT; 
        %axis([x(1)-1 x(end)+1  0 max(pd)*1.1])
    end
    
elseif and(all(mix.dist(1:6)=='Multin'),finmix)
    
    if  all(mix.Ti==1)
        figure(nfig);
        
       if ~isfield(mix,'cat') mix.cat=2; end
       if ~isfield(mix,'label') mix.label=[1:mix.cat]'; end

       
        x=mix.label;
        
        pd=mixturepdf(mix,x);
        
        bar(x,pd,0.5); %SHADING FLAT; 
        %axis([x(1)-1 x(end)+1  0 max(pd)*1.1])
    else
        'ADD MULTINOMIAL with T>1 IN mixtureplot'
        
    end
    
    
    %% mixtures of Exponential distributions
    
elseif mix.dist(1:6)=='Expone'
    
    % define the grid on the x-axis    
    M=1000;  % number of grid points
    [ss is]=sort(mix.par); %sort the means to define the domain of the plot
    cl=0.01; % define the lower bound from the lower quantile    
    cu=0.95; % define the upper bound from the upper quantile    
    
    x=linspace(-log(1-cl)/mix.par(is(mix.K)),-log(1-cu)/mix.par(is(1)),M);
    
    pd=mixturepdf(mix,x);
    
    plot(x,pd,'k');
    
    
    
elseif finmix
    ['Mixture type ' mix.dist ' unknown']
    
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% times series models: plotac 

if  and(mix.indicmod.dist(1:6)=='Markov',finmix) 
    if or(mix.dist(1:6)=='Normal',mix.dist(1:6)=='Poisso')
        mom=moments(mix);
        nfig=nfig+1;        figure(nfig);
        subplot(2,1,1);        bar(mom.ac);title('Autocorrelation of Y_t'); xlabel('lag');
        subplot(2,1,2);        bar(mom.acsqu);title('Autocorrelation of Y_t^2'); xlabel('lag');
    else
        'ADD PLOTTING THE AC FUNCTION'
    end     
end

if nargout==1 varargout{1}=nfig; end
