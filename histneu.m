function histneu(x,k,cl)

% plot a histogram of the data
% modification of the matlab function hist

% x ... observations
% k ... number of bins
% cl ... color of plot

[nh,bc] = hist(x,k);
dx=bc(2)-bc(1);
d=nh/sum(nh)/dx;
c=[bc-dx*0.5 bc(end)+dx*0.5];
for i=1:k
   plot([c(i) c(i+1)],[d(i) d(i)],cl)
   hold on
   plot([c(i) c(i)],[0 d(i)],cl)
   plot([c(i+1) c(i+1)],[0 d(i)],cl)
end
hold off;
