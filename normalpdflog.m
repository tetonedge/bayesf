function fd = normalpdflog(par,theta)

% computes the logarithm of univariate normal posterior density over a grid
 
% input
% par ... parameter of the aposteriori density, structrual array with the
%         fields mu and sigma; 
% theta ... argument of the  aposteriori density

%output: log of the posterior density
%        M times N array with N being the maximum number of row in par and
%        theta

% Author: Sylvia Fruehwirth-Schnatter
% Last change: September 22, 2006


if ~isfield(par,'mu') warn(['Field mu missing in function normalpdflog']);fd=[];return; end  
if ~isfield(par,'sigma') warn(['Field sigma missing in function normalpdflog']);fd=[];return; end  

if or(size(par.mu,1)~=size(par.sigma,1),size(par.mu,2)~=size(par.sigma,2)) 
     warn(['Size disagreement in the variable par in function normalpdflog']); fl=[]; return
end     

if ~(size(par.mu,2)==size(theta,2))   warn(['Size disagreement in function normalpdflog']); fl=[]; return; end

if size(par.mu,1)==1  % evaluate a single density at a sequence of draws
    
   par.mu=repmat(par.mu,size(theta,1),1);
   par.sigma=repmat(par.sigma,size(theta,1),1);
   
elseif size(theta,1)==1  % evaluate a sequence of density at a single draw
    
   theta=repmat(theta,size(par.mu,1),1);
    
elseif ~(size(par.mu,1)==size(theta,1))
    warn(['Size disagreement in function normalpdflog']); fl=[]; return
else    
%    evaluate  density in row i at the corresponding draw in row i
end

fd=-0.5*(log(2*pi*par.sigma)+(theta-par.mu).^2./par.sigma);
