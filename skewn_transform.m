function mix=skewn_transform(mix,varargin);

if nargin==1
    kall=[1:mix.K];
else
    kall=varargin{1};
end

if mix.r==1
    delta=mix.par.lambda./sqrt(1+mix.par.lambda.^2);
    mix.par.psi=(mix.par.sigma.^.5).*delta;
    mix.par.sigmaeps=mix.par.sigma.*(1-delta.^2);
    
else
    if ~isfield(mix.par,'sigmaeps')  mix.par.sigmaeps=zeros(size(mix.par.sigma)); end
    if ~isfield(mix.par,'sigmaepsinv')  mix.par.sigmaepsinv=zeros(size(mix.par.sigma)); end
    if ~isfield(mix.par,'psi')  mix.par.psi=zeros(size(mix.par.mu)); end

    for k=kall
        
        dum1=diag(squeeze(mix.par.sigma(:,:,k))).^(-.5).*mix.par.lambda(:,k);  % omega invertiert
        dum2=squeeze(mix.par.sigma(:,:,k))*dum1;
        mix.par.psi(:,k)=dum2/(1+dum1'*dum2)^.5;
        
        mix.par.sigmaeps(:,:,k)=squeeze(mix.par.sigma(:,:,k))-mix.par.psi(:,k)*mix.par.psi(:,k)';
        mix.par.sigmaepsinv(:,:,k)= inv(mix.par.sigmaeps(:,:,k));

        

        
    end
end

if nargin==1
         if isfield(mix.par,'df')
             scale=sqrt(2/pi)*sqrt(mix.par.df/2).*exp(gammaln((mix.par.df-1)/2)-gammaln(mix.par.df/2));
             mix.par.mean=mix.par.mu+repmat(scale,mix.r,1).*mix.par.psi;
         else
             mix.par.mean=mix.par.mu+sqrt(2/pi)*mix.par.psi;
         end
end

