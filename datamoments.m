function moments=datamoments(data)

% compute sample moments of data
% Author: Sylvia Fruehwirth-Schnatter
% data are handled as data stored by row

ibycolumn=isfield(data,'bycolumn');
if ibycolumn  ibycolumn=data.bycolumn; end  % ibycolumn true: data stored by column 
if ibycolumn      y=data.y'; else     y=data.y; end   

if isfield(data,'r') r=data.r;else r=size(y,1); end
if isfield(data,'N') N=data.N;else N=size(y,2); end
if ~isfield(data,'istimeseries') data.istimeseries=false; end
if ~isfield(data,'type') for j=1:r; data.type{j}='continuous';end; end


moments.mean=mean(y,2);

if r==1 
    moments.var=var(y,1);
else
    moments.var=cov(y',1);
end


contdat=true;
for j=1:r;
    contdat=and(contdat,data.type{j}(1:3)=='con');
end
disdat=true;for j=1:r;disdat=and(disdat,data.type{j}(1:3)=='dis');end


if contdat
    if r>1
    cd=diag(1./diag(moments.var).^.5);
    moments.corr=cd*moments.var*cd;
end
    d=y-repmat(moments.mean,1,N);
    moments.high=zeros(r,4);
    moments.high(:,1)=0;
    moments.high(:,2)=mean(d.^2,2);
    moments.high(:,3)=mean(d.^3,2);
    moments.high(:,4)=mean(d.^4,2);
    moments.skewness= moments.high(:,3)./moments.high(:,2).^1.5; 
    moments.kurtosis=moments.high(:,4)./moments.high(:,2).^2; 
    
    if and(isfield(data,'S'),isfield(data,'model'))
        if isfield(data.model,'K');
            S=data.S;
            K=data.model.K;
            Nk=countex(data.S,K)';
            if r>1 Wk=zeros(r,r,K); yk=zeros(r,K);  else  Wk=zeros(1,K); yk=zeros(1,K);  end;   varW=Wk;
            
            for k=1:K
                if Nk(k)>0
                    yk(:,k)=sum(y(:,S==k),2)/Nk(k);
                    dk=(y(:,S==k)-yk(:,k*ones(1,Nk(k))));
                    if r>1
                        Wk(:,:,k)=(dk*dk');
                        varW(:,:,k)=Wk(:,:,k)/Nk(k);
                    else
                        Wk(:,k)=(dk*dk');
                        varW(:,k)=Wk(:,k)/Nk(k);
                    end
                end
            end
            moments.groupmom.Nk=Nk;
            moments.groupmom.mean=yk;
            moments.groupmom.Wk=Wk;
            moments.groupmom.var=varW;
            if r==1 
                moments.W=sum(Wk,2);
            else
                moments.W=sum(Wk,3);
            end    
            moments.T=N*moments.var;
            moments.B=moments.T-moments.W;
            if r==1
                moments.R=1-moments.W/moments.T;
            else
                
                moments.Rtr=1-trace(moments.W)/trace(moments.T);
                
                moments.Rdet=1-det(moments.W)/det(moments.T);
            end
        end
    end    
    
elseif disdat
    moments.over=moments.var-moments.mean;
    moments.factorial=zeros(1,4);
    moments.factorial(1)=moments.mean; 
    moments.factorial(2)= mean(y.*max(y-1,0),2);
    moments.factorial(3)= mean(y.*max(y-1,0).*max(y-2,0),2);
    moments.factorial(4)= mean(y.*max(y-1,0).*max(y-2,0).*max(y-3,0),2);
    moments.zero=sum(y==0,2)/N;
end

if  data.istimeseries
lag=20;
[moments.ac, dum1, dum2, dum, dum4, dum5] = autocov(data.y',lag);
[moments.acsqu, dum1, dum2, dum, dum4, dum5] = autocov(data.y'.^2,lag);
end