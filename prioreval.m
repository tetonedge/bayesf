function logprior=prioreval(model,prior)

% Evaluates the prior of a model at the parameter values stored in model

% Author: Sylvia Fruehwirth-Schnatter/HW
 
%             
mix.dist=model.dist;
studmix=any([all(mix.dist(1:6)=='Stumul') all(mix.dist(1:6)=='Studen') all(mix.dist(1:6)=='SkewSt') all(mix.dist(1:6)=='SkStmu')]);
skew=any([all(mix.dist(1:6)=='SkewNo') all(mix.dist(1:6)=='SkNomu') all(mix.dist(1:6)=='SkewSt') all(mix.dist(1:6)=='SkStmu')]);
norstudskew=any([all(mix.dist(1:6)=='Normal') all(mix.dist(1:6)=='Normul') skew studmix]);  %% incluse skew?
glm=any([all(mix.dist(1:6)=='Poisso') all(mix.dist(1:6)=='Binomi') all(mix.dist(1:6)=='Multin')  all(mix.dist(1:6)=='Negbin') ]);
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
%HW
if all(model.dist(1:6)=='Expone')
    
    logprior=mixexpprior(model,prior); 
    
elseif glm
    
    logprior=mixglmprior(model,prior); 

    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
elseif norstudskew
    
    logprior=mixnorprior(model,prior); 
     
else
    warn(['Mixture type ' model.dist '  not supported by function prioreval'])
end

if or(studmix,all(mix.dist(1:6)=='Negbin'))
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %   add the prior of the degrees of freedom
    %

    if isfield(model,'parfix')
        if isfield(model.parfix,'df')
            dffix=model.parfix.df;
        else
            dffix=false;
        end
    else
        dffix=false;
    end
    if ~dffix  
        %nu=model.par.df; 
        %dl=gamcdf(nu,2,1/prior.par.df.lmax)-gamcdf(nu,2,1/prior.par.df.lmin);
        %if dl>0
        %    fnu=log(gamcdf(nu,2,1/prior.par.df.lmax)-gamcdf(nu,2,1/prior.par.df.lmin))-2*log(nu)-log(prior.par.df.lmax-prior.par.df.lmin);
        %else  % exponential prior
       %     fnu=log(prior.par.df.lmin)-prior.par.df.lmin.*nu;
       % end
        fnu=mixstudprior(model,prior);
        logprior=logprior+ sum(fnu);
    end
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   add the prior of the indicator model
%
if ~isfield(model,'K') model.K=1; end
if ~isfield(model,'indicmod') model.indicmod.dist='Multinomial';  end
if ~isfield(model,'indicfix') model.indicfix=false;  end

if and(model.K>1,~model.indicfix)
    
    if model.indicmod.dist(1:6)=='Multin' 
         
         logprior=logprior+ dirichpdflog(prior.weight,model.weight);
        
    elseif model.indicmod.dist(1:6)=='Markov' 
        
        logprior=logprior+ sum(dirichpdflog(prior.indicmod.xi,model.indicmod.xi),1);
        
    end
end
