function simout = studmultsim(par,varargin)

% simulates from a Multivariate normal density of dimension r

% input
% par ... parameters of the  density, structrual array with the following
%         fields:
%         mu  (mean)
%         sigma (covariance matrix) or sigmachol  (cholesky decomposition of the covariance matrix); 
%         df ... degrees of freedom

% for two argument:  varargin determines the number of draws, otherwise it assumed that a single draw is required  

%   par.mu  could be an array, in which case varargin could be omitted.   


%output: simout ... simulated values - if mu is a column vector (r times varargin)  array 
%                                    - if mu is a row vector (varargin times r)  array 
%                                     - of the same as mu, if mu is an array

% Author: Sylvia Fruehwirth-Schnatter


if ~isfield(par,'mu') warn(['Field mu missing in function studmultsim']);fd=[];return; end  
if and(~isfield(par,'sigma'),~isfield(par,'sigmachol')) warn(['Field sigma or sigmachol must be specified in function normultsim']);fd=[];return; end  

 if isfield(par,'sigma')     ss=size(par.sigma);  else   ss=size(par.sigmachol); end 
if or(and(size(par.mu,1)~=ss(1),size(par.mu,2)~=ss(1)),  ss(1)~=ss(2))
    warn(['Size disagreement in the variable par in function studmultsim']); fl=[]; return
end     

if nargin==2     
    M=varargin{1};
    if all([size(par.mu,1)~=M size(par.mu,2)~=M size(par.mu,1)~=1 size(par.mu,2)~=1])   
        warn(['Size disagreement in the variable par in function studmultsim']); fl=[]; return;   end 
else 
    if size(par.mu,1)==size(par.sigma,1)
       M=size(par.mu,2);
    elseif  size(par.mu,2)==size(par.sigma,1)
        M=size(par.mu,1); 
    end    
end

if isfield(par,'sigmachol')
    simout=par.sigmachol*randn(size(par.sigmachol,1),M);
else
   simout=chol(par.sigma)'*randn(size(par.sigma,1),M);
end

dfall=repmat(par.df,1,M);
omega=gamrnd(dfall/2.,2./dfall);
simout=simout./repmat(omega^.5,size(simout,1),1);

if  all(size(par.mu)==size(simout)) %  a sequence of  draws from a M densities with different mean, stored as a sequence of column vectors
    simout=par.mu+simout;
elseif  all(size(par.mu)==size(simout')) %  a sequence of  draws from a M densities with different mean,  stored as a sequence of row vectors
    simout=par.mu+simout';
elseif size(par.mu,1)==size(simout,1)  % a sequence of draws from a single density,   stored as a sequence of column vectors
    simout=repmat(par.mu,1,M)+simout;
elseif size(par.mu,2)==size(simout,2)  % a sequence of draws from a single density,  stored as a sequence of row vectors
    simout=repmat(par.mu,M,1)+simout';
end

