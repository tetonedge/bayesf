function mix=mcmcextract(mcmcout,m);

% stores the m-th MCMC draw as mixture model
% Author: Sylvia Fruehwirth-Schnatter
% Last change: November 22, 2006

mix.dist=mcmcout.model.dist;
if ~isfield(mcmcout.model,'K')  mcmcout.model.K=1; end
mix.K=mcmcout.model.K; 

%if ~isfield(mcmcout.model,'r')  mcmcout.model.r=?; end
mix.r=mcmcout.model.r;


if isfield(mcmcout.model,'indicmod')
    if mcmcout.model.indicmod.dist(1:6)=='Markov' 
        if isfield(mcmcout.model.indicmod,'init')
            mix.indicmod=struct('dist','Markovchain','init',mcmcout.model.indicmod.init); 
        else    
            mix.indicmod.dist='Markovchain'; 
            mcmcout.model.indicmod.dist='Markovchain';
        end
    end
else
    mix.indicmod.dist='Multinomial';
    mcmcout.model.indicmod.dist='Multinomial';
end    
if isfield(mcmcout.model,'indicfix') 
    mix.indicfix=mcmcout.model.indicfix;   
else
    mcmcout.model.indicfix=false;
end

if isfield(mcmcout.model,'d')  mix.d=mcmcout.model.d; end
if isfield(mcmcout.model,'indexdf') mix.indexdf=mcmcout.model.indexdf; end
if isfield(mcmcout.model,'ar')  mix.ar=mcmcout.model.ar; end
if isfield(mcmcout.model,'arf')  mix.arf=mcmcout.model.arf; end
if isfield(mcmcout.model,'par') if isfield(mcmcout.model.par,'indexar')  mix.par.indexar=mcmcout.model.par.indexar; end; end



if ~isstruct(mcmcout.par) 
    mix.par=mcmcout.par(m,:);
else
    if isfield(mcmcout.par,'beta') 
        if size(mcmcout.par.beta(m,:,:),3) == 1
            mix.par.beta = squeeze(mcmcout.par.beta(m,:,:))';  
        elseif size(mcmcout.par.beta(m,:,:),2) > 1
            mix.par.beta = squeeze(mcmcout.par.beta(m,:,:));
        else    
            mix.par.beta = squeeze(mcmcout.par.beta(m,:,:))';    
        end
    end
    if isfield(mcmcout.par,'alpha')      mix.par.alpha=squeeze(mcmcout.par.alpha(m,:))';     end

    if isfield(mcmcout.par,'lambda')
        if or(all(mcmcout.model.dist(1:6)=='SkNomu'),all(mcmcout.model.dist(1:6)=='SkStmu'))
            if size(size(mcmcout.par.lambda),2)==2
                mix.par.lambda=mcmcout.par.lambda(m,:)';
                mix.par.mean=mcmcout.par.mean(m,:)';
            else
                mix.par.lambda=squeeze(mcmcout.par.lambda(m,:,:));
                mix.par.mean=squeeze(mcmcout.par.mean(m,:,:));
            end
        else
            mix.par.lambda=mcmcout.par.lambda(m,:);
            mix.par.mean=mcmcout.par.mean(m,:);
        end
    end
    
    if isfield(mcmcout.par,'df')         mix.par.df=mcmcout.par.df(m,:);     end
    
    if isfield(mcmcout.par,'mu') 
        if any([all(mcmcout.model.dist(1:6)=='Normul') all(mcmcout.model.dist(1:6)=='Stumul') all(mcmcout.model.dist(1:6)=='SkStmu') all(mcmcout.model.dist(1:6)=='SkNomu') ])
            if size(size(mcmcout.par.mu),2)==2
                mix.par.mu=mcmcout.par.mu(m,:)';
            else
            mix.par.mu=squeeze(mcmcout.par.mu(m,:,:));
           end
        else    
            mix.par.mu=mcmcout.par.mu(m,:); 
        end    
    end
    
    if isfield(mcmcout.par,'sigma')
        if any([all(mcmcout.model.dist(1:6)=='Normul') all(mcmcout.model.dist(1:6)=='Stumul') all(mcmcout.model.dist(1:6)=='SkStmu') all(mcmcout.model.dist(1:6)=='SkNomu') ])
            if size(mcmcout.par.sigma(m,:,:),3) > 1
                mix.par.sigma=qinmatrmult(squeeze(mcmcout.par.sigma(m,:,:)));
            else
                mix.par.sigma=qinmatrmult(squeeze(mcmcout.par.sigma(m,:,:))');
            end
        else
            mix.par.sigma=mcmcout.par.sigma(m,:);
        end
    end
    
    if isfield(mcmcout.par,'pi') 
        mix.par.pi=squeeze(mcmcout.par.pi(m,:,:));
    end

end

if all(mcmcout.model.dist(1:6)=='Multin')  mix.cat=mcmcout.model.cat; end
     
    
if mcmcout.model.K>1
    if ~mcmcout.model.indicfix
         if mcmcout.model.indicmod.dist(1:6)=='Multin' 
             mix.weight=mcmcout.weight(m,:);
         elseif mcmcout.model.indicmod.dist(1:6)=='Markov' 
             mix.indicmod.xi=squeeze(mcmcout.indicmod.xi(m,:,:));
        end
    end
end

    