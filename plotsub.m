function [nr, nc]=plotsub(r);

if r<=2 
    nr=1;nc=r;
elseif r<=4
    nr=2;nc=2;
elseif r<=6
    nr=2;nc=3;  
elseif r<=8
    nr=2;nc=4;  
else
     nr=ceil(sqrt(r-1));
     nc=ceil(r/nr);
end