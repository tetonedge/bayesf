function  varargout=mixturepoint(mix,varargin)
% Plot the point process representation a finite mixture distribution, stored in the structure array mix 

% Author: Sylvia Fruehwirth-Schnatter/HW
% Last change: July 16, 2006

if nargin==1 nfig=1; else nfig=fix(varargin{1}); end

if ~isfield(mix,'K') K=1; mix.weight=1; % single member from the distribution family
else
    K=mix.K;
    if ~isfield(mix,'weight') mix.weight=1;  end
end  
if ~isfield(mix,'indicmod') mix.indicmod.dist='Multinomial';  end
if   mix.indicmod.dist(1:6)=='Markov'  mix.weight=marstat(mix.indicmod.xi); end

 
scale=300;

% 

c=['b';'r';'g';'y';'k';'m'];
if K>6 'K too large in mixture point' ,end

if and(mix.dist(1:6)=='Poisso',~isfield(mix,'d'))
    figure(nfig);
    hold on;
    
    if min(mix.par)< 1  plots.logscat=true; else  plots.logscat=false; end
    if ~plots.logscat plotpar=mix.par; else plotpar=log(mix.par); end
    if   mix.indicmod.dist(1:6)=='Markov' ploty=diag(mix.indicmod.xi)'; else  ploty=zeros(1,K); end
    for j=[1:K]
        scatter(plotpar(j),ploty(j),max(mix.weight(j),0.2)*scale,c(j),'filled');hold on;
    end
    if ~plots.logscat xlabel('\mu'); else xlabel('log(\mu)'); end
    if   mix.indicmod.dist(1:6)=='Markov' 
        axis([min(plotpar)-1 max(plotpar)+1 0 1]);
        ylabel('\xi_{kk}'); 
    else
        axis([min(plotpar)-1 max(plotpar)+1 -3 3]);
    end
    legend(num2str([1:K]'));
    
    title('Point process representation');
    
    hold off;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%HW    
elseif and(mix.dist(1:6)=='Expone',~isfield(mix,'d'))
    figure(nfig);
    hold on;
    
    if min(mix.par)< 1  plots.logscat=true; else  plots.logscat=false; end
    if ~plots.logscat plotpar=mix.par; else plotpar=log(mix.par); end
    if   mix.indicmod.dist(1:6)=='Markov' ploty=diag(mix.indicmod.xi)'; else  ploty=zeros(1,K); end
    for j=[1:K]
        scatter(plotpar(j),ploty(j),max(mix.weight(j),0.2)*scale,c(j),'filled');hold on;
    end
    if ~plots.logscat xlabel('\mu'); else xlabel('log(\mu)'); end
    if   mix.indicmod.dist(1:6)=='Markov' 
        axis([min(plotpar)-1 max(plotpar)+1 0 1]);
        ylabel('\xi_{kk}'); 
    else
        axis([min(plotpar)-1 max(plotpar)+1 -3 3]);
    end
    legend(num2str([1:K]'));
    
    title('Point process representation');
    
    hold off;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5    

elseif and(or(mix.dist(1:6)=='Normal',mix.dist(1:6)=='Studen'),~isfield(mix,'d'))   % univariate mixtures of normals 
    
    figure(nfig);hold on;
    for j=1:K
        scatter(mix.par.mu(j),mix.par.sigma(j),mix.weight(j)*scale,c(j),'filled');hold on;
    end
    
    axis([min(mix.par.mu)-1 max(mix.par.mu)+1 0 max(mix.par.sigma)+1]);
    xlabel('\mu');
    ylabel('\sigma^2');
    
    legend(num2str([1:K]'));
    
    title('Point process representation');
    
    hold off;
    
elseif and(or(mix.dist(1:6)=='Normal',mix.dist(1:6)=='Poisso'),isfield(mix,'d')) % mixture regression models 
    
    %%% point process for each pair of regression parameters
    
    nfig=nfig+1;   figure(nfig);         hold on;
    if ~isfield(mix,'df') mix.df=0;end
    if mix.d>1
        ij=nchoosek([1:(mix.d-mix.df)]',2);
        [nr, nc]=plotsub(size(ij,1));
    else
        ij=1; nr=1; nc=1;
    end
    
    for ii=1:size(ij,1)
        subplot(nr,nc,ii);  
        for j=1:K
            if    size(ij,1)>1
                scatter(mix.par.beta(ij(ii,1),j),mix.par.beta(ij(ii,2),j),mix.weight(j)*scale,c(j),'filled'); 
                ylabel(['\beta_{.,' num2str(ij(ii,2)) '}']);
                axis(reshape([min(mix.par.beta(ij(ii,:),:),[],2)-1 max(mix.par.beta(ij(ii,:),:),[],2)+1]',1,4));

            else
                scatter(mix.par.beta(ij(ii,1),j),0,mix.weight(j)*scale,c(j),'filled');
                axis(reshape([min(mix.par.beta(1,:),[],2)-1 max(mix.par.beta(1,:),[],2)+1 -3 3]',1,4));

            end
        end
        if ii==size(ij,1)         legend(num2str([1:K]'));   end
        xlabel(['\beta_{.,' num2str(ij(ii,1)) '}']);
    end
    hold off;
    
    %%% point process for each pair of regression parameters
    if mix.dist(1:6)=='Normal'
        nfig=nfig+1;   figure(nfig);     hold on;
        for j=[1:K]
            scatter(mix.par.sigma(j),0,mix.weight(j)*scale,c(j),'filled');hold on;
        end
        axis([0 max(mix.par.sigma)+1 -3 3]);
        xlabel('\sigma^2'); 
        title('Point process representation for the variance');
        hold off;
    end
    
    
    
    
elseif or(mix.dist(1:6)=='Normul',mix.dist(1:6)=='Stumul')
    %%% point process for each marginal density
    figure(nfig); hold on;
    for m=1:mix.r
        
        if m==1 [nr, nc]=plotsub(mix.r); end
        subplot(nr,nc,m); hold on;
        
        mixm=mixturemar(mix,m);
        for j=1:K
            scatter(mixm.par.mu(j),mixm.par.sigma(j),mixm.weight(j)*scale,c(j),'filled');
        end
        
        if m==mix.r 
            legend(num2str([1:K]'));
            
        end
        title(['Feature ' num2str(m)]);
        axis([min(mixm.par.mu)-1 max(mixm.par.mu)+1 0 max(mixm.par.sigma)+1]);
        xlabel(['\mu_{.,' num2str(m) '}']);
        ylabel(['\Sigma_{.,' num2str(m) num2str(m) '}']);
        hold off;
        
    end
    %%% point process for each bivariate density
    
    nfig=nfig+1;
    figure(nfig); hold on;
    ij=nchoosek([1:mix.r]',2);
    [nr, nc]=plotsub(size(ij,1));
    
    for ii=1:size(ij,1)
        subplot(nr,nc,ii); hold on;
        mixm=mixturemar(mix,ij(ii,:));
        for j=1:K
            scatter(mixm.par.mu(1,j),mixm.par.mu(2,j),mixm.weight(j)*scale,c(j),'filled');
        end
        if ii==size(ij,1)         legend(num2str([1:K]'));   end
        axis(reshape([min(mixm.par.mu,[],2)-1 max(mixm.par.mu,[],2)+1]',1,4));
        xlabel(['\mu_{.,' num2str(ij(ii,1)) '}']);
        ylabel(['\mu_{.,' num2str(ij(ii,2)) '}']);
        hold off;
    end
    
    
    %%% point process for the covariance matrices
    
    nfig=nfig+1;
    figure(nfig); 
    
    subplot(1,2,1); hold on;
    if ~isfield(mix.par,'logdet') compdet=true; else compdet=false;  end
    
    for k=1:K;
        
        if k==1 trS=zeros(K,1);  end
        if compdet    
            if k==1  mix.par.logdet=zeros(K,1);   end
            invQ=inv(mix.par.sigma(:,:,k));
            mix.par.logdet(k)=log(det(invQ));
        end
        trS(k)=trace(mix.par.sigma(:,:,k));
        
        scatter(mix.par.logdet(k),trS(k),mix.weight(k)*scale,c(k),'filled');%hold on;
    end
    if k==K         legend(num2str([1:K]'));   end
    axis([min(mix.par.logdet)-1  max(mix.par.logdet)+1   min(trS)-1 max(trS)+1]);
    xlabel(['log(det \Sigma^{-1})']);
    ylabel(['trace(\Sigma)']);
    hold off;
    
    
    subplot(1,2,2); hold on;
    for k=1:K;
        
        if k==1 ew1=zeros(K,1); ew2=zeros(K,1);  end
        ew=eig(mix.par.sigma(:,:,k));
        ew1(k)=ew(1); ew2(k)=ew(end);
        
        
        scatter(ew1(k),ew2(k),mix.weight(k)*scale,c(k),'filled');hold on;
    end
    if k==K         legend(num2str([1:K]'));   end
    axis([0 max(ew1)*1.5   0 max(ew2)*1.5]);
    xlabel(['smallest eigenvalue of \Sigma']);
    ylabel(['largest eigenvalue of \Sigma']);
    hold off;
    
    
else 
    ['Mixture type ' mix.dist '  not supported by function mixturepoint']
end

if nargout==1 varargout{1}=nfig; end
