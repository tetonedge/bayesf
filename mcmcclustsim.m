function lossim= mcmcclustsim(S,K)

% classification based on the similarity matrix
% computes the loss function

N=size(S,2);
SM=size(S,1);
Wloss= zeros(N);  
index=[1:N];
for m=1:SM;
    ['m = ' num2str(m) '(max:' num2str(SM) ')']
    for k=1:K
        id=index(S(m,:)==k); % index  of all observations in group k
        Wloss(id,id)=Wloss(id,id)+1;
    end
end
%  Wloss/SM ...  approximate similarity matrix
Wloss=1-2*Wloss/SM; 

lossim=zeros(SM,1); 
for m=1:SM;
    ['m = ' num2str(m) '(max:' num2str(SM) ')']
    for k=1:K
        id=index(S(m,:)==k);
        lossim(m)=sum(sum(Wloss(id,id)));
    end
end
