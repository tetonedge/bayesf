%%  Data: eye tracking data

data=dataget('eye');

 
%%  Estimate finite mixtures of Poisson distributions models for increasing K
 
post=[];
Kmin=1;
Kmax=7;
for K=Kmin:Kmax

    clear mix;
    mix.dist='Poisson';
    mix.K=K;

    [data,mix,mcmc]=mcmcstart(data,mix);  
    clear prior;
    prior=priordefine(data,mix);
    
    % select the prior as in fruehwirth-Schnatter (2006), Section 9.2.4
    
    prior.par.a=0.1*ones(1,mix.K);
    prior.par.g=0.5;
    prior.par.G=prior.par.g*mean(data.y)/prior.par.a(1);
    prior.par.b=prior.par.g/prior.par.G*ones(1,mix.K);
 
    mcmcout=mixturemcmc(data,mix,prior,mcmc);
    if isfield(data,'S') data=rmfield(data,'S'); end % starting classification for current K has to be deleted 

    [marlik,mcmcout]=mcmcbf(data,mcmcout);
    post=[post;marlik.bs];
    
    mcmcout.name= ['store_' data.name '_K' num2str(K)];
    mcmcstore(mcmcout);

end

%%  Select model with largest posterior probabiliity
%   prior of a model: Poisson(1)

post=exp(post)./factorial([Kmin:Kmax]');
post=post/sum(post);

[postsort is]=sort(post,1,'descend');
'Ranking according to model posterior probabiliity'
format bank;[is postsort],format short
Kselect=(Kmin-1)+is(1);

%% evaluate a fitted model for fixed K

K=Kselect;
eval(['load store_' data.name '_K' num2str(K)]);
mcmcplot(mcmcout);
[est,mcmcout]=mcmcestimate(mcmcout);

'posterior mean',est.ident.par

'posterior weight',est.ident.weight