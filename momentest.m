function mix=momentest(mix,data)

%% CHECK
if ~isfield(mix,'K') mix.K=1; end
if ~isfield(data,'N') data.N=size(data.y,2); end

%% moment estimator

if and(mix.K==1,all(mix.dist(1:6)=='SkewNo'))
    
    m1=mean(data.y);
    m2=var(data.y);
    m3=data.N/(data.N-1)*mean((data.y-m1).^3);
    
    a1=sqrt(2/pi);
    b1=(4/pi-1)*a1;
    sk=sign(m3)*(abs(m3)/b1)^(1/3);
    sdd=a1^2+m2/sk^2;
    if sdd>1
        deltahat=1./sqrt(sdd)*sign(m3);
    else
        deltahat=0;
    end
    
    mix.par.mu=m1-a1*sk;
    mix.par.sigma=m2+a1^2*sk^2;
    mix.par.lambda=deltahat/sqrt(1-deltahat^2);

elseif and(mix.K==1,all(mix.dist(1:6)=='SkNomu'))
    
    % comment: skewness parameter determined from marginal distribution
    
    m1=mean(data.y,2);
    m2=cov(data.y');
    m3=data.N/(data.N-1)*mean((data.y-repmat(m1,1,data.N)).^3,2);
    
    a1=sqrt(2/pi);
    b1=(4/pi-1)*a1;
    sk=sign(m3).*(abs(m3)/b1).^(1/3);
   
     sdd=a1^2+var(data.y')'./sk.^2;
    if sdd>1
        deltahat=1./sqrt(sdd).*sign(m3);
    else
        deltahat=zeros(size(sdd));
    end
    
    mix.par.mu=m1-a1*sk;
    mix.par.sigma=m2+a1^2*sk*sk';
    mix.par.lambda=deltahat./sqrt(1-deltahat.^2);
    
    
end