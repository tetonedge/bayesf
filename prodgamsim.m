function simout = prodgamsim(par,varargin)

% simulates from a product of Gamma posterior density 
 
% input
% par ... parameter of the  density, structrual array with the
%         fields a and b; 
%         the number of colums determine the  number K of products
%         for a single colum, this reduces to a single gamma density

% for two argument:  varargin determines the number of draws, otherwise it assumed that a single darw is required  

%output: simout ... simulated values  (varargin times K)  array 

% Author: Sylvia Fruehwirth-Schnatter
% Last change: September 13, 2006


if ~isfield(par,'a') warn(['Field a missing in function prodgamsim']);fd=[];return; end  
if ~isfield(par,'b') warn(['Field b missing in function prodgamsim']);fd=[];return; end  

if or(size(par.a,1)~=size(par.a,1),size(par.a,2)~=size(par.a,2)) 
     warn(['Size disagreement in the variable par in function prodgamsim']); fl=[]; return
end     

if nargin==2    
     M=varargin{1};
    if and(size(par.a,1)~=M,size(par.a,1)~=1) warn(['Size disagreement in the variable par in function prodgamsim']); fl=[]; return;     end 
else 
    M=size(par.a,1); 
end
    

if size(par.a,1)==1  %  a sequence of M draws from a single density  
    
  simout = max(gamrnd(repmat(par.a,M,1),1),1e-10)./repmat(par.b,M,1);     

   
else  % a single draw from a sequence of densities
    
  simout = max(gamrnd(par.a,1),1e-10)./[par.b];     
    
 end

