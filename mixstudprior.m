function fnu=mixstudprior(model,prior);       

% computes the prior p(nu_k) for each group
% output: fnu: k x 1


if isfield(prior.par.df,'trans')
    nu=model.par.df-prior.par.df.trans;   % numin lower bound for the degrees of freedom
else
    nu=model.par.df;
end
K=size(nu,1)*size(nu,2);

if all(prior.par.df.type(1:4)=='Cong')
    dl=gamcdf(nu,2,1./prior.par.df.lmax)-gamcdf(nu,2,1./prior.par.df.lmin);
    if dl>0
        fnu=log(gamcdf(nu,2,1./prior.par.df.lmax)-gamcdf(nu,2,1./prior.par.df.lmin))-2*log(nu)-log(prior.par.df.lmax-prior.par.df.lmin);
    else  % exponential prior
        fnu=log(prior.par.df.lmin)-prior.par.df.lmin.*nu;
    end
elseif all(prior.par.df.type(1:4)=='Stee')
    fnu=log(nu)-log(100)-nu/10;
elseif all(prior.par.df.type(1:4)=='inhi')
    if prior.par.df.b0==1
      fnu=log(prior.par.df.d*prior.par.df.a0)+(prior.par.df.a0-1)*log(nu)-(prior.par.df.a0+1)*log(nu+prior.par.df.d);
    else
      fnu= prior.par.df.b0*log(prior.par.df.d) + (prior.par.df.a0-1)*log(nu) -(prior.par.df.a0+prior.par.df.b0)*log(nu+prior.par.df.d) - betaln(prior.par.df.a0,prior.par.df.b0);  
    end
elseif all(prior.par.df.type(1:4)=='hier')
    fnu= prior.par.df.b0*log(prior.par.df.d) + K*prior.par.df.a0*log(prior.par.df.a0)+gammaln(K*prior.par.df.a0+prior.par.df.b0)+(prior.par.df.a0-1)*sum(log(nu));
    fnu=fnu-K*gammaln(prior.par.df.a0)-gammaln(prior.par.df.b0)-(K*prior.par.df.a0+prior.par.df.b0)*log(prior.par.df.a0*sum(nu)+prior.par.df.d);  
elseif all(prior.par.df.type(1:4)=='unif')    
    fnu=zeros(size(nu));
    fnu(nu>prior.par.df.d)=-999;
end

fnu=fnu';