function [lh,  maxl, llh] = likeli_skewstudmult(y,mu,Q,Qinv,detQinv,df,lambda)

% multivariate mixtures of skew-t distributions 

% S .. (1 times N) vector of group-number

K = size(Qinv,3);
r = size(y,1);
d = size(Qinv,1);
N = size(y,2);
m = zeros(r,K);

llh = zeros(K,N); 
for k=1:K,
    eps = y - mu(:,k*ones(1,N));
    err=sum(eps'*squeeze(Qinv(:,:,k)).*eps',2)';
    llh(k,:) = gammaln((df(k)+r)/2)-gammaln(df(k)/2) +.5*detQinv(k) -.5*r*log(df(k)*pi) -(df(k)+r)/2.*log(1+err./df(k));

    omegainvla=lambda(:,k)'*(diag(diag(squeeze(Q(:,:,k))).^(-.5)));
    sc=(omegainvla*eps).*((df(k)+r)./(df(k)+err)).^.5;
    if df(k)<600
        % FN= max(tcdf(sc,r+df(k)),exp(-700));  % built-in MATLAB function tcdf too slow
        FN= max(eval_tcdf_skewt(sc,r+df(k)),exp(-700));
    else
        % use normal approximation
        FN=normcdf(sc);
    end

    llh(k,:) = log(2)+llh(k,:) + log(FN);
end
maxl = max(llh,[],1);
lh = exp(llh - maxl(ones(K,1),:));