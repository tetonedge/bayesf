function [sebs,ach,acr] = marginallikelihood_eval(marlik,plotfig,ifig);

% evaluate marginal likleihood: marginallikelihood_eval.m

sebs=zeros(1,5);
ach=zeros(1,4);
acr=zeros(1,4);

est_tail=false;


psq=marlik.log.loglikq+marlik.log.priorq;  % non-normlaized posterior evaluated at q-draws
mq=max(marlik.log.qq);
fqbs=psq-mq-log(exp(marlik.log.qq-mq) + exp(psq-marlik.bs-mq));
if plotfig  subplot(2,2,1); end
if est_tail
[acr(1),c,logmom]=moment_estimator(fqbs,2,ifig);
 [sd,ach(1),acp,MnoAC]= tailindex(fqbs);
end

psmc=marlik.log.loglikmc+marlik.log.priormc;  % non-normlaized posterior evaluated at MCMC-draws
mq=max(marlik.log.qmc);
fmcbs=marlik.log.qmc-mq-log(exp(marlik.log.qmc-mq) + exp(psmc-marlik.bs-mq));
if plotfig  subplot(2,2,1); end
if est_tail
[acr(2),c,logmom]=moment_estimator(fmcbs,2,ifig);
[sd,ach(2),acp,MnoAC]= tailindex(fmcbs);
end

[sebs(2:3),sebs(1)] = bridgesampling_se(50,fqbs,fmcbs);


fq=marlik.log.loglikq+marlik.log.priorq-marlik.log.qq;
if plotfig  subplot(2,2,1); end
if est_tail
    [acr(3),c,logmom]=moment_estimator(fq,2,ifig);
[sd,ach(3),acp,MnoAC]= tailindex(fq);
end
[dum,sebs(4)] = chib_se(50,fq);


fmc=marlik.log.qmc-marlik.log.loglikmc-marlik.log.priormc;
if plotfig  subplot(2,2,1); end
if est_tail
[acr(4),c,logmom]=moment_estimator(fmc,2,ifig);
[sd,ach(4),acp,MnoAC]= tailindex(fmc);

end
[dum,sebs(5)] = chib_se(50,fmc);