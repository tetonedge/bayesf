function priorq=mixglmprior(mix,prior)

% ADD: CHECK THE PARAMETERS, NEUE PROGRAMME VERWENDEN, SEE MIXTURE BF

% Evaluates the prior of a Poisson mixture model at the parameter values
% stored in mix

% Author: Sylvia Fruehwirth-Schnatter


if ~isfield(mix,'K') mix.K=1; end
if ~isfield(mix,'df') mix.df=0; end

if isfield(mix,'d')    % compute prior beta in a finite mixture regression model
    if (mix.d-mix.df)==1
        priorq=prodnorpdflog(struct('mu',prior.par.beta.b,'sigma',1./squeeze(prior.par.beta.Binv)'),mix.par.beta);
    else
        
        priorq=prodnormultpdflog(struct('mu',prior.par.beta.b,'sigmainv',prior.par.beta.Binv),mix.par.beta);
    end
    if mix.df>0
        priorq=priorq+prodnormultpdflog(struct('mu',prior.par.alpha.a,'sigmainv',prior.par.alpha.Ainv),mix.par.alpha);
    end
    
else        
    
    if all(mix.dist(1:6)=='Poisso')
        lambda=mix.par;
        K=mix.K;

        prl=[prior.par.a' prior.par.b'];
        hier_prior=(isfield(prior,'hier'));if hier_prior hier_prior=prior.hier;end   %  hierarchical prior);


        if ~hier_prior
            priorq = prodgamlog(lambda',prl);  %alte progrmme
        else
            g0=prior.par.g;
            G0=prior.par.G;
            priorq = logprior_mixpoi_hpmarg(K,lambda',prior.par.a(1),g0,G0);  % alte programme
        end
    
    elseif all(mix.dist(1:6)=='Binomi')
         priorq = prodbetapdflog(prior.par,mix.par); 
         
    elseif all(mix.dist(1:6)=='Multin')
        if ~isfield(mix,'r') mix.r=1; end
        if ~isfield(mix,'cat') mix.cat=2*ones(mix.r,1); end
         priorq=0;
                 for j=1:mix.r    
              ij=sum(mix.cat(1:j-1))+[1:mix.cat(j)]';
             priorq=  priorq + sum(proddirichpdflog(prior.par.pi(ij,:)',mix.par.pi(ij,:)')); 
        end
      
    end
end