function varargout=mcmcplot(mcmcout,varargin)

% Monitoring of the unidentified MCMC draws   

% Author: Sylvia Fruehwirth-Schnatter
% Last change: September 4, 2006

if nargin==1
    ifig=1;
else
    ifig=fix(varargin{1});
end    
cl='k';

if ~isfield(mcmcout.model,'indicfix') mcmcout.model.indicfix=false; end
if ~isfield(mcmcout.model,'K') mcmcout.model.K=1; end
if ~isfield(mcmcout,'ranperm') mcmcout.ranperm=false; end
finmix=all([~isfield(mcmcout.model,'d') ~isfield(mcmcout.model,'ar') ~isfield(mcmcout.model,'arf')]);
skew=any([all(mcmcout.model.dist(1:6)=='SkewNo') all(mcmcout.model.dist(1:6)=='SkNomu') all(mcmcout.model.dist(1:6)=='SkewSt') all(mcmcout.model.dist(1:6)=='SkStmu')]);

if and(mcmcout.ranperm,~isfield(mcmcout,'parperm'))
% identify the model before plotting

 mcmcout=mcmcpermute(mcmcout);

end
 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

figure(ifig);

if isfield(mcmcout.model,'K')   K=mcmcout.model.K; else      K=1;   end    

nr=1+(K>1);

subplot(nr,3,1);plot(mcmcout.log.mixlik,'k');ylabel('Mixture Likelihood')
set(gca,'xlim',[-20 mcmcout.M]);

if isfield(mcmcout,'name') title(['Posterior draws for ' mcmcout.name]);end


subplot(nr,3,2);plot(mcmcout.log.mixprior,'k');ylabel('Mixture Prior')
set(gca,'xlim',[-20 mcmcout.M]);
subplot(nr,3,3);plot(mcmcout.log.mixlik+mcmcout.log.mixprior,'k');ylabel('Mixture Posterior')
set(gca,'xlim',[-20 mcmcout.M]);

if and(K>1,~mcmcout.model.indicfix)
    
    subplot(nr,3,4);plot(mcmcout.log.cdpost,'k');ylabel('Complete Data Posterior')
    set(gca,'xlim',[-20 mcmcout.M]);
    subplot(nr,3,5);plot(mcmcout.entropy,'k');ylabel('Entropy')
    set(gca,'xlim',[-20 mcmcout.M]);
    
end

if ~isfield(mcmcout.model,'indicmod') mcmcout.model.indicmod.dist='Multinomial';  end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%  compute implied moments

if finmix margmom=mcmcmargmom(mcmcout); else margmom.empty=true;  end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%  plot MCMC

if  any([all(mcmcout.model.dist(1:6)=='Normal') all(mcmcout.model.dist(1:6)=='Studen') all(mcmcout.model.dist(1:6)=='SkewSt') all(mcmcout.model.dist(1:6)=='SkewNo')]) 
    
      
    if isfield(margmom,'mean')
        ifig=ifig+1;figure(ifig);
        
        subplot(3,2,1);plot(margmom.R,'k');ylabel('R');
        subplot(3,2,2);plot(margmom.mean,'k');ylabel('mean');
        subplot(3,2,3);plot(margmom.var,'k');ylabel('var');
        subplot(3,2,4);plot(margmom.skewness,'k');ylabel('skewness');
        subplot(3,2,5);plot(margmom.kurtosis,'k');ylabel('kurtosis');
    end
    
    if or(~mcmcout.ranperm,mcmcout.model.K==1)
        if isfield(mcmcout.par,'mu')
            ifig=ifig+1;figure(ifig);
            plot(mcmcout.par.mu);
            legend([repmat('k=',K,1) num2str([1:K]')]);
            if skew
               title(['Posterior draws for  \xi_k']);
            else
                title(['Posterior draws for  \mu_k']);
            end
        end
        
        if isfield(mcmcout.par,'sigma')
            ifig=ifig+1;figure(ifig);
            plot(mcmcout.par.sigma);
            legend([repmat('k=',K,1) num2str([1:K]')]);
            title(['Posterior draws for  \sigma^2_k']);
        end
        
         if isfield(mcmcout.par,'df')
            ifig=ifig+1;figure(ifig);
            plot(mcmcout.par.df);
            legend([repmat('k=',K,1) num2str([1:K]')]);
            title(['Posterior draws for  \nu_k']);
         end
        
         if isfield(mcmcout.par,'lambda')
            ifig=ifig+1;figure(ifig);
            plot(mcmcout.par.lambda);
            legend([repmat('k=',K,1) num2str([1:K]')]);
            title(['Posterior draws for  \lambda_k']);
        end
        
    elseif isfield(mcmcout,'parperm')
        if isfield(mcmcout.parperm,'mu')
            ifig=ifig+1;figure(ifig);
            plot(mcmcout.parperm.mu);
            legend([repmat('k=',K,1) num2str([1:K]')]);
            if skew
                title(['Posterior draws for  \xi_k']);
            else
                title(['Posterior draws for  \mu_k']);
            end

        end
        if isfield(mcmcout.parperm,'sigma')
            ifig=ifig+1;figure(ifig);
            plot(mcmcout.parperm.sigma);
            legend([repmat('k=',K,1) num2str([1:K]')]);
            title(['Posterior draws for  \sigma^2_k']);
        end
         if isfield(mcmcout.parperm,'df')
            ifig=ifig+1;figure(ifig);
            plot(mcmcout.parperm.df);
            legend([repmat('k=',K,1) num2str([1:K]')]);
            title(['Posterior draws for  \nu_k']);
         end
        if isfield(mcmcout.parperm,'lambda')
            ifig=ifig+1;figure(ifig);
            plot(mcmcout.parperm.lambda);
            legend([repmat('k=',K,1) num2str([1:K]')]);
            title(['Posterior draws for  \lambda_k']);
        end
    end
    
    if  skew
        ifig=ifig+1;figure(ifig);
        if ~mcmcout.ranperm
            plot(squeeze(mcmcout.par.mean));
        else
            plot(squeeze(mcmcout.parperm.mean));
        end
        legend([repmat('k=',K,1) num2str([1:K]')]);
        title(['Posterior draws of  mean \mu_k']);
    end
    
elseif any([all(mcmcout.model.dist(1:6)=='Normul') all(mcmcout.model.dist(1:6)=='Stumul') all(mcmcout.model.dist(1:6)=='SkStmu') all(mcmcout.model.dist(1:6)=='SkNomu')])      
    
    r=size(mcmcout.par.mu,2);
    
    if ~isfield(margmom,'empty')
    ifig=ifig+1;figure(ifig);
    ij=nchoosek([1:r]',2);
    [nr, nc]=plotsub(size(ij,1)+2);
    
    subplot(nr,nc,1);plot(margmom.Rdet,'k');ylabel('R det');
    subplot(nr,nc,2);plot(margmom.Rtr,'k');ylabel('R tr');
    
    for ii=1:size(ij,1)
        subplot(nr,nc,ii+2); 
        plot(margmom.corr(:,ij(ii,1),ij(ii,2)),'k');
        
        chi=[num2str(ij(ii,1)) ',' num2str(ij(ii,2))];
        ylabel(['corr(' chi ')']); 
        
    end
    
    for j=1:r
        ifig=ifig+1;figure(ifig);
        subplot(2,2,1);plot(margmom.mean(:,j),'k');ylabel('mean');
        title(['Feature ' num2str(j)]);
        subplot(2,2,2);plot(margmom.var(:,j,j),'k');ylabel('var');
        subplot(2,2,3);plot(margmom.skewness(:,j),'k');ylabel('skewness');
        subplot(2,2,4);plot(margmom.kurtosis(:,j),'k');ylabel('kurtosis');
    end
    end
    
    for j=1:r
        ifig=ifig+1;figure(ifig);
        if isfield(mcmcout,'parperm')
            plot(squeeze(mcmcout.parperm.mu(:,j,:)));
        elseif ~mcmcout.ranperm
            plot(squeeze(mcmcout.par.mu(:,j,:)));
        end
        legend([repmat('k=',K,1) num2str([1:K]')]);
        if skew
            title(['Posterior draws of  \xi_{k,' num2str(j) '} of feature ' num2str(j)]);
        else
            title(['Posterior draws of  \mu_{k,' num2str(j) '} of feature ' num2str(j)]);
        end
    end   
    
    ifig=ifig+1;figure(ifig);
    if isfield(mcmcout,'parperm')
        plot(-mcmcout.parperm.logdet);
    elseif ~mcmcout.ranperm
        plot(-mcmcout.par.logdet);
    end
    legend([repmat('k=',K,1) num2str([1:K]')]);
    title(['Posterior draws of  log(det(\Sigma_k))']);
    
    
    ifig=ifig+1;figure(ifig);
    indexdiag=[1:fix(r*(r+1)/2)]';        
    indexdiag=diag(qinmatr(indexdiag))';     
    if isfield(mcmcout,'parperm')
        plot(squeeze(sum(mcmcout.parperm.sigma(:,indexdiag,:),2)));
    elseif ~mcmcout.ranperm
        plot(squeeze(sum(mcmcout.par.sigma(:,indexdiag,:),2)));
    end
    legend([repmat('k=',K,1) num2str([1:K]')]);
    title(['Posterior draws of  trace(\Sigma_k)']);
    
    if isfield(mcmcout,'parperm')
     if isfield(mcmcout.parperm,'df')
            ifig=ifig+1;figure(ifig);
            plot(mcmcout.parperm.df);
            legend([repmat('k=',K,1) num2str([1:K]')]);
            title(['Posterior draws for  \nu_k']);
     end
    elseif or(mcmcout.model.K==1,~mcmcout.ranperm)
        if isfield(mcmcout.par,'df')
            ifig=ifig+1;figure(ifig);
            plot(mcmcout.par.df);
            legend([repmat('k=',K,1) num2str([1:K]')]);
            title(['Posterior draws for  \nu_k']);
        end
    end
    for j=1:r
        if isfield(mcmcout,'parperm')
            if isfield(mcmcout.parperm,'lambda')
                ifig=ifig+1;figure(ifig);
                plot(squeeze(mcmcout.parperm.lambda(:,j,:)));
                legend([repmat('k=',K,1) num2str([1:K]')]);
                title(['Posterior draws of  \lambda_{k,' num2str(j) '} of feature ' num2str(j)]);

            end
        elseif or(mcmcout.model.K==1,~mcmcout.ranperm)
            if isfield(mcmcout.par,'lambda')
                ifig=ifig+1;figure(ifig);
                plot(squeeze(mcmcout.par.lambda(:,j,:)));
                legend([repmat('k=',K,1) num2str([1:K]')]);
                title(['Posterior draws of  \lambda_{k,' num2str(j) '} of feature ' num2str(j)]);
            end
        end
    end

    
    if  skew
        r=size(mcmcout.par.mu,2);
        for j=1:r
            ifig=ifig+1;figure(ifig);
            if ~mcmcout.ranperm
                plot(squeeze(mcmcout.par.mean(:,j,:)));
            else
                plot(squeeze(mcmcout.parperm.mean(:,j,:)));
            end

            legend([repmat('k=',K,1) num2str([1:K]')]);
            title(['Posterior draws of  mean \mu_{k,' num2str(j) '} of feature ' num2str(j)]);
        end
    end
    
elseif any([all(mcmcout.model.dist(1:6)=='Poisso') all(mcmcout.model.dist(1:6)=='Expone') all(mcmcout.model.dist(1:6)=='Binomi')])
   
    if isfield(margmom,'mean')
        ifig=ifig+1;figure(ifig);
        subplot(3,2,1);plot(margmom.mean,'k');ylabel('mean');
        subplot(3,2,2);plot(margmom.var,'k');ylabel('variance');
        subplot(3,2,3);plot(margmom.over,'k');ylabel('overdispersion');
        subplot(3,2,4);plot(margmom.zero,'k');ylabel('fraction zeros');
        subplot(3,2,5);plot(margmom.factorial(:,3),'k');ylabel('3rd factorial moment');
        subplot(3,2,6);plot(margmom.factorial(:,4),'k');ylabel('4th factorial moment');
    end
    
    if finmix
        if ~mcmcout.ranperm
            ifig=ifig+1;figure(ifig);
            plot(mcmcout.par);
            legend([repmat('k=',K,1) num2str([1:K]')]);
            title(['Posterior draws for  \mu_k']);
        elseif isfield(mcmcout,'parperm')
            ifig=ifig+1;figure(ifig);
            plot(mcmcout.parperm);
            legend([repmat('k=',K,1) num2str([1:K]')]);
            title(['Posterior draws for  \mu_k']);
        end
            if isfield(mcmcout.par,'df')
            ifig=ifig+1;figure(ifig);
            plot(mcmcout.par.df,cl);
            legend([repmat('k=',K,1) num2str([1:K]')]);
            title(['Posterior draws for  \nu_k']);
         end

    end
end

if ~finmix
    if or(~mcmcout.ranperm,isfield(mcmcout,'parperm')) 
        
        if ~isfield(mcmcout.model,'ar') indexar=0; else indexar=mcmcout.model.par.indexar; end
        ireg=0; iar=0;
        for j=1:size(mcmcout.par.beta,2)
            ifig=ifig+1;figure(ifig);
            if isfield(mcmcout,'parperm')
                %if size(size(mcmcout.parperm.beta),2)>2
                    plot(squeeze(mcmcout.parperm.beta(:,j,:)));
                %else
                %    plot(mcmcout.parperm.beta);
                %end
            elseif ~mcmcout.ranperm
                %if size(size(mcmcout.par.beta),2)>2
                    plot(squeeze(mcmcout.par.beta(:,j,:)));
                %else
                %    plot(mcmcout.par.beta);
                %end
            end
            legend([repmat('k=',K,1) num2str([1:K]')]);
            if any(j==indexar)
                iar=iar+1;
                title(['Posterior draws of AR coefficient  \phi_{k,' num2str(iar) '}']);
            else    
                ireg=ireg+1;
                if mcmcout.model.d>1
                title(['Posterior draws of switching regression parameter  \beta_{k,' num2str(ireg) '}']);
                else
                    title(['Posterior draws of switching intercept']);
                end
            end

        end
        
        if isfield(mcmcout.par,'df')
            ifig=ifig+1;figure(ifig);
            if isfield(mcmcout,'parperm')
                plot(mcmcout.parperm.df,cl);
            else
                plot(mcmcout.par.df,cl);
            end
            legend([repmat('k=',K,1) num2str([1:K]')]);
            title(['Posterior draws for  \nu_k']);
        end

        
        if or(isfield(mcmcout.model,'arf'),isfield(mcmcout.model,'indexdf'))
            if ~isfield(mcmcout.model,'indexdf') df=0; else df=size(mcmcout.parperm.alpha,2); end
            if ~isfield(mcmcout.model,'arf') indexar=0; ar=0;  else  ar=mcmcout.model.arf;indexar=mcmcout.model.par.indexar; end
            
            if ar>0 
                ifig=ifig+1;figure(ifig);
                if isfield(mcmcout,'parperm')
                    plot(squeeze(mcmcout.parperm.alpha(:,indexar)));
                elseif ~mcmcout.ranperm
                    plot(squeeze(mcmcout.par.alpha(:,indexar)));
                end
                legend([repmat('j=',ar,1) num2str([1:ar]')]);
                title(['Posterior draws of fixed AR parameters']);
            end
            
            if df>ar
                indexreg=all(repmat([1:df]',1,max(ar,1))~=repmat(indexar',df,1),2);
                ifig=ifig+1;figure(ifig);
                if isfield(mcmcout,'parperm')
                    plot(squeeze(mcmcout.parperm.alpha(:,indexreg)));
                elseif ~mcmcout.ranperm
                    plot(squeeze(mcmcout.par.alpha(:,indexreg)));
                end
                legend([repmat('j=',df-ar,1) num2str([1:df-ar]')]);
                
                
                title(['Posterior draws of fixed regression parameters']);
            end
        end
        
    else
        warn(['The fitted model is not supported by function mixtureplot']);
    end
end


%%%%%%%%%%%%%%%%%%%%%%%% HIDDEN INDICATOR %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if K>1
    if mcmcout.model.indicmod.dist(1:6)=='Markov' 
        if isfield(margmom,'ac') 
            ifig=ifig+1;figure(ifig);
            for j=1:3
                subplot(3,2,2*j-1);plot(squeeze(margmom.ac(:,1,j)),'k');ylabel(['AC(' num2str(j) ')']);
                subplot(3,2,2*j);plot(squeeze(margmom.acsqu(:,1,j)),'k');ylabel(['AC-squ(' num2str(j) ')']);
            end
        end
        
        if or(~mcmcout.ranperm,isfield(mcmcout.indicmod,'xiperm'))
            ifig=ifig+1;figure(ifig);
            for k=1:K
                for j=1:K
                    subplot(K,K,(k-1)*K+j);
                    
                    if isfield(mcmcout.indicmod,'xiperm')
                        plot(mcmcout.indicmod.xiperm(:,k,j));
                    elseif ~mcmcout.ranperm
                        plot(mcmcout.indicmod.xi(:,k,j));
                    end
                    axis([0 mcmcout.M 0 1])
                end
            end
            subplot(K,K,2); title(['Posterior draws for transtion matrix']);
            
        end
        
    elseif and(mcmcout.model.indicmod.dist(1:6)=='Multin',~mcmcout.model.indicfix)     
        if or(~mcmcout.ranperm,isfield(mcmcout,'weightperm'))
            ifig=ifig+1;figure(ifig);
            if isfield(mcmcout,'weightperm')
                plot(mcmcout.weightperm);
            elseif ~mcmcout.ranperm
                plot(mcmcout.weight);
            end
            legend([repmat('k=',K,1) num2str([1:K]')]);
            title(['Posterior draws for  \eta_k']);
        end     
    end
end

ifig=mcmcsamrep(mcmcout,ifig+1);

if nargout==1
    varargout{1}=ifig;
end    
