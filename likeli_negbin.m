function [lh, maxl,llh] = likeli_negbin(y,lmda,nu)

% likelihood function of the negative binomial distribution (including all normlaizing constants)
%  density: (nu + y -1)   [  nu    ]^ nu   [  lmda    ]^ y
%           (         )   |--------|       |----------|
%           (nu - 1   )   [nu+lmda ]       [  nu+lmda ]

n=size(y,1);
nst = size(lmda,1);

if  size(lmda,2)==1     lmda=lmda(:,ones(1,n));end    
if  size(nu,2)==1     nu=nu(:,ones(1,n));end    

%lmda=max(lmda,0.0001);

maxl=zeros(1,nst);
yall=repmat(y,1,nst)';
llh  =  gammaln(nu+yall)-gammaln(nu)-gammaln(yall+1);
llh = llh + nu.*log(nu)+ yall.*log(lmda)-(nu+yall).*log(nu+lmda);
 
 

maxl  = max(llh,[],1);
lh  = exp(llh  - repmat(maxl,nst,1));
 