function plotclasscross(y,S);

 
K=max(S);
c=['b';'r';'g';'y';'k';'m'];
if K>6 'K too large in plotclasscross' ,end

 
for k=1:K
    stabdiagramm(y(1,S==k),'k')
    hold on;
    scatter(y(1,S==k),k*ones(1,sum(S==k)),c(k),'filled');
end    
axis([min(y)-1 max(y)+1 0 K+1]); 