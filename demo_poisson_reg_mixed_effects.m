%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Poisson regression models with fixed effects 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 
clear mixreg mix prior;


mixreg.dist='Poisson';
 
 %mixreg.par.beta=[-4 ;-3 ; -2;0 ; 0.1];  % almost only 0 counts
 %mixreg.par.beta=[3 ;2 ; 1;0 ; 0.1];  %  observations practically normal
 mixreg.par.beta=[-2 ;1 ; -1;0 ; 0.1];
 mixreg.d=size(mixreg.par.beta,1);
 
data=simulate(mixreg,1000);
 
mcmc=struct('burnin',2000,'M',1000,'storeS',500,'storepost',true);

mcmc=struct('burnin',2000,'M',1000,'storeS',500,'storepost',true,'ranperm',true,'startpar',true);


% fit  regression model

 
istartpar=isfield(mcmc,'startpar'); if istartpar istartpar=mcmc.startpar;end 
if istartpar   
    mixreg.par.beta=zeros(mixreg.d,1); 
end


prior=priordefine(data,mixreg);
mcmcout=mixturemcmc(data,mixreg,prior,mcmc); 
est=mcmcestimate(mcmcout);
marlik=mixturebf(mcmcout,data);
 