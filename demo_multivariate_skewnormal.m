
clear mixtrue;
mixtrue.dist='SkNomu';
mixtrue.r=2;

mixtrue.par.mu=[0 0]';
mixtrue.par.sigma=eye(2);

%mixtrue.par.lambda=[0.5 2]';
mixtrue.par.lambda=[1 -3]';

%% simulate data

data=simulate(mixtrue,500);

Strue=data.S;
data=rmfield(data,'S');

dataplot(data,1)

hold on;
mixtureplot(mixtrue,10);

%% Fit a skew-normal distribution

clear mix;
mix.dist='SkNomu';

prior=priordefine(data,mix);

%%  define mcmc

[data, mix,mcmc]=mcmcstart(data,mix);   % sample random effects from the prior

mcmc.M=8000;mcmc.burnin=3000;mcmc.storeS=100;mcmc.storepost='true';
%mcmc.M=3000;mcmc.burnin=1000;mcmc.storeS=100;mcmc.storepost='true';


%% run mcmc

%data.empty=true;

mcmcout=mixturemcmc(data,mix,prior,mcmc);
[est,mcmcout]=mcmcestimate(mcmcout);

[marlik,mcmcout]=mcmcbf(data,mcmcout)



