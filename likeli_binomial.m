function [lh, maxl,llh] = likeli_binomial(y,Ti,pii)

% likelihood function of a binomial distribution (including all normlaizing
% constants)

% input: pii ... probabaility  
%        logitpi ... logit(probabaility  )


n=size(y,1);
nst = size(pii,1);

if  size(pii,2)==1  pii=pii(:,ones(1,n));end    

maxl=zeros(1,nst);
yall=repmat(y,1,nst)';
if size(Ti,2)==1
    Ti=repmat(Ti,nst,n);
else
    Ti=repmat(Ti,nst,1);
end
llh  =  gammaln(Ti+1)-gammaln(Ti-yall+1)-gammaln(yall+1);
llh = llh + yall.*log(pii)+(Ti-yall).*log(1-pii);

maxl  = max(llh,[],1);
lh  = exp(llh  - repmat(maxl,nst,1));
 