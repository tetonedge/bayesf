function [lh, maxl,llh] = likeli_multinomial(y,Ti,mix)

% likelihood function of a multinomial distribution (including all normlaizing
% constants)

% input: mix.par.pi .. probability  sum(D_j) times K  

if ~isfield(mix,'r') mix.r=1; end
n=size(y,1);

if  size(mix.par.pi,3)==1  
    pii=mix.par.pi(:,:,ones(1,n)); 
else
    pii=mix.par.pi;
end    

iy=ones(mix.cat(1),1);
        for j=2:mix.r
            iy=[iy; j*ones(mix.cat(j),1)];
        end

yall=permute(repmat(y(:,iy),[1 1 mix.K]),[2  3 1]);  % dimension  sum(cat) times  K times N

labelall=repmat(mix.label,[ 1 mix.K ]); % dimension  sum(cat) times  K
labelall=repmat(labelall,[ 1 1 n]);

if size(Ti,2)==1
    if mix.K>1
        llh = squeeze(sum(log(pii).*(yall==labelall),1));
    else
        llh = squeeze(sum(log(pii).*(yall==labelall),1))';
    end
else
    'ADD MULTINOMIAL with T>1 IN mixtureplot'
end

maxl  = max(llh,[],1);
lh  = exp(llh  - repmat(maxl,mix.K,1));
 