%% demo_mix_normal %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
% demonstrate fitting a mixture of univariate normal distributions for simulated data
%
%% define the mixture model

clear mixtrue;
mixtrue.dist='Normal';
mixtrue.K=3;
mixtrue.weight=[0.3 0.4 0.3];
mixtrue.par.mu=[-1 2 4];
mixtrue.par.sigma(:,:,1)=[0.1 0.3 0.7];

%% simulate the data
N=1000;
data=simulate(mixtrue,N);
data.Strue=data.S;
data=rmfield(data,'S');


%% plot the data
dataplot(data)

%% fit a mixture with three compnents

clear mix;
mix.dist='Normal';
mix.K=3; 

prior=priordefine(data,mix);

[data,mix,mcmc]=mcmcstart(data,mix);
mcmcout=mixturemcmc(data,mix,prior,mcmc);
mcmcplot(mcmcout,2); % plot mcmcoutput, starting with figure 2

[est,mcmcout]=mcmcestimate(mcmcout)
[marlik,mcmcout]=mcmcbf(data,mcmcout)

mcmcout.name= 'store_mix_normal_K3';
mcmcstore(mcmcout);