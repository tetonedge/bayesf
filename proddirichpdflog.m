function fl = proddirichpdflog(par,eta)
% computes the logarithm of a product of Dirichlet posterior density 
 
% input
% par ... parameter of the aposteriori density, the number of colums
%         determines the  number of categories
% eta ... arguments of the  aposteriori density

%output: log of the posteriro density
%        M times 1 array with M being the maximum number of row in par and
%        eta

% Author: Sylvia Fruehwirth-Schnatter
% Last change: October 28, 2006


if and(size(size(par),2)<3,size(size(eta),2)<3) % evaluate a single density at a single draw
   fl=sum(dirichpdflog(par,eta),1);

   if or(size(par,1)~=size(eta,1),size(par,2)~=size(eta,2))  
       warn(['Size disagreement in function logdirichpdflog']); 
       fl=[]; return; 
   end

elseif and(size(size(par),2)==2,size(size(eta),2)==3) % evaluate a single density at  a sequence of draws
    
  par=permute(repmat(par,[1 1 size(eta,1)]),[3 1 2]);

elseif and(size(size(par),2)==3,size(size(eta),2)==2)  % evaluate a sequence of density at a single draw
    
  eta=permute(repmat(eta,[1 1 size(par,1)]),[3 1 2]);
    
 end
 if any([size(par,1)~=size(eta,1) size(par,2)~=size(eta,2) size(par,3)~=size(eta,3)])  
       warn(['Size disagreement in function logdirichpdflog']); 
       fl=[]; return; 
   end
fl=sum(gammaln(sum(par,3))+sum((par-1).*log(eta)-reshape(gammaln(par),size(par,1),size(par,2),size(par,3)),3),2);

