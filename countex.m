function ct = countex(sst,nst)
%
n=size(sst,2); 
states=1:nst;
ct=sum(sst(ones(1,nst),:)==states(ones(1,n),:)',2);
