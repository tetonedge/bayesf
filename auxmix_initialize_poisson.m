function [mixaux1,mixaux2]= auxmix_initialize_poisson(data,mix,mcmc)

%%%%%%%%%%%%%%%%%%%%%%%%%%
%% initialize


% initialise the necessary mixture approximations

mixaux1.dist='Normal';
[mixaux1.weight,mixaux1.par.mu,mixaux1.par.sigma,mixaux1.K]=compute_mixture(1);

[ys is]=sort(data.y);
index=[2:data.N]';
indexz=[1;index(diff(ys)~=0)];
ny= ys(:,indexz);
clear ys;
ny=ny(ny~=0)';  % ny contains the different degrees of freedom

Ny=sum(data.y~=0,2);
[dum1,dum2,dum3,Kmax]=compute_mixture(ny(1));
mixaux2=struct('dist','Normal','K',zeros(Ny,1),'par',struct('mu',-1000*ones(Ny,Kmax),'sigma',0.001*ones(Ny,Kmax)),'weight',zeros(Ny,Kmax));
for nj=1:size(ny,1)
    [waux,maux,saux,Kaux]=compute_mixture(ny(nj));
    %           mixaux2.weight(nj,1:Kaux)=waux;mixaux2.par.mu(nj,1:Kaux)=maux;mixaux2.par.sigma(nj,1:Kaux)=saux;mixaux2.K(nj,1:Kaux)=Kaux;
    nm=sum(data.y==ny(nj),2);
    mixaux2.weight(data.y(data.y~=0)==ny(nj),1:Kaux)=repmat(waux,nm,1);
    mixaux2.par.mu(data.y(data.y~=0)==ny(nj),1:Kaux)=repmat(maux,nm,1);
    mixaux2.par.sigma(data.y(data.y~=0)==ny(nj),1:Kaux)=repmat(saux,nm,1);
    mixaux2.K(data.y(data.y~=0)==ny(nj),1)=repmat(Kaux,nm,1);
end
