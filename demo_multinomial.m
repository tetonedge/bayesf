%% demo_multinomial.m

%% mutlinomial latent class model

model.dist='Multinomial';
model.r=3;
model.cat=[4 3 3];

model.K=2;

model.weight=[0.5 0.5];
model.par.pi=zeros(sum(model.cat),model.K);

model.par.pi(:,1)=[.22 .6 .12 .06 .71 .08 .21 .74 .25 .01];
model.par.pi(:,2)=[.14 .19 .4 .27 .28 .31 .41  .01 .32 .67];

N=1000;
data=simulate(model,N);

%%%

clear mix;

mix.dist='Multinomial';
mix.r=3;
mix.cat=[4 3 3];
mix.K=2;
    
prior=priordefine(data,model);

[data,model,mcmc] = mcmcstart(data,model);
mcmc.ranperm=false;

mcmcout=mixturemcmc(data,model,prior,mcmc);