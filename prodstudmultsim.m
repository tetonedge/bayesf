function simout = prodstudmultsim(par,varargin)

% simulates from a product of Multivariate student-t density of dimension r

% input
% par ... parameters of the  density, structrual array with the
%         fields mu  and sigma (covariance matrix) or sigmachol  (cholesky decomposition of the covariance matrix); ; 
%         mu is of dimension r times K
%         sigma or sigmachol is of dimension r times r times K
%         df is of dimension 1 times K

% for two argument:  varargin determines the number of draws, otherwise it assumed that a single draw is required  

%output: simout ... simulated values  

% if r is equal to 1, size of simout is equal to vargin times K
% if varargin>1  vargin times r times K times ;  otherwise r times K

% Author: Sylvia Fruehwirth-Schnatter
% Last change: September 14, 2006


if ~isfield(par,'mu') warn(['Field mu missing in function prodstudmultsim']);simout=[];return; end  
if and(~isfield(par,'sigma'),~isfield(par,'sigmachol')) warn(['Field sigma or sigmachol must be specified in function prodnormultsim']);fd=[];return; end  

if nargin==2     M=varargin{1}; else   M=1;  end

r=size(par.mu,1); K=size(par.mu,2);

if r==1
    if isfield(par,'sigma') 
        ss=size(par.sigma); 
        par.sigmachol= par.sigma.^.5;
    else 
        ss=size(par.sigmachol); 
    end 
    if any([size(par.mu,1)~=ss(1) size(par.mu,2)~=ss(2)])
        warn(['Size disagreement in the variable par in function prodnormultsim']); simout=[]; return
    end     
    
    dfall=repmat(par.df,M,1);
    omega=gamrnd(dfall/2.,2./dfall);
    simout=repmat(par.mu,M,1)  +  repmat(par.sigmachol,M,1).*randn(M,K)./sqrt(omega);
else
    if isfield(par,'sigma')     ss=size(par.sigma);  else   ss=size(par.sigmachol); end 
    if K>1
        if any([size(par.mu,1)~=ss(1) size(par.mu,2)~=ss(3)  ss(1)~=ss(2)])
            warn(['Size disagreement in the variable par in function prodnormultsim']); simout=[]; return
        end   
    else
        if any([size(par.mu,1)~=ss(1)  ss(1)~=ss(2)])
            warn(['Size disagreement in the variable par in function prodnormultsim']); simout=[]; return
        end   
    end
    
    if M==1  simout=zeros(r,K);    else      simout=zeros(M,r,K);  end
    for k=1:K  
        if isfield(par,'sigmachol')
        simmu=studmultsim(struct('mu',par.mu(:,k),'sigmachol',par.sigmachol(:,:,k),'df',par.df(1,k)),M);
        else
        simmu=studmultsim(struct('mu',par.mu(:,k),'sigma',par.sigma(:,:,k),'df',par.df(1,k)),M);
    end
        if M==1  simout(:,k)=simmu;  else    simout(:,:,k)=simmu';  end
    end
end

