function [data,mix]=designar(data,mix)    

 %% Converts an MSAR model to be fitted to a time series stored in data.y 
 %%           to a regression model with appropriate data matrix  data.X
 %%           by condiitoning on the first data.t0 observations (or the
 %%           first p observations, if data.t0 is missing)
 
 %% Similarly  for a dynamic Markov switching regression model
 
 %%  Author: Sylvia Fruehwirth-Schnatter
 %%  Last change: November 22, 2006

 
 if ~isfield(mix,'d') mix.d=1; end

if ~isfield(data,'X')  
    data.X=ones(1,data.N); 
end

if mix.d~=size(data.X,1) warn('size inconsistency between the field d in the model and the size of the design matrix'); end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%    Festlegen der Indicces, wenn keine uebergeben wurden

if ~isfield(mix,'indexdf') 
    df=0; 
else  
    if size(mix.indexdf,2)>size(mix.indexdf,1) mix.indexdf=mix.indexdf'; end; 
    mix.df=size(mix.indexdf,1)*size(mix.indexdf,2);  
    df=mix.df; 
end

if isfield(mix,'ar') 
    arp=mix.ar;
    if mix.ar>0
        if isfield(mix,'par') 
            if ~isfield(mix.par,'indexar')  mix.par.indexar=(mix.d-df)+[1:mix.ar]';   end
        else           
            mix.par.indexar=(mix.d-df)+[1:mix.ar]';
        end    
        if df>0
            % reorder columns
            indexdr=all(repmat(mix.indexdf,1,mix.d)~=repmat([1:mix.d],mix.df,1),1);
            data.X=[data.X(mix.indexdf,:); data.X(indexdr,:)];
            mix.indexdf=[1:df]';    
        end    
        indexar = df+mix.par.indexar;
    end
    
elseif isfield(mix,'arf') 
    arp=mix.arf;
    if  mix.arf>0 
        if isfield(mix,'par') 
            if ~isfield(mix.par,'indexar')   mix.par.indexar=df+[1:mix.arf]';  end
        else           
            mix.par.indexar=df+[1:mix.arf]';
        end   
        dfreg=df+mix.arf;
        indexdfreg=zeros(dfreg,1);
        if df>0
            idf=all(repmat([1:dfreg]',1,mix.arf)~=repmat(mix.par.indexar',dfreg,1),2);
            indexdfreg(idf)=mix.indexdf;
        end
        indexdfreg(mix.par.indexar)= mix.d+[1:mix.arf]'; 
        mix.indexdf= indexdfreg;      
        
    end
    indexar=mix.indexdf(mix.par.indexar);
else
    arp=0;
end


if arp>0
    if size(indexar,1)>size(indexar,2) indexar=indexar';end % mix.indexar is row vector
    d=size(data.X,1)+arp;
    index=[1:d]';
    indexreg = index(all(repmat([1:d]',1,arp)~=repmat(indexar,d,1),2));
    mix.indexar=indexar; % refers to the column of the large regression model    

else
    d=size(data.X,1);
    indexreg=[1:d]';    
end    
    

%   definition von X und y


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Festlegen von t0

if ~isfield(data,'t0') 
    data.t0=arp+1;
elseif ~(data.t0>arp) 
    data.t0=arp+1;
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


data.X=[data.X(:,data.t0:end);zeros(arp,data.N-(data.t0-1))];
data.X(indexreg,:)=data.X(1:mix.d,:);

for j=1:arp
    data.X(indexar(j),:)=data.y(1,data.t0-j:data.N-j);
end
 
data.y=data.y(1,data.t0:end);
data.N=data.N-(data.t0-1);
if isfield(data,'S') data.S=data.S(1,data.t0:end); end
data=rmfield(data,'t0');

if isfield(mix,'indexdf')  
    mix.df=size(mix.indexdf,1)*size(mix.indexdf,2); else  mix.df=0;     end  
if isfield(mix,'ar')  
    mix.d=mix.ar+mix.d;  
    mix=rmfield(mix,'ar');
elseif isfield(mix,'arf')  
    mix.d=mix.arf+mix.d; 
    mix=rmfield(mix,'arf');
end
if isfield(mix,'par') if isfield(mix.par,'indexar') mix.par=rmfield(mix.par,'indexar'); end; end