%%  Data: GDP data

data=dataget('gdp');

%%  Estimate MSAR model with switching intercept for increasing K and different AR orders
 
post=[];
Kmin=2;Kmax=3;
pmin=1;pmax=4;

for K=Kmin:Kmax
    for p=pmin:pmax  
        
    clear msreg;
    msreg.dist='Normal';
    msreg.K=K;
    msreg.d=1;   
    msreg.indicmod.dist='Markovchain';    
    msreg.indicmod.init='uniform'; 
    if p>0  
        msreg.arf=p; 
    end

    [data,msreg,mcmc]=mcmcstart(data,msreg);  
    clear prior;
    
    
    %% modify default prior to obtain the same prior as in Fruehwirth-Schnatter (2006), Subsection 12.2.6
    prior=priordefine(data,msreg);
    prior.hier=false;
    prior.par.sigma.c=2*ones(1,K);
    prior.par.sigma.C=0.5*ones(1,K);
    if msreg.K>1
        prior.par.beta.Binv(1,1,:)=1;
        for k=1:msreg.K prior.indicmod.xi(k,k)=2; end;
    else
        prior.par.beta.Binv=1;
        if p>0 for j=1:p;prior.par.alpha.Ainv(j,j,:)=1;end; end
    end
   
    mcmcout=mixturemcmc(data,msreg,prior,mcmc);
    if isfield(data,'S') data=rmfield(data,'S'); end % starting classification for current K has to be deleted 

    data.t0 = pmax +1;
    [marlik,mcmcout]=mcmcbf(data,mcmcout);
    data=rmfield(data,'t0');
     
    post=[post;marlik.bs];
    
    mcmcout.name= ['store_' data.name '_msar_swi_K' num2str(K) '_p' num2str(p)];
    mcmcstore(mcmcout);
    end
end

%%  Select model with largest marginal likelihood 
post=reshape(post,pmax-pmin+1,Kmax-Kmin+1)

%% evaluate the fitted model for K=2 and p=2

K=2; p=2;
eval(['load store_' data.name '_msar_swi_K' num2str(K) '_p' num2str(p)]);
mcmcplot(mcmcout);
[est,mcmcout]=mcmcestimate(mcmcout);