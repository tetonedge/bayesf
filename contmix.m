function  contmix(mix,options);

% contourplot for a bivariate mixture of normal distributions

% Author: Sylvia Fruehwirth-Schnatter
% Last change: June 13, 2007

if isfield(options,'npoint') npoint=options.npoint; else npoint=100; end
if isfield(options,'cc') cc=options.cc; else cc='k'; end
if ~isfield(mix,'K') mix.K=1; mix.weight=1;  end


mix1=mixturemar(mix,1);
[ss is]=sort(mix1.par.mu); %sort the means to define the domain of the plot
cl=-3; % define the lower and the upper bound from the quantile    
cu=3;

x1min=mix1.par.mu(is(1))+cl*sqrt(max(mix1.par.sigma));
x1max=mix1.par.mu(is(mix.K))+cu*sqrt(max(mix1.par.sigma));
x1=linspace(x1min,x1max,npoint);

mix2=mixturemar(mix,2);
[ss is]=sort(mix2.par.mu); %sort the means to define the domain of the plot

x2min=mix2.par.mu(is(1))+cl*sqrt(max(mix2.par.sigma));
x2max=mix2.par.mu(is(mix.K))+cu*sqrt(max(mix2.par.sigma));
x2=linspace(x2min,x2max,npoint);

[x1,x2]=meshgrid(x1,x2);

L=zeros(size(x1)); 

if and(isfield(mix.par,'sigmainv'),isfield(mix.par,'logdet')) 
    Qinv=mix.par.sigmainv;
    detQ=exp(mix.par.logdet);
else
    Qinv=zeros(size(mix.par.sigma)); detQ=zeros(1,mix.K);    
    for k=1:mix.K;
        Qinv(:,:,k)=inv(mix.par.sigma(:,:,k));detQ(k)=det(Qinv(:,:,k));
    end
end
 
for k=1:mix.K;
    d1=x1-mix.par.mu(1,k);
    d2=x2-mix.par.mu(2,k);
    L=L+mix.weight(k)*detQ(k)^.5*exp(-(d1.^2/2*Qinv(1,1,k)+ d1.*d2*Qinv(1,2,k) + d2.^2/2*Qinv(2,2,k) ));
end
ML=max(max(L));

 
%% [c,h]=contour(x1,x2,L,20,cc); 
%  alternative axis


Ls=sort(reshape(L,npoint^2,1));
frac=0.001;
frac=0.01;
Lslarge=Ls(Ls>frac*ML);
nc=20;
dd=fix(size(Lslarge)/nc);
v=Lslarge(dd:dd:end);
Lssmall=Ls(and(Ls<=frac*ML,Ls>0.01*frac*ML));
Lss=Lssmall([1==1;diff(Lssmall)./Lssmall(2:end)>1e-3]);
nc=20;
dd=fix(size(Lss)/nc);
v=[Lss(7*dd:dd:end-1);v];
[c,h]=contour(x1,x2,L,v,cc);



%  axis equal
set(gca,'xlim',[x1min x1max]);
set(gca,'ylim',[x2min x2max]);
%  title('Posterior of (\mu_1,\mu_2)');
%  hold on;
%  plot(xlim,xlim);

% Posterior of sigma  

 