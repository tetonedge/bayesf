function cdfy = eval_tcdf_skewt(y,nu)

% evaluate the cdf of a standard t-density with df equals input argument nu
% at all points defined by input argument y

% nu .. scalar degrees of freedom
%  y .. argument, array of size 1 x N

% use a trapezoid rule over a grid and linear interpolation

if all(y==0) cdfy=0.5*ones(size(y)); return; end


%% reflect positive argument to negative one 

ya=-abs(y);

%% define Mt grid points between  and 0

Mt=1000;
%alpha=0.005;xmin=tinv(alpha,nu);  % choose the alpha quantile
xmin=min(ya);      % choose the nimimum observation


x=linspace(xmin,0,Mt)'; 

%% evaluate the kernel of the t_nu density over the grid

f=1./(1+x.^2/nu).^((nu+1)*0.5);

% test: should be 0.5
% fi=(sum(f)-f(end)*0.5)*(x(end)-x(end-1));
% tcdf(xmin,nu)+fi*exp(gammaln((nu+1)*0.5)-gammaln(nu*0.5)-0.5*log(nu*pi))

%% compute the cdf

fy=1./(1+ya.^2/nu).^((nu+1)*0.5);

% find the left hand side grid point

dh=(x(end)-x(end-1));
%iff=sum(repmat(x,1,size(y,2))<=repmat(ya,size(x,1),1));
iff=1+floor((ya-xmin)/dh);

% the following formula used the trapezoid rule over the grid point smaller
% than the argument y and adds two correction terms:
%  1. correct for truncation at xmin by adding the cdf at xmin
%  2. use linear interpolation between the two grid points containing the argument y 
%     to estimate the additonal cdf between the last grid point and the
%     argument y

nc=exp(gammaln((nu+1)*0.5)-gammaln(nu*0.5)-0.5*log(nu*pi)); % normalising constant
fsum=(cumsum(f)-0.5*f(1))*dh;
% ftr= nc*((sum(repmat(f,1,size(y,2)).*index,1)-0.5*f(1))*dh  +0.5*fy.*(ya-x(iff)'));
ftr= nc*(fsum(iff(iff>0))' +0.5*fy(iff>0).*(ya-x(iff(iff>0))'));
cdfy= tcdf(xmin,nu)*ones(1,size(y,2));
cdfy(iff>0)=cdfy(iff>0)+ftr;


% reflect cdf for all positive arguments
cdfy(y>0)=1-cdfy(y>0);


% compare with tcdf
%toc;
%tic;
%cdfyt=tcdf(y,nu);
%toc
%figure(41);plot(y,(cdfy-cdfyt)./cdfyt,'r')
%figure(42);plot(y,(cdfy-cdfyt),'r')