%% Fisher's iris data
%
%  fit a multivariate normal mixture with increasing number of components
%  (one to five)

%% load the data
clear data;
data=dataget('iris');

%% increase K

Kmin=1; Kmax=5;
for K=Kmin:Kmax

    clear mix;
    mix.dist='Normul';
    mix.K=K;

%%  define the prior
    clear prior;
    prior=priordefine(data,mix);

%%  run MCMC

    [data,mix,mcmc]=mcmcstart(data,mix);
    mcmcout=mixturemcmc(data,mix,prior,mcmc);
    if isfield(data,'S') data=rmfield(data,'S'); end % starting classification for current K has to be deleted 

%% compute the marginal likelihood

    [marlik,mcmcout]=mcmcbf(data,mcmcout)

%% store the reults

    mcmcout.name=['store_iris_K' num2str(K)];
    mcmcstore(mcmcout);

end