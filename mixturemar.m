function mixm=mixturemar(mix,j);
% compute marginals of a mixture distribution

% Author: Sylvia Fruehwirth-Schnatter
% Last change: July 15, 2006

if ~isfield(mix,'K')   mix.K=1; mix.weight=1; end % single member from the distribution family

if and(~isfield(mix,'weight'),mix.K==1)  mix.weight=1; end % single member from the distribution family

if all(mix.dist(1:6)=='Normul')
    
    if size(j,2)==1    
        % extract a univariate marginal density
        par=struct('mu',mix.par.mu(j,:),'sigma',squeeze(mix.par.sigma(j,j,:))');
        
        mixm=struct('dist','Normal','K',mix.K,'r',1,'weight',mix.weight,'par',par);
        
    else 
        % extract a size(j,2)-variate marginal density
        
        par=struct('mu',mix.par.mu(j,:),'sigma',mix.par.sigma(j,j,:));
        
        mixm=struct('dist','Normul','K',mix.K,'r',size(j,2),'weight',mix.weight,'par',par);
    end
    
elseif all(mix.dist(1:6)=='Stumul')
    
    if size(j,2)==1    
        % extract a univariate marginal density
        par=struct('mu',mix.par.mu(j,:),'sigma',squeeze(mix.par.sigma(j,j,:))','df',mix.par.df);
        
        mixm=struct('dist','Student','K',mix.K,'r',1,'weight',mix.weight,'par',par);
        
    else 
        % extract a size(j,2)-variate marginal density
        
        par=struct('mu',mix.par.mu(j,:),'sigma',mix.par.sigma(j,j,:),'df',mix.par.df);
        
        mixm=struct('dist','Stumul','K',mix.K,'r',size(j,2),'weight',mix.weight,'par',par);
    end
    
elseif all(mix.dist(1:6)=='SkNomu')
    
    lt=zeros(size(j,2),mix.K);
    if size(j,2)==1  
        j2=[1:j-1 j+1:mix.r];
    else
        j2=[1:min(j)-1 min(j)+1:max(j)-1 max(j)+1:mix.r];
    end
    
    for k=1:mix.K

        Omega=diag(diag(squeeze(mix.par.sigma(:,:,k))).^.5);
        omegainv=diag(diag(squeeze(mix.par.sigma(:,:,k))).^(-.5));
        Sbar=omegainv*squeeze(mix.par.sigma(:,:,k))*omegainv;
        al2=mix.par.lambda(j2,k);
        if size(j,2)==1
            om11inv=1/Sbar(j,j);
        else
            om11inv=inv(Sbar(j,j));
        end
        om12=Sbar(j,j2);
        om221=Sbar(j2,j2)- om12'*om11inv*om12;

        lt(:,k)= mix.par.lambda(j,k)+ om11inv*om12*al2;
        lt(:,k)=lt(:,k)/(1+ al2'*om221*al2)^.5;
    end
    
    
    if size(j,2)==1    
        % extract a univariate marginal density
        par=struct('mu',mix.par.mu(j,:),'sigma',squeeze(mix.par.sigma(j,j,:))','lambda',lt);
        
        mixm=struct('dist','SkewNo','K',mix.K,'r',1,'weight',mix.weight,'par',par);
        
    else 
        % extract a size(j,2)-variate marginal density
        
        par=struct('mu',mix.par.mu(j,:),'sigma',mix.par.sigma(j,j,:),'lambda',lt);
        
        mixm=struct('dist','SkNomu','K',mix.K,'r',size(j,2),'weight',mix.weight,'par',par);
    end
    
elseif all(mix.dist(1:6)=='SkStmu')   % CHECK marginal densities of a Skew Student density
    
    lt=zeros(size(j,2),mix.K);
    if size(j,2)==1  
        j2=[1:j-1 j+1:mix.r];
    else
        j2=[1:min(j)-1 min(j)+1:max(j)-1 max(j)+1:mix.r];
    end
    
    for k=1:mix.K

        Omega=diag(diag(squeeze(mix.par.sigma(:,:,k))).^.5);
        omegainv=diag(diag(squeeze(mix.par.sigma(:,:,k))).^(-.5));
        Sbar=omegainv*squeeze(mix.par.sigma(:,:,k))*omegainv;
        al2=mix.par.lambda(j2,k);
        if size(j,2)==1
            om11inv=1/Sbar(j,j);
        else
            om11inv=inv(Sbar(j,j));
        end
        om12=Sbar(j,j2);
        om221=Sbar(j2,j2)- om12'*om11inv*om12;

        lt(:,k)= mix.par.lambda(j,k)+ om11inv*om12*al2;
        lt(:,k)=lt(:,k)/(1+ al2'*om221*al2)^.5;
    end
    
    if size(j,2)==1    
        % extract a univariate marginal density
        par=struct('mu',mix.par.mu(j,:),'sigma',squeeze(mix.par.sigma(j,j,:))','lambda',lt,'df',mix.par.df);
        
        mixm=struct('dist','SkewSt','K',mix.K,'r',1,'weight',mix.weight,'par',par);
        
    else 
        % extract a size(j,2)-variate marginal density
        
        par=struct('mu',mix.par.mu(j,:),'sigma',mix.par.sigma(j,j,:),'lambda',lt,'df',mix.par.df);
        
        mixm=struct('dist','SkStmu','K',mix.K,'r',size(j,2),'weight',mix.weight,'par',par);
    end
    
elseif all(mix.dist(1:6)=='Multin')
    
    mixm.dist='Multinomial';
    mixm.r=1;
    mixm.K=mix.K;
    mixm.weight=mix.weight;
    mixm.cat=mix.cat(j);
    ij=sum(mix.cat(1:j-1))+[1:mix.cat(j)]';
    mixm.par.pi=mix.par.pi(ij,:);
    
else 
    ['Mixture type ' mix.dist '  not supported by function mixturemar']
end

