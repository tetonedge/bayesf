function boxplotvar(value,varargin)
% boxplot with variable input

% Author: Sylvia Fruehwirth-Schnatter
% Last change: September 9, 2006


X=[squeeze(varargin{1})]; 

for m=2:nargin-1
  X=[X  squeeze(varargin{m})];
end  
 
boxplot(X); 
  
hold on; 
if value~=-9999;
plot([0 nargin+1],value*ones(1,2),'k'); 
%legend('observed value');
end
hold off;
