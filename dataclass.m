function [class, varargout]= dataclass(data,mix,varargin)

% Classify data through  of a finite mixture model, stored in the structure array mix

% variable output argument: simulate indicator from this density
% variable input argument: surpress computation of the smooting density to speed up MCMC

% Author: Sylvia Fruehwirth-Schnatter 


if nargin==2 % check the dimension of the data
    % data are handled as data stored by row
    ibycolumn=isfield(data,'bycolumn');
    if ibycolumn  ibycolumn=data.bycolumn; end  % ibycolumn true: data stored by column
    if ibycolumn      data.y=data.y'; data.bycolumn ='false'; end
end

if ~isfield(mix,'K') mix.K=1;  end  % single member from the distribution family

if isfield(data,'t0') class.t0=data.t0; datat0=data.t0; else datat0=1; end


if any([isfield(mix,'ar') isfield(mix,'arf') datat0>1])
    % convert MSAR model into a large Regression model
    [data,mix]=designar(data,mix);
end

mix.type=mix.dist;

glm=any([all(mix.type(1:6)=='Poisso') all(mix.type(1:6)=='Negbin') all(mix.type(1:6)=='Binomi') all(mix.type(1:6)=='Multin')]);

%%%%%%%%%%%%   compute the likleihood  l(y_i<|theta_k)  for all i and  forall k

y=data.y;

n = size(y,2);

nst=mix.K;
if  mix.type(1:6)=='Normal'
    if isfield(mix,'d')   % mixture regression model
        if isfield(mix,'indexdf')  mix.df=size(mix.indexdf,1)*size(mix.indexdf,2); else  mix.df=0;     end

        if mix.df>0
            intercept=[1:mix.d];
            indexdr=intercept(all(repmat(mix.indexdf,1,mix.d)~=repmat(intercept,mix.df,1),1));
            mu=mix.par.beta'*data.X(indexdr,:);
            mu=mu+repmat(mix.par.alpha'*data.X(mix.indexdf,:),mix.K,1);
        else
            mu=mix.par.beta'*data.X;
        end

        modseq=struct('mu',mu','sigma',repmat(mix.par.sigma,n,1));
        llh= normalpdflog(modseq,repmat(data.y',1,mix.K))';
        maxl = max(llh,[],1);
        lh = exp(llh - maxl(ones(nst,1),:));

    else  % finite mixture model
        [lh  maxl llh] = likeli_normal(y',mix.par.mu',mix.par.sigma');
        if size(nst,1)*size(nst,2)~=1  nst=max(nst); end
    end

elseif  all(mix.type(1:6)=='SkewNo')

    if ~isfield(mix,'d')   % finite mixture model
        [lh  maxl llh] = likeli_skewnormal(y',mix.par.mu',mix.par.sigma',mix.par.lambda');
        if size(nst,1)*size(nst,2)~=1  nst=max(nst); end
    end

elseif  all(mix.type(1:6)=='SkewSt')

    if ~isfield(mix,'d')   % finite mixture model
        [lh  maxl llh] = likeli_skewstudent(y',mix.par.mu',mix.par.sigma',mix.par.df',mix.par.lambda');
        if size(nst,1)*size(nst,2)~=1  nst=max(nst); end
    end

elseif  all(mix.type(1:6)=='SkNomu')

    if and(isfield(mix.par,'sigmainv'),isfield(mix.par,'logdet'))
        [lh  maxl llh] = likeli_skewnormult(y,mix.par.mu,mix.par.sigmainv,mix.par.logdet,mix.par.lambda);
    else
        Qinv=zeros(size(mix.par.sigma)); detQ=zeros(1,mix.K);
        for k=1:mix.K;
            Qinv(:,:,k)=inv(mix.par.sigma(:,:,k));detQ(k)=log(det(Qinv(:,:,k)));
        end
        [lh  maxl llh] = likeli_skewnormult(y,mix.par.mu,Qinv,detQ,mix.par.lambda);
    end

elseif  all(mix.type(1:6)=='SkStmu')

    if and(isfield(mix.par,'sigmainv'),isfield(mix.par,'logdet'))
        [lh  maxl llh] = likeli_skewstudmult(y,mix.par.mu,mix.par.sigma,mix.par.sigmainv,mix.par.logdet,mix.par.df,mix.par.lambda);
    else
        Qinv=zeros(size(mix.par.sigma)); detQ=zeros(1,mix.K);
        for k=1:mix.K;
            Qinv(:,:,k)=inv(mix.par.sigma(:,:,k));detQ(k)=log(det(Qinv(:,:,k)));
        end
        [lh  maxl llh] = likeli_skewstudmult(y,mix.par.mu,mix.par.sigma,Qinv,detQ,mix.par.df,mix.par.lambda);
    end


elseif all(mix.dist(1:6)=='Studen')

    if ~isfield(mix,'d')   % finite mixture model
        [lh  maxl llh] = likeli_student(y',mix.par.mu',mix.par.sigma',mix.par.df');
        if size(nst,1)*size(nst,2)~=1  nst=max(nst); end

    elseif isfield(mix,'d')   % mixture regression model
        warn(['Mixture of  regression models based on the t-distribution is not yet implemented']);
    end

elseif glm

    if and(~isfield(mix,'d'),all(mix.type(1:6)=='Poisso'))   % finite mixture of Poisson distribution
        if isfield(data,'exposures')
            mu=mix.par'*data.exposures;
        else
            mu=mix.par';
        end
        [lh  maxl llh] = likeli_poisson(y',mu);

    elseif and(~isfield(mix,'d'),all(mix.type(1:6)=='Binomi'))   % finite mixture of Binomial distribution
        [lh  maxl llh] = likeli_binomial(y',data.Ti,mix.par');

    elseif and(~isfield(mix,'d'),all(mix.type(1:6)=='Multin'))   % finite mixture of multinomial distribution
        if ~isfield(data,'Ti') data.Ti=1; end
        [lh  maxl llh] = likeli_multinomial(y',data.Ti,mix);
        
    elseif isfield(mix,'d')   % mixture regression model

        if isfield(mix,'indexdf')  mix.df=size(mix.indexdf,1)*size(mix.indexdf,2); else  mix.df=0;     end
        if ~isfield(mix,'df') mix.df=0; end

        if mix.df>0
            intercept=[1:mix.d];
            indexdr=intercept(all(repmat(mix.indexdf,1,mix.d)~=repmat(intercept,mix.df,1),1));
            mu=mix.par.beta'*data.X(indexdr,:);
            mu=mu+repmat(mix.par.alpha'*data.X(mix.indexdf,:),mix.K,1);
        else
            mu=mix.par.beta'*data.X;
        end

        if isfield(data,'exposures')   mu=mu+repmat(log(data.exposures),size(mu,1),1);  end

        if  all(mix.type(1:6)=='Poisso')
            [lh  maxl llh] = likeli_poisson(y',exp(mu));
        elseif  all(mix.type(1:6)=='Negbin')
            [lh  maxl llh] = likeli_negbin(y',exp(mu),mix.par.df');
        elseif all(mix.type(1:6)=='Binomi')
            [lh  maxl llh] = likeli_binomial(y',data.Ti,exp(mu)./(1+exp(mu)));
        end

    end


elseif all(mix.type(1:6)=='Normul')

    if and(isfield(mix.par,'sigmainv'),isfield(mix.par,'logdet'))
        [lh  maxl llh] = likeli_normult(y,mix.par.mu,mix.par.sigmainv,mix.par.logdet);
    else
        Qinv=zeros(size(mix.par.sigma)); detQ=zeros(1,mix.K);
        for k=1:mix.K;
            Qinv(:,:,k)=inv(mix.par.sigma(:,:,k));detQ(k)=log(det(Qinv(:,:,k)));
        end
        [lh  maxl llh] = likeli_normult(y,mix.par.mu,Qinv,detQ);

    end

elseif all(mix.dist(1:6)=='Stumul')

    if and(isfield(mix.par,'sigmainv'),isfield(mix.par,'logdet'))
        [lh  maxl llh] = likeli_stumult(y,mix.par.mu,mix.par.sigmainv,mix.par.logdet,mix.par.df);
    else
        Qinv=zeros(size(mix.par.sigma)); detQ=zeros(1,mix.K);
        for k=1:mix.K;
            Qinv(:,:,k)=inv(mix.par.sigma(:,:,k));detQ(k)=log(det(Qinv(:,:,k)));
        end
        [lh  maxl llh] = likeli_stumult(y,mix.par.mu,Qinv,detQ,mix.par.df);

    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % HW
elseif all(mix.type(1:6)=='Expone')

    if ~isfield(mix,'d')   % finite mixture model
        [lh  maxl llh] = likeli_expon(y',mix.par');

    elseif isfield(mix,'d')   % mixture regression model
        warn(['Mixture of exponential regression models not yet implemented']);
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
else
    ['Mixture type ' mix.type ' not supported by function dataclass']
end

class.logpy=llh; % log-likelihood of the data given the fully specified model


if ~isfield(mix,'indicmod') mix.indicmod.dist='Multinomial';  end
if mix.K==1
    mix.weight=1;
    if   mix.indicmod.dist(1:6)=='Markov' mix.indicmod.dist='Multinomial'; end
elseif   mix.indicmod.dist(1:6)=='Markov'
    mix.weight=marstat(mix.indicmod.xi);
end

if ~isfield(mix,'indicfix') mix.indicfix=false; end

if ~mix.indicfix
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    %  Determine the posterior of the indicators

    if mix.indicmod.dist(1:6)=='Multin'

        if   size(mix.weight,1)==1     mix.weight=mix.weight(ones(n,1),:);  end

        p = mix.weight'.*lh;
        sump=sum(p,1);
        class.mixlik = sum(log(sump)+maxl);
        p = p ./ sump(ones(nst,1),:);
        class.prob = p';

        if nargout>1  % SIMULATE InDiCaTOR
            if max(mix.K) > 1
                rnd = rand(n,1);
                S = (sum(cumsum(class.prob,2) < rnd(:,ones(max(mix.K),1)),2) + 1)';      % sampling%sst 1 times n
                varargout{1}=S;
            else
                varargout{1} = ones(1,n);
            end
            class.postS=sum(log(sum((repmat(varargout{1}',1,max(mix.K))==repmat([1:max(mix.K)],n,1)).*class.prob,2)));   % p(S|thmod,y)

        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    elseif mix.indicmod.dist(1:6)=='Markov'

        if nargin==2
            smoothdens=true;
        else   % surpress computation of the smooting density to speed up MCMC
            smoothdens=false;
        end

        if ~isfield(mix.indicmod,'init') mix.indicmod.init='ergodic';  end

        if  mix.indicmod.init=='ergodic'
            pinit=marstat(mix.indicmod.xi)';
        elseif mix.indicmod.init=='uniform'
            pinit=ones(mix.K,1)/mix.K;
        end
        %tic; [S,  sp, p_filt, p_smooth, pd]=simstate_ms_old(mix.indicmod.xi,lh,pinit);    toc;
        %tic;
        [S,  sp, p_filt,  p_smooth, pd]= simstate_ms(mix.indicmod.xi,lh,pinit,smoothdens);
        %toc;
        class.mixlik = sp+sum(maxl);
        class.prob = p_filt(:,2:end)';
        if smoothdens class.probsmooth = p_smooth(:,2:end)'; end

        if nargout>1
            varargout{1}=S;
            class.postS=pd;
        end

    end
    %% compute  entropy

    logp=zeros(size(class.prob));
    for k=1:nst;
        logp(class.prob(:,k)==0,k)=-99;logp(class.prob(:,k)~=0,k)=log(class.prob(class.prob(:,k)~=0,k));
    end

    class.entropy= -sum(sum(logp.*class.prob));

end


%% compute complete data likelihood for indicators stored in data.S
% if new indicators were sampled in dataclass (i.e. if nargout>1 )  no
% complete data likelihood is computed


if nargout==1
    if  and(isfield(data,'S'),mix.K>1)
        indk=repmat(data.S,mix.K,1)==repmat([1:mix.K]',1,size(data.S,2));
        class.loglikcd=sum(indk.*class.logpy,2);
    else
        class.loglikcd=class.mixlik;
    end
end
