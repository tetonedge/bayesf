%% demo_binomial.m
%
%  Bayesian analysis for data from of a binomial distributions
%
%%  simulate data

N=100;

clear mix;
mix.dist='Binomial';
mix.par=0.3;

reprate=10;  
%data.Ti=reprate;              % T  fixed
data.Ti=poissrnd(reprate,1,N); % Ti random: Poisson with expected number of repetitions equals reprate

data=simulate(mix,N,data);
mixtrue=mix;clear mix

dataplot(data)

%% BAYESIAN ANALYSIS 

mix.dist='Binomial';
prior=priordefine(data,mix);

[data, mix,mcmc]=mcmcstart(data,mix);
%[data, mix,mcmc]=mcmcstart(data,mix,mcmc);

mcmcout=mixturemcmc(data,mix,prior,mcmc);
[est,mcmcout]=mcmcestimate(mcmcout)

%% marginal likelihoods

[marlik,mcmcout]=mcmcbf(data,mcmcout)
