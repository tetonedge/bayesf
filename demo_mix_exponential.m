clear mixture;
mixtrue.dist='Exponential';
mixtrue.K=2;
mixtrue.weight=[0.1 0.9];
mixtrue.par=[1 10];


data=simulate(mixtrue,100);
data.Strue=data.S;
data=rmfield(data,'S');
dataplot(data)

clear mix;
mix.dist='Exponential';
mix.K=2;

prior=priordefine(data,mix);
[data,mix,mcmc]=mcmcstart(data,mix);

mcmcout=mixturemcmc(data,mix,prior,mcmc);
mcmcplot(mcmcout,2);

[est,mcmcout]=mcmcestimate(mcmcout);
[marlik,mcmcout]=mcmcbf(data,mcmcout)

mcmcout.name= 'store_mix_exponential_K2';
mcmcstore(mcmcout);