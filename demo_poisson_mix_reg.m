%%%%%%%%%%%%%%%%%%%%%%%%  
%%
%%  Estimate a mixture of Poisson Regression Models including model
%%  selection

%%  Data: simulated data  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% define mixture
clear mix  mixtrue;

%mixtrue=struct('dist','Poisson','K',3,'weight',[0.3 0.4 0.3],'par',[0.1 2 5]); % small mean component
mixtrue=struct('dist','Poisson','K',2,'weight',[0.7 0.3],'par',[1 5]);


% simulate data

data=simulate(mixtrue,1000);

mcmc=struct('burnin',{2000},'M',{1000},'storeS',{500},'storepost',{true});

% fit mixture

mix.dist=mixtrue.dist;

for K=1:4


% data.empty=true; % test marginal likelihoods, no data, only prior

 mix.K=K;

% hierarchical prior
clear prior1;
prior1=priordefine(data,mix);
eval(['mcmcoutK' num2str(K) 'hp=mixturemcmc(data,mix,prior1,mcmc);']);

% conditional conjugate prior
clear prior2;
prior2.hier=false;
prior2=priordefine(data,mix,prior2);
eval(['mcmcoutK'  num2str(K)  'cp=mixturemcmc(data,mix,prior2,mcmc);']);

% parameter estimation

eval(['estK' num2str(K)  'hp=mcmcestimate(mcmcoutK'   num2str(K)   'hp);']);
eval(['estK' num2str(K)  'cp=mcmcestimate(mcmcoutK'   num2str(K)   'cp);']);

eval(['marlikK' num2str(K) 'hp=mcmcbf(data,mcmcoutK'   num2str(K)   'hp)']);

 
eval(['marlikK' num2str(K) 'cp=mcmcbf(data,mcmcoutK'   num2str(K)   'cp)']);

 
end