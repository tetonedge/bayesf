%% demo_regression_negbin.m
%
%  Bayesian analysis for data from of a negative binomial distributions
%
%    - estimated using auxiliary mixture sampling
%    - marginal likelihood using bridge sampling
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%  simulate data

N=1000;

clear mix;
mix.dist='Negbin';
mix.par.df=10;
%mix.par.df=50;

mix.par.beta=[-1;0.3];  % covariate U\sim[-1,1]; intercept  
mix.d=size(mix.par.beta,1);

data=simulate(mix,N);   
mixtrue=mix; clear mix


dataplot(data)

%% BAYESIAN ANALYSIS 


mix.dist='Negbin';
mix.d=2;

prior=priordefine(data,mix);

[data,mix,mcmc]=mcmcstart(data,mix);

mcmc.M=3000;mcmc.burnin=1000;
mcmc.M=1000;mcmc.burnin=500;

mcmc.mh.tune.df=2; 

mcmcout=mixturemcmc(data,mix,prior,mcmc);
[est,mcmcout]=mcmcestimate(mcmcout)

%% marginal likelihoods

[marlik,mcmcout]=mcmcbf(data,mcmcout)
