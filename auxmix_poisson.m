function [datareg,regvar]=auxmix_poisson(data,loglambda,mixaux1,mixaux2)

%% auxiliary mixture sampling for poisson counts

% determines the left hand variable and the variance of the augmented model
%   datareg = log lambda + error, error sim Normal(0,regvar),
%  where 
%    datareg= -log tau - m(R) - log(exposures);
%  (tau is the arrival time and R is the indicator) 
%   log(exposures) is 0, if no exposures are observed

%% input variables
%  data ... data.y contains the observed count
%           data.exposures ...  contains exposures
%           data.N ... number of observations 
%  loglambda ... value of the loglinear predictor, vector of size 1 x data.N  
%  mixaux1 and mixaux2 .. initialize with auxmix_initialize_poisson

%% output variables
% datareg ... left hand variable of the augmented normal (regression) model
%             note that the mean of the mixture approximation is already substracted from this variable   
% datareg(1:data.N) ... augmentation of last arrivial time before 1 for i=1,...,N   
% datareg(data.N+1,end) ... augmentation of first arrivial time after 1 (only for non-zero observations)   

% regvar ... error variance of the augmented normal (regression) model   
% regvar(1:data.N) ... augmentation of last arrivial time before 1 for i=1,...,N   
% regvar(data.N+1,end) ... augmentation of first arrivial time after 1 (only for non-zero observations)   

  
%% check input

if ~isfield(data,'exposures') data.exposures=ones(1,data.N); end
regvar=zeros(1,data.N+sum(data.y~=0));


%% auxiliary mixture sampling

if data.empty  datareg.empty=data.empty;  else datareg.empty=false; end
datareg.y=zeros(1,data.N+sum(data.y~=0));
datareg.y(1,data.N+1:end)= betarnd(data.y(data.y~=0),1);  % tau2, last jump before 1
datareg.y(1,1:data.N)=1-log(rand(1,data.N))./exp(loglambda+log(data.exposures));   % tau1, first jump after 1
%datareg.y(1,1:data.N)=1+exprnd(1,1,data.N)./exp(loglambda+log(data.exposures));    % tau1, first jump after 1
datareg.y(1,data.y~=0)=datareg.y(1,data.y~=0)-datareg.y(1,data.N+1:end);
datareg.y=-log(datareg.y);

datareg.y=datareg.y-log([data.exposures  data.exposures(1,data.y~=0)]);     % exposures; changes June 2007

% sample the INDICATORs for tau1:

dataaux.y=datareg.y(1,1:data.N)-loglambda;
[classaux ri] =dataclass(dataaux,mixaux1,0);
datareg.y(1,1:data.N)=datareg.y(1,1:data.N)-mixaux1.par.mu(ri);

regvar(1,1:data.N)=mixaux1.par.sigma(ri);

% sample the INDICATORs for tau2:
ny=sum(data.y~=0);
if ny>0
    dataaux.y=datareg.y(1,data.N+1:end)-loglambda(data.y~=0);
    [classaux ri]=dataclass(dataaux,mixaux2,0);ri=ri';
    datareg.y(1,data.N+1:end)=datareg.y(1,data.N+1:end)-sum(mixaux2.par.mu.*(repmat(ri,1,max(mixaux2.K))==repmat([1:max(mixaux2.K)],ny,1)  ),2)';
    regvar(1,data.N+1:end)= sum(mixaux2.par.sigma.*(repmat(ri,1,max(mixaux2.K))==repmat([1:max(mixaux2.K)],ny,1)  ),2)';
end
clear dataaux classaux ri;
