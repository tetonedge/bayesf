%% demo_regression_mix_binomial.m

%% Bayesian analysis of a mixture of binomial regression models

%    - estimated using auxiliary mixture sampling
%    - marginal likelihood using bridge sampling
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% define the true model
clear mixtrue;
mixtrue.dist='Binomial';
mixtrue.par.beta=[-1 1;0.7 0.3];  % covariate U\sim[-1,1]; intercept  
mixtrue.d=size(mixtrue.par.beta,1);
mixtrue.K=size(mixtrue.par.beta,2);
mixtrue.weight=[0.3 0.7];


%% simulate data

clear data;
N=1000;
reprate=10;  
%data.Ti=reprate;                     % T  fixed
data.Ti=max(poissrnd(reprate,1,N),mixtrue.K); % Ti random: Poisson with expected number of repetitions equals reprate

data=simulate(mixtrue,N,data);  %% binomial data, repetition parameter stored in data.Ti
data.Strue=data.S; data=rmfield(data,'S');

dataplot(data)

%% BAYESIAN ANALYSIS 

clear mix;
mix.dist='Binomial';
mix.d=2;
mix.K=2;

prior=priordefine(data,mix);

[data,mix,mcmc]=mcmcstart(data,mix);

mcmcout=mixturemcmc(data,mix,prior,mcmc);

mcmcplot(mcmcout)
[est,mcmcout]=mcmcestimate(mcmcout)

%% marginal likelihoods

[marlik,mcmcout]=mcmcbf(data,mcmcout)

mcmcout.name= 'store_regression_mix_binomial_K2';
mcmcstore(mcmcout);