%%%%%%%%%%%%%%%%%%%%%%%%
%  Fabric fault data

data=dataget('fabricfault');

%%  Estimate a mixture of Negative Binomial Regression Models including model selection

post=[];
Kmin=1;
Kmax=3;
for K=Kmin:Kmax

    clear mix;
    mix.dist='Negbin';
    mix.d=2;
    mix.K=K;

    [data, mix,mcmc]=mcmcstart(data,mix);
    mcmc.burnin=2000; mcmc.M=10000;

    % choose scale for uniform random walk proposal
    if K==1
        mcmc.mh.tune.df=2;
    else
        mcmc.mh.tune.df=4*ones(1,K);
    end

    prior=priordefine(data,mix);
    
    % change the default prior to match the prior used in
    % fruehwirth-schnatter/fruehwirth/held/Rue (2009), Statistics and
    % computing
    prior.par.df.b0=1; 
    prior.par.df.d=10/(1+sqrt(2));
    
    mcmcout=mixturemcmc(data,mix,prior,mcmc);
    if isfield(data,'S') data=rmfield(data,'S'); end % starting classification for current K has to be deleted 

    [marlik,mcmcout]=mcmcbf(data,mcmcout);
    post=[post;marlik.bs];
    
    mcmcout.name= ['store_' data.name '_negbin_mixreg_K' num2str(K)];
    mcmcstore(mcmcout);
end

%%  Select model with largest marginal likelihood 

[postsort is]=sort(post,1,'descend');
'Ranking according to marginal likelihood'
format bank;[is postsort],format short
Kselect=(Kmin-1)+is(1);

%% evaluate a fitted model for fixed K

K=Kselect;
eval(['load store_fabricfault_negbin_mixreg_K' num2str(K)]);
ifig=mcmcplot(mcmcout);
[est,mcmcout]=mcmcestimate(mcmcout);

figure(ifig+1);histneu(min(mcmcout.par.df,100),50,'k');
