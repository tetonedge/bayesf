function data=dataget(name)

% upload data named name; valid names are:
% 'eye'
% 'fabricfault'   
% 'fishery'
% 'gdp'
% 'iris'
% 'lamb'

% Author: Sylvia Fruehwirth-Schnatter


if nargin==0  
    data={'eye','fabricfault','fishery','gdp','iris','lamb'};return
end


nn=size(name,2);

if nn==11
    if name=='fabricfault'
        data_fabric_fault;
        data.X=ones(2,data.N);
        data.X(2,:)=log(length_of_roll);
        data.r=1; 
        data.sim=false; 
        data.name=name;
        data.type={'discrete'};
    else
        ['unknown data in dataget:' name]
    end

elseif nn==9
    if all(name=='starclust')
        star_clust_dat;
        data.N=size(data.y,2);
        data.r=2; 
        data.sim=false; 
        data.name=name;
        data.type={'continuous','continuous'};
    else
        ['unknown data in dataget:' name]
    end

elseif nn==8
    if name=='children'
        
        load child_exp;
        data.y=y';
        exposures=Et';
        data.N=size(data.y,2);
        data.r=1; 
        data.sim=false; 
        data.name=name;
        data.type={'discrete'};    
        data.istimeseries=true;
    else
        ['unknown data in dataget:' name]
    end
    
    
elseif nn==7
    
    if all(name=='fishery')
        
        fish_data;
        
        data.y=y;
        data.N=size(y,2);
        data.r=1; 
        data.sim=false; 
        data.name=name;
        data.type={'continuous'};
        
    elseif all(name=='acidity')
        load acidity_dat;
        data.y=acity';
        data.N=size(acity',2);
        data.r=1; 
        data.sim=false; 
        data.name=name;
        data.type={'continuous'};
        
    elseif  all(name=='titanic')
        data_titanic;
        data.r=1; 
        data.sim=false; 
        data.name=name;
        data.type={'discrete'};
    
    else
        ['unknown data in dataget:' name]
    end

 elseif nn==6
    if name=='senior'
        
        load sen_exp;
        data.y=sen';
        exposures=Et';
        data.N=size(data.y,2);
        data.r=1; 
        data.sim=false; 
        data.name=name;
        data.type={'discrete'};    
        data.istimeseries=true;
        
    elseif name=='enzyme'
        load enzyme_dat;
        data.y=enzyme';
        data.N=size(enzyme,1);
        data.r=1; 
        data.sim=false; 
        data.name=name;
        data.type={'continuous'};
        
    elseif name=='galaxy'
        load galaxy_dat;
        data.y=galaxy_dat';
        data.N=size(data.y,2);
        data.r=1;
        data.sim=false;
        data.name=name;
        data.type={'continuous'};

    else
        ['unknown data in dataget:' name]
    end
   
    
 elseif nn==5
   
     if name=='polio'    
         load polio;  
         obs=data; clear data;
         data.y=obs(:,2)';
         data.N=size(data.y,2);
         data.sim=false; 
         data.X=obs(:,3:end)';
          
         %index=[1:size(data.X,1)];intercept=sum(index(all(diff(data.X,[],2)==0,2))); % determine the index of the intercept
         %if intercept==0  % nointercept
         data.X=[ones(1,data.N);data.X];  
         %end    
          
         data.name=name;
        data.type={'discrete'};    
        data.istimeseries=true;
    else
        ['unknown data in dataget:' name]
    end

elseif nn==4 
    
    if all(name=='iris')
        
        load -ascii iris_data.dat;
        data.y=iris_data(:,1:4)';
        data.Strue=iris_data(:,5)';
        data.N=size(data.y,2);
        data.r=4; 
        data.sim=false; 
        data.name=name;
        data.type={'continous','continous','continous','continous'};        
        
    elseif all(name=='lamb')    
         load lamb_dat;  
         data.y=y';
         data.N=size(data.y,2);
         data.sim=false; 
        data.name=name;
        data.type={'discrete'};    
        data.istimeseries=true;
         
    elseif all(name=='seed')
        data_seed;
        data.sim=false; 
        data.name=name;
        data.type={'discrete'};
        data.r=1;
        
    else
        ['unknown data in dataget:' name]
    end
    
    
    
elseif nn==3 
    
    if name=='eye'
        
        load eye_dat;
        %data.y=y;
        %data.bycolumn=true;
        %data.N=size(y,1);
        data.y=y';
        data.N=size(y,1);
        data.r=1; 
        data.sim=false; 
        data.name=name;
        data.type={'discrete'};
        
    elseif name=='gdp'
        
        %load gdp_us_dat;  % startet mit  y(5);
        %data.y=Y';
        load -ascii GNP.dat;
        data.y=GNP(:,3)';
        
        data.N=size(data.y,2);
        data.r=1; 
        data.sim=false; 
        data.name=name;
        data.type={'continuous'};
        data.istimeseries=true;

        
    else
        ['unknown data in dataget:' name]
    end
    
    
else
    ['unknown data in dataget:' name]
    
end    
